package opendemo.activity.group;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;
import opendemo.activity.base.BaseActivity;
import opendemo.activity.chatview.GroupChatActivity;
import opendemo.activity.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GroupActivity extends BaseActivity implements View.OnClickListener {
	// data
	private String mGroupID;

	// ui
	private EditText mMessageEditView;
	private EditText mCustomMessageEditView;

	// ui
	private EditText mGroupNameEditText;
	private EditText mGroupInfoEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_group);

		mGroupID = getIntent().getStringExtra("groupID");

		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

		// 设置标题栏显示内容
		setTitle(groupInfo.getGroupName());

		TextView rightTitleBar = (TextView) findViewById(R.id.right);

		rightTitleBar.setVisibility(View.VISIBLE);
		rightTitleBar.setText("群成员");
		rightTitleBar.setOnClickListener(this);

		((Button) findViewById(R.id.group_send_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.group_chat_demo_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.group_submit_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.group_submit_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.group_release_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.group_exit_btn)).setOnClickListener(this);

		mMessageEditView = (EditText) findViewById(R.id.group_message_edittext);
		mCustomMessageEditView = (EditText) findViewById(R.id.group_custom_message_edittext);
		mGroupNameEditText = (EditText) findViewById(R.id.group_name_edittext);
		mGroupInfoEditText = (EditText) findViewById(R.id.group_info_edittext);
	}

	private final int MESSAGE_SENDSUCCESS = 0;
	private final int MESSAGE_SENDFAIL = 1;

	private final int GROUP_DELETESUCCESS = 10;
	private final int GROUP_DELETEFAIL = 11;

	private final int GROUP_EXITSUCCESS = 15;
	private final int GROUP_EXITFAIL = 16;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.right: {
			Intent intent = new Intent(GroupActivity.this, MembersActivity.class);

			intent.putExtra("groupID", mGroupID);
			startActivity(intent);
		}
			break;
		case R.id.group_send_btn:
			final String content = mMessageEditView.getText().toString();
			final String customMessageContent = mCustomMessageEditView.getText()
					.toString();

			if (content != null && content.length() != 0) {
				IMMyselfGroup.sendText(content, mGroupID, new OnActionListener() {
					@Override
					public void onSuccess() {
						mMessageEditView.setText("");
						showToast(content, MESSAGE_SENDSUCCESS);
					}

					@Override
					public void onFailure(String error) {
						showToast(error, MESSAGE_SENDFAIL);
					}
				});
			} else if (customMessageContent != null
					&& customMessageContent.length() != 0) {
			}
			break;
		case R.id.group_chat_demo_btn: {
			Intent intent = new Intent(this, GroupChatActivity.class);

			intent.putExtra("groupID", mGroupID);
			startActivity(intent);
		}
			break;
		case R.id.group_submit_btn: {
			final IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);
			final String groupName = mGroupNameEditText.getText().toString();
			final String customGroupInfo = mGroupInfoEditText.getText().toString();
			boolean hasNameChanged = !groupInfo.equals(groupInfo.getGroupName());
			boolean hasInfoChanged = !groupInfo.equals(groupInfo.getCustomGroupInfo());

			if (hasNameChanged || hasInfoChanged) {
				groupInfo.setGroupName(groupName);
				groupInfo.setCustomGroupInfo(customGroupInfo);

				groupInfo.commitGroupInfo(new OnActionListener() {
					@Override
					public void onSuccess() {
						// 设置标题栏显示内容
						setTitle(groupName);
						Toast.makeText(GroupActivity.this, "群信息 修改成功！",
								Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onFailure(String error) {
						Toast.makeText(GroupActivity.this, "群信息 修改失败：" + error,
								Toast.LENGTH_SHORT).show();
					}
				});
			} else {
				Toast.makeText(GroupActivity.this, "信息并未修改，亲！", Toast.LENGTH_SHORT)
						.show();
			}
		}
			break;
		case R.id.group_release_btn:
			IMMyselfGroup.deleteGroup(mGroupID, new OnActionListener() {
				@Override
				public void onSuccess() {
					IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

					showToast(groupInfo.getGroupName(), GROUP_DELETESUCCESS);
				}

				@Override
				public void onFailure(String error) {
					showToast(error, GROUP_DELETEFAIL);
				}
			});
			break;
		case R.id.group_exit_btn:
			IMMyselfGroup.quitGroup(mGroupID, new OnActionListener() {
				@Override
				public void onSuccess() {
					IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

					showToast(groupInfo.getGroupName(), GROUP_EXITSUCCESS);
				}

				@Override
				public void onFailure(String error) {
					showToast(error, GROUP_EXITFAIL);
				}
			});
			break;
		default:
			break;
		}
	}

	private void showToast(String message, int state) {
		String result = null;

		switch (state) {
		case MESSAGE_SENDSUCCESS:
			result = "消息 " + message + " 发送成功！";
			break;
		case MESSAGE_SENDFAIL:
			result = "消息  发送失败 :" + message + " !";
			break;
		case GROUP_DELETESUCCESS:
			result = "删除群组 " + message + " 成功！";
			setResult(Activity.RESULT_OK);
			finish();
			break;
		case GROUP_DELETEFAIL:
			result = "删除群组 失败：" + message;
			break;
		case GROUP_EXITSUCCESS:
			result = "退出群组 " + message + " 成功！";
			setResult(Activity.RESULT_OK);
			finish();
			break;
		case GROUP_EXITFAIL:
			result = "退出群组 失败：" + message;
			break;

		default:
			break;
		}

		Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
	}
}