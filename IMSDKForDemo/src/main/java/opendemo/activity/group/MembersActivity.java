package opendemo.activity.group;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;

import java.lang.reflect.Member;
import java.util.ArrayList;

import opendemo.activity.base.BaseListActivity;
import opendemo.activity.R;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

public class MembersActivity extends BaseListActivity implements View.OnClickListener {
	// data
	private String mGroupID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mGroupID = getIntent().getStringExtra("groupID");

		// 获取群用户列表前，获取IMGroupInfo对象
		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

		// 获取群用户列表，初始化界面
		initBaseList(R.layout.activity_group_members, groupInfo.getMemberList());

		// 设置标题栏显示内容
		setTitle("成员列表");

		TextView rightTitleBar = (TextView) findViewById(R.id.right);

		rightTitleBar.setText("添加群成员");
		rightTitleBar.setOnClickListener(this);

		// 只有自己创建的群，才有权利添加或删除群成员
		if (IMMyselfGroup.isMyOwnGroup(mGroupID)) {
			rightTitleBar.setVisibility(View.VISIBLE);
		} else {
			rightTitleBar.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// 获取群用户列表前，获取IMGroupInfo对象
		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);
		// 获取群用户列表
		final ArrayList<String> membersList = groupInfo.getMemberList();
		final String userToBeRemoved = membersList.get(position - 1);

		if(!userToBeRemoved.equals(IMMyself.getCustomUserID())) {
			Intent intent = new Intent(MembersActivity.this, MemberInfoActivity.class);
			intent.putExtra("GroupID", mGroupID);
			intent.putExtra("CustomUserID", userToBeRemoved);
			MembersActivity.this.startActivity(intent);
		} else {
			Toast.makeText(MembersActivity.this, "你不能操作自己", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.right: {
			Intent intent = new Intent(MembersActivity.this, AddMemberActivity.class);

			intent.putExtra("groupID", mGroupID);
			startActivityForResult(intent, 1000);
		}
			break;
		default:
			break;
		}
	}


}
