package opendemo.activity.group;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMPrivateMyself;
import imsdk.data.IMMyself;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;
import opendemo.activity.R;
import opendemo.customview.SwitchView;

public class ManagerSettingsActivity extends Activity {

    private static final int HANDLER_NOTIFY_VIEW = 1;

    private String mGroupID;
    private String mCustomUserID;

    private IMGroupInfo mGroupInfo;

    private TextView tvTitle;
    private CheckBox checkBox1;
    private CheckBox checkBox2;
    private CheckBox checkBox3;
    private EditText tvAuthName;

    private final StaticHandler mHandler = new StaticHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_settings);

        mGroupID = getIntent().getStringExtra("GroupID");
        mCustomUserID = getIntent().getStringExtra("CustomUserID");

        if(TextUtils.isEmpty(mGroupID)) {
            Toast.makeText(this, "群ID为空", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        if(TextUtils.isEmpty(mCustomUserID)) {
            Toast.makeText(this, "用户ID为空", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // 获取群名前，先获取IMGroupInfo对象
        mGroupInfo = IMSDKGroup.getGroupInfo(mGroupID);

        if(mGroupInfo == null) {
            Toast.makeText(this, "群信息不存在", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        initView();
        initData();

    }

    public void initView() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        checkBox1 = (CheckBox) findViewById(R.id.checkBox1);
        checkBox2 = (CheckBox) findViewById(R.id.checkBox2);
        checkBox3 = (CheckBox) findViewById(R.id.checkBox3);

        tvAuthName = (EditText) findViewById(R.id.tvAuthName);
    }

    public void initData() {
        if(mGroupInfo.getOwnerCustomUserID().equals(mCustomUserID)) {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是群主");
        } else if(mGroupInfo.isGroupManager(Long.valueOf(mCustomUserID))) {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是管理员，权限名称为\"" + mGroupInfo.getUserAuthName(Long.valueOf(mCustomUserID)) + "\"");
        } else {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是普通用户");
        }

        ArrayList<Integer> authList = DTTool.seperateAuth(mGroupInfo.getUserAuthLevel(Long.valueOf(mCustomUserID)));

        checkBox1.setChecked(authList.contains(1) ? true : false);
        checkBox2.setChecked(authList.contains(2) ? true : false);
        checkBox3.setChecked(authList.contains(4) ? true : false);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveBtn1:
                if(mGroupInfo.getOwnerCustomUserID().equals(IMMyself.getCustomUserID())) {
                    int authLevel = 0;
                    authLevel += checkBox1.isChecked() ? 1 : 0;
                    authLevel += checkBox2.isChecked() ? 2 : 0;
                    authLevel += checkBox3.isChecked() ? 4 : 0;

                    IMMyselfGroup.changeManagerAuthLevel(mGroupID, Long.valueOf(mCustomUserID), authLevel, new IMMyself.OnActionListener() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(ManagerSettingsActivity.this, "设置成功", Toast.LENGTH_SHORT).show();
                            mHandler.sendEmptyMessage(HANDLER_NOTIFY_VIEW);
                        }

                        @Override
                        public void onFailure(String error) {
                            Toast.makeText(ManagerSettingsActivity.this, "设置失败：" + error, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(this, "非群主不能设置管理员", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.saveBtn2:
                if(mGroupInfo.getOwnerCustomUserID().equals(IMMyself.getCustomUserID())) {
                    String authName = tvAuthName.getText().toString();

                    IMMyselfGroup.changeManagerAuthName(mGroupID, Long.valueOf(mCustomUserID), authName, new IMMyself.OnActionListener() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(ManagerSettingsActivity.this, "设置成功", Toast.LENGTH_SHORT).show();
                            mHandler.sendEmptyMessage(HANDLER_NOTIFY_VIEW);
                        }

                        @Override
                        public void onFailure(String error) {
                            Toast.makeText(ManagerSettingsActivity.this, "设置失败：" + error, Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    Toast.makeText(this, "非群主不能设置管理员", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private static class StaticHandler extends Handler {


        WeakReference<ManagerSettingsActivity> mReference;

        public StaticHandler(ManagerSettingsActivity activity) {
            mReference = new WeakReference<ManagerSettingsActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            ManagerSettingsActivity activity = mReference.get();

            if(activity != null) {
                switch (msg.what) {
                    case HANDLER_NOTIFY_VIEW:
                        activity.notifyView();
                        break;
                }
            }
        }
    }

    private void notifyView() {
        if(mGroupInfo.getOwnerCustomUserID().equals(mCustomUserID)) {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是群主");
        } else if(mGroupInfo.isGroupManager(Long.valueOf(mCustomUserID))) {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是管理员，权限名称为\"" + mGroupInfo.getUserAuthName(Long.valueOf(mCustomUserID)) + "\"");
        } else {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是普通用户");
        }
    }

}
