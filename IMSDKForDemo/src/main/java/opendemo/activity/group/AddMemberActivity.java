package opendemo.activity.group;

import am.imsdk.model.IMPrivateMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;

import java.util.ArrayList;

import opendemo.activity.base.BaseActivity;
import opendemo.activity.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddMemberActivity extends BaseActivity implements View.OnClickListener {
	// data
	private String mGroupID;
	private final static int SUCCESS = 0;
	private final static int FAIL = -1;
	private ArrayList<String> mAddedMembersList;

	// ui
	private EditText mCustomUserIDEditText;
	private Button mAddMemberBtn;

	private IMGroupInfo mGroupInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_group_addmember);

		mGroupID = getIntent().getStringExtra("groupID");

		findViewById(R.id.left).setOnClickListener(this);

		mGroupInfo = IMSDKGroup.getGroupInfo(mGroupID);

		// 设置标题栏显示内容
		setTitle(mGroupInfo.getGroupName());

		mAddMemberBtn = (Button) findViewById(R.id.addmember_btn);
		mAddMemberBtn.setOnClickListener(this);

		mCustomUserIDEditText = (EditText) findViewById(R.id.addmember_edittext);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.left:
			if (mAddedMembersList != null && mAddedMembersList.size() > 0) {
				Intent intent = new Intent();

				intent.putStringArrayListExtra("addedMembersList", mAddedMembersList);
				setResult(Activity.RESULT_OK, intent);
			}

			this.finish();
			break;
		case R.id.addmember_btn:
			if(!mGroupInfo.getAuthAddUserList().contains(IMPrivateMyself.getInstance().getUID())) {
				Toast.makeText(AddMemberActivity.this, "你没有拉人的权限", Toast.LENGTH_SHORT).show();
				return;
			}

			final String customUserID = mCustomUserIDEditText.getText().toString();

//			mAddMemberBtn.setEnabled(false);

			if (!TextUtils.isEmpty(customUserID) && customUserID.trim().length() > 0) {
//				IMMyselfGroup.addMember(customUserID, mGroupID,
//						new OnActionListener() {
//							@Override
//							public void onSuccess() {
//								if (mAddedMembersList == null) {
//									mAddedMembersList = new ArrayList<String>();
//								}
//
//								mAddedMembersList.add(customUserID);
//								showDialog(customUserID, SUCCESS, null);
//								mCustomUserIDEditText.setText("");
//							}
//
//							@Override
//							public void onFailure(String error) {
//								showDialog(customUserID, FAIL, error);
//							}
//						});

				IMMyselfGroup.addToGroupRequest(customUserID, mGroupID,
						new OnActionListener() {

							@Override
							public void onSuccess() {
								Toast.makeText(AddMemberActivity.this, "发送邀请成功", Toast.LENGTH_LONG).show();
							}

							@Override
							public void onFailure(String error) {
								Toast.makeText(AddMemberActivity.this, "发送失败", Toast.LENGTH_LONG).show();
							}
						});
			}
			break;
		default:
			break;
		}
	}

	private void showDialog(String customUserID, int state, String error) {
		mAddMemberBtn.setEnabled(true);

		String result;

		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

		if (state == SUCCESS) {
			result = customUserID + "已成功加入群 " + groupInfo.getGroupName() + " !";
		} else {
			result = customUserID + "加入群 " + groupInfo.getGroupName() + " 失败:" + error;
		}

		Toast.makeText(AddMemberActivity.this, result, Toast.LENGTH_SHORT).show();
	}
}
