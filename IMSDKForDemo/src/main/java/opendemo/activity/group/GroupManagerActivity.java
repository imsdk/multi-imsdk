package opendemo.activity.group;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import am.dtlib.model.c.tool.DTTool;
import am.imsdk.action.IMAction;
import am.imsdk.action.group.IMActionGroupGetInfo;
import am.imsdk.model.a2.IMMyselfGroup2;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import imsdk.data.IMMyself;
import imsdk.data.custommessage.IMMyselfCustomMessage;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;
import opendemo.activity.R;
import opendemo.activity.base.BaseListActivity;
import opendemo.customview.CustomProgressDialog;

public class GroupManagerActivity extends BaseListActivity {

    private static final int HANDLER_PROCESS_EXTRADATA = 1;
    private static final int HANDLER_LIST_NOTIFY = 2;

    private EditText mGroupNameEditView;

    private Dialog mWaitingDialog;
    private MyHandler mHandler = new MyHandler(this);

    private ArrayList<String> mGroupNamesList = new ArrayList<>();
    private ArrayList<IMPrivateTeamInfo> mTeamInfosList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBaseList(R.layout.activity_group_manager, mGroupNamesList);

        // 设置标题栏显示内容
        setTitle("群搜索与添加");

        initView();

        Message msg = new Message();
        msg.obj = getIntent();
        msg.what = HANDLER_PROCESS_EXTRADATA;
        mHandler.sendMessageDelayed(msg, 100);
    }

    private void initView() {
        mGroupNameEditView = (EditText) findViewById(R.id.mGroupNameEditView);
        findViewById(R.id.mSearchBtn).setOnClickListener(this);
    }

    private static class MyHandler extends Handler {

        private final WeakReference<GroupManagerActivity> mActivity;

        public MyHandler(GroupManagerActivity activity) {
            mActivity = new WeakReference<GroupManagerActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if(mActivity.get() != null) {
                switch (msg.what) {
                    case HANDLER_PROCESS_EXTRADATA:
                        Intent intent = (Intent) msg.obj;
                        mActivity.get().processExtraData(intent);
                        break;
                    case HANDLER_LIST_NOTIFY:
                        if(!(msg.obj instanceof ArrayList)) {
                            return;
                        }

                        ArrayList<Long> teamIDList = (ArrayList<Long>) msg.obj;

                        mActivity.get().refreshList(teamIDList);
                        break;
                }
            }
        }
    }

    private void processExtraData(Intent intent) {
        boolean notice = intent.getBooleanExtra("notice", false);

        if(notice) {
            final String groupID = intent.getStringExtra("groupID");
            final String content = intent.getStringExtra("content");
            final String nickName = intent.getStringExtra("nickName");
            int customMsgType = intent.getIntExtra("customMsgType", 0);

            switch (IMMyselfCustomMessage.CustomMsgType.fromInt(customMsgType)) {
                case AddUserRequestFromUser: {
                    IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(groupID);

                    String showContent = "用户\"" + nickName + "\"申请加入你的群\"" + groupInfo.getGroupName() + "\"，申请理由为：\"" + content + "\"，请问是否同意？";
                    showRequestDialog(showContent, groupID);
                    break;
                }
                case AddUserRequestFromManager: {
                    mWaitingDialog = CustomProgressDialog.create(GroupManagerActivity.this, "");
                    mWaitingDialog.show();

                    IMActionGroupGetInfo action = new IMActionGroupGetInfo();

                    action.mOnActionFailedEndListener = new IMAction.OnActionFailedEndListener() {
                        @Override
                        public void onActionFailedEnd(String error) {
                            mWaitingDialog.cancel();

                            Toast.makeText(GroupManagerActivity.this, "拉取群信息失败", Toast.LENGTH_SHORT).show();
                        }
                    };

                    action.mOnActionDoneEndListener = new IMAction.OnActionDoneEndListener() {
                        @Override
                        public void onActionDoneEnd() {
                            mWaitingDialog.cancel();

                            IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(groupID);

                            String showContent = "群管理员\"" + nickName + "\"邀请你加入群\"" + groupInfo.getGroupName() + "\"请问是否同意？";
                            showRequestDialog(showContent, groupID);
                        }
                    };

                    action.mGroupID = groupID;
                    action.begin();
                    break;
                }
            }


        }
    }

    private void refreshList(ArrayList<Long> teamIDList) {
        mGroupNamesList.clear();
        mTeamInfosList.clear();

        if (IMMyselfGroup.isInitialized()) {
            for (Long teamID : teamIDList) {
                IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);
                mTeamInfosList.add(teamInfo);

                mGroupNamesList.add("名称：" + teamInfo.mTeamName + "，ID：" + teamInfo.mTeamID);
            }

            mAdapter.notifyDataSetChanged();
        }

        mInitProgressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Message msg = new Message();
        msg.obj = intent;
        msg.what = HANDLER_PROCESS_EXTRADATA;
        mHandler.sendMessageDelayed(msg, 100);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mSearchBtn:
                mInitProgressBar.setVisibility(View.VISIBLE);
                IMMyselfGroup.searchGroupByName(0, mGroupNameEditView.getText().toString(), 0, 20, new IMMyself.OnActionResultListener() {
                    @Override
                    public void onSuccess(Object result) {
                        mHandler.obtainMessage(HANDLER_LIST_NOTIFY, result).sendToTarget();
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(GroupManagerActivity.this, "未查询到数据", Toast.LENGTH_SHORT).show();
                        mHandler.obtainMessage(HANDLER_LIST_NOTIFY, new ArrayList<Long>()).sendToTarget();
                    }
                });
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        addToGroupDialog(position);
    }

    protected void showRequestDialog(String content, final String groupID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(GroupManagerActivity.this);

        builder.setTitle("群成员添加");

        builder.setMessage(content);
        builder.setCancelable(false);

        builder.setPositiveButton("同意", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                addMember(groupID);
            }
        });

        builder.setNegativeButton("拒绝", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();


            }
        });

        builder.create().show();
    }

    protected void addToGroupDialog(int position) {
        IMPrivateTeamInfo teamInfo = mTeamInfosList.get(position);

        final String groupID = DTTool.getGroupIDFromTeamID(teamInfo.mTeamID);

        AlertDialog.Builder builder = new AlertDialog.Builder(GroupManagerActivity.this);

        builder.setTitle("申请加入");

        builder.setMessage("你确认要申请加入群\"" + teamInfo.mTeamName + "\"吗");
        builder.setCancelable(false);

        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                addToGroup(groupID);
            }
        });

        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();


            }
        });

        builder.create().show();
    }

    private void addMember(final String groupID) {
        IMMyselfGroup.addMember(IMMyself.getCustomUserID(), groupID, new IMMyself.OnActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(GroupManagerActivity.this, "您已加入群：" + groupID, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(GroupManagerActivity.this, "进群失败" + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addToGroup(String groupID) {
        IMMyselfGroup.addToGroupRequest(IMMyself.getCustomUserID(), groupID, new IMMyself.OnActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(GroupManagerActivity.this, "成功发出申请", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(GroupManagerActivity.this, "发送请求失败", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
