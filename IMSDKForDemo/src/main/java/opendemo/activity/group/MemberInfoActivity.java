package opendemo.activity.group;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import am.imsdk.model.IMPrivateMyself;
import imsdk.data.IMMyself;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;
import opendemo.activity.R;
import opendemo.customview.SwitchView;

public class MemberInfoActivity extends Activity {

    private static final int HANDLER_NOTIFY_VIEW = 1;

    private String mGroupID;
    private String mCustomUserID;

    private IMGroupInfo mGroupInfo;

    //控件
    private SwitchView switchView;
    private TextView tvTitle;

    private final StaticHandler mHandler = new StaticHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member_info);

        mGroupID = getIntent().getStringExtra("GroupID");
        mCustomUserID = getIntent().getStringExtra("CustomUserID");

        if(TextUtils.isEmpty(mGroupID)) {
            Toast.makeText(this, "群ID为空", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        if(TextUtils.isEmpty(mCustomUserID)) {
            Toast.makeText(this, "用户ID为空", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // 获取群名前，先获取IMGroupInfo对象
        mGroupInfo = IMSDKGroup.getGroupInfo(mGroupID);

        if(mGroupInfo == null) {
            Toast.makeText(this, "群信息不存在", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        initView();
        initData();
    }

    public void initView() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        switchView = (SwitchView) findViewById(R.id.switchView);
    }

    public void initData() {
        if(mGroupInfo.getOwnerCustomUserID().equals(mCustomUserID)) {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是群主");
        } else if(mGroupInfo.isGroupManager(Long.valueOf(mCustomUserID))) {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是管理员，权限名称为\"" + mGroupInfo.getUserAuthName(Long.valueOf(mCustomUserID)) + "\"");
        } else {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是普通用户");
        }

        if(mGroupInfo.getSilenceUserList().contains(Long.valueOf(mCustomUserID))) {
            switchView.setState(true);
        } else {
            switchView.setState(false);
        }

        switchView.setOnStateChangedListener(new SwitchView.OnStateChangedListener() {
            @Override
            public void toggleToOn(View view) {
                if(!mGroupInfo.getAuthKeepSilenceList().contains(IMPrivateMyself.getInstance().getUID())) {
                    Toast.makeText(MemberInfoActivity.this, "你没有操作禁言的权限", Toast.LENGTH_SHORT).show();

                    switchView.setState(false);
                    return;
                }

                mGroupInfo.insertSilenceUser(Long.valueOf(mCustomUserID), new IMMyself.OnActionListener() {
                    @Override
                    public void onSuccess() {
                        mHandler.sendEmptyMessage(HANDLER_NOTIFY_VIEW);

                        switchView.setState(true);
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(MemberInfoActivity.this, error, Toast.LENGTH_SHORT).show();

                        switchView.setState(false);
                    }
                });
            }

            @Override
            public void toggleToOff(View view) {
                if(!mGroupInfo.getAuthKeepSilenceList().contains(IMPrivateMyself.getInstance().getUID())) {
                    Toast.makeText(MemberInfoActivity.this, "你没有操作禁言的权限", Toast.LENGTH_SHORT).show();

                    switchView.setState(false);
                    return;
                }

                mGroupInfo.removeSilenceUser(Long.valueOf(mCustomUserID), new IMMyself.OnActionListener() {
                    @Override
                    public void onSuccess() {
                        mHandler.sendEmptyMessage(HANDLER_NOTIFY_VIEW);
                    }

                    @Override
                    public void onFailure(String error) {
                        Toast.makeText(MemberInfoActivity.this, error, Toast.LENGTH_SHORT).show();

                        switchView.setState(false);
                    }
                });
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.kickoutBtn:
                showAlertDialog();
                break;
            case R.id.managerBtn:
                if(mGroupInfo.getOwnerCustomUserID().equals(IMMyself.getCustomUserID())) {
                    Intent intent = new Intent(this, ManagerSettingsActivity.class);
                    intent.putExtra("GroupID", mGroupID);
                    intent.putExtra("CustomUserID", mCustomUserID);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "非群主不能设置管理员", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private static class StaticHandler extends Handler {


        WeakReference<MemberInfoActivity> mReference;

        public StaticHandler(MemberInfoActivity activity) {
            mReference = new WeakReference<MemberInfoActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MemberInfoActivity activity = mReference.get();

            if(activity != null) {
                switch (msg.what) {
                    case HANDLER_NOTIFY_VIEW:
                        activity.notifyView();
                        break;
                }
            }
        }
    }

    private void notifyView() {
        if(mGroupInfo.getOwnerCustomUserID().equals(mCustomUserID)) {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是群主");
        } else if(mGroupInfo.isGroupManager(Long.valueOf(mCustomUserID))) {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是管理员，权限名称为\"" + mGroupInfo.getUserAuthName(Long.valueOf(mCustomUserID)) + "\"");
        } else {
            tvTitle.setText("用户\"" + mCustomUserID + "\"是普通用户");
        }
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MemberInfoActivity.this);

        // 通过IMGroupInfo对象，获取群名
        String groupName = mGroupInfo.getGroupName();

        // 获取群用户列表

        builder.setTitle("www.IMSDK.im");
        builder.setMessage("确定将" + mCustomUserID + "从" + groupName
                + "移除？");
        builder.setCancelable(false);

        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                IMMyselfGroup.removeMember(mCustomUserID, mGroupID,
                        new IMMyself.OnActionListener() {
                            @Override
                            public void onSuccess() {
                                Toast.makeText(MemberInfoActivity.this,
                                        "移除成员" + mCustomUserID + "成功！",
                                        Toast.LENGTH_SHORT).show();

                                MemberInfoActivity.this.finish();
                            }

                            @Override
                            public void onFailure(String error) {
                                Toast.makeText(MemberInfoActivity.this,
                                        "移除成员" + mCustomUserID + "失败！" + error,
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.create().show();
    }
}
