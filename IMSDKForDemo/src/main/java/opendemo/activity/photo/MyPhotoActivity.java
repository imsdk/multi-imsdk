package opendemo.activity.photo;

import imsdk.data.IMMyself;
import imsdk.data.IMSDK.OnActionProgressListener;
import imsdk.data.mainphoto.IMMyselfMainPhoto;
import imsdk.data.mainphoto.IMSDKMainPhoto;
import imsdk.data.mainphoto.IMSDKMainPhoto.OnBitmapRequestProgressListener;

import java.io.FileNotFoundException;
import java.io.IOException;

import opendemo.activity.base.BaseActivity;
import opendemo.cropimage.CacheUtils;
import opendemo.cropimage.CropHandler;
import opendemo.cropimage.CropHelper;
import opendemo.cropimage.CropParams;
import opendemo.activity.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

// 我的自定义头像页面
// 主要演示功能：
// 1. 获取自定义头像
// 2. 设置自定义头像
public final class MyPhotoActivity extends BaseActivity implements CropHandler {
	// data
	private CropParams mCropParams; // 截图的参数

	// ui
	private ImageView mImageView;
	private TextView mTipTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_myphoto);

		// 设置标题栏显示内容
		setTitleWithCustomUserID(IMMyself.getCustomUserID());

		TextView rightTitleBar = (TextView) findViewById(R.id.right);

		rightTitleBar.setText("上传");
		rightTitleBar.setVisibility(View.VISIBLE);

		mImageView = (ImageView) findViewById(R.id.myphoto_imageview);
		mTipTextView = (TextView) findViewById(R.id.myphoto_tip_textview);

		mCropParams = new CropParams(
				CacheUtils.getCacheDirectory(MyPhotoActivity.this),
				"IMSDKTempPhoto.jpg");

		// 获取本地存储的本用户头像
		Bitmap bitmap = IMSDKMainPhoto.get(IMMyself.getCustomUserID());

		if (bitmap != null) {
			mImageView.setImageBitmap(bitmap);
		}

		// 不论本地是否存有数据，都重新从服务端刷新一遍
		// 已实现智能节省流量
		getBitmapFromNetwork();
	}

	private void getBitmapFromNetwork() {
		// 请求刷新本用户头像
		IMSDKMainPhoto.request(IMMyself.getCustomUserID(), 30,
				new OnBitmapRequestProgressListener() {
					@Override
					public void onSuccess(Bitmap mainPhoto, byte[] buffer) {
						if (mainPhoto == null) {
							mTipTextView.setText("用户还没有设置图像");
						} else {
							mTipTextView.setText("下载成功！");
							mImageView.setImageBitmap(mainPhoto);
						}
					}

					@Override
					public void onProgress(double progress) {
						int percent = (int) (progress * 100);

						mTipTextView.setText("正在从服务器获取图片……下载进度 : " + percent + "%");
					}

					@Override
					public void onFailure(String error) {
						mTipTextView.setText("下载失败 : " + error);
					}
				});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.right:
			showAlbumDialog();
			break;
		default:
			break;
		}
	}

	public void showAlbumDialog() {
		final AlertDialog albumDialog = new AlertDialog.Builder(MyPhotoActivity.this)
				.create();

		albumDialog.setCanceledOnTouchOutside(true);

		Window window = albumDialog.getWindow();
		WindowManager.LayoutParams layoutParams = window.getAttributes();

		albumDialog.onWindowAttributesChanged(layoutParams);

		layoutParams.x = 0;
		layoutParams.y = dp2px(MyPhotoActivity.this, 134);

		View view = LayoutInflater.from(MyPhotoActivity.this).inflate(
				R.layout.dialog_usericon, null);

		albumDialog.show();
		albumDialog.setContentView(view);
		albumDialog.getWindow().setGravity(Gravity.TOP);

		ImageView albumImageView = (ImageView) view.findViewById(R.id.album_pic);
		ImageView cameraImageView = (ImageView) view.findViewById(R.id.camera_pic);

		albumImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				albumDialog.dismiss();

				startActivityForResult(
						CropHelper.buildCropFromGalleryIntent(mCropParams),
						CropHelper.REQUEST_CROP);
			}
		});

		cameraImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				albumDialog.dismiss();

				Intent intent = CropHelper.buildCaptureIntent(mCropParams.temUri);

				startActivityForResult(intent, CropHelper.REQUEST_CAMERA);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		CropHelper.handleResult(this, requestCode, resultCode, data);
	}

	// 取消 操作的回调
	@Override
	public void onCropCancel() {
	}

	// 截图失败 操作的回调
	@Override
	public void onCropFailed(String message) {
		Toast.makeText(MyPhotoActivity.this, message, Toast.LENGTH_SHORT).show();
	}

	// 截图时需要的上下文对象
	@Override
	public Activity getContext() {
		return MyPhotoActivity.this;
	}

	// 截图的参数
	@Override
	public CropParams getCropParams() {
		return mCropParams;
	}

	// 剪切图片成功后的回调
	@Override
	public void onPhotoCropped(Uri uri) {
		Bitmap bitmap = null;

		try {
			bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}

		mImageView.setImageBitmap(bitmap);

		// 向IMSDK服务器，上传图片信息
		IMMyselfMainPhoto.upload(bitmap, new OnActionProgressListener() {
			@Override
			public void onSuccess() {
				mTipTextView.setText("上传成功");
			}

			@Override
			public void onProgress(double progress) {
				// 上传进度的回调
				// progress为取值范围从0到1的浮点数
				mTipTextView.setText("上传进度 : " + (int) (progress * 100) + "%");
			}

			@Override
			public void onFailure(String error) {
				mTipTextView.setText("上传失败 : " + error);
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		// 删除拍照后的缓存照片
		CropHelper.clearCachedCropFile(mCropParams.temUri);
	}

	public int dp2px(Context context, float dpVal) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
				context.getResources().getDisplayMetrics());
	}
}