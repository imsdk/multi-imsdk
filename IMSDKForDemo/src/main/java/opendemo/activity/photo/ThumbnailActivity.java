package opendemo.activity.photo;

import imsdk.data.mainphoto.IMSDKMainPhoto;
import imsdk.data.mainphoto.IMSDKMainPhoto.OnBitmapRequestProgressListener;
import opendemo.activity.base.BaseActivity;
import opendemo.activity.R;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public final class ThumbnailActivity extends BaseActivity implements
		View.OnClickListener {
	// data
	private String mCustomUserID;

	// ui
	private EditText mImageHeightEditText;
	private EditText mImageWidthEditText;
	private ImageView mImageView;
	private TextView mTipTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_thumbnail);

		mCustomUserID = getIntent().getStringExtra("CustomUserID");

		// 设置标题栏显示内容
		setTitleWithCustomUserID(mCustomUserID);
		
		((Button) findViewById(R.id.thumbnail_request_btn)).setOnClickListener(this);

		mImageHeightEditText = (EditText) findViewById(R.id.thumbnail_image_height_edittext);
		mImageWidthEditText = (EditText) findViewById(R.id.thumbnail_image_width_edittext);
		mImageView = (ImageView) findViewById(R.id.thumbnail_imageview);
		mTipTextView = (TextView) findViewById(R.id.thumbnail_tip_textview);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.thumbnail_request_btn:
			String requestHeight = mImageHeightEditText.getText().toString();
			String requestWidth = mImageWidthEditText.getText().toString();

			if (requestHeight == null || requestHeight.length() == 0) {
				showDialog("请输入图片的高（单位 px）");
				mImageHeightEditText.requestFocus();
				return;
			}

			if (requestWidth == null || requestWidth.length() == 0) {
				showDialog("请输入图片的宽（单位 px）");
				mImageWidthEditText.requestFocus();
				return;
			}

			int width = Integer.parseInt(requestWidth);
			int height = Integer.parseInt(requestHeight);

			IMSDKMainPhoto.request(mCustomUserID, width, height,
					new OnBitmapRequestProgressListener() {
						@Override
						public void onSuccess(Bitmap mainPhoto, byte[] buffer) {
							if (mainPhoto == null) {
								mImageView.setVisibility(View.GONE);
								mTipTextView.setVisibility(View.GONE);
								showDialog("用户还没有设置图像！");
							} else {
								mTipTextView.setVisibility(View.VISIBLE);
								mTipTextView.setText("下载进度 成功");
								mImageView.setImageBitmap(mainPhoto);
							}
						}

						@Override
						public void onProgress(double progress) {
							mTipTextView.setVisibility(View.VISIBLE);
							mTipTextView.setText("下载进度 : " + (int) (progress / 100)
									+ "%");
						}

						@Override
						public void onFailure(String error) {
							mTipTextView.setVisibility(View.VISIBLE);
							mTipTextView.setText("下载失败 : " + error);
						}
					});
			break;
		default:
			break;
		}
	}
}
