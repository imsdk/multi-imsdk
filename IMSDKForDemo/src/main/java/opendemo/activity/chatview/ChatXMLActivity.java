package opendemo.activity.chatview;

import java.util.HashMap;

import am.imsdk.model.im.IMUserChatMsgHistory;
import am.imsdk.model.im.IMUserChatMsgHistoryWithTag;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import imsdk.data.IMMyself;
import imsdk.views.IMChatView;
import opendemo.activity.base.BaseActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

public class ChatXMLActivity extends BaseActivity {
	// data
	private String mCustomUserID;

	// ui
	private IMChatView mChatView;

	private View commentLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 通过配置xml的方式创建一个IMChatView
		initBase(R.layout.activity_chat_xml);

		mCustomUserID = getIntent().getExtras().getString("CustomUserID");

		// 获取已创建的IMChatView实例
		mChatView = (IMChatView) findViewById(R.id.chat_xml_chatview);

		// 配置IMChatView实例
		mChatView.addTag("orderId", 157345255); //一定要在setCustomUserID之前
		mChatView.setCustomUserID(mCustomUserID);
		mChatView.setMaxGifCountInMessage(10);
		mChatView.setUserMainPhotoVisible(true);
		mChatView.setUserMainPhotoCornerRadius(90);
		mChatView.setTitleBarVisible(true);
		mChatView.setOperationViewVisible(true);

		mChatView.setMainHead(null); //设置自己头像
		mChatView.setChatingHead(null); //设置聊天对象头像

		mChatView.setTitleRightBtn("清空", new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				IMUserChatMsgHistoryWithTag history = IMUserMsgHistoriesMgr.getInstance().getUserChatMsgHistory(mCustomUserID, "157345255");

				if(history != null) {
					history.clear();
				}
			}
		});

		// 添加头像点击事件监听
		mChatView.setOnHeadPhotoClickListener(new IMChatView.OnHeadPhotoClickListener() {

			@Override
			public void onClick(View v, String customUserID) {
				Toast.makeText(ChatXMLActivity.this, "您点击了" + customUserID,
						Toast.LENGTH_SHORT).show();
			}

		});

		mChatView.setOrderContinueListener(new IMChatView.OnOrderClickListener() {
			@Override
			public void onClick(View v) {
				if(!(v.getTag() instanceof Integer)) {
					return;
				}
				int position = (Integer) v.getTag();

//				mChatView.setOrderContinue(position);

				Toast.makeText(ChatXMLActivity.this, "继续订单", Toast.LENGTH_SHORT).show();
				mChatView.setOperationViewVisible(true);
				commentLayout.setVisibility(View.GONE);

				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("orderId", 157345255);

//				IMMyself.sendNoticeMsg("对方不同意结束服务", mCustomUserID, map, 5, new IMMyself.OnActionListener() {
//
//					@Override
//					public void onSuccess() {
//						Toast.makeText(ChatXMLActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
//					}
//
//					@Override
//					public void onFailure(String error) {
//						Toast.makeText(ChatXMLActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
//					}
//				});
			}
		});

		mChatView.setOrderConfirmListener(new IMChatView.OnOrderClickListener() {
			@Override
			public void onClick(View v) {
				if(!(v.getTag() instanceof Integer)) {
					return;
				}
				int position = (Integer) v.getTag();

//				mChatView.setOrderConfirm(position);

				Toast.makeText(ChatXMLActivity.this, "确定订单", Toast.LENGTH_SHORT).show();
				mChatView.setOperationViewVisible(false);
				commentLayout.setVisibility(View.VISIBLE);

//				HashMap<String, Object> map = new HashMap<String, Object>();
//				map.put("orderId", 157345255);
//
//				IMMyself.sendNoticeMsg("对方同意结束服务", mCustomUserID, map, 5, new IMMyself.OnActionListener() {
//
//					@Override
//					public void onSuccess() {
//						Toast.makeText(ChatXMLActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
//					}
//
//					@Override
//					public void onFailure(String error) {
//						Toast.makeText(ChatXMLActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
//					}
//				});
			}
		});

//		mChatView.setExtraBtnVisible(true);
		mChatView.setExtraBtnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mChatView.setOperationViewVisible(false);
			}
		});

		commentLayout = findViewById(R.id.commentLayout);
	}

	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.commentBtn:
				mChatView.setOperationViewVisible(true);
				commentLayout.setVisibility(View.GONE);
				break;
			case R.id.btn: {
			IMMyself.sendRequestForOrderFinish(mCustomUserID, 157345255, 5, new IMMyself.OnActionListener() {

				@Override
				public void onSuccess() {
					Toast.makeText(ChatXMLActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onFailure(String error) {
					Toast.makeText(ChatXMLActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
				}
			});
//				HashMap<String, Object> map = new HashMap<String, Object>();
//				map.put("orderId", 157345255);
//				IMMyself.showNoticeMsg("我发的系统消息，吼吼", mCustomUserID, map);
//				break;
			}
			case R.id.btn2: {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("orderId", 157345255);
//			IMMyself.sendNoticeMsg("我发的系统消息，吼吼", mCustomUserID, map, 10, new IMMyself.OnActionListener() {
//				@Override
//				public void onSuccess() {
//
//				}
//
//				@Override
//				public void onFailure(String error) {
//
//				}
//			});
//			String audioPath = "/storage/sdcard0/IMSDK.im/opendemo.activity/IMAudio/1f758724-aa35-4263-8a8e-dbf393d9e81d";
//			IMMyself.sendAudio(audioPath, 10 * 1000, mCustomUserID, map, 10, new IMMyself.OnActionProgressListener() {
//				@Override
//				public void onSuccess() {
//
//				}
//
//				@Override
//				public void onProgress(double progress) {
//
//				}
//
//				@Override
//				public void onFailure(String error) {
//
//				}
//			});
				break;
			}
			default:
				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 为了实现捕获用户选择的图片
		mChatView.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 为了实现点击返回键隐藏表情栏
		mChatView.onKeyDown(keyCode, event);
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		mChatView.onDestroy();
	}
}
