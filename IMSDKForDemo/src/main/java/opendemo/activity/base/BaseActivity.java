package opendemo.activity.base;

import opendemo.activity.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

public class BaseActivity extends Activity implements OnClickListener {
	protected void initBase(int layoutResID) {
		// 使得音量键控制媒体声音
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		if (layoutResID != 0) {
			setContentView(layoutResID);
		}

		ImageButton leftBtn = ((ImageButton) findViewById(R.id.left));

		if (leftBtn != null) {
			leftBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
		}

		TextView rightTextView = ((TextView) findViewById(R.id.right));

		if (rightTextView != null) {
			rightTextView.setOnClickListener(this);
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		super.setTitle(title);
		((TextView) findViewById(R.id.mid)).setText(title);
	}

	@Override
	public void setTitle(int titleId) {
		super.setTitle(titleId);
		((TextView) findViewById(R.id.mid)).setText(getString(titleId));
	}

	protected void setTitleWithCustomUserID(String customUserID) {
		String title = customUserID;

		// 免注册一键登录会产生@imsdk.im结尾的UserID
		// 将其末尾部分截断
		if (title.endsWith("@imsdk.im")) {
			title = title.substring(0, title.indexOf("@imsdk.im"));
		}

		((TextView) findViewById(R.id.mid)).setText(title);
	}

	protected void showDialog(String info) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("www.IMSDK.im");
		builder.setMessage(info);
		builder.setCancelable(false);

		builder.setPositiveButton("完成", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		builder.create().show();
	}

	protected void showDialog(String customUserID, String info) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		String title = customUserID;

		// 免注册一键登录会产生@imsdk.im结尾的UserID
		// 将其末尾部分截断
		if (title.endsWith("@imsdk.im")) {
			title = title.substring(0, title.indexOf("@imsdk.im"));
		}

		builder.setTitle(title);
		builder.setMessage(info);
		builder.setCancelable(false);

		builder.setPositiveButton("完成", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		builder.create().show();
	}

	@Override
	public void onClick(View v) {
	}
}
