package opendemo.activity.social;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.relations.IMMyselfRelations;
import opendemo.activity.R;
import opendemo.activity.base.BaseActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddFriendActivity extends BaseActivity {
	
	private final static int TIMEOUT = 5;

	private Button mAddfriendBtn;
	private EditText mRequestPhoneNumEditText;
	private EditText mRequestContentEditText;
	
	private String mPhoneNum;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_add_friend);
		
		// 设置标题栏显示内容
		setTitle("添加好友");
		
		mAddfriendBtn = (Button) findViewById(R.id.add_friend_send_friend_request);
		mAddfriendBtn.setOnClickListener(this);

		mRequestPhoneNumEditText = (EditText) findViewById(R.id.add_friend_phonenum_edittext);
		mRequestContentEditText = (EditText) findViewById(R.id.add_friend_content_edittext);
	}
	
	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.add_friend_send_friend_request:
			String phoneNum = mRequestPhoneNumEditText.getText().toString();
			String content = mRequestContentEditText.getText().toString();
			
			if(TextUtils.isEmpty(phoneNum)) {
				mRequestPhoneNumEditText.setError("好友手机号不能为空");
			} else {
				mPhoneNum = phoneNum;
			}
			
			IMMyselfRelations.sendFriendRequest(content, mPhoneNum, TIMEOUT,
					new OnActionListener() {
						@Override
						public void onSuccess() {
							showAddFriendDialog();
							v.setEnabled(true);
						}

						@Override
						public void onFailure(String error) {
							Toast.makeText(AddFriendActivity.this, "添加好友失败：" + error,
									Toast.LENGTH_SHORT).show();
							v.setEnabled(true);
						}
					});
			break;

		default:
			break;
		}
	}
	
	protected void showAddFriendDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(AddFriendActivity.this);

		builder.setTitle("www.IMSDK.im");

		String content = "已向 " + mPhoneNum + " 发送好友请求";

		builder.setMessage(content);
		builder.setCancelable(false);

		builder.setPositiveButton("确定", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		builder.create().show();
	}
}
