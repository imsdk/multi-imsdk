package opendemo.activity.social;

import imsdk.data.IMSDK.OnDataChangedListener;
import imsdk.data.recentcontacts.IMMyselfRecentContacts;
import opendemo.activity.base.BaseListActivity;
import opendemo.activity.chatview.ChatActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

public class RecentActivity extends BaseListActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBaseList(R.layout.activity_recent, IMMyselfRecentContacts.getUsersList());

		// 设置标题栏显示内容
		setTitle("最近联系人");

		// 监听最近联系人列表变化
		IMMyselfRecentContacts.setOnDataChangedListener(new OnDataChangedListener() {
			@Override
			public void onDataChanged() {
				mAdapter.notifyDataSetChanged();
			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent(RecentActivity.this, ChatActivity.class);

		intent.putExtra("CustomUserID", IMMyselfRecentContacts.getUser(position - 1));
		startActivity(intent);
	}
}
