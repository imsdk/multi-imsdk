package opendemo;

import am.dtlib.model.a.base.DTAppEnv;
import imsdk.data.IMMyself;
import opendemo.tool.ProcessTool;
import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

import am.dtlib.model.a.base.DTAppEnv;
import imsdk.data.IMMyself;
import opendemo.tool.ProcessTool;

public class MainApplication extends Application {
	public static final String REAL_PACKAGE_NAME = "opendemo.activity";

	@Override
	public void onCreate() {
		super.onCreate();

		LeakCanary.install(this);

		String processName = ProcessTool.getProcessName(this, android.os.Process.myPid());

		System.out.println("---------Application onCreate ---------：" + processName);

		if (processName != null) {
			boolean defaultProcess = processName.equals(REAL_PACKAGE_NAME);
			if (defaultProcess) {
				initAppForMainProcess();
			} else if (processName.contains(":im")) {
				initAppForIMSocketProcess();
			}
		}


	}

	private void initAppForMainProcess() {
		CrashHandler crashHandler = CrashHandler.getInstance();
		// 注册crashHandlerl
		crashHandler.init(this);

		DTAppEnv.isDebuggable = false;
		DTAppEnv.isLoggable = true;
		IMMyself.init(getApplicationContext());
	}

	private void initAppForIMSocketProcess() {
		CrashHandler crashHandler = CrashHandler.getInstance();
		// 注册crashHandlerl
		crashHandler.init(this);

		DTAppEnv.isDebuggable = false;
		DTAppEnv.isLoggable = true;
	}

}
