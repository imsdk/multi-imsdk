//package opendemo.customview;
//
//import org.a131.fzj.familylink.R;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.TextView;
//
//public class CustomAlertDialog {
//
//	private AlertDialog mDialog;
//
//	private TextView tvTitle;
//	private TextView tvMessage;
//	private Button positiveBtn;
//	private Button negativeBtn;
//	private Window window;
//
//	public CustomAlertDialog(Context context) {
//		mDialog = new AlertDialog.Builder(context).create();
//		mDialog.show();
//
//		window = mDialog.getWindow();
//		window.setContentView(R.layout.dialog_custom_alertdialog);
//	}
//
//	public CustomAlertDialog(Context context, boolean isServiceDialog) {
//		mDialog = new AlertDialog.Builder(context).create();
//		window = mDialog.getWindow();
//
//		if(isServiceDialog) {
//			window.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//		}
//		mDialog.show();
//
//		window.setContentView(R.layout.dialog_custom_alertdialog);
//	}
//
//	public CustomAlertDialog setTitle(CharSequence text) {
//		tvTitle = (TextView) window.findViewById(R.id.tvTitle);
//		tvTitle.setText(text);
//
//		return this;
//	}
//
//	public CustomAlertDialog setMessage(CharSequence text) {
//		tvMessage = (TextView) window.findViewById(R.id.tvMessage);
//		tvMessage.setText(text);
//
//		return this;
//	}
//
//	public CustomAlertDialog setPositiveButton(CharSequence text, final OnClickListener listener) {
//		positiveBtn = (Button) window.findViewById(R.id.positiveBtn);
//		positiveBtn.setOnClickListener(listener);
//		positiveBtn.setText(text);
//		positiveBtn.setVisibility(View.VISIBLE);
//
//		return this;
//	}
//
//	public CustomAlertDialog setNegativeButton(CharSequence text, final OnClickListener listener) {
//		negativeBtn = (Button) window.findViewById(R.id.negativeBtn);
//		negativeBtn.setOnClickListener(listener);
//		negativeBtn.setText(text);
//		negativeBtn.setVisibility(View.VISIBLE);
//
//		return this;
//	}
//
////	public void show() {
////		if(mDialog != null) {
////			mDialog.show();
////		}
////	}
//
//	public boolean isShown() {
//		if(mDialog != null) {
//			return mDialog.isShowing();
//		}
//
//		return false;
//	}
//
//	public void dismiss() {
//		if(mDialog != null) {
//			mDialog.dismiss();
//		}
//	}
//
//	public void cancel() {
//		if(mDialog != null) {
//			mDialog.cancel();
//		}
//	}
//
//	public void setCancelable(boolean flag) {
//		mDialog.setCancelable(flag);
//	}
//
//}
