package opendemo.customview;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import am.imsdk.demo.util.CPResourceUtil;
import opendemo.activity.R;
 
public class CustomProgressDialog {
	
    /** 
     * 得到自定义的progressDialog 
     * @param context 
     * @param msg 
     * @return 
     */  
    public static Dialog create(Context context, String msg) {
    	final Dialog loadingDialog = new Dialog(context, CPResourceUtil.getStyleId(context, "loading_dialog"));// 创建自定义样式dialog
  
    	View v = LayoutInflater.from(context).inflate(CPResourceUtil.getLayoutId(context, "dialog_customprogress"), null);// 得到加载view
        
        // main.xml中的ImageView  
//        TextView tipTextView = (TextView) v.findViewById(R.id.tipTextView);// 提示文字
//        if(TextUtils.isEmpty(msg)) {
//        	tipTextView.setVisibility(View.GONE);
//        } else {
//        	tipTextView.setVisibility(View.VISIBLE);
//        	tipTextView.setText(msg);// 设置加载信息
//        }
        
        final ImageView loadImageView = (ImageView) v.findViewById(CPResourceUtil.getId(context, "loadingImageView"));
        loadImageView.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				loadingDialog.dismiss();
				return false;
			}
		});
        
        final Animation anim = AnimationUtils.loadAnimation(context, CPResourceUtil.getAnimId(context, "im_progress_round"));
        LinearInterpolator lir = new LinearInterpolator();
        anim.setInterpolator(lir);
  
  
        loadingDialog.setContentView(v, new LinearLayout.LayoutParams(  
                LinearLayout.LayoutParams.MATCH_PARENT,  
                LinearLayout.LayoutParams.MATCH_PARENT));// 设置布局  
        
        loadingDialog.setOnShowListener(new DialogInterface.OnShowListener() {
			
			@Override
			public void onShow(DialogInterface dialog) {
				loadImageView.startAnimation(anim);
			}
		});
        
        loadingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				loadImageView.clearAnimation();
			}
		});
        
        return loadingDialog;
    }
}