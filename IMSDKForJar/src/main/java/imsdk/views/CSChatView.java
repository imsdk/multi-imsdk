package imsdk.views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;

public final class CSChatView extends IMChatView {
	public CSChatView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CSChatView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	// open Interface
	// init
	public CSChatView(Activity activity, String customUserID) {
		super(activity, customUserID);
	}
}