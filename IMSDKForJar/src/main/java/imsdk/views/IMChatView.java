package imsdk.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.FileUtils;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.im.IMUserMsgHistory;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import am.imsdk.ui.adapter.IMChatViewAdapter;
import am.imsdk.ui.views.IMBaseChatView;
import am.imsdk.ui.views.dialog.OperationChatDialog;
import am.imsdk.ui.views.dialog.ShowTextMsgDialog;
import imsdk.data.IMMessage;
import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;

public class IMChatView extends IMBaseChatView {
	// init data
	private String mCustomUserID = "";
	private IMUserMsgHistory mUserChatMsgHistory;

	private ClipboardManager clipboard;
	private AudioManager audioManager;

	public IMChatView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public IMChatView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	//listener
	public interface OnHeadPhotoClickListener{
		void onClick(View v, String customUserID);
	}

	public interface OnOrderClickListener{
		void onClick(View v);
	}

	public void setOnHeadPhotoClickListener(OnHeadPhotoClickListener listener){
		if(mAdapter != null){
			mAdapter.setOnHeadPhotoClickListener(listener);
		}
	}

	public void setOrderContinueListener(OnOrderClickListener listener) {
		if(mAdapter != null){
			mAdapter.setOrderContinueListener(listener);
		}
	}

	public void setOrderContinue(int position) {
		if(mAdapter != null){
			IMChatViewAdapter adapter = (IMChatViewAdapter) mAdapter;
			adapter.dealOrder1(position);
		}
	}

	public void setOrderConfirmListener(OnOrderClickListener listener) {
		if(mAdapter != null){
			mAdapter.setOrderConfirmListener(listener);
		}
	}

	public void setOrderConfirm(int position) {
		if(mAdapter != null){
			IMChatViewAdapter adapter = (IMChatViewAdapter) mAdapter;
			adapter.dealOrder2(position);
		}
	}

	public void setMainHead(Bitmap bp) {
		if(mAdapter != null){
			mAdapter.setMainHead(bp);
			mAdapter.notifyDataSetChanged();
		}
	}

	public void setChatingHead(Bitmap bp) {
		if(mAdapter != null){
			mAdapter.setChatingHead(bp);
			mAdapter.notifyDataSetChanged();
		}
	}

//	public void setOnContentLongClickListener(View.OnLongClickListener l) {
//		if(mAdapter != null) {
//			mAdapter.setOnContentLongClickListener(l);
//		}
//	}

	// open Interface
	// init
	public IMChatView(Activity activity, String customUserID) {
		super(activity);
		setCustomUserID(customUserID);
	}

	// init
	public void setCustomUserID(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.log(customUserID);
			DTLog.logError();
			throw new RuntimeException(IMParamJudge.getLastError());
		}

		mCustomUserID = customUserID;

		if(tags != null && tags.size() > 0 && tags.containsKey("orderId")) {
			mUserChatMsgHistory = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID, tags.get("orderId").toString());
		} else {
			mUserChatMsgHistory = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID);
		}

		if (mUserChatMsgHistory == null) {
			DTLog.logError();
			return;
		}

		mUserChatMsgHistory.mUnreadMessageCount = 0;
		mUserChatMsgHistory.saveFile();

		clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
		audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

		if (mAdapter == null) {
			mAdapter = new IMChatViewAdapter(this,mActivityReference.get(),
					mGifEmotionUtils, mIDs, mUserNameVisible, mUserMainPhotoVisible,
					mUserMainPhotoCornerRadius, mOnChatViewTouchListener,mCustomUserID, tags);
			mListView.setAdapter(mAdapter);
			mListView.setSelection(mAdapter.getCount() - 1);
			mAdapter.setOnImageClickListener(this);
			mAdapter.setOnContentLongClickListener(this);
		} else {
			((IMChatViewAdapter) mAdapter).setCustomUserID(mCustomUserID, tags);
		}

		if (mTitleBarVisible && mTitleTextView != null && mCustomUserID != null) {
			String title = mCustomUserID;

			if (title.length() > 19) {
				title = title.substring(0, 19);
			}

			mTitleTextView.setText(title);
		}

		DTNotificationCenter.getInstance().addObserver(
				mUserChatMsgHistory.getNewMsgNotificationKey(), new Observer() {
					@Override
					public void update(Object data) {
						DTLog.sign("getNewMsgNotificationKey");

						if (mAdapter == null) {
							DTLog.logError();
							return;
						}

						mAdapter.notifyDataSetChanged();
						mListView.setSelection(mAdapter.getCount() - 1);
					}
				});

	}

	@Override
	protected boolean onSendText(String text) {
//		if (IMMyself2.getLastSendTextTimeMillis() != 0
//				&& System.currentTimeMillis() - IMMyself2.getLastSendTextTimeMillis() < 1000) {
//			return false;
//		}

		IMMyself.sendText(text, mCustomUserID, tags, 10, new OnActionListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，暂不能收到消息",
							Toast.LENGTH_LONG).show();
				} else {
					DTLog.logError();
					DTLog.log(error);
					DTLog.sign(error);
				}
			}
		});

		return true;
	}

	@Override
	protected void onSendBitmap(Bitmap bitmap) {
		IMMyself.sendBitmap(bitmap, mCustomUserID, tags, 30, new OnActionProgressListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onProgress(double progress) {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，暂不能收到消息",
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	protected void onSendBitmap(String path) {
		IMMyself.sendBitmap(path, mCustomUserID, tags, 30, new OnActionProgressListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onProgress(double progress) {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，暂不能收到消息",
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private static class IMChatViewHandler extends Handler {
		private final WeakReference<IMChatView> mChatViewReference;
		private final WeakReference<Activity> mActivityReference;

		public IMChatViewHandler(IMChatView chatView, Activity activity, Looper looper) {
			super(looper);
			mChatViewReference = new WeakReference<IMChatView>(chatView);
			mActivityReference = new WeakReference<Activity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			if (mActivityReference.get() == null) {
				return;
			}

			if (mChatViewReference.get() == null) {
				return;
			}

			mChatViewReference.get().processHandlerMessage(msg);
		}
	}

	private Handler mHandler;

	@Override
	public Handler getHandler() {
		if (mHandler == null) {
			synchronized (this) {
				if (mHandler == null) {
					mHandler = new IMChatViewHandler(IMChatView.this,
							mActivityReference.get(), mActivityReference.get()
							.getMainLooper());
				}
			}
		}

		return mHandler;
	}

	public void processHandlerMessage(Message msg) {
		switch (msg.what) {
			case MSG_SEND_CLASSICAL_EMOTION:
				showNormalMsg((String) msg.obj);
				break;
			default:
				break;
		}
	}

	@Override
	protected void onUpdateScreenGif(int startPos, int endPos) {
		for (int i = startPos; i < endPos; i++) {
			IMUserMsg userMsg = mUserChatMsgHistory.getUserMsg(mUserChatMsgHistory
					.getCount() - 1 - i);

			if (userMsg == null) {
				DTLog.logError();
				return;
			}

			if (userMsg.mUserMsgType != UserMsgType.Normal
					|| !userMsg.mContent.contains("}")
					|| !userMsg.mContent.contains("{")) {
				continue;
			}

			View itemView = getViewByPosition(i, mListView);

			if (itemView != null) {
				TextView contentTextView = (TextView) itemView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_chatcontent"));

				mGifEmotionUtils.setSpannableText(contentTextView, userMsg.mContent,
						mHandler);
			}
		}
	}

	@Override
	protected boolean onStartRecording() {
		return IMMyself.startRecording(mCustomUserID);
	}

	@Override
	public void setTitleBarVisible(boolean visible) {
		super.setTitleBarVisible(visible);

		if (mTitleBarVisible && mTitleTextView != null && mCustomUserID != null) {
			String title = mCustomUserID;

			if (title.length() > 19) {
				title = title.substring(0, 19);
			}

			mTitleTextView.setText(title);
		}
	}

	private OperationChatDialog longClickDialog1;
	private ShowTextMsgDialog longClickDialog2;

	@Override
	public boolean onLongClick(View v) {
		final int position = (Integer) v.getTag();

		final int actualPosition = mUserChatMsgHistory.getCount() - 1 - position;

		if(actualPosition >= 0 && actualPosition < mUserChatMsgHistory.getCount()) {
			final IMUserMsg userMsg = mUserChatMsgHistory.getUserMsg(actualPosition);

			if(userMsg != null) {
				switch (userMsg.mUserMsgType) {
					case Audio:
						if (longClickDialog1 != null && longClickDialog1.isShowing()) {
							longClickDialog1.cancel();
							longClickDialog1 = null;
						}

						if (longClickDialog2 != null && longClickDialog2.isShowing()) {
							longClickDialog2.cancel();
							longClickDialog2 = null;
						}

						longClickDialog1 = new OperationChatDialog(getContext());
						longClickDialog1.show();

						longClickDialog1.setDeleteClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {
								userMsg.removeFile();

								mUserChatMsgHistory.removeUserMsg(actualPosition);
								mUserChatMsgHistory.saveFile();

								mAdapter.notifyDataSetChanged();

								if (longClickDialog1 != null && longClickDialog1.isShowing()) {
									longClickDialog1.dismiss();
								}
							}
						});
						longClickDialog1.setPlayModeClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {
								if (longClickDialog1 != null && longClickDialog1.isShowing()) {
									longClickDialog1.dismiss();
								}

								TextView temp = (TextView) view;
								if (temp.getText().toString().equals(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_receiver_mode")))) {
									temp.setText(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_speaker_mode")));

									if (audioManager != null) {
										audioManager.setMode(AudioManager.MODE_NORMAL);
									}
								} else {
									temp.setText(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_receiver_mode")));

									if (audioManager != null) {
										audioManager.setMode(AudioManager.MODE_IN_CALL);
									}
								}
							}
						});

						if(userMsg.mIsRecv) {
							longClickDialog1.setRevokeVisibility(View.GONE);
						} else {
							longClickDialog1.setRevokeVisibility(View.VISIBLE);
							longClickDialog1.setRevokeClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									if (longClickDialog1 != null && longClickDialog1.isShowing()) {
										longClickDialog1.dismiss();
									}

									mAdapter.revoke(position);
								}
							});
						}

						if(userMsg.mStatus == IMMessage.FAILURE) {
							longClickDialog1.setResendClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									if (mAdapter != null) {
										if (longClickDialog1 != null && longClickDialog1.isShowing()) {
											longClickDialog1.dismiss();
										}

										mAdapter.resend(position);
									}
								}
							});
						}

						break;
					case Normal:
					case Photo:
						if (longClickDialog1 != null && longClickDialog1.isShowing()) {
							longClickDialog1.cancel();
							longClickDialog1 = null;
						}

						if (longClickDialog2 != null && longClickDialog2.isShowing()) {
							longClickDialog2.cancel();
							longClickDialog2 = null;
						}

						longClickDialog2 = new ShowTextMsgDialog(getContext(), (userMsg.mUserMsgType == UserMsgType.Photo));
						longClickDialog2.show();

						longClickDialog2.setCopyClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								if (longClickDialog2 != null && longClickDialog2.isShowing()) {
									longClickDialog2.dismiss();
								}

								TextView temp = (TextView) v;
								if (temp.getText().toString().equals("保存到手机")) {
									IMImagePhoto imagePhoto = IMImagesMgr.getInstance().getPhoto(userMsg.getFileID());

									if (imagePhoto != null && imagePhoto.isLocalFileExist()) {
										String localPath = imagePhoto.getLocalFullPath();

										SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
										String fileName = df.format(new Date());
										String newPath = FileUtils.getStorePath() + "youwo/images/" + fileName + ".jpg";
										if (FileUtils.copyFile(localPath, newPath)) {
											Toast.makeText(getContext(), "图片保存路径：" + newPath, Toast.LENGTH_SHORT).show();
										} else {
											Toast.makeText(getContext(), "图片保存失败", Toast.LENGTH_SHORT).show();
										}
									} else {
										Toast.makeText(getContext(), "图片保存失败， 本地缓存不存在", Toast.LENGTH_SHORT).show();
									}

								} else {
									clipboard.setText(userMsg.mContent);
									Toast.makeText(getContext(), "复制成功", Toast.LENGTH_SHORT).show();
								}
							}
						});

						longClickDialog2.setForwardClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								//功能尚未实现
							}
						});

						longClickDialog2.setDeleteClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								if (userMsg.mUserMsgType == UserMsgType.Photo) {
									String fileID = userMsg.getFileID();

									IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(fileID);

									if (photo == null) {
										DTLog.logError();
										return;
									}

									photo.removeFile();
								}

								userMsg.removeFile();

								mUserChatMsgHistory.removeUserMsg(actualPosition);
								mUserChatMsgHistory.saveFile();

								mAdapter.notifyDataSetChanged();

								if (longClickDialog2 != null && longClickDialog2.isShowing()) {
									longClickDialog2.dismiss();
								}
							}
						});

						longClickDialog2.setAtHeClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								mContentEditText.setText("@" + userMsg.mFromCustomUserID);

								if (longClickDialog2 != null && longClickDialog2.isShowing()) {
									longClickDialog2.dismiss();
								}
							}
						});

						if(userMsg.mIsRecv) {
							longClickDialog2.setRevokeVisibility(View.GONE);
						} else {
							longClickDialog2.setRevokeVisibility(View.VISIBLE);
							longClickDialog2.setRevokeClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									if (longClickDialog2 != null && longClickDialog2.isShowing()) {
										longClickDialog2.dismiss();
									}

									mAdapter.revoke(position);
								}
							});
						}

						if(userMsg.mStatus == IMMessage.FAILURE) {
							longClickDialog2.setResendClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									if (mAdapter != null) {
										if (longClickDialog2 != null && longClickDialog2.isShowing()) {
											longClickDialog2.dismiss();
										}

										mAdapter.resend(position);
									}
								}
							});
						}

						break;
					default:
						break;
				}
			}
		}
		return false;
	}

	@Override
	public void onDestroy() {
		if(mAdapter != null) {
			mAdapter.stopAudio();
			mAdapter.destroy();
		}

		DTNotificationCenter.getInstance().removeObservers(mUserChatMsgHistory.getNewMsgNotificationKey());

		if (mVoiceRecordDialog != null) {
			mVoiceRecordDialog.setOnErrorListener(null);
			mVoiceRecordDialog.setOnTimeLimitListener(null);
		}
	}
}
