package imsdk.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;

import am.dtlib.model.a.base.DTAppEnv;
import am.imsdk.demo.util.CPResourceUtil;

/**
 * Created by xieyuan on 2015/11/26.
 */
public class BubbleImageView extends BaseImageView {

    private int bg_resource;

    public BubbleImageView(Context context) {
        super(context);
    }

    public BubbleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        sharedConstructor(context, attrs);
    }

    public BubbleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        sharedConstructor(context, attrs);
    }

    private void sharedConstructor(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, CPResourceUtil.getstyleableIds(DTAppEnv.getContext(), "IMBubbleChat"));
        int resource = a.getResourceId(CPResourceUtil.getstyleableId(DTAppEnv.getContext(), "IMBubbleChat_bg_resource"), 0);
        if(resource > 0) {
            bg_resource = resource;
        }
        a.recycle();
    }

    @Override
    public Bitmap getBitmap() {
        return BitmapFactory.decodeResource(getResources(), bg_resource);
    }
}
