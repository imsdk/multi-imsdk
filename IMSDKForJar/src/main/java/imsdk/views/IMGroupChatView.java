package imsdk.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.FileUtils;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.imteam.IMTeamChatMsgHistory;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import am.imsdk.ui.adapter.IMGroupChatViewAdapter;
import am.imsdk.ui.views.IMBaseChatView;
import am.imsdk.ui.views.dialog.OperationChatDialog;
import am.imsdk.ui.views.dialog.ShowTextMsgDialog;
import imsdk.data.IMMessage;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;

public class IMGroupChatView extends IMBaseChatView {
	// init data
	private String mGroupID = "";
	private long mTeamID;
	private IMTeamChatMsgHistory mTeamChatMsgHistory;

	private ClipboardManager clipboard;
	private AudioManager audioManager;

	//listener
	public void setOnHeadPhotoClickListener(IMChatView.OnHeadPhotoClickListener listener){
		if(mAdapter != null){
			mAdapter.setOnHeadPhotoClickListener(listener);
		}
	}

	// open Interface
	// init
	public IMGroupChatView(Activity activity, String groupID) {
		super(activity);
		setGroupID(groupID);
	}

	// init
	public void setGroupID(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			DTLog.logError();
			throw new RuntimeException(IMParamJudge.getLastError());
		}

		mGroupID = groupID;
		mTeamID = DTTool.getTeamIDFromGroupID(mGroupID);
		mTeamChatMsgHistory = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(mTeamID);

		if (mTeamChatMsgHistory == null) {
			DTLog.logError();
			return;
		}

		clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
		audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

		if (mTitleBarVisible && mTitleTextView != null && mGroupID != null) {
			IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

			mTitleTextView.setText(groupInfo.getGroupName());
		}

		if (mAdapter == null) {
			mAdapter = new IMGroupChatViewAdapter(this, mActivityReference.get(),
					mGifEmotionUtils, mIDs, mUserNameVisible, mUserMainPhotoVisible,
					mUserMainPhotoCornerRadius, mOnChatViewTouchListener, mGroupID);
			mListView.setAdapter(mAdapter);
			mListView.setSelection(mAdapter.getCount() - 1);
			mAdapter.setOnImageClickListener(this);
			mAdapter.setOnContentLongClickListener(this);
		} else {
			((IMGroupChatViewAdapter) mAdapter).setGroupID(mGroupID);
		}

		if (!IMParamJudge.isTeamIDLegal(DTTool.getTeamIDFromGroupID(groupID))) {
			throw new RuntimeException(IMParamJudge.getLastError());
		}

		mGroupID = groupID;
		mTeamID = DTTool.getTeamIDFromGroupID(groupID);
		mTeamChatMsgHistory = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(mTeamID);

		if (mTeamChatMsgHistory == null) {
			DTLog.logError();
			return;
		}

		mTeamChatMsgHistory.mUnreadMessageCount = 0;
		mTeamChatMsgHistory.saveFile();

		DTNotificationCenter.getInstance().addObserver(
				mTeamChatMsgHistory.getNewMsgNotificationKey(), new Observer() {
					@Override
					public void update(Object data) {
						DTLog.sign("getNewMsgNotificationKey");

						if (mAdapter == null) {
							return;
						}

						mAdapter.notifyDataSetChanged();
						mListView.setSelection(mAdapter.getCount() - 1);
					}
				});

		IMMyselfGroup.setOnGroupBitmapListener(new IMMyselfGroup.OnGroupBitmapListener() {
			@Override
			public void onReceiveBitmapMessage(String messageID, String groupID, String fromCustomUserID, String nickName, long serverActionTime) {

			}

			@Override
			public void onReceiveBitmap(Bitmap bitmap, String messageID, String groupID, String fromCustomUserID, long serverActionTime) {
				if (mAdapter != null) {
					DTNotificationCenter.getInstance().postNotification("IMChatImageMessageID:" + DTTool.getUnsecretIntValue(messageID), 1d);
				}
			}

			@Override
			public void onReceiveBitmapProgress(double progress, String messageID, String groupID, String fromCustomUserID, long serverActionTime) {
				if (mAdapter != null) {
					DTNotificationCenter.getInstance().postNotification("IMChatImageMessageID:" + DTTool.getUnsecretIntValue(messageID), progress);
				}
			}
		});
	}

	public IMGroupChatView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public IMGroupChatView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected boolean onSendText(String text) {
//		if (IMMyselfGroup2.getLastSendTextTimeMillis() != 0
//				&& System.currentTimeMillis()
//						- IMMyselfGroup2.getLastSendTextTimeMillis() < 1000) {
//			return false;
//		}

		IMMyselfGroup.sendText(text, mGroupID, new OnActionListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，赞不能收到消息",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(mActivityReference.get(), error,
							Toast.LENGTH_LONG).show();
				}
			}
		});

		return true;
	}

	@Override
	protected void onSendBitmap(Bitmap bitmap) {
		IMMyselfGroup.sendBitmap(bitmap, mGroupID, 30, new OnActionProgressListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onProgress(double progress) {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，赞不能收到消息",
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	protected void onSendBitmap(String path) {

	}

	@Override
	protected void onUpdateScreenGif(int startPos, int endPos) {
		for (int i = startPos; i <= endPos; i++) {
			IMTeamMsg teamMsg = mTeamChatMsgHistory.getTeamMsg(mTeamChatMsgHistory
					.getCount() - 1 - i);

			if (teamMsg == null) {
				DTLog.logError();
				return;
			}

			if (!(teamMsg instanceof IMTeamMsg)) {
				DTLog.logError();
				return;
			}

			if (teamMsg.mTeamMsgType != TeamMsgType.Normal
					|| teamMsg.mContent.indexOf("}") == -1
					|| teamMsg.mContent.indexOf("{") == -1) {
				continue;
			}

			View itemView = getViewByPosition(i, mListView);

			if (itemView != null) {
				TextView contentTextView = (TextView) itemView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_chatcontent"));

				mGifEmotionUtils.setSpannableText(contentTextView, teamMsg.mContent,
						mHandler);
			}
		}
	}

	@Override
	protected boolean onStartRecording() {
		return IMMyselfGroup.startRecording(mGroupID);
	}

	@Override
	public void setTitleBarVisible(boolean visible) {
		super.setTitleBarVisible(visible);

		if (mTitleBarVisible && mTitleTextView != null && mGroupID != null) {
			IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

			mTitleTextView.setText(groupInfo.getGroupName());
		}
	}

	@Override
	public boolean onLongClick(View v) {
		final int position = (Integer) v.getTag();

		final int actualPosition = mTeamChatMsgHistory.getCount() - 1 - position;

		if(actualPosition >= 0 && actualPosition < mTeamChatMsgHistory.getCount()) {
			final IMTeamMsg teamMsg = mTeamChatMsgHistory.getTeamMsg(actualPosition);

			if(teamMsg != null) {
				switch (teamMsg.mTeamMsgType) {
					case Audio:
						final OperationChatDialog dialog = new OperationChatDialog(getContext());
						dialog.show();

						dialog.setDeleteClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {
								teamMsg.removeFile();

								mTeamChatMsgHistory.removeTeamMsg(actualPosition);
								mTeamChatMsgHistory.saveFile();

								mAdapter.notifyDataSetChanged();

								if (dialog != null && dialog.isShowing()) {
									dialog.dismiss();
								}
							}
						});
						dialog.setPlayModeClickListener(new OnClickListener() {

							@Override
							public void onClick(View view) {
								if (dialog != null && dialog.isShowing()) {
									dialog.dismiss();
								}

								TextView temp = (TextView) view;
								if(temp.getText().toString().equals(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_receiver_mode")))) {
									temp.setText(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_speaker_mode")));

									if(audioManager != null) {
										audioManager.setMode(AudioManager.MODE_NORMAL);
									}
								} else {
									temp.setText(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_receiver_mode")));

									if(audioManager != null) {
										audioManager.setMode(AudioManager.MODE_IN_CALL);
									}
								}
							}
						});

						if(teamMsg.mStatus == IMMessage.FAILURE) {
							dialog.setResendClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									if(mAdapter != null) {
										if (dialog != null && dialog.isShowing()) {
											dialog.dismiss();
										}

										mAdapter.resend(position);
									}
								}
							});
						}

						break;
					case Normal:
					case Photo:
						final ShowTextMsgDialog showTextMsgDialog = new ShowTextMsgDialog(getContext(), (teamMsg.mTeamMsgType == TeamMsgType.Photo));
						showTextMsgDialog.show();

						showTextMsgDialog.setCopyClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
									showTextMsgDialog.dismiss();
								}

								TextView temp = (TextView) v;
								if (temp.getText().toString().equals("保存到手机")) {
									IMImagePhoto imagePhoto = IMImagesMgr.getInstance().getPhoto(teamMsg.getFileID());

									if(imagePhoto.isLocalFileExist()) {
										String localPath = imagePhoto.getLocalFullPath();

										SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
										String fileName = df.format(new Date());
										String newPath =  FileUtils.getStorePath() + "youwo/images/" + fileName + ".jpg";
										if(FileUtils.copyFile(localPath, newPath)) {
											Toast.makeText(getContext(), "图片保存路径：" + newPath, Toast.LENGTH_SHORT).show();
										} else {
											Toast.makeText(getContext(), "图片保存失败", Toast.LENGTH_SHORT).show();
										}
									} else {
										Toast.makeText(getContext(), "图片保存失败， 本地缓存不存在", Toast.LENGTH_SHORT).show();
									}

								}else{
									clipboard.setText(teamMsg.mContent);
									Toast.makeText(getContext(), "复制成功", Toast.LENGTH_SHORT).show();
								}
							}
						});

						showTextMsgDialog.setForwardClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								//功能尚未实现
							}
						});

						showTextMsgDialog.setDeleteClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								if(teamMsg.mTeamMsgType == TeamMsgType.Photo) {
									String fileID = teamMsg.getFileID();

									IMImagesMgr.getInstance().getPhoto(fileID).removeFile();
								}

								teamMsg.removeFile();

								mTeamChatMsgHistory.removeTeamMsg(actualPosition);
								mTeamChatMsgHistory.saveFile();

								mAdapter.notifyDataSetChanged();

								if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
									showTextMsgDialog.dismiss();
								}
							}
						});

						showTextMsgDialog.setAtHeClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								mContentEditText.setText("@" + teamMsg.mFromCustomUserID);

								if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
									showTextMsgDialog.dismiss();
								}
							}
						});

						if(teamMsg.mStatus == IMMessage.FAILURE) {
							showTextMsgDialog.setResendClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									if(mAdapter != null) {
										if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
											showTextMsgDialog.dismiss();
										}

										mAdapter.resend(position);
									}
								}
							});
						}

						break;
					default:
						break;
				}
			}
		}
		return false;
	}

	@Override
	public void onDestroy() {
		if(mAdapter != null) {
			mAdapter.stopAudio();
		}

		IMMyselfGroup.setOnGroupBitmapListener(null);

		DTNotificationCenter.getInstance().removeObservers(mTeamChatMsgHistory.getNewMsgNotificationKey());
	}
}
