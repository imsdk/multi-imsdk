package imsdk.views;

import am.imsdk.ui.a2.RoundedImageView2;
import android.content.Context;
import android.util.AttributeSet;

public class RoundedImageView extends RoundedImageView2 {
	public RoundedImageView(Context context) {
		super(context);
	}

	public RoundedImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}
