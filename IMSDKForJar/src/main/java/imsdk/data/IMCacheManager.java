package imsdk.data;

import java.io.File;
import java.security.PrivateKey;

import am.imsdk.action.ammsgs.IMFileText;
import am.imsdk.action.ammsgs.IMFileTextsMgr;
import am.imsdk.demo.util.FileUtils;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;

/**
 * Created by fzj on 2016/1/12.
 * 清除缓存文件
 */
public class IMCacheManager {

    public static void clearAudioCache() {
        IMAudio audio = IMAudiosMgr.getInstance().getAudio("cache");

        if(audio == null) {
            return;
        }

        String cacheDirPath = new File(audio.getLocalFullPath()).getParent();

        File cacheDir = new File(cacheDirPath);

        if (cacheDir != null && cacheDir.isDirectory()) {
            File[] list = cacheDir.listFiles();
            if(list != null && list.length > 0) {
                for (File f : list) {
                    f.delete();
                }
            }
        }
    }

    public static void clearImageCache() {
        IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto("cache");

        if(photo == null) {
            return;
        }

        String cacheDirPath = new File(photo.getLocalFullPath()).getParent();

        File cacheDir = new File(cacheDirPath);

        if (cacheDir != null && cacheDir.isDirectory()) {
            File[] list = cacheDir.listFiles();
            if(list != null && list.length > 0) {
                for (File f : list) {
                    f.delete();
                }
            }
        }
    }

    public static void clearFileTextCache() {
        IMFileText text = IMFileTextsMgr.getInstance().getFileText("cache");

        if(text == null) {
            return;
        }

        String cacheDirPath = new File(text.getLocalFullPath()).getParent();

        File cacheDir = new File(cacheDirPath);

        if (cacheDir != null && cacheDir.isDirectory()) {
            File[] list = cacheDir.listFiles();
            if(list != null && list.length > 0) {
                for (File f : list) {
                    f.delete();
                }
            }
        }
    }

    public static void clearTakePhotoCache() {
        File cacheDir = new File(FileUtils.getStorePath() + "im/dcim");

        if (cacheDir != null && cacheDir.isDirectory()) {
            File[] list = cacheDir.listFiles();
            if(list != null && list.length > 0) {
                for (File f : list) {
                    f.delete();
                }
            }
        }
    }

}
