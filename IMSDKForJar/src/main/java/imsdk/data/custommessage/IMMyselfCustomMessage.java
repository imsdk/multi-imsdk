package imsdk.data.custommessage;

import android.util.SparseArray;

import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.IMMyself.OnActionListener;

import am.dtlib.model.c.tool.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.a2.IMMyself2;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;

public final class IMMyselfCustomMessage extends IMMyself2 {
	public interface OnReceiveCustomMessageListener {
		void onReceiveCustomMessage(String customMessage,
									String fromCustomUserID, long serverActionTime, String extraData);
	}

	public static long send(String customMessage, String toCustomUserID) {
		return send(customMessage, toCustomUserID, 0, null);
	}

	public static long send(String customMessage, String toCustomUserID,
			long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendCustomMessage", actionTime);
			return actionTime;
		}

		if (!IMParamJudge.isIMTextLegal(customMessage)) {
			commonActionFailure(l, "sendCustomMessage", actionTime);
			return actionTime;
		}

		if (sLastSendCustomMessageTime != 0
				&& System.currentTimeMillis() - sLastSendCustomMessageTime < 1000) {
			commonActionFailure(l, "sendCustomMessage", actionTime);
			return actionTime;
		}

		return actionTime;
	}
	
	public enum CustomMsgType {
		AddUserRequestFromUser(1), //用户申请加入群
		AddUserRejectByManager(2), //管理员不同意进群
		AddUserRequestFromManager(3), //管理员拉人，向用户发出申请
		AddUserRejectByUser(4),    //用户不同意进群
		RevokeUserMessage(5);    //撤回已发送消息

		private final int value;

		CustomMsgType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<CustomMsgType> sValuesArray = new SparseArray<CustomMsgType>();

		static {
			for (CustomMsgType type : CustomMsgType.values()) {
				sValuesArray.put(type.value, type);
			}
		}

		public static CustomMsgType fromInt(int i) {
			CustomMsgType type = sValuesArray.get(Integer.valueOf(i));

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}

	static {
		DTNotificationCenter.getInstance().addObserver("IMReceiveCustomMessage",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Custom) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						if (sReceiveCustomMessageListener != null) {
							sReceiveCustomMessageListener.onReceiveCustomMessage(
									userMsg.mContent, userMsg.mFromCustomUserID,
									userMsg.mServerSendTime, userMsg.getExtraData2().toString());
						}
					}
				});
	}

	public static void setOnReceiveCustomMessageListener(
			OnReceiveCustomMessageListener l) {
		sReceiveCustomMessageListener = l;
	}

	private static OnReceiveCustomMessageListener sReceiveCustomMessageListener;
	private static long sLastSendCustomMessageTime;
}
