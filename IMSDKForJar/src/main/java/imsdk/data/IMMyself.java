package imsdk.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;

import java.util.HashMap;
import java.util.List;

import am.imsdk.model.a2.IMMyself2;

public class IMMyself {
	public interface IMMyselfListener {
		// actions
		void onActionSuccess(String actionType, long clientActionTime);

		void onActionFailure(String actionType, String error,
							 long clientActiontime);

		// login
		void onDisconnected(boolean loginConflict);

		void onReconnected();

		void onReceiveText(String text, String fromCustomUserID, String nickName,
						   long serverActionTime, String extraData);

		void onReceiveSystemText(long msgID, String fromCustomUserID, String text, long serverActionTime);

		void onReceiveAudio(String messageID, String fromCustomUserID, String nickName,
							long serverActionTime, String extraData);

		// bitmap
		void onReceiveBitmapMessage(String messageID, String fromCustomUserID, String nickName,
									long serverActionTime, String extraData);

		void onReceiveBitmap(Bitmap bitmap, String messageID,
							 String fromCustomUserID, long serverActionTime);

		void onReceiveBitmapProgress(double progress, String messageID,
									 String fromCustomUserID, long serverActionTime);
	}

	public interface OnReceiveTextListener {
		void onReceiveText(String text, String fromCustomUserID, String nickName,
						   long serverActionTime, String extraData);

		void onReceiveSystemText(long msgID, String fromCustomUserID, String text, long serverActionTime);

		void onReceiveAudio(String messageID, String fromCustomUserID, String nickName,
							long serverActionTime, String extraData);
	}

	public interface OnReceiveBitmapListener {
		void onReceiveBitmapMessage(String messageID, String fromCustomUserID, String nickName,
									long serverActionTime, String extraData);

		void onReceiveBitmap(Bitmap bitmap, String messageID,
							 String fromCustomUserID, long serverActionTime);

		void onReceiveBitmapProgress(double progress, String messageID,
									 String fromCustomUserID, long serverActionTime);
	}

	public interface OnReceiveOrderListener {
		void onReceiveOrder(String text, String fromCustomUserID, String nickName,
							long serverActionTime, String extraData);
	}

	public interface OnConnectionChangedListener {
		void onDisconnected(boolean loginConflict);

		void onReconnected();
	}

	public interface OnAutoLoginListener {
		void onAutoLoginBegan();

		void onAutoLoginSuccess();

		void onAutoLoginFailure(boolean loginConflict);
	}

	public interface OnActionListener {
		void onSuccess();

		void onFailure(String error);
	}

	public interface OnActionResultListener {
		void onSuccess(Object result);

		void onFailure(String error);
	}

	public interface OnActionProgressListener {
		void onSuccess();

		void onProgress(double progress);

		void onFailure(String error);
	}

	public interface OnLoginStatusChangedListener {
		void onLoginStatusChanged(LoginStatus oldLoginStatus,
								  LoginStatus newLoginStatus);
	}

	public interface OnInitializedListener {
		void onInitialized();
	}

	public interface OnCheckServiceLoginStateListener {
		void onConnected(String userCustomID);

		void onNoConnection();
	}

	public interface OnAudioPlayListener extends MediaPlayer.OnCompletionListener {
		void onError();
	}


	/**
	 * @method
	 * @brief 初始化登录信息，可能触发自动登录
	 * @param applicationContext
	 *            Android应用上下文环境
	 */
	public static boolean init(Context applicationContext) {
		return IMMyself2.init(applicationContext);
	}

	// 可能触发退出登录
	public static boolean setCustomUserID(String customUserID) {
		return IMMyself2.setCustomUserID(customUserID);
	}

	// 可能触发退出登录
	public static boolean setPassword(String password) {
		return IMMyself2.setPassword(password);
	}

	public static String getPassword() {
		return IMMyself2.getPassword();
	}

	public static boolean isLogined() {
		return IMMyself2.isLogined();
	}

	public static long register(long timeoutInterval, final OnActionListener l) {
		return IMMyself2.register(timeoutInterval, l);
	}

	public static void login() {
		IMMyself2.login();
	}

	public static void login(boolean autoLogin, long timeoutInterval,
							 final OnActionListener l) {
		IMMyself2.login(autoLogin, timeoutInterval, l);
	}

	public static void logout(final OnActionListener l) {
		IMMyself2.logout(l);
	}

	public static void stopPushService() {
		IMMyself2.stopSocketService();
	}

	public static long sendText(String text, String toCustomUserID) {
		return IMMyself2.sendText(text, toCustomUserID);
	}

	public static long sendText(String text, String toCustomUserID,
								long timeoutInterval, final OnActionListener l) {
		return IMMyself2.sendText(text, toCustomUserID, timeoutInterval, l);
	}

	public static long sendText(String text, String toCustomUserID, HashMap<String, Object> tags,
								long timeoutInterval, final OnActionListener l) {
		return IMMyself2.sendText(text, toCustomUserID, tags, timeoutInterval, l);
	}

	public static long showNoticeMsg(String text, String toCustomUserID,
									 HashMap<String, Object> tags) {
		return IMMyself2.showNoticeMsg(text, toCustomUserID, tags);
	}

	public static long sendNoticeMsg(String text, String toCustomUserID, HashMap<String, Object> tags,
									 long timeoutInterval, final OnActionListener l) {
		return IMMyself2.sendNoticeMsg(text, toCustomUserID, tags, timeoutInterval, l);
	}

	/**
	 *
	 * @Title: sendOrderState
	 * @Description: 发送结束订单请求
	 * @param orderId 订单ID
	 * @return long    返回类型 
	 * @author 方子君
	 */
	public static long sendRequestForOrderFinish(String toCustomUserID, long orderId,
												 long timeoutInterval, final OnActionListener l) {
		HashMap<String, Object> tag = new HashMap<String, Object>();
		tag.put("orderId", orderId);

		return IMMyself2.sendOrderState(0, toCustomUserID, tag, timeoutInterval, l);
	}

	public static boolean startRecording(String toCustomUserID) {
		return IMMyself2.startRecording(toCustomUserID);
	}

	public static long stopRecording(boolean needSend, HashMap<String, Object> tags, final long timeoutInterval,
									 final OnActionListener l) {
		return IMMyself2.stopRecording(needSend, tags, timeoutInterval, l);
	}

	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID,
								  final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyself2.sendBitmap(bitmap, toCustomUserID, timeoutInterval, l);
	}

	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID, HashMap<String, Object> tags,
								  final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyself2.sendBitmap(bitmap, toCustomUserID, tags, timeoutInterval, l);
	}

	public static long sendBitmap(final String imagePath, final String toCustomUserID, HashMap<String, Object> tags,
								  final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyself2.sendBitmap(imagePath, toCustomUserID, tags, timeoutInterval, l);
	}

	/**
	 * 根据语音文件路径发送消息
	 * @param audioPath 音频文件路径
	 * @param durationInMilliSeconds 音频时长
	 * @param toCustomUserID 发送对象
	 * @param tags 附加信息
	 * @param timeoutInterval 超时时间
	 * @param l 进度监听器
	 * @return
	 */
	public static long sendAudio(final String audioPath, long durationInMilliSeconds, final String toCustomUserID, HashMap<String, Object> tags,
								 final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyself2.sendAudio(audioPath, durationInMilliSeconds, toCustomUserID, tags, timeoutInterval, l);
	}

	/**
	 * 批量上传转发消息（即A中转， 代替B、C向对方发送消息）
	 * @param picList 图片集
	 * @param msgText 消息文本
	 * @param hintText1 只发给对象B的文本
	 * @param hintText2 只发给对象C的文本
	 * @param audioPath 音频存储路径
	 * @param audioDuration 音频时长
	 * @param toCustomUserID1 对象B
	 * @param toCustomUserID2 对象C
	 * @param nickName1 对象B的昵称
	 * @param nickName2 对象C的昵称
	 * @param tags1 对象B的附加消息
	 * @param tags2 对象C的附加消息
	 * @param l 进度监听器
	 * @return actionTime
	 */
	public static long sendTransferMsgs(final List<Uri> picList, final String msgText,
										final String hintText1, final String hintText2,
										final String audioPath, final long audioDuration,
										final String toCustomUserID1, final String toCustomUserID2,
										final String nickName1, final String nickName2,
										HashMap<String, Object> tags1, HashMap<String, Object> tags2,
										final OnActionProgressListener l) {
		return IMMyself2.sendTransferMsgs(picList, msgText, hintText1, hintText2, audioPath,
				audioDuration, toCustomUserID1, toCustomUserID2,
				nickName1, nickName2, tags1, tags2, l);
	}

	public static void closeSocket() {
		IMMyself2.closeSocket();
	}

	public static void setListener(IMMyselfListener listener) {
		IMMyself2.setListener(listener);
	}

	public static String getCustomUserID() {
		return IMMyself2.getCustomUserID();
	}

	public static String getAppKey() {
		return IMMyself2.getAppKey();
	}

	public static void setNickName(String nickName) {
		IMMyself2.setNickName(nickName);
	}

	public enum LoginStatus {
		// 未登录
		None(0), Logining(1), Reconnecting(2), AutoLogining(4),

		// 已登录
		Logined(11);

		private final int value;

		LoginStatus(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static LoginStatus getIndex(int index) {
			switch (index) {
				case 1:
					return Logining;
				case 2:
					return Reconnecting;
				case 4:
					return AutoLogining;
				case 11:
					return Logined;
				case 0:
				default:
					return None;
			}
		}
	}

	public static LoginStatus getLoginStatus() {
		return IMMyself2.getLoginStatus();
	}

	public static void setOnReceiveTextListener(OnReceiveTextListener l) {
		IMMyself2.setOnReceiveTextListener(l);
	}

	public static void setOnReceiveBitmapListener(OnReceiveBitmapListener l) {
		IMMyself2.setOnReceiveBitmapListener(l);
	}

	public static void setOnReceiveOrderListener(OnReceiveOrderListener l) {
		IMMyself2.setOnReceiveOrderListener(l);
	}

	public static void setOnConnectionChangedListener(OnConnectionChangedListener l) {
		IMMyself2.setOnConnectionChangedListener(l);
	}

	public static void setOnLoginStatusChangedListener(OnLoginStatusChangedListener l) {
		IMMyself2.setOnLoginStatusChangedListener(l);
	}

	public static void setOnOffLineMsgInitializedListener(OnInitializedListener l) {
		IMMyself2.setOnOffLineMsgInitializedListener(l);
	}
}
