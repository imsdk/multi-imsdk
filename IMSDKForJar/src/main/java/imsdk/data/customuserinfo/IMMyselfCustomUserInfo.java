package imsdk.data.customuserinfo;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnInitializedListener;
import am.imsdk.model.a2.IMMyselfCustomUserInfo2;

public final class IMMyselfCustomUserInfo {
	
	public static boolean isInitialized() {
		return IMMyselfCustomUserInfo2.isInitialized();
	}
	
	public static void setOnInitializedListener(OnInitializedListener l){
		IMMyselfCustomUserInfo2.setOnInitializedListener(l);
	}
	
	
	public static String get() {
		return IMMyselfCustomUserInfo2.get();
	}

	public static long commit(final String customUserInfo, final OnActionListener l) {
		return IMMyselfCustomUserInfo2.commit(customUserInfo, l);
	}
	
	public static String getLastError() {
		return IMMyselfCustomUserInfo2.getLastError();
	}
}
