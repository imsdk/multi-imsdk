package imsdk.data.group;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.IMMyself.OnActionResultListener;
import imsdk.data.IMMyself.OnInitializedListener;

import java.util.ArrayList;

import am.imsdk.model.a2.IMMyselfGroup2;
import android.graphics.Bitmap;

public final class IMMyselfGroup {
	public interface OnGroupEventsListener {
		// init
		void onInitialized();

		// event notifications
		void onGroupDeletedByUser(String groupID, String customUserID,
										 long actionServerTime);

		void onGroupNameUpdated(String newGroupName, String groupID,
									   long actionServerTime);

		void onCustomGroupInfoUpdated(String newGroupInfo, String groupID,
											 long actionSeverTime);

		void onGroupMemberUpdated(ArrayList<String> memberList, String groupID,
										 long actionServerTime);

		void onAddedToGroup(String groupID, long actionServerTime);

		void onRemovedFromGroup(String groupID, String customUserID, String nickName,
									   long actionServerTime);
	}

	public interface OnGroupMessageListener {
		// im text
		void onReceiveText(String text, String groupID, String fromCustomUserID, String nickName,
								  long actionServerTime);

		void onReceiveAudio(String messageID, String groupID, String fromCustomUserID, String nickName,
							long serverActionTime);

		void onReceiveCustomMessage(String customMessage, String groupID, String fromCustomUserID, String nickName,
									long serverActionTime);

		// im bitmap
		void onReceiveBitmapMessage(String messageID, String groupID, String fromCustomUserID, String nickName,
									long serverActionTime);

		void onReceiveBitmap(Bitmap bitmap, String messageID, String groupID, String fromCustomUserID, long serverActionTime);

		void onReceiveBitmapProgress(double progress, String messageID, String groupID,
											String fromCustomUserID, long serverActionTime);
	}

	public interface OnGroupBitmapListener {
		void onReceiveBitmapMessage(String messageID, String groupID, String fromCustomUserID, String nickName,
									long serverActionTime);

		void onReceiveBitmap(Bitmap bitmap, String messageID, String groupID, String fromCustomUserID, long serverActionTime);

		void onReceiveBitmapProgress(double progress, String messageID, String groupID,
									 String fromCustomUserID, long serverActionTime);
	}

	public interface OnGroupActionsListener {
		void onActionSuccess(String actionType, long clientActionTime);

		void onActionFailure(String actionType, String error,
									long clientActiontime);
	}

	public static boolean isInitialized() {
		return IMMyselfGroup2.isInitialized();
	}

	public static void setOnInitializedListener(final OnInitializedListener l) {
		IMMyselfGroup2.setOnInitializedListener(l);
	}

	public static void setOnGroupEventsListener(final OnGroupEventsListener l) {
		IMMyselfGroup2.setOnGroupEventsListener(l);
	}

	public static void setOnGroupActionsListener(final OnGroupActionsListener l) {
		IMMyselfGroup2.setOnGroupActionsListener(l);
	}

	public static void setOnGroupMessageListener(final OnGroupMessageListener l) {
		IMMyselfGroup2.setOnGroupMessageListener(l);
	}

	public static void setOnGroupBitmapListener(final OnGroupBitmapListener l) {
		IMMyselfGroup2.setOnGroupBitmapListener(l);
	}

	public static ArrayList<String> getMyGroupsList() {
		return IMMyselfGroup2.getMyGroupsList();
	}

	public static ArrayList<String> getMyOwnGroupIDList() {
		return IMMyselfGroup2.getMyOwnGroupIDList();
	}

	public static boolean isGroupInMyList(String groupID) {
		return IMMyselfGroup2.isGroupInMyList(groupID);
	}

	public static boolean isMyOwnGroup(String groupID) {
		return IMMyselfGroup2.isMyOwnGroup(groupID);
	}

	public static long createGroup(final String groupName,
			final OnActionResultListener l) {
		return IMMyselfGroup2.createGroup(groupName, l);
	}

	public static long deleteGroup(final String groupID, final OnActionListener l) {
		return IMMyselfGroup2.deleteGroup(groupID, l);
	}

	public static long addToGroupRequest(String customUserID, String toGroupID,
								 final OnActionListener l) {
		return IMMyselfGroup2.addToGroupRequest(customUserID, toGroupID, l);

	}

	public static long addMember(String customUserID, String toGroupID,
								 final OnActionListener l) {
		return IMMyselfGroup2.addMember(customUserID, toGroupID, l);

	}

	public static long removeMember(String customUserID, String fromGroupID,
			final OnActionListener l) {
		return IMMyselfGroup2.removeMember(customUserID, fromGroupID, l);
	}

	public static long sendText(String text, String toGroupID, final OnActionListener l) {
		return IMMyselfGroup2.sendText(text, toGroupID, l);
	}

	public static long sendNoticeMsg(String text, String toGroupID, final OnActionListener l) {
		return IMMyselfGroup2.sendNoticeMsg(text, toGroupID, l);
	}

	public static boolean startRecording(String toGroupID) {
		return IMMyselfGroup2.startRecording(toGroupID);
	}

	public static long stopRecording(boolean needSend, final long timeoutInterval,
			final OnActionListener l) {
		return IMMyselfGroup2.stopRecording(needSend, timeoutInterval, l);
	}

	public static long sendBitmap(final Bitmap bitmap, final String toGroupID,
			final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyselfGroup2.sendBitmap(bitmap, toGroupID, timeoutInterval, l);
	}

	public static long quitGroup(String fromGroupID, final OnActionListener l) {
		return IMMyselfGroup2.quitGroup(fromGroupID, l);
	}

	public static long changeManagerAuthName(String groupID, long userID, String authName, final OnActionListener l) {
		return IMMyselfGroup2.changeManagerAuthName(groupID, userID, authName, l);
	}

	public static long changeManagerAuthLevel(String groupID, long userID, int authValue, final OnActionListener l) {
		return IMMyselfGroup2.changeManagerAuthLevel(groupID, userID, authValue, l);
	}

	public static long searchGroupByName(int type, String groupName, int start, int end, final OnActionResultListener l) {
		return IMMyselfGroup2.searchGroupByName(type, groupName, start, end, l);
	}

	public static String getLastError() {
		return IMMyselfGroup2.getLastError();
	}
}
