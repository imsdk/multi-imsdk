package imsdk.data.group;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.aacmd.team.IMCmdTeamGetInfo;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.group.IMActionGroupKeepSilence;
import am.imsdk.action.group.IMActionGroupRemoveSilence;
import am.imsdk.action.group.IMActionGroupSetInfo;
import am.imsdk.action.group.IMActionUserRequestUpdateMemberList;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userinfo.IMUsersMgr;
import imsdk.data.IMMyself.OnActionListener;

public final class IMGroupInfo {
	private long mTeamID;
	private IMPrivateTeamInfo mTeamInfo;
	private String mGroupName = "";
	private String mCustomGroupInfo = "";

	public IMGroupInfo() {
		mTeamID = IMTeamsMgr.getInstance().getCreatingGroupInfoTeamID();

		if (mTeamID == 0) {
			return;
		}

		mTeamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

		mGroupName = mTeamInfo.mTeamName;
		mCustomGroupInfo = mTeamInfo.mExInfo;

		update();

		DTNotificationCenter.getInstance().addObserver("TeamUpdated:" + mTeamID,
				new Observer() {
					@Override
					public void update(Object data) {
						IMGroupInfo.this.update();
					}
				});
	}

	private void update() {
		DTLog.sign("GroupInfo update");
		DTLog.sign("mTeamID:" + mTeamID);

		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

		mGroupName = teamInfo.mTeamName;
		mCustomGroupInfo = teamInfo.mExInfo;
		DTLog.sign("teamInfo.getUIDList():" + teamInfo.getUIDList());
	}

	public String getGroupID() {
		if (mTeamInfo == null) {
			DTLog.logError();
			return "";
		}

		return DTTool.getSecretString(mTeamInfo.mTeamID);
	}

	public int getMaxUserCount() {
		if (mTeamInfo == null) {
			DTLog.logError();
			return 0;
		}

		return (int) mTeamInfo.mMaxCount;
	}

	public String getOwnerCustomUserID() {
		if (mTeamInfo == null) {
			DTLog.logError();
			return "";
		}

		String result = IMUsersMgr.getInstance().getCustomUserID(mTeamInfo.mFounderUID);

		if (!IMParamJudge.isCustomUserIDLegal(result)) {
			DTLog.logError();
			return "";
		}

		return result;
	}

	public String getGroupName() {
		return mGroupName;
	}

	public void setGroupName(String groupName) {
		mGroupName = groupName;
	}

	public String getCustomGroupInfo() {
		return mCustomGroupInfo;
	}

	public void setCustomGroupInfo(String customGroupInfo) {
		mCustomGroupInfo = customGroupInfo;
	}

	public ArrayList<String> getMemberList() {
		if (mTeamInfo == null) {
			DTLog.logError();
			return new ArrayList<String>();
		}

		ArrayList<String> result = mTeamInfo.getCustomUserIDsList();
		
		if (IMMyselfGroup.isInitialized() && result.size() == 0) {
			DTLog.logError();
		}

		return result;
	}

	public long requestUpdateGroupInfo(final OnActionListener l) {
		long actionTime = System.currentTimeMillis();

		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		if (mTeamID == 0) {
			DTLog.logError();
			return actionTime;
		}

		final IMCmdTeamGetInfo cmd = new IMCmdTeamGetInfo();

		cmd.addTeamID(mTeamID);

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (l != null) {
					switch (type) {
					case SendFailed:
					case NoRecv:
						l.onFailure(type.toString());
						break;
					case RecvError:
						l.onFailure(errorJsonObject != null ? errorJsonObject
								.toString() : "Error code:" + errorCode);
						break;
					default:
						DTLog.logError();
						break;
					}
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		cmd.send();
		return actionTime;
	}

	public long requestUpdateMemberList(final OnActionListener l) {
		long actionTime = System.currentTimeMillis();

		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		if (mTeamID == 0) {
			DTAppEnv.postOnUIThread(new Runnable() {
				@Override
				public void run() {
					l.onFailure("IMSDK Error");
				}
			});

			DTLog.logError();
			return actionTime;
		}

		IMActionUserRequestUpdateMemberList action = new IMActionUserRequestUpdateMemberList();

		action.mGroupID = getGroupID();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.begin();
		return actionTime;
	}

	public long commitGroupInfo(final OnActionListener l) {
		long actionTime = System.currentTimeMillis();

		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		if (mTeamID == 0) {
			DTAppEnv.postOnUIThread(new Runnable() {
				@Override
				public void run() {
					l.onFailure("IMSDK Error");
				}
			});

			DTLog.logError();
			return actionTime;
		}

		IMActionGroupSetInfo action = new IMActionGroupSetInfo();

		action.mGroupID = getGroupID();
		action.mGroupName = mGroupName;
		action.mExInfo = mCustomGroupInfo;

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.begin();
		return actionTime;
	}

	public ArrayList<Long> getManagerList() {
		ArrayList<Long> managerIDList = new ArrayList<Long>();

		for (IMPrivateTeamInfo.OwnerInfo ownerInfo : mTeamInfo.getManagerInfoList()) {
			managerIDList.add(ownerInfo.getUID());
		}

		return managerIDList;
	}

	public IMPrivateTeamInfo.OwnerInfo getUserOwnerInfo(long userID) {
		IMPrivateTeamInfo.OwnerInfo userOwnerInfo = null;

		for (IMPrivateTeamInfo.OwnerInfo ownerInfo : mTeamInfo.getManagerInfoList()) {
			if(ownerInfo.getUID() == userID) {
				userOwnerInfo = ownerInfo;
			}
		}

		return userOwnerInfo;
	}

	/**
	 * 是否是群管理员
	 * @param userID
	 * @return
	 */
	public boolean isGroupManager(long userID) {
		for (IMPrivateTeamInfo.OwnerInfo ownerInfo : mTeamInfo.getManagerInfoList()) {
			if(ownerInfo.getUID() == userID) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 获取管理员的权限名
	 * @param userID
	 * @return
	 */
	public String getUserAuthName(long userID) {
		String authName = "";
		for (IMPrivateTeamInfo.OwnerInfo ownerInfo : mTeamInfo.getManagerInfoList()) {
			if(ownerInfo.getUID() == userID) {
				authName = ownerInfo.getAuthName();
			}
		}

		return authName;
	}

	/**
	 * 获取管路员的权限值
	 * @param userID
	 * @return
	 */
	public Integer getUserAuthLevel(long userID) {
		ArrayList<Integer> authList = new ArrayList<Integer>();

		for (IMPrivateTeamInfo.OwnerInfo ownerInfo : mTeamInfo.getManagerInfoList()) {
			if(ownerInfo.getUID() == userID) {
				return ownerInfo.getAuthLevel();
			}
		}

		return 0;
	}

	public ArrayList<Long> getAuthKickOutList() {
		return mTeamInfo.getAuthKickOutList();
	}

	public ArrayList<Long> getAuthKeepSilenceList() {
		return mTeamInfo.getAuthKeepSilenceList();
	}

	public ArrayList<Long> getAuthAddUserList() {
		return mTeamInfo.getAuthAddUserList();
	}

	public long changeManagerAuthInfo(long userID, String authName, int authLevel, final OnActionListener l) {
		// 1. 登录用户是否完成初始化操作
		// 2. 当前TeamID是否正确
		// 3. 当前群是否有用户
		// 4. 当前群是否存在被修改用户userID
		// 5. 修改用户权限级别

		long actionTime = System.currentTimeMillis();

		// 1. 登录用户是否完成初始化操作
		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		// 2. 当前TeamID是否正确
		if (mTeamID == 0) {
			DTAppEnv.postOnUIThread(new Runnable() {
				@Override
				public void run() {
					l.onFailure("IMSDK Error");
				}
			});

			DTLog.logError();
			return actionTime;
		}

		// 3. 当前群是否有用户
		if (getMemberList().size() == 0) {
			DTLog.logError();
			return actionTime;
		}

		// 4. 当前群是否存在被修改用户userID
		if (!getMemberList().contains(String.valueOf(userID))) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("can't edit the user info while he doesn't in the group");
					}
				});
			}

			return actionTime;
		}

		// 5. 修改用户权限级别
		if(mTeamInfo.changeAuthInfo(userID, authName, authLevel)) {
			IMActionGroupSetInfo action = new IMActionGroupSetInfo();
			action.mGroupID = getGroupID();
			action.mGroupName = mGroupName;
			action.mExInfo = mCustomGroupInfo;
			action.mCoreInfo = mTeamInfo.packageCoreInfo();

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					if (l != null) {
						l.onFailure(error);
					}
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					mTeamInfo.saveFile();

					if (l != null) {
						l.onSuccess();
					}
				}
			};

			action.begin();
		} else {
			DTAppEnv.postOnUIThread(new Runnable() {
				@Override
				public void run() {
					l.onFailure("edit auth name failed,can't update to server");
				}
			});
		}

		return actionTime;
	}

	/**
	 * 设置userID禁言
	 * @param userID
	 * @param l
	 * @return
	 */
	public long insertSilenceUser(long userID, final OnActionListener l) {
		// 1. 登录用户是否完成初始化操作
		// 2. 当前TeamID是否正确
		// 3. 当前群是否有用户
		// 4. 当前群是否存在被修改用户userID
		// 5. 不能设置自己禁言
		// 6. 群主不能被禁言
		// 7. 设置禁言并修改群信息

		long actionTime = System.currentTimeMillis();

		// 1. 登录用户是否完成初始化操作
		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		// 2. 当前TeamID是否正确
		if (mTeamID == 0) {
			DTAppEnv.postOnUIThread(new Runnable() {
				@Override
				public void run() {
					l.onFailure("IMSDK Error");
				}
			});

			DTLog.logError();
			return actionTime;
		}

		// 3. 当前群是否有用户
		if (getMemberList().size() == 0) {
			DTLog.logError();
			return actionTime;
		}

		// 4. 当前群是否存在被修改用户userID
		if (!getMemberList().contains(String.valueOf(userID))) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("can't edit the user info while he doesn't in the group");
					}
				});
			}

			return actionTime;
		}

		// 5. 不能设置自己禁言
		if(userID == IMPrivateMyself.getInstance().getUID()) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("can't silence yourself");
					}
				});
			}

			return actionTime;
		}


		// 6. 群主不能被禁言
		if(getOwnerCustomUserID().equals(String.valueOf(userID))) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("can't silence the owner of group");
					}
				});
			}

			return actionTime;
		}

		// 7. 设置禁言并修改群信息
		mTeamInfo.insertSilenceUser(userID);

		IMActionGroupKeepSilence action = new IMActionGroupKeepSilence();
		action.mCustomUserID = userID + "";
		action.mGroupID = getGroupID();
		action.mGroupName = mGroupName;
		action.mExtraInfo = mCustomGroupInfo;
		action.mCoreInfo = mTeamInfo.packageCoreInfo();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				mTeamInfo.saveFile();

				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.begin();

		return actionTime;
	}

	public long removeSilenceUser(long userID, final OnActionListener l) {
		// 1. 登录用户是否完成初始化操作
		// 2. 当前TeamID是否正确
		// 3. 当前群是否有用户
		// 4. 当前群是否存在被修改用户userID
		// 5. 设置禁言并修改群信息

		long actionTime = System.currentTimeMillis();

		// 1. 登录用户是否完成初始化操作
		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		// 2. 当前TeamID是否正确
		if (mTeamID == 0) {
			DTAppEnv.postOnUIThread(new Runnable() {
				@Override
				public void run() {
					l.onFailure("IMSDK Error");
				}
			});

			DTLog.logError();
			return actionTime;
		}

		// 3. 当前群是否有用户
		if (getMemberList().size() == 0) {
			DTLog.logError();
			return actionTime;
		}

		// 4. 当前群是否存在被修改用户userID
		if (!getMemberList().contains(userID)) {
			if (l != null) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						l.onFailure("can't edit the user info while he doesn't in the group");
					}
				});
			}

			return actionTime;
		}

		// 5. 设置禁言并修改群信息
		mTeamInfo.removeSilenceUser(userID);

		IMActionGroupRemoveSilence action = new IMActionGroupRemoveSilence();
		action.mCustomUserID = userID + "";
		action.mGroupID = getGroupID();
		action.mGroupName = mGroupName;
		action.mExtraInfo = mCustomGroupInfo;
		action.mCoreInfo = mTeamInfo.packageCoreInfo();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				mTeamInfo.saveFile();

				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.begin();

		return actionTime;
	}

	public ArrayList<Long> getSilenceUserList() {
		return mTeamInfo.getSilenceUserList();
	}
}
