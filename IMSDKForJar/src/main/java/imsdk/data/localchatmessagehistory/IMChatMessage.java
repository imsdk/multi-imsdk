package imsdk.data.localchatmessagehistory;

import imsdk.data.IMMessage;
import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;

public final class IMChatMessage extends IMMessage {
	public IMChatMessage() {
		if (IMUserMsg.sCreatingUserMsg == null) {
			DTLog.logError();
			return;
		}

		mUserMsg = IMUserMsg.sCreatingUserMsg;
	}

	public String getFromCustomUserID() {
		if (mUserMsg == null) {
			DTLog.logError();
			return "";
		}

		return mUserMsg.mFromCustomUserID;
	}

	public String getToCustomUserID() {
		if (mUserMsg == null) {
			DTLog.logError();
			return "";
		}

		return mUserMsg.mToCustomUserID;
	}

	public String getText() {
		if (mUserMsg == null) {
			DTLog.logError();
			return "";
		}
		
		if (mUserMsg.mUserMsgType == UserMsgType.Audio) {
			return "语音消息";
		} else if (mUserMsg.mUserMsgType == UserMsgType.Photo) {
			return "图片消息";
		}

		return mUserMsg.mContent;
	}

	public long getClientSendTime() {
		if (mUserMsg == null) {
			DTLog.logError();
			return 0;
		}

		return mUserMsg.mClientSendTime;
	}

	public long getServerSendTime() {
		if (mUserMsg == null) {
			DTLog.logError();
			return 0;
		}

		return mUserMsg.mServerSendTime;
	}

	public int getState() {
		if (mUserMsg == null) {
			DTLog.logError();
			return 0;
		}

		return mUserMsg.mStatus;
	}

	public boolean isReceivedMessage() {
		if (mUserMsg == null) {
			DTLog.logError();
			return false;
		}

		return mUserMsg.mIsRecv;
	}

	private IMUserMsg mUserMsg;

	@Override
	public int getMsgType() {
		switch (mUserMsg.mUserMsgType) {
		case Normal:
			return TXT;
		case Audio:
			return VOICE;
		case File:
			return FILE;
		case Photo:
			return IMAGE;
		case Video:
			return VIDEO;
		case Custom:
			return CUSTOMTEXT;
		default:
			return 0;
		}
	}
}
