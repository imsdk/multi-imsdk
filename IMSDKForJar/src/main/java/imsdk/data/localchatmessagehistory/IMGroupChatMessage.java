package imsdk.data.localchatmessagehistory;

import imsdk.data.IMMessage;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.amimteam.IMTeamMsg;

public final class IMGroupChatMessage extends IMMessage {
	public IMGroupChatMessage() {
		if (IMTeamMsg.sCreatingTeamMsg == null) {
			DTLog.logError();
			return;
		}

		mTeamMsg = IMTeamMsg.sCreatingTeamMsg;
	}

	public String getFromCustomUserID() {
		if (mTeamMsg == null) {
			DTLog.logError();
			return "";
		}

		return mTeamMsg.mFromCustomUserID;
	}

	public String getGroupID() {
		if (mTeamMsg == null) {
			DTLog.logError();
			return "";
		}

		return DTTool.getSecretString(mTeamMsg.mTeamID);
	}

	public String getText() {
		if (mTeamMsg == null) {
			DTLog.logError();
			return "";
		}

		return mTeamMsg.mContent;
	}

	public long getClientSendTime() {
		if (mTeamMsg == null) {
			DTLog.logError();
			return 0;
		}

		return mTeamMsg.mClientSendTime;
	}

	public long getServerSendTime() {
		if (mTeamMsg == null) {
			DTLog.logError();
			return 0;
		}

		return mTeamMsg.mServerSendTime;
	}
	
	public boolean isReceivedMessage() {
		if (mTeamMsg == null) {
			DTLog.logError();
			return false;
		}
		return mTeamMsg.mIsRecv;
	}

	private IMTeamMsg mTeamMsg;
	
	@Override
	public String getToCustomUserID() {
		return "";
	}

	@Override
	public int getState() {
		if (mTeamMsg == null) {
			DTLog.logError();
			return 0;
		}
		
		return mTeamMsg.mStatus;
	}

	@Override
	public int getMsgType() {
		switch (mTeamMsg.mTeamMsgType) {
		case Normal:
			return TXT;
		case Audio:
			return VOICE;
		case NormalFileText:
			return FILE;
		case Photo:
			return IMAGE;
		case Video:
			return VIDEO;
		case Custom:
			return CUSTOMTEXT;
		default:
			return 0;
		}
	}
	
}
