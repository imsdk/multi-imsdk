package imsdk.data;

public abstract class IMMessage {
	public abstract String getFromCustomUserID();

	public abstract String getToCustomUserID();

	public abstract String getText();

	public abstract long getClientSendTime();

	public abstract long getServerSendTime();

	public abstract int getState();
	
	public abstract int getMsgType();
	
	public abstract boolean isReceivedMessage();
	
	public final static int FAILURE = -1; // 消息发送或接收失败
	public final static int SUCCESS = 0;
	public final static int SENDING_OR_RECVING = 1;
	
	public final static int TXT = 1;
	public final static int IMAGE = 2;
	public final static int VOICE = 3;
	public final static int VIDEO = 4;
	public final static int FILE = 5;
	public final static int CUSTOMTEXT = 6;
	
}
