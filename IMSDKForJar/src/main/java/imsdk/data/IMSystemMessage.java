package imsdk.data;

import am.dtlib.model.c.tool.DTLocalModel;

public final class IMSystemMessage extends DTLocalModel {
	public long mServerSendTime;
	public String mContent = "";
	public String mCustomUserId = "";

	public IMSystemMessage() {
		super();

		addRenameField("mCustonUserId", "mCustomUserId");
	}
}
