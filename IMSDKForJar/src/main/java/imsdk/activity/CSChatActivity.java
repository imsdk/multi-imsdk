package imsdk.activity;

import imsdk.views.CSChatView;
import am.dtlib.model.b.log.DTLog;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.model.IMParamJudge;
import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;

public final class CSChatActivity extends Activity {
	// data
	private String mCustomUserID;
	private String mAppKey;
	private String mClickSource;

	// ui
	private CSChatView mChatView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		Uri uri = intent.getData();

		if (uri == null) {
			DTLog.logError();
			return;
		}

		String scheme = uri.getScheme();

		if (!scheme.equals("imsdk")) {
			DTLog.logError();
			return;
		}

		String totalString = uri.toString();

		String[] array = totalString.split("&k=");

		if (array.length != 2) {
			DTLog.logError();
			return;
		}

		mCustomUserID = getCustomUserID(totalString);
		mAppKey = getAppKey(totalString);
		mClickSource = getClickSource(totalString);

		if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
			DTLog.logError();
			return;
		}

		// clear warning by lyc.
		setTitle(mAppKey);
		setTitle(mClickSource);

		// 使得音量键控制媒体声音
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		ImageButton leftBtn = ((ImageButton) findViewById(CPResourceUtil.getId(this, "left")));

		if (leftBtn != null) {
			leftBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();

					// 打开点击来源app
				}
			});
		}

		// 创建一个IMChatView实例
		mChatView = new CSChatView(this, mCustomUserID);

		// 为IMChatView实例配置参数
		mChatView.setMaxGifCountInMessage(10);
		mChatView.setUserMainPhotoVisible(false);
		mChatView.setUserMainPhotoCornerRadius(10);
		mChatView.setTitleBarVisible(true);

		// 添加到当前activity
		setContentView(mChatView);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 为了实现捕获用户选择的图片
		mChatView.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 为了实现点击返回键隐藏表情栏
		mChatView.onKeyDown(keyCode, event);
		return super.onKeyDown(keyCode, event);
	}

	private String getCustomUserID(String totalString) {
		String[] array = totalString.split("&k=");

		if (array.length == 2) {
			totalString = array[0];
		}

		array = totalString.split("&f=");

		if (array.length == 2) {
			totalString = array[0];
		}

		if (totalString.length() <= 8) {
			DTLog.logError();
			return "";
		}

		return totalString.substring(8);
	}

	private String getAppKey(String totalString) {
		String[] array = totalString.split("&k=");

		if (array.length != 2) {
			return "";
		}

		if (array.length == 2) {
			totalString = array[1];
		}

		array = totalString.split("&f=");

		if (array.length == 2) {
			totalString = array[0];
		}

		return totalString;
	}

	private String getClickSource(String totalString) {
		String[] array = totalString.split("&f=");

		if (array.length != 2) {
			return "";
		}

		if (array.length == 2) {
			totalString = array[1];
		}

		array = totalString.split("&k=");

		if (array.length == 2) {
			totalString = array[0];
		}

		return totalString;
	}
}
