package am.dtlib.model.d;

import remote.service.NetConnectedObserver;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public final class DTNetChangeReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		NetConnectedObserver.getInstance().detect();
	}
}
