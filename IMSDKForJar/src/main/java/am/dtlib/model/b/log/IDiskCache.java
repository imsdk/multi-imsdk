package am.dtlib.model.b.log;

import java.io.File;

/** 内存管理接口 */
public interface IDiskCache {
    /** 获取文件夹 */
    File getDirectory();

    /** 存入文件 */
    boolean putFile(File file);

    /** 获取文件 */
    File getFile(String fileName);

    /** 移除文件 */
    boolean removeFile(String fileName);

    /** 清空资源 */
    void clear();

    /** 关闭缓存管理，释放资源 */
    void close();
}
