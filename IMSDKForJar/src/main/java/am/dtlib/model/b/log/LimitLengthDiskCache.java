package am.dtlib.model.b.log;

import android.text.TextUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by fzj on 2016/1/8.
 * 空间限制类的缓存管理规则：
 * 1、单个文件有固定大小
 * 2、文件列表长度有限制
 *
 */
public class LimitLengthDiskCache extends BaseDiskCache {

    private final int DEFAULT_FILELIST_LENGTH = 5; //默认为5个文件
    private final int DEFAULT_FILE_MAX_BUFFER_SIZE = 100 * 1024; //默认单文件最大大小100K

    /** 文件列表长度 */
    private int mFileListLength = DEFAULT_FILELIST_LENGTH;

    /** 单文件最大长度 */
    private int mFileBufferSize = DEFAULT_FILE_MAX_BUFFER_SIZE;

    /** 文件命名前缀 */
    private String mFileNameGenerator;

    public LimitLengthDiskCache(File dir) {
        super(dir);
    }

    /** 添加命名规则，符合该规则的文件列表需要被加入内存管理行列 */
    public void addRule(String ruleName) {
        mFileNameGenerator = ruleName;
    }

    /**
     * 通过命名规则，获取符合规则的文件
     * 1. 判断文件列表是否超过长度
     * 2. 判断文件大小是否超出限制
     */
    public File getFileWithRule() {
        // 通过规则获取文件列表
        File[] files = cacheDir.listFiles(fileFilter);

        int createOrAppend = 0;  // 创建为1，追加为2， 出错为0
        String fileName = "";

        if (files == null || files.length == 0) {
            createOrAppend = 1;
        } else {
            ArrayList<FileInfo> fileList = new ArrayList<FileInfo>();//将需要的子文件信息存入到FileInfo里面
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                FileInfo fileInfo = new FileInfo();
                fileInfo.name = file.getName();
                fileInfo.path = file.getPath();
                fileInfo.length = file.length();
                fileInfo.lastModified= file.lastModified();

                fileList.add(fileInfo);
            }

            //通过重写Comparator的实现类排序
            Collections.sort(fileList, new FileComparator());

            // 1. 判断文件列表是否超过长度
            if (files.length > mFileListLength) {
                //超过列表长度的文件都删掉
                ArrayList<String> removeNames = new ArrayList<String>();
                for (int i = 0; i < files.length - mFileListLength; i++) {
                    removeNames.add(fileList.get(i).name);
                }

                for (int i = 0; i < removeNames.size(); i++) {
                    removeFile(removeNames.get(i));
                }
            }

            // 2. 判断文件大小是否超出限制
            FileInfo lastFile = fileList.get(fileList.size() - 1);
            if (lastFile != null && mFileBufferSize - lastFile.length >= 100) {
                createOrAppend = 2;
                fileName = lastFile.name;
            } else {
                createOrAppend = 1;
            }
        }

        if(createOrAppend != 0) {
            // 直接创建文件
            if (createOrAppend == 2) {
                return super.getFile(fileName);
            } else {
                SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                String date = sDateFormat.format(new java.util.Date());

                fileName = mFileNameGenerator + date + ".txt";

                File newFile = super.getFile(fileName);
                if(!newFile.exists()) {
                    try {
                        newFile.createNewFile();

                        return newFile;
                    } catch (IOException e) {
                        DTLog.logError(e.toString());
                        e.printStackTrace();
                    }
                }

                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 设置文件列表大小
     * @param mFileListLength
     */
    public void setFileListLength(int mFileListLength) {
        this.mFileListLength = mFileListLength;
    }

    /**
     * 设置单一文件最大大小
     * @param mFileBufferSize
     */
    public void setFileBufferSize(int mFileBufferSize) {
        this.mFileBufferSize = mFileBufferSize;
    }

    /** 文件按修改时间从小到大排序 */
    private class FileComparator implements Comparator<FileInfo> {
        public int compare(FileInfo file1, FileInfo file2) {
            if (file1.lastModified < file2.lastModified) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    /**
     * 文件过滤
     * 1. 必须是文件而不是目录
     * 2. 若设置了文件命名规则，必须符合规则
     */
    private FileFilter fileFilter = new FileFilter() {
        public boolean accept(File file) {
            if(!file.isFile()) {
                return  false;
            }

            if(TextUtils.isEmpty(mFileNameGenerator)) {
                return true;
            }

            String tmp = file.getName().toLowerCase();
            if (tmp.startsWith(mFileNameGenerator)) {
                return true;
            }
            return false;
        }
    };
}
