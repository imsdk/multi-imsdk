package am.dtlib.model.b.log;

import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import am.dtlib.model.a.base.DTAppEnv;

public final class DTLog {
	public static void i(String tag, String msg) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.i(tag, msg);
			}
		}
	}

	public static void i(String tag, String msg, Throwable throwable) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.i(tag, msg, throwable);
			}
		}
	}

	public static void d(String tag, String msg) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.d(tag, msg);
			}
		}
	}

	public static void d(String tag, String msg, Throwable throwable) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.d(tag, msg, throwable);
			}
		}
	}

	public static void w(String tag, String msg) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.w(tag, msg);
			}
		}
	}

	public static void w(String tag, String msg, Throwable throwable) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.w(tag, msg, throwable);
			}
		}
	}

	public static void e(String tag, String msg) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.e(tag, msg);
			}
		}
	}

	public static void e(String tag, String msg, Throwable throwable) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.e(tag, msg, throwable);
			}
		}
	}

	public static void v(String tag, String msg) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.v(tag, msg);
			}
		}
	}

	public static void v(String tag, String msg, Throwable throwable) {
		if (DTAppEnv.isSDKLoggable()) {
			if(tag != null && msg != null){
				Log.v(tag, msg, throwable);
			}
		}
	}

	private static LimitLengthDiskCache getErrDiskCache() {
		if(mErrDiskCache != null) return mErrDiskCache;

		File errorLogDir = new File(DTAppEnv.getIMSDKDirectoryFullPath(), "log");

		mErrDiskCache = new LimitLengthDiskCache(errorLogDir);
		mErrDiskCache.addRule("err_");
//        mErrDiskCache.setFileListLength(3);
        mErrDiskCache.setFileBufferSize(1024 * 1024);

		return mErrDiskCache;
	}

	private static LimitLengthDiskCache getCommonDiskCache() {
		if(mCommonDiskCache != null) return mCommonDiskCache;

		File errorLogDir = new File(DTAppEnv.getIMSDKDirectoryFullPath(), "log");

		mCommonDiskCache = new LimitLengthDiskCache(errorLogDir);
		mCommonDiskCache.addRule("log_");
//        mCommonDiskCache.setFileListLength(3);
        mCommonDiskCache.setFileBufferSize(1024 * 1024);

		return mCommonDiskCache;
	}

	private static LimitLengthDiskCache mCommonDiskCache;
	private static LimitLengthDiskCache mErrDiskCache;

	private static String getErrorLogFileFullPath() {
		File file = getErrDiskCache().getFileWithRule();

		if(file == null) {
			DTLog.logError();
			return null;
		}

		return file.toString();
	}

	private static String getCommonLogFileFullPath() {
		File file = getCommonDiskCache().getFileWithRule();

		if(file == null) {
			DTLog.logError();
			return null;
		}

		return file.toString();
	}

	public static boolean isFolderExists(String folderFullPath) {
		File file = new File(folderFullPath);

		return file.exists() && file.isDirectory();
	}

	public static void checkCreateDirectory(String fullPath) {
		File file = new File(fullPath);

		if (file.exists() && file.isDirectory()) {
			return;
		}

		if (!file.mkdirs()) {
			DTLog.logError();
		}
	}

	public static void sign() {
		signPrivate(4);
	}

	public static void sign(boolean ifSign, String strSignContent) {
		if (ifSign) {
			addLog(strSignContent, false);
			sign(strSignContent);
		}
	}


	public static void d(String logmsg){
		if(DTAppEnv.isSDKLoggable()){
			System.out.println(logmsg);
			Log.d("IMSDK", logmsg);
		}
	}

	public static void sign(String strSignContent) {
		if (!DTAppEnv.isSDKLoggable()) {
			return;
		}

		signPrivate(5);

		if (strSignContent == null) {
			System.out.println("null");
			Log.d("IMSDK", "null");
		} else if (strSignContent.length() == 0) {
			System.out.println("Empty String");
			Log.d("IMSDK", "Empty String");
		} else {
			System.out.println(strSignContent);
			Log.d("IMSDK", strSignContent);
		}
	}

	public static void sign(int nSignContent) {
		signPrivate(4);
		System.out.println("" + nSignContent);
	}

	public static void sign(float fSignContent) {
		signPrivate(4);
		Log.d("IMSDK", "" + fSignContent);
		System.out.println("" + fSignContent);
	}

	public static void sign(long lSignContent) {
		signPrivate(4);
		Log.d("IMSDK", "" + lSignContent);
		System.out.println("" + lSignContent);
	}

	public static void sign(byte byteSignContent) {
		signPrivate(4);
		Log.d("IMSDK", "" + byteSignContent);
		System.out.println("" + byteSignContent);
	}

	public static void sign(boolean bSignContent) {
		signPrivate(4);
		Log.d("IMSDK", "" + bSignContent);
		System.out.println("" + bSignContent);
	}

	public static void signGrade(int nGradeCount) {
		for (int i = 0; i < nGradeCount; i++) {
			signPrivate(4 + i);
		}
	}

	private static void signPrivate(int nGrade) {
		StringBuilder sBuilder = new StringBuilder();

		sBuilder.append("File:");
		sBuilder.append(Thread.currentThread().getStackTrace()[nGrade].getFileName());
		sBuilder.append(", Line:");
		sBuilder.append(Thread.currentThread().getStackTrace()[nGrade].getLineNumber());
		sBuilder.append(", Class:");
		sBuilder.append(Thread.currentThread().getStackTrace()[nGrade].getClassName());
		sBuilder.append(", Method:");
		sBuilder.append(Thread.currentThread().getStackTrace()[nGrade].getMethodName());

		System.out.println(sBuilder.toString());
		Log.d("IMSDK", sBuilder.toString());
	}

	public static void log() {
		addLog(null, false);
		sign();
	}

	public static void log(String strContent) {
		addLog(strContent, false);
		sign(strContent);
	}

	public static void logError() {
		addLog("Error Occured!", true);
		addLog("Error Occured!", false);
	}

	public static void logError(String errerMsg) {
		addLog("Error Occured! "+errerMsg, true);
		addLog("Error Occured! "+errerMsg, false);
	}

	// 中文字符只能输出到文件
	public static void log(byte[] btLogContent) {
		String strLogContent = "";

		try {
			strLogContent = new String(btLogContent, "UTF8");
		} catch (UnsupportedEncodingException e) {
		}

		addLog(strLogContent, false);
	}

	// 参数为null时输出title和空行
	private static void addLog(String strLogContent, boolean error) {
		int nGrade = 4;
		final String strTab = "-";
		final String strEndline = "  \r\n";
//		final String strEndline = "";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS",
				Locale.getDefault());

		StringBuilder sBuilder = new StringBuilder();

		sBuilder.append(simpleDateFormat.format(new Date())).append(strEndline)
				.append("File:")
				.append(Thread.currentThread().getStackTrace()[nGrade].getFileName())
				.append(strEndline).append(strTab).append(strTab).append(strTab).append("Line:")
				.append(Thread.currentThread().getStackTrace()[nGrade].getLineNumber())
				.append(strEndline).append(strTab).append(strTab).append(strTab).append("Class:")
				.append(Thread.currentThread().getStackTrace()[nGrade].getClassName())
				.append(strEndline).append(strTab).append(strTab).append(strTab).append("Method:")
				.append(Thread.currentThread().getStackTrace()[nGrade].getMethodName())
				.append(strEndline);

		if (Thread.currentThread().getStackTrace().length > ++nGrade) {
			sBuilder.append(simpleDateFormat.format(new Date())).append(strEndline)
					.append("File:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getFileName())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Line:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getLineNumber())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Class:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getClassName())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Method:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getMethodName())
					.append(strEndline);
		}

		if (Thread.currentThread().getStackTrace().length > ++nGrade) {
			sBuilder.append(simpleDateFormat.format(new Date())).append(strEndline)
					.append("File:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getFileName())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Line:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getLineNumber())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Class:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getClassName())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Method:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getMethodName())
					.append(strEndline);
		}

		if (Thread.currentThread().getStackTrace().length > ++nGrade) {
			sBuilder.append(simpleDateFormat.format(new Date())).append(strEndline)
					.append("File:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getFileName())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Line:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getLineNumber())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Class:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getClassName())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Method:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getMethodName())
					.append(strEndline);
		}

		if (Thread.currentThread().getStackTrace().length > ++nGrade) {
			sBuilder.append(simpleDateFormat.format(new Date())).append(strEndline)
					.append("File:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getFileName())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Line:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getLineNumber())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Class:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getClassName())
					.append(strEndline).append(strTab).append(strTab).append(strTab).append("Method:")
					.append(Thread.currentThread().getStackTrace()[nGrade].getMethodName())
					.append(strEndline);
		}

		if (TextUtils.isEmpty(strLogContent)) {
			sBuilder.append(strEndline);
		} else {
			sBuilder.append(strTab).append(strTab).append(strTab).append(strLogContent).append(strEndline);
		}

		try {
			String logPath = error ? getErrorLogFileFullPath() : getCommonLogFileFullPath();

			if(TextUtils.isEmpty(logPath)) {
				return;
			}

			FileWriter file = new FileWriter(logPath, true);

			file.append(sBuilder.toString());
			file.append(strEndline);

			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (error) {
			System.err.println(strLogContent + " " + sBuilder.toString());
			Log.w("IMSDK", strLogContent + " " + sBuilder.toString());
		}
	}

	// 获取日志 并清空
	public static String getErrorLogThenClear() {
		String strResult = "";

		try {
			FileReader fileReader = new FileReader(getErrorLogFileFullPath());
			BufferedReader bufferReader = new BufferedReader(fileReader, 500);
			String strLine;

			while ((strLine = bufferReader.readLine()) != null) {
				strResult += strLine;
				strResult += "\r\n";
			}

			bufferReader.close();
			fileReader.close();
		} catch (FileNotFoundException e) {
			DTLog.logError();
			DTLog.log(e.toString());
			return "";
		} catch (IOException e) {
			DTLog.logError();
			DTLog.log(e.toString());
			return "";
		}

		return strResult;
	}

	public static void clearAll() {
		File file = new File(getErrorLogFileFullPath());

		if (!file.delete()) {
			DTLog.logError();
		}

		file = new File(getCommonLogFileFullPath());

		if (!file.delete()) {
			DTLog.logError();
		}
	}
}
