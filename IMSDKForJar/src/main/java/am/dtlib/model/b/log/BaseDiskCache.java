package am.dtlib.model.b.log;

import java.io.File;

public abstract class BaseDiskCache implements IDiskCache {

    public static final int DEFAULT_DISK_SIZE = 512 * 1024; // 512 Kb

    protected int mDirMaxSize = DEFAULT_DISK_SIZE;

    protected final File cacheDir;

    public BaseDiskCache(File dir) {
        if(dir == null) {
            throw new IllegalArgumentException("cacheDir cannot be null");
        }

        cacheDir = dir;
    }

    @Override
    public File getDirectory() {
        return cacheDir;
    }

    @Override
    public boolean putFile(File file) {
        return false;
    }

    @Override
    public File getFile(String fileName) {
        if (!cacheDir.exists() && !cacheDir.mkdirs()) {
            return null;
        }
        return new File(cacheDir, fileName);
    }

    @Override
    public boolean removeFile(String fileName) {
        File file = this.getFile(fileName);

        if(file != null && file.exists()) {
            return file.delete();
        }

        return false;
    }

    @Override
    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files != null) {
            for (File f : files) {
                f.delete();
            }
        }
    }

    @Override
    public void close() {

    }

    /**
     * 文件夹最大容量
     * @param dirSize
     */
    public void setDirMaxSize(int dirSize) {
        mDirMaxSize = dirSize;
    }
}
