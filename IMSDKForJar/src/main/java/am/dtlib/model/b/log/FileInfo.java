package am.dtlib.model.b.log;

/**
 * Created by xieyuan on 2016/1/11.
 */
public class FileInfo {

    public String name; //文件名
    public String path; //文件路径
    public long lastModified; //最后修改日期
    public long length; //文件大小

}
