package am.dtlib.model.a.base;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;

import java.io.File;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTFileTool;
import am.dtlib.model.c.tool.SharedPreferencesUtils;

// 本类不能调用DTLog
public final class DTAppEnv {
	public static void setContext(Context context) {
		if (null == context) {
			return;
		}

		sContext = context;
		sMainHandler = new Handler(context.getMainLooper());
	}

	public static Context getContext() {
		return sContext;
	}

	public static Handler getMainHandler() {
		if (sMainHandler == null) {
			return null;
		}

		return sMainHandler;
	}

	public static void setRootDirectory(String rootDirectory) {
		sRootDirectory = rootDirectory;
	}

	public static String getRootDirectory() {
		return sRootDirectory == null ? "IMSDK.im" : sRootDirectory;
	}

	public static String getRootDirectoryFullPath() {
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);

		String rootPath = SharedPreferencesUtils.getValue("DefaultPath", "filePath");

		if(!TextUtils.isEmpty(rootPath)) {
			if(DTFileTool.checkFsWritable(rootPath)) {
				return rootPath;
			}
		}

		if (sdCardExist) {
			String externalStorageDirectory = Environment.getExternalStorageDirectory()
					.toString();

			if(DTFileTool.checkFsWritable(externalStorageDirectory)) {
				File file = new File(externalStorageDirectory, DTAppEnv.getRootDirectory());

				SharedPreferencesUtils.saveValue("DefaultPath", "filePath", file.toString());

				return file.toString();
			} else {
				File file = new File(getDocumentDirectoryFullPath(), DTAppEnv.getRootDirectory());

				SharedPreferencesUtils.saveValue("DefaultPath", "filePath", file.toString());

				return file.toString();
			}
		} else {
			File file = new File(getDocumentDirectoryFullPath(), DTAppEnv.getRootDirectory());

			SharedPreferencesUtils.saveValue("DefaultPath", "filePath", file.toString());

			return file.toString();
		}
	}

	public static String getLocalDirectoryFullPath() {
		String localDirectoryFullPath;
		if (DTAppEnv.getContext() != null) {
			localDirectoryFullPath = DTAppEnv.getContext().getFilesDir().toString();
		} else {
			localDirectoryFullPath = "";
		}

		return new File(localDirectoryFullPath, DTAppEnv.getRootDirectory())
				.toString();
	}

	public static String getPackageName() {
		if (getContext() != null) {
			return getContext().getPackageName();
		} else {
			return "am.imsdk.demo";
		}
	}

	public static String getDocumentDirectoryFullPath() {
		if (sDocumentDirectoryFullPath != null) {
			return sDocumentDirectoryFullPath;
		}

		boolean sdCardExist = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);

		if (sdCardExist) {
			sDocumentDirectoryFullPath = new File(Environment
					.getExternalStorageDirectory().toString(), getPackageName())
					.toString();

			if(!DTFileTool.checkFsWritable(sDocumentDirectoryFullPath)) {
				if (DTAppEnv.getContext() != null) {
					sDocumentDirectoryFullPath = DTAppEnv.getContext().getFilesDir().toString();
				} else {
					sDocumentDirectoryFullPath = "";
				}
			}
		} else if (DTAppEnv.getContext() != null) {
			sDocumentDirectoryFullPath = DTAppEnv.getContext().getFilesDir().toString();
		} else {
			sDocumentDirectoryFullPath = "";
		}

		return sDocumentDirectoryFullPath;
	}

	public static String getIMSDKDirectoryFullPath() {
		if (sIMSDKDirectoryFullPath != null) {
			return sIMSDKDirectoryFullPath;
		}

		sIMSDKDirectoryFullPath = new File(getRootDirectoryFullPath(), getPackageName())
				.toString();
		System.out.println("IMSDKDirectory-----------------------" + sIMSDKDirectoryFullPath);
		return sIMSDKDirectoryFullPath;
	}

	public static String getTheKeyDirectoryFullPath() {
		if (sTheKeyDirectoryFullPath != null) {
			return sTheKeyDirectoryFullPath;
		}

		sTheKeyDirectoryFullPath = new File(getLocalDirectoryFullPath(), getPackageName())
				.toString();
		System.out.println("TheKeyDirectory-----------------------" + sTheKeyDirectoryFullPath);
		return sTheKeyDirectoryFullPath;
	}

	public static boolean isSDKDebugable() {
		if (DTAppEnv.sContext == null) {
			return true;
		}

		// 发布时用false
		return isDebuggable;
	}

	public static boolean isSDKLoggable() {
		if (DTAppEnv.sContext == null) {
			return true;
		}

		// 发布时用false
		return isLoggable;
	}

	public static void cancelPreviousPerformRequest(Runnable runnable) {
		if (runnable == null) {
			return;
		}

		if (sMainHandler == null) {
			DTLog.logError();
			return;
		}

		sMainHandler.removeCallbacks(runnable);
	}

	public static void cancelAllPreviousPerformRequests() {
		if (sMainHandler == null) {
			DTLog.logError();
			return;
		}

		sMainHandler.removeCallbacksAndMessages(null);
	}

	public static void postDelayOnUIThread(double timeInterval, Runnable runnable) {
		if (null == sMainHandler) {
			DTLog.logError();
			return;
		}

		sMainHandler.postDelayed(runnable, (long) (timeInterval * 1000));
	}

	public static void postOnUIThread(Runnable runnable) {
		if (null == sMainHandler) {
			DTLog.logError();
			return;
		}

		sMainHandler.post(runnable);
	}

	public static boolean isUIThread() {
		if (DTAppEnv.getContext() == null) {
			return false;
		}

		return Looper.myLooper() == DTAppEnv.getContext().getMainLooper();
	}

	public static boolean isNetworkConnected() {
		if (sContext == null) {
			return false;
		}

		ConnectivityManager connectivityManager = (ConnectivityManager) sContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (null == connectivityManager) {
			return false;
		}

		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

		if (null == networkInfo) {
			return false;
		}

		return networkInfo.isAvailable();
	}

	public static Handler getThreadHandler() {
		if (sThreadHandler == null || sThreadHandler.getLooper() == null) {
			synchronized (sSyncThreadLock) {
				if(sThreadHandler == null || sThreadHandler.getLooper() == null) {
					sThread = new HandlerThread("GLOBAL_THREAD");
					sThread.start();

					sThreadHandler = new Handler(sThread.getLooper());
				}
			}
		}

		return sThreadHandler;
	}

	private static final Object sSyncThreadLock = new Object();
	private static HandlerThread sThread = null;  //全局子线程
	private static Handler sThreadHandler = null; //子线程Handler
	private static Handler sMainHandler = null; //主线程Handler
	private static Context sContext = null;
	private static String sRootDirectory = "";
	private static String sDocumentDirectoryFullPath = null;
	private static String sIMSDKDirectoryFullPath = null;
	private static String sTheKeyDirectoryFullPath = null;

	public static String sIMSDKIpAddress;

	public static boolean isDebuggable = true;
	public static boolean isLoggable = true;

	private static int sUserType = -1;
	public static void setUserType(int type) {
		if (type == 2 || type == 1) {
			sUserType = type;
		}
	}
	public static int getUserType() {
		return sUserType;
	}
}
