package am.dtlib.model.c.socket;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.im.IMPushCmdIMSystemMsg;
import am.imsdk.aacmd.im.IMPushCmdIMTeamMsg;
import am.imsdk.aacmd.im.IMPushCmdIMUserMsg;
import am.imsdk.aacmd.user.IMPushCmdUserLoginConflict;
import am.imsdk.action.fileserver.IMActionFile;
import am.imsdk.model.IMPrivateMyself;
import imsdk.data.IMMyself;
import imsdk.data.IMMyself.LoginStatus;
import remote.service.SocketService;
import remote.service.aidl.IMainToSocketService;
import remote.service.aidl.OnLoginRecvListener;
import remote.service.aidl.OnRemoteAddressChangedListener;
import remote.service.aidl.OnRemoteCmdRecvListener;
import remote.service.aidl.OnRemoteLoginStatusChangedListener;
import remote.service.aidl.OnRemoteSocketStatusChangedListener;

public final class DTRemoteMgr {
	public void checkConnectState(OnConnectServiceCallback listener) {
		if (mRemoteInterface == null) {
			if (!ProcessTool.isRemoteProcessExist(DTAppEnv.getContext())) {
				DTAppEnv.getContext().startService(new Intent(DTAppEnv.getContext(), SocketService.class));
			}

			mLocalServiceConnection = new LocalServiceConnection(listener);

			DTAppEnv.getContext().bindService(new Intent(DTAppEnv.getContext(), SocketService.class),
					mLocalServiceConnection, Context.BIND_AUTO_CREATE);
		} else {
			if (listener != null) {
				listener.onConnected();
			}
		}
	}

	// public void getLoginState(final OnActionListener l) {
	// OnConnectServiceListener listener = new OnConnectServiceListener() {
	//
	// @Override
	// public void onConnected() {
	// try {
	// l.onSuccess(connectListener.getLoginState());
	// } catch (RemoteException e) {
	// e.printStackTrace();
	// l.onFailure();
	// }
	// }
	// };
	//
	// checkConnectState(listener);
	// }

	public void reconnect() {
		OnConnectServiceCallback listener = new OnConnectServiceCallback() {

			@Override
			public void onConnected() {
				DTAppEnv.getThreadHandler().post(new Runnable() {
					@Override
					public void run() {
						try {
							mRemoteInterface.reconnect();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
				});

			}
		};

		checkConnectState(listener);
	}

	public void logout(final IMMyself.OnActionListener l) {
		removeAllCommand();

		OnConnectServiceCallback listener = new OnConnectServiceCallback() {

			@Override
			public void onConnected() {
				DTAppEnv.getThreadHandler().post(new Runnable() {
					@Override
					public void run() {
						try {
							mRemoteInterface.logout();

							if(l != null) {
								if(DTAppEnv.isUIThread()) {
									l.onSuccess();
								} else {
									DTAppEnv.postOnUIThread(new Runnable() {
										@Override
										public void run() {
											l.onSuccess();
										}
									});
								}
							}
						} catch (RemoteException e) {
							e.printStackTrace();
							if(l != null) {
								if(DTAppEnv.isUIThread()) {
									l.onFailure("");
								} else {
									DTAppEnv.postOnUIThread(new Runnable() {
										@Override
										public void run() {
											l.onFailure("");
										}
									});
								}
							}
						}
					}
				});

			}
		};

		checkConnectState(listener);
	}

	// public void getSocketStatus(final OnActionListener l) {
	// OnConnectServiceListener listener = new OnConnectServiceListener() {
	//
	// @Override
	// public void onConnected() {
	// try {
	// l.onSuccess(connectListener.getSocketState());
	// } catch (RemoteException e) {
	// e.printStackTrace();
	// l.onFailure();
	// }
	// }
	// };
	//
	// checkConnectState(listener);
	// }

	public void loginOrRegister(final int loginType, final String customUserID, final String pwd) {
		OnConnectServiceCallback listener = new OnConnectServiceCallback() {

			@Override
			public void onConnected() {
				DTAppEnv.getThreadHandler().post(new Runnable() {
					@Override
					public void run() {
						try {
							mRemoteInterface.loginOrRegister(loginType, customUserID, pwd, mOnLoginRecvListener);
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
				});
			}
		};

		checkConnectState(listener);
	}

	public void sendCmd(final DTCmd cmd) {
		if (cmd == null)
			return;

		DTAppEnv.getThreadHandler().post(mCheckCmdRunnable);

		OnConnectServiceCallback listener = new OnConnectServiceCallback() {

			@Override
			public void onConnected() {
				DTAppEnv.getThreadHandler().post(new Runnable() {
					@Override
					public void run() {
						try {
							if (mRemoteInterface.sendCmd(cmd)) {
								synchronized (mSynAryCmdSent) {
									cmd.mSendTime = System.currentTimeMillis() / 1000;
									mSynAryCmdSent.add(cmd);
								}

								synchronized (mSynAryCmdSent) {
									mSynAryCmdSent.notifyAll();
								}
							} else {
								if(!DTAppEnv.isNetworkConnected()) {
									DTAppEnv.postOnUIThread(new Runnable() {
										@Override
										public void run() {
											if (cmd.mOnSendFailedListener != null) {
												cmd.mOnSendFailedListener.onSendFailed();
											} else if (cmd.mOnCommonFailedListener != null) {
												try {
													cmd.mOnCommonFailedListener.onCommonFailed(FailedType.RecvError, 0l, null);
												} catch (JSONException e) {
													e.printStackTrace();
												}
											} else {
												cmd.onSendFailed();
											}
										}
									});

									return;
								}

								// 尚未登录， 保存等待登录状态改变
								synchronized (mSynAryCmdNeedSend) {
									mSynAryCmdNeedSend.add(cmd);
								}

								synchronized (mSynAryCmdNeedSend) {
									mSynAryCmdNeedSend.notifyAll();
								}

								DTLog.e("Debug", "sendCmd---LoginStatus:" + IMPrivateMyself.getInstance().getLoginStatus());

								String customUserID = IMMyself.getCustomUserID();
								String pwd = IMPrivateMyself.getInstance().getPassword();

								if (!TextUtils.isEmpty(pwd) && !TextUtils.isEmpty(customUserID)) {
									DTAppEnv.postOnUIThread(new Runnable() {
										@Override
										public void run() {
											IMMyself.login(true, 0 ,null); //自动登录
										}
									});

								}
							}
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
				});

			}
		};

		checkConnectState(listener);
	}

	OnRemoteCmdRecvListener.Stub mOnRemoteCmdRecvListener = new OnRemoteCmdRecvListener.Stub() {
		@Override
		public void onRecv(int cmdTypeValue, int sequenceID, long errorCode, String json) throws RemoteException {
			DTLog.sign(DTAppEnv.isSDKLoggable(), cmdTypeValue + " DTRemoteMsg recv Code: " + errorCode + ", sequenceID:" + sequenceID
					+ "\n recv Json:" + json);

			if (sequenceID > 0) {
				// 应答包
				DTCmd cmdSent = null;

				synchronized (mSynAryCmdSent) {
					for (DTCmd temp : mSynAryCmdSent) {
						if (temp.getCmdTypeValue() == cmdTypeValue && temp.getSequenceID() == sequenceID) {
							cmdSent = temp;
							mSynAryCmdSent.remove(temp);
							break;
						}
					}
				}

				if (cmdSent != null) {
					DTRecvPerform perform = new DTRecvPerform();

					perform.mJson = json;
					perform.mErrorCode = errorCode;
					perform.mCmdObj = cmdSent;
					performRecv(perform);
				} else {
					DTLog.e("Debug", "命令未找到, cmdType: " + cmdTypeValue + ",sequenceID: " + sequenceID);
				}
			} else {
				// 推送包
				analysePushData(cmdTypeValue, json, false);
			}
		}

	};

	private OnRemoteSocketStatusChangedListener.Stub mOnRemoteSocketChangeListener = new OnRemoteSocketStatusChangedListener.Stub() {
		@Override
		public void onSocketStatusChanged(int status) throws RemoteException {
			DTLog.e("Debug", "接到AIDL socket改变：" + DTSocketStatus.getEnum(status));
			DTNotificationCenter.getInstance().postNotification("SOCKET_UPDATED", status);
		}
	};

	private OnRemoteLoginStatusChangedListener.Stub mOnLoginChangeListener = new OnRemoteLoginStatusChangedListener.Stub() {
		@Override
		public void onLoginStatusChanged(int status) throws RemoteException {
			DTLog.e("Debug", "接到AIDL LoginStatus改变：" + LoginStatus.getIndex(status));

			IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.getIndex(status));
			if (status == LoginStatus.Logined.getValue()) {
				DTLog.e("Debug", "开始发送未发送的数据");

				sendStoredCmd();
			} else {
			}
		}
	};

	private OnLoginRecvListener.Stub mOnLoginRecvListener = new OnLoginRecvListener.Stub() {
		@Override
		public void onRecvSuccess(String jsonObj) throws RemoteException {
			DTNotificationCenter.getInstance().postNotification("LoginReceiveSuccess", jsonObj);
		}

		@Override
		public void onRecvFailure(String err) throws RemoteException {
			DTNotificationCenter.getInstance().postNotification("LoginReceiveFailure", err);
		}
	};

	private OnRemoteAddressChangedListener.Stub mOnRemoteAddressChangedListener = new OnRemoteAddressChangedListener.Stub() {
		@Override
		public void addressChanged(int type, String address) throws RemoteException {
			DTLog.e("Debug", "---------IMPath:" + address);
			DTAppEnv.sIMSDKIpAddress = address;
			IMActionFile.IM_FILE_URL_DOMAIN_NAME = address;
			IMActionFile.IM_FILE_URL_REAL_IP = address;
		}
	};

	private void analysePushData(int cmdTypeValue, String json, final boolean isOffLineMsg) {

		final DTPushCmd cmdPush = getPushCmdObject(cmdTypeValue);

		if (cmdPush == null) {
			if (cmdTypeValue != IMCmdType.IM_CMD_USER_HELLO.getValue()) {
				DTLog.logError();
			}
			return;
		}

		JSONObject jsonObject = null;

		try {
			if (json.length() > 0) {
				jsonObject = new JSONObject(json);
			} else {
				jsonObject = new JSONObject();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError(json);
			return;
		}

		final JSONObject finalJsonObject = jsonObject;
		final String finalJson = json;

		DTAppEnv.postOnUIThread(new Runnable() {
			@Override
			public void run() {
				DTLog.sign(DTAppEnv.isSDKLoggable(), "OnRecv Push:" + cmdPush.getClass().getSimpleName());
				DTLog.sign(DTAppEnv.isSDKLoggable(), "OnRecv Push:" + finalJson);

				try {
					cmdPush.onRecv(finalJsonObject, isOffLineMsg);
				} catch (JSONException e) {
					e.printStackTrace();
					DTLog.logError();
					return;
				}
			}
		});

	}

	public void setOffline() {
		OnConnectServiceCallback listener = new OnConnectServiceCallback() {

			@Override
			public void onConnected() {
				try {
					mRemoteInterface.setOffline();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		};
	}

	public void getLoginInfo(final OnActionListener l) {
		if(l == null) {
			return;
		}

		OnConnectServiceCallback listener = new OnConnectServiceCallback() {
			@Override
			public void onConnected() {
				DTAppEnv.getThreadHandler().post(new Runnable() {
					@Override
					public void run() {
						try {
							final String result = mRemoteInterface.getLoginInfo();

							DTAppEnv.postOnUIThread(new Runnable() {
								@Override
								public void run() {
									l.onSuccess(result);
								}
							});

						} catch (RemoteException e) {
							e.printStackTrace();

							DTAppEnv.postOnUIThread(new Runnable() {
								@Override
								public void run() {
									l.onFailure();
								}
							});
						}
					}
				});

			}
		};

		checkConnectState(listener);
	}

	private final ArrayList<DTCmd> mSynAryCmdNeedSend = new ArrayList<DTCmd>();
	private final ArrayList<DTCmd> mSynAryCmdSent = new ArrayList<DTCmd>();

	private Runnable mCheckCmdRunnable = new Runnable() {
		@Override
		public void run() {
			checkCmdSent();
		}
	};

	private void sendStoredCmd() {
		OnConnectServiceCallback listener = new OnConnectServiceCallback() {
			@Override
			public void onConnected() {
				DTAppEnv.getThreadHandler().post(new Runnable() {
					@Override
					public void run() {
						try {
							synchronized (mSynAryCmdNeedSend) {
								for (int i = mSynAryCmdNeedSend.size() - 1; i >= 0; i--) {
									final DTCmd cmd = mSynAryCmdNeedSend.get(i);

									if (mRemoteInterface.sendCmd(cmd)) {
										synchronized (mSynAryCmdSent) {
											cmd.mSendTime = System.currentTimeMillis() / 1000;
											mSynAryCmdSent.add(cmd);
										}

										mSynAryCmdNeedSend.remove(cmd);

										synchronized (mSynAryCmdSent) {
											mSynAryCmdSent.notifyAll();
										}
									} else {
										DTLog.e("Debug", "service判断登录状态异常，不允许发送数据");
										break;
									}

								}
							}

							checkCmdSent();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
				});

			}
		};

		checkConnectState(listener);
	}

	private void checkCmdSent() {
		DTAppEnv.getThreadHandler().removeCallbacks(mCheckCmdRunnable);
		DTAppEnv.getThreadHandler().postDelayed(mCheckCmdRunnable, 5 * 1000);

		if (DTAppEnv.getContext() == null) {
			DTLog.logError();
			return;
		}

		synchronized (mSynAryCmdSent) {
			for (int i = mSynAryCmdSent.size() - 1; i >= 0; i--) {
				final DTCmd cmd = mSynAryCmdSent.get(i);
				long timeInterval = System.currentTimeMillis() / 1000 - cmd.getSendTime();

				if (timeInterval > cmd.getTimeoutInterval()) {
					DTAppEnv.postOnUIThread(new Runnable() {
						@Override
						public void run() {
							DTLog.sign(DTAppEnv.isSDKLoggable(), "DTSocket NoRecv1:" + cmd.getClass().getSimpleName());

							if (cmd.getSenderUID() != IMPrivateMyself.getInstance().getUID()) {
								DTLog.logError();
								return;
							}

							if (cmd.mOnNoRecvListener != null) {
								cmd.mOnNoRecvListener.onNoRecv();
							} else if (cmd.mOnCommonFailedListener != null) {
								try {
									cmd.mOnCommonFailedListener.onCommonFailed(FailedType.NoRecv, 0, new JSONObject());
								} catch (JSONException e) {
									e.printStackTrace();
									DTLog.logError();
									return;
								}
							} else {
								cmd.onNoRecv();
							}
						}
					});

					mSynAryCmdSent.remove(i);
				}
			}
		}
	}

	/**
	 * 手动登出时所有指令都成为无效指令，全部清空
	 */
	private void removeAllCommand() {
		synchronized (mSynAryCmdNeedSend) {
			for (final DTCmd cmd : mSynAryCmdNeedSend) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						if (cmd.mOnSendFailedListener != null) {
							cmd.mOnSendFailedListener.onSendFailed();
						} else if (cmd.mOnCommonFailedListener != null) {
							try {
								cmd.mOnCommonFailedListener.onCommonFailed(FailedType.SendFailed, 0, new JSONObject());
							} catch (JSONException e) {
								e.printStackTrace();
								DTLog.logError();
								return;
							}
						} else {
							cmd.onSendFailed();
						}
					}
				});
			}

			mSynAryCmdNeedSend.clear();
		}

		synchronized (mSynAryCmdSent) {
			for (final DTCmd cmd : mSynAryCmdSent) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						if (cmd.mOnSendFailedListener != null) {
							cmd.mOnSendFailedListener.onSendFailed();
						} else if (cmd.mOnCommonFailedListener != null) {
							try {
								cmd.mOnCommonFailedListener.onCommonFailed(FailedType.SendFailed, 0, new JSONObject());
							} catch (JSONException e) {
								e.printStackTrace();
								DTLog.logError();
								return;
							}
						} else {
							cmd.onSendFailed();
						}
					}
				});
			}

			mSynAryCmdSent.clear();
		}
	}

	private final class DTRecvPerform {
		public long mErrorCode;
		public DTCmd mCmdObj;
		public String mJson;
	}

	private void performRecv(final DTRecvPerform perform) {
		if (perform == null) {
			DTLog.logError();
			return;
		}

		DTAppEnv.postOnUIThread(new Runnable() {
			@Override
			public void run() {
				if (perform.mCmdObj == null) {
					DTLog.logError();
					return;
				}

				DTLog.sign(DTAppEnv.isSDKLoggable(), "OnRecv: " + perform.mCmdObj.getClass().getSimpleName());

				if (perform.mCmdObj.getSenderUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.logError();
					return;
				}

				JSONObject jsonObject = null;

				if (perform.mJson != null && perform.mJson.length() > 0) {
					try {
						jsonObject = new JSONObject(perform.mJson);
					} catch (JSONException e) {
						e.printStackTrace();
						DTLog.logError();
						return;
					}
				}

				try {
					if (perform.mErrorCode > 0) {
						DTLog.sign(DTAppEnv.isSDKLoggable(), "DTSocket RecvError:" + perform.mErrorCode + " "
								+ perform.mCmdObj.getClass().getSimpleName());

						if (perform.mCmdObj.mOnRecvErrorListener != null) {
							perform.mCmdObj.mOnRecvErrorListener.onRecvError(perform.mErrorCode, jsonObject);
						} else if (perform.mCmdObj.mOnCommonFailedListener != null) {
							perform.mCmdObj.mOnCommonFailedListener.onCommonFailed(FailedType.RecvError,
									perform.mErrorCode, jsonObject);
						} else {
							perform.mCmdObj.onRecvError(perform.mErrorCode, jsonObject);
						}
					} else {
						if (perform.mCmdObj.mOnRecvListener != null) {
							perform.mCmdObj.mOnRecvListener.onRecv(jsonObject);
						} else {
							perform.mCmdObj.onRecv(jsonObject);
						}

						if (perform.mCmdObj.mOnRecvEndListener != null) {
							perform.mCmdObj.mOnRecvEndListener.onRecvEnd(jsonObject);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
					DTLog.logError();
					return;
				}
			}
		});
	}

	private DTPushCmd getPushCmdObject(int cmdTypeValue) {
		if (cmdTypeValue == 0) {
			DTLog.logError();
			return null;
		}

		IMCmdType cmdType = IMCmdType.fromInt(cmdTypeValue);

		if (cmdType == null) {
			DTLog.logError();
			return null;
		}

		switch (cmdType) {
			case IM_PUSH_CMD_IM_USER_MSG:
				return new IMPushCmdIMUserMsg();
			case IM_PUSH_CMD_IM_SYSTEM_MSG:
				return new IMPushCmdIMSystemMsg();
			case IM_PUSH_CMD_IM_TEAM_MSG:
				return new IMPushCmdIMTeamMsg();
			case IM_PUSH_CMD_USER_LOGIN_CONFLICT:
				return new IMPushCmdUserLoginConflict();
			case IM_CMD_USER_HELLO:
				return null;
			default:
				break;
		}

		DTLog.logError();
		DTLog.log(cmdType.toString());

		return null;
	}

	private IMainToSocketService mRemoteInterface;
	private LocalServiceConnection mLocalServiceConnection;

	class LocalServiceConnection implements ServiceConnection {
		private OnConnectServiceCallback connectionCallback;

		public LocalServiceConnection(OnConnectServiceCallback listener) {
			this.connectionCallback = listener;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d("Debug", "Service 绑定成功");
			mRemoteInterface = IMainToSocketService.Stub.asInterface(service);

			if (connectionCallback != null) {
				connectionCallback.onConnected();
				connectionCallback = null;
			}

			try {
				mRemoteInterface.setOnRemoteSocketStatusChangedListener(mOnRemoteSocketChangeListener);
				mRemoteInterface.setOnRemoteCmdRecvListener(mOnRemoteCmdRecvListener);
				mRemoteInterface.setOnRemoteLoginStatusChangedListener(mOnLoginChangeListener);
				mRemoteInterface.setOnRemoteAddressChangedListener(mOnRemoteAddressChangedListener);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			DTLog.e("Debug", "Service 绑定断开");
			mRemoteInterface = null;

//			removeAllCommand();
			try {
				DTAppEnv.getContext().unbindService(mLocalServiceConnection);
			} catch(Exception ex) {

			}
		}
	}

	private interface OnConnectServiceCallback {
		void onConnected();
	}

	public interface OnActionListener {
		void onSuccess(Object result);

		void onFailure();
	}

	private void printBuffer(byte[] buff, int offset, int count) {
		System.out.println("=========begin Client===========");

		for (int i = offset; i < count; i++) {
			String hex = Integer.toHexString(buff[i] & 0xFF);

			if (hex.length() == 1) {
				hex = '0' + hex;
			}

			System.out.print(hex.toUpperCase() + " ");
		}

		System.out.println("");
		System.out.println("=========End Client===========");
	}

	// singleton
	private volatile static DTRemoteMgr sSingleton;

	private DTRemoteMgr() {
	}

	public static void newInstance() {
		synchronized (DTRemoteMgr.class) {
			if (sSingleton == null) {
				sSingleton = new DTRemoteMgr();
			}
		}
	}

	public static DTRemoteMgr getInstance() {
		return sSingleton;
	}

	// singleton end
}
