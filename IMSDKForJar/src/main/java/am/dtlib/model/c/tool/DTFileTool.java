package am.dtlib.model.c.tool;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.demo.util.FileUtils;

public final class DTFileTool {
	public static void checkRemoveDirectory(String directoryFullPath) {
		if (directoryFullPath.length() == 0) {
			DTLog.logError();
			return;
		}

		File file = new File(directoryFullPath);

		checkRemoveFile(file);
	}
	
	private static void checkRemoveFile(File file) {
		if (file.isDirectory()) {
			for (File child : file.listFiles()) {
				checkRemoveFile(child);
		    }
		}
		
		if (file.exists() && !file.delete()) {
			DTLog.logError();
			return;
		}
	}

	public static void checkRemoveDirectoryOfFilePath(String fileFullPath) {
		DTLog.sign("fileFullPath:" + fileFullPath);
		
		if (fileFullPath.length() == 0) {
			DTLog.logError();
			return;
		}

		File file = new File(fileFullPath);
		String directoryPath = file.getParent();

		file = new File(directoryPath);

		checkRemoveFile(file);
	}
	
	public static boolean writeToFile(byte[] buffer, String localFullPath) {
		FileOutputStream fileOutputStream = null;

		try {
			fileOutputStream = new FileOutputStream(localFullPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			return false;
		}

		try {
			fileOutputStream.write(buffer);
			fileOutputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			return false;
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				return false;
			}
		}
		
		return true;
	}
	
	public static byte[] getContentOfFile(String localFullPath) {
		File file = new File(localFullPath);

		if (!file.exists()) {
			return null;
		}

		FileInputStream fileInputStream;

		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			return null;
		}

		byte[] buffer;

		try {
			buffer = new byte[fileInputStream.available()];
			fileInputStream.read(buffer);
			fileInputStream.close();
		} catch (IOException e) {
			return null;
		}
		
		return buffer;
	}

	public static byte[] getContentOfFile(String localFullPath, int offset) {
		if (offset < 0) {
			DTLog.logError();
			return null;
		}
		
		File file = new File(localFullPath);

		if (!file.exists()) {
			return null;
		}

		FileInputStream fileInputStream;

		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			return null;
		}

		byte[] buffer;

		try {
			if (fileInputStream.available() <= offset) {
				fileInputStream.close();
				return null;
			}
			
			buffer = new byte[fileInputStream.available() - offset];
			fileInputStream.read(buffer);
			fileInputStream.close();
		} catch (IOException e) {
			return null;
		}
		
		return buffer;
	}
	
	public static boolean fileExistsAtPath(String localFullPath) {
		File file = new File(localFullPath);
		
		return file.exists() && !file.isDirectory();
	}
	
	public static boolean copyImGifToSdcard(Context context, String path) {
	    AssetManager assetManager = context.getAssets();
	    String assets[] = null;
	    try {
	        assets = assetManager.list(path);
	        if (assets.length == 0) {
	        	copyAssertFileToSdcard(context, path);
	        } else {
	            String fullPath = FileUtils.getStorePath() + path;
	            File dir = new File(fullPath);
	            if (!dir.exists())
	                dir.mkdir();
	            for (int i = 0; i < assets.length; ++i) {
	            	copyImGifToSdcard(context, path + "/" + assets[i]);
	            }
	        }
	        
	        return true;
	    } catch (IOException ex) {
//	        Log.e("tag", "I/O Exception", ex);
	    	return false;
	    }
	    
	}
	 
	public static void copyAssertFileToSdcard(Context context, String filename) {
	    AssetManager assetManager = context.getAssets();
	 
	    InputStream in = null;
	    OutputStream out = null;
	    try {
	        in = assetManager.open(filename);
	        String newFileName = FileUtils.getStorePath() + filename;
	        out = new FileOutputStream(newFileName);
	 
	        byte[] buffer = new byte[1024];
	        int read;
	        while ((read = in.read(buffer)) != -1) {
	            out.write(buffer, 0, read);
	        }
	        in.close();
	        in = null;
	        out.flush();
	        out.close();
	        out = null;
	    } catch (Exception e) {
//	        Log.e("tag", e.getMessage());
	    }
	 
	}

	public static String getFromAssets(Context context, String fileName){
		try {
			InputStreamReader inputReader = new InputStreamReader( context.getResources().getAssets().open(fileName) );
			BufferedReader bufReader = new BufferedReader(inputReader);
			String line="";
			String Result="";
			while((line = bufReader.readLine()) != null)
				Result += line;
			return Result;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// 测试外置sd卡是否卸载，不能直接判断外置sd卡是否为null，因为当外置sd卡拔出时，仍然能得到外置sd卡路径。我这种方法是按照android谷歌测试DICM的方法，
	// 创建一个文件，然后立即删除，看是否卸载外置sd卡
	// 注意这里有一个小bug，即使外置sd卡没有卸载，但是存储空间不够大，或者文件数已至最大数，此时，也不能创建新文件。此时，统一提示用户清理sd卡吧
	public static boolean checkFsWritable(String dir) {

		if (dir == null)
			return false;

		File directory = new File(dir);

		if (!directory.isDirectory()) {
			if (!directory.mkdirs()) {
				return false;
			}
		}

		File f = new File(directory, ".keysharetestgzc");
		try {
			if (f.exists()) {
				f.delete();
			}
			if (!f.createNewFile()) {
				return false;
			}
			f.delete();
			return true;

		} catch (Exception e) {
		}
		return false;

	}
}
