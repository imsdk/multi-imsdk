package am.dtlib.model.c.tool;

public class ReadWriteLock{
    // 读状态
    private boolean isRead;
    
    // 写状态
    private boolean isWrite;

    // 不能在主线程调用
    public synchronized void readLock(){
        // 有写入时读取线程停止
        while(isWrite){
            try{
                //System.out.println("有线程在进行写入,读取线程停止,进入等待状态");
                wait();
            }
            catch(InterruptedException ex){
                ex.printStackTrace();
            }
        }
        
        //System.out.println("设定锁为读取状态");
        isRead=true;
    }

    // 不能在主线程调用
    public synchronized void readUnlock(){
        //System.out.println("解除读取锁");
        isRead=false;
        notifyAll();
    }

    // 不能在主线程调用
     public synchronized void writeLock(){
        // 有读取时读取线程停止
        while(isRead){
            try{    
                //System.out.println("有线程在进行读取,写入线程停止,进入等待状态");
                wait();
            }
            catch(InterruptedException ex){
                ex.printStackTrace();
            }
        }
        
        // 有写入时写入线程也一样要停止
        while(isWrite){
            try{    
                //System.out.println("有线程在进行写入,写入线程停止,进入等待状态");
                wait();
            }
            catch(InterruptedException ex){
                ex.printStackTrace();
            }
        }
        
        //System.out.println("设定锁为写入状态");
        isWrite=true;
    }

    // 不能在主线程调用
    public synchronized void writeUnlock(){
        //System.out.println("解除写入锁");
        isWrite=false;
        notifyAll();
    }
}