package am.dtlib.model.c.tool;

import am.dtlib.model.a.base.DTAppEnv;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {

	public static void saveValue(String filename, String key, String value) {
		if(DTAppEnv.getContext() == null) return;
		
		// 获得SharedPreferences对象
		SharedPreferences settings = DTAppEnv.getContext().getSharedPreferences(filename, Context.MODE_PRIVATE);

		// 获得SharedPreferences.Editor
		SharedPreferences.Editor editor = settings.edit();

		// 保存组件的值 为优先级设置填写不同类型的内容
		editor.putString(key, value);

		// 提交保存的结果 将改变写到系统中
		editor.commit();
	}
	
	public static String getValue(String filename, String key) {
		if(DTAppEnv.getContext() == null) return "";
		
		SharedPreferences settings = DTAppEnv.getContext().getSharedPreferences(filename, Context.MODE_PRIVATE);
		
		return settings.getString(key, "");
	} 
	
}
