package am.dtlib.model.c.socket;

import android.content.Context;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import am.core.jni.IMProtocol;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.d.DTDevice;
import am.imsdk.action.fileserver.IMActionFile;
import remote.service.MainToSocketProcessStub;
import remote.service.data.IMRemoteMyself;

public abstract class DTSocket {
	protected volatile static DTSocket sSingleton;
//	private static boolean DTAppEnv.isSDKLoggable() = true;

	public static DTSocket getInstance() {
		if (sSingleton == null) {
			DTLog.logError();
		}

		return sSingleton;
	}

	public enum DTSocketStatus {
		None(0), Connecting(1), Connected(2);

		private final int value;

		DTSocketStatus(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static DTSocketStatus getEnum(int index) {
			switch (index) {
			case 0:
			default:
				return None;
			case 1:
				return Connecting;
			case 2:
				return Connected;
			}
		}
	}

	//多线程调用
	public DTSocketStatus getStatus() {
		synchronized (mSyncStatus) {
			return mStatus;
		}
	}

	//多线程调用
	private void setStatus(DTSocketStatus newStatus) {
		if (getStatus() == newStatus) {
			return;
		}

		synchronized (mSyncStatus) {
			mStatus = newStatus;
		}

		if (newStatus == DTSocketStatus.Connected) {
			synchronized (mSyncConnectCount) {
				Log.e("Debug", "当前尝试次数：" + mReconnectCount);
				mReconnectCount = 0;
			}

			DTDevice.getInstance().mLastIMAddress = mCurrentIMAddress;
			DTDevice.getInstance().saveFile();

			DTAppEnv.getThreadHandler().removeCallbacks(mReconnectRunnable);
			DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
		}

		if (DTAppEnv.isUIThread()) {
			synchronized (mSyncStatus) {
				if (mStatusNotified == mStatus) {
					return;
				}

				mStatusNotified = mStatus;
				DTNotificationCenter.getInstance().postSyncNotification("SOCKET_UPDATED", mStatus);

				if (MainToSocketProcessStub.getInstance().getSocketStatusChangedListener() != null) {
					try {
						MainToSocketProcessStub.getInstance().getSocketStatusChangedListener()
								.onSocketStatusChanged(mStatus.getValue());
					} catch (RemoteException e) {
						e.printStackTrace();
						MainToSocketProcessStub.getInstance().onDestroy();
					}
				}

				DTLog.sign("newStatus:" + mStatus.toString());
			}
		} else {
			DTAppEnv.postOnUIThread(new Runnable() {
				@Override
				public void run() {
					synchronized (mSyncStatus) {
						if (mStatusNotified == mStatus) {
							return;
						}

						mStatusNotified = mStatus;
						DTNotificationCenter.getInstance().postSyncNotification("SOCKET_UPDATED", mStatus);

						if (MainToSocketProcessStub.getInstance().getSocketStatusChangedListener() != null) {
							try {
								MainToSocketProcessStub.getInstance().getSocketStatusChangedListener()
										.onSocketStatusChanged(mStatus.getValue());
							} catch (RemoteException e) {
								e.printStackTrace();
								MainToSocketProcessStub.getInstance().onDestroy();
							}
						}

						DTLog.sign("newStatus:" + mStatus.toString());
					}
				}
			});
		}
	}

	public synchronized boolean isRunningSIS() {
		return mIsRunningSIS;
	}

	public void setSISDefaultAddress(String sisDefaultAddress) {
		mSISDefaultAddress = sisDefaultAddress;
	}

	public String getSISDefaultAddress() {
		return mSISDefaultAddress;
	}

	public void setSISDomainName(String sisDomainName) {
		mSISDomainName = sisDomainName;
	}

	public String getSISDomainName() {
		return mSISDomainName;
	}

	public void setDomainName(String domainName) {
		mDomainName = domainName;
	}

	public String getDomainName() {
		return mDomainName;
	}

	public void setDefaultIMAddress(String defaultIMAddress) {
		mDefaultIMAddress = defaultIMAddress;
	}

	public String getDefaultIMAddress() {
		return mDefaultIMAddress;
	}

	public void setForceIMAddress(String forceIMAddress) {
		mForceIMAddress = forceIMAddress;
	}

	public String getForceIMAddress() {
		return mForceIMAddress;
	}

	public String getCurrentIMAddress() {
		return mCurrentIMAddress;
	}

	public void runSIS() {
		if (mSISDomainName.length() == 0) {
			DTLog.logError();
			return;
		}

		if (mSISDefaultAddress.length() == 0) {
			DTLog.logError();
			return;
		}

		Thread parseDomainThread = new Thread(new Runnable() {
			@Override
			public void run() {
				InetAddress netAddress = null;

				try {
					netAddress = InetAddress.getByName(mSISDomainName);
				} catch (UnknownHostException e) {
					if (DTAppEnv.isNetworkConnected()) {
						// 有可能域名解析失败
						DTLog.logError();
						DTLog.log(e.toString());
					}
				}

				final String sisAddress = netAddress != null ? netAddress.getHostAddress() : "";

				if (sisAddress.length() > 0) {
					DTDevice.getInstance().mLastSISAddress = sisAddress + ":18000";
					DTDevice.getInstance().saveFile();
					DTLog.log("sis domain name " + mSISDomainName + " to ip:"
							+ DTDevice.getInstance().mLastSISAddress);
				} else {
					DTLog.log("Failed transfer sis domain name " + mSISDomainName + " to IP Address");
				}

				if (mDomainName.length() == 0) {
					DTLog.logError();
					return;
				}

				netAddress = null;

				try {
					netAddress = InetAddress.getByName(mDomainName);
				} catch (UnknownHostException e) {
					if (DTAppEnv.isNetworkConnected()) {
						// 有可能域名解析失败
						DTLog.logError();
						DTLog.log(e.toString());
					}
				}

				final String imAddress = netAddress != null ? netAddress.getHostAddress() : "";

				DTLog.sign("imAddress: " + imAddress);

				if (imAddress.length() > 0) {
					DTDevice.getInstance().mIMAddressFromDomain = sisAddress + ":9100";
					DTDevice.getInstance().saveFile();
					DTLog.log("domain name " + mDomainName + " to ip:"
							+ DTDevice.getInstance().mIMAddressFromDomain);
				} else {
					DTLog.log("Failed transfer sis domain name " + mDomainName + " to IP Address");
				}
			}
		});

		parseDomainThread.start();

		String currentSISAddress;

		if (DTDevice.getInstance().mLastSISAddress.length() > 0) {
			currentSISAddress = DTDevice.getInstance().mLastSISAddress;
		} else {
			if (mSISDefaultAddress.length() == 0) {
				DTLog.logError();
				return;
			}

			currentSISAddress = mSISDefaultAddress;
		}

		if (currentSISAddress.length() == 0) {
			DTLog.logError();
			return;
		}

		String[] address = currentSISAddress.split(":");

		if (address.length != 2) {
			DTLog.logError();
			return;
		}

		final String ipAddress = address[0];
		final int port = Integer.parseInt(address[1]);

		if (ipAddress.length() == 0) {
			DTLog.logError();
			return;
		}

		if (port == 0) {
			DTLog.logError();
			return;
		}

		Thread udpThread = new Thread(new Runnable() {
			@Override
			public void run() {
				DTLog.sign("udpThread");

				short length = 128;
				DatagramSocket udpSocket;

				try {
					udpSocket = new DatagramSocket();
				} catch (SocketException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					return;
				}

				byte[] buffer = new byte[128];

				buffer[0] = (byte) (length >> 8);
				buffer[1] = (byte) length;

				String string = "UE";
				byte[] jsonBuf;

				try {
					jsonBuf = string.getBytes("UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					return;
				}

				buffer[2] = jsonBuf[0];
				buffer[3] = jsonBuf[1];

				string = "NH-am.test";

				try {
					jsonBuf = string.getBytes("UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					return;
				}

				for (int i = 0; i < 30 && i < jsonBuf.length; i++) {
					buffer[4 + i] = jsonBuf[i];
				}

				TelephonyManager telephonyManager = (TelephonyManager) DTAppEnv.getContext().getSystemService(
						Context.TELEPHONY_SERVICE);

				if (telephonyManager == null) {
					DTLog.logError();
					return;
				}

				String networkOperator = telephonyManager.getNetworkOperator();

				if (networkOperator == null) {
					DTLog.logError();
					return;
				}

				if (networkOperator.length() != 0) {
					if (networkOperator.length() < 4) {
						DTLog.logError();
						return;
					}

					int tel_opera;

					try {
						int mcc = Integer.parseInt(networkOperator.substring(0, 3));
						int mnc = Integer.parseInt(networkOperator.substring(3));

						tel_opera = mcc * 100 + mnc;
					} catch (Exception e) {
						e.printStackTrace();
						tel_opera = 0;
					}

					buffer[34] = (byte) (tel_opera >> 24);
					buffer[35] = (byte) (tel_opera >> 16);
					buffer[36] = (byte) (tel_opera >> 8);
					buffer[37] = (byte) tel_opera;
				}

				string = "qswddddddsddddsdwwssdsd";

				try {
					jsonBuf = string.getBytes("UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					return;
				}

				for (int i = 0; i < 50 && i < jsonBuf.length; i++) {
					buffer[42 + i] = jsonBuf[i];
				}

				string = "0.1.0";

				try {
					jsonBuf = string.getBytes("UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					return;
				}

				for (int i = 0; i < 10 && i < jsonBuf.length; i++) {
					buffer[92 + i] = jsonBuf[i];
				}

				InetAddress inetAddress;

				try {
					inetAddress = InetAddress.getByName(ipAddress);
				} catch (UnknownHostException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					return;
				}

				DatagramPacket sendPacket = new DatagramPacket(buffer, buffer.length, inetAddress, port);

				try {
					udpSocket.send(sendPacket);
				} catch (IOException e) {
					if (DTAppEnv.isNetworkConnected()) {
						e.printStackTrace();
						DTLog.logError();
						DTLog.log(e.toString());
					}

					return;
				}

				DTLog.sign("udpSocket sent");

				byte[] recvBuffer = new byte[1024];
				byte[] resultBuffer = new byte[1024];
				int resultBufferLength = 0;
				int retryTimes = 0;
				int parseStringRetryTimes = 0;
				int parseJsonRetryTimes = 0;
				JSONObject jsonResult = null;

				while (true) {
					DatagramPacket recvPacket = new DatagramPacket(recvBuffer, recvBuffer.length);

					try {
						udpSocket.receive(recvPacket);
					} catch (IOException e) {
						e.printStackTrace();
						DTLog.logError();
						DTLog.log(e.toString());
						return;
					}

					DTLog.sign("udpSocket recv Length:" + recvPacket.getLength());

					if (recvPacket.getLength() == 0) {
						retryTimes++;

						if (retryTimes > 3) {
							break;
						}

						continue;
					}

					retryTimes = 0;

					for (int i = 0; i < recvPacket.getLength(); i++) {
						resultBuffer[resultBufferLength + i] = recvBuffer[i];
					}

					resultBufferLength += recvPacket.getLength();

					String recvStr = new String(resultBuffer, 0, resultBufferLength);

					DTLog.sign("udpSocket recv String:" + recvStr);

					if (recvStr.length() == 0) {
						parseStringRetryTimes++;

						if (parseStringRetryTimes > 3) {
							break;
						}

						continue;
					}

					parseStringRetryTimes = 0;

					try {
						jsonResult = new JSONObject(recvStr);
					} catch (JSONException e) {
					}

					if (jsonResult == null) {
						parseJsonRetryTimes++;

						if (parseJsonRetryTimes > 3) {
							break;
						}

						continue;
					}

					parseJsonRetryTimes = 0;

					DTLog.sign("UDP Result:" + recvStr);
					udpSocket.close();
					break;
				}

				if (jsonResult == null) {
					DTLog.log("UDP Failed");
				} else {
					DTLog.log("UDP Succeeded");

					JSONArray array;

					try {
						array = jsonResult.getJSONArray("ips");
					} catch (JSONException e) {
						e.printStackTrace();
						DTLog.logError();
						DTLog.log(e.toString());
						return;
					}

					if (array == null) {
						DTLog.logError();
						return;
					}

					final JSONArray finalArray = array;

					DTAppEnv.postOnUIThread(new Runnable() {
						@Override
						public void run() {
							DTDevice.getInstance().mIMAddressListFromSIS.clear();

							for (int i = 0; i < finalArray.length(); i++) {
								try {
									DTDevice.getInstance().mIMAddressListFromSIS.add(finalArray.getString(i));
								} catch (JSONException e) {
									e.printStackTrace();
									DTLog.logError();
									DTLog.log(e.toString());
									return;
								}
							}

							DTDevice.getInstance().saveFile();
						}
					});
				}
			}
		});

		udpThread.start();
	}

	public void setSessionID(long sessionID) {
		mSessionID = sessionID;
	}

	public long getSessionID() {
		return mSessionID;
	}

	public void setUID(long uid) {
		mUID = uid;
	}

	public long getUID() {
		return mUID;
	}

	public void sendCmd(final DTCmd cmd) {
		if (mStatus != DTSocketStatus.Connected) {
			DTLog.logError();
			return;
		}

		DTAppEnv.getThreadHandler().post(mCheckCmdRunnable);

		if (cmd != null) {
			synchronized (mSynAryCmdNeedSend) {
				mSynAryCmdNeedSend.add(cmd);
			}
		}

		DTLog.sign("notifyAll：cmdType=" + cmd.getCmdTypeValue());

		synchronized (mSynAryCmdNeedSend) {
			mSynAryCmdNeedSend.notifyAll();
		}
	}

	private Runnable mCheckCmdRunnable = new Runnable() {

		@Override
		public void run() {
			checkCmdsSent();
		}
	};

	//子线程调用
	private void checkCmdsSent() {
		DTAppEnv.getThreadHandler().removeCallbacks(mCheckCmdRunnable);
		DTAppEnv.getThreadHandler().postDelayed(mCheckCmdRunnable, 5 * 1000);

		synchronized (mSynAryCmdSent) {
			for (int i = mSynAryCmdSent.size() - 1; i >= 0; i--) {
				final DTCmd cmd = mSynAryCmdSent.get(i);
				long timeInterval = System.currentTimeMillis() / 1000 - cmd.getSendTime();

				if (timeInterval > cmd.getTimeoutInterval()) {
					DTAppEnv.postOnUIThread(new Runnable() {
						@Override
						public void run() {
							DTLog.sign("DTSocket NoRecv1:" + cmd.mCmdTypeValue + ", sequenceID:" + cmd.getSequenceID());

							// if (cmd.getSenderUID() != mUID) {
							// DTLog.logError();
							// return;
							// }

							if (cmd.mOnNoRecvListener != null) {
								cmd.mOnNoRecvListener.onNoRecv();
							} else if (cmd.mOnCommonFailedListener != null) {
								try {
									cmd.mOnCommonFailedListener.onCommonFailed(FailedType.NoRecv, 0, new JSONObject());
								} catch (JSONException e) {
									e.printStackTrace();
									DTLog.logError();
									return;
								}
							} else {
								cmd.onNoRecv();
							}
						}
					});

					mSynAryCmdSent.remove(i);
				}
			}
		}
	}

	// socket内部接口
	// 可以用于延时执行
	private void checkReconnectWithNextIMAddress() {
		if (getStatus() != DTSocketStatus.Connecting) {
			return;
		}

		this.reconnectWithNextIMAddress();
	}

	private void nextIMAddress() {
		ArrayList<String> array = new ArrayList<String>();

		// 1. aryAddressesFromSIS
		if (DTDevice.getInstance().mIMAddressListFromSIS.size() > 0) {
			for (String string : DTDevice.getInstance().mIMAddressListFromSIS) {
				array.add(string);
			}
		}

		// 2. lastIMAddress
		if (DTDevice.getInstance().mLastIMAddress.length() > 0) {
			array.remove(DTDevice.getInstance().mLastIMAddress);
			array.add(DTDevice.getInstance().mLastIMAddress);
		}

		// 3. imAddressFromDomain
		if (DTDevice.getInstance().mIMAddressFromDomain.length() > 0) {
			array.remove(DTDevice.getInstance().mIMAddressFromDomain);
			array.add(DTDevice.getInstance().mIMAddressFromDomain);
		}

		// 4. defaultIMAddress
		if (mDefaultIMAddress.length() > 0) {
			array.remove(mDefaultIMAddress);
			array.add(mDefaultIMAddress);
		}

		if (array.size() <= 0) {
			DTLog.logError();
			return;
		}

		int index = array.indexOf(mCurrentIMAddress);

		if (index == -1) {
			mCurrentIMAddress = array.get(0);
		} else {
			index++;

			if (index >= array.size()) {
				mCurrentIMAddress = null;
			} else {
				if (index >= array.size()) {
					DTLog.logError();
					return;
				}

				mCurrentIMAddress = array.get(index);

				if (TextUtils.isEmpty(mCurrentIMAddress)) {
					DTLog.logError();
					return;
				}

				if (!mCurrentIMAddress.contains(":")) {
					DTLog.logError();
					return;
				}
			}
		}
	}

	// 子线程调用
	private void reconnectWithNextIMAddress() {
		DTLog.sign("reconnectWithNextIMAddress");

		if (DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		DTAppEnv.getThreadHandler().removeCallbacks(mReconnectRunnable);
		DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);

		double nSleep;
		synchronized (mSyncConnectCount) {
			if(mReconnectCount == 0) {
				nSleep = (Math.random() + Math.random());
			} else {
				nSleep = (long) Math.pow(2, mReconnectCount); // 延迟重连时间依次是2s,4s,8s,16s,16s....
				if (nSleep > 16) {
					nSleep = 16;
				}
			}
			Log.e("Debug", "重连次数：" + mReconnectCount + ", 延时重连等待时间(s)：" + nSleep);
		}

		this.checkCloseSubThreads();
		this.nextIMAddress();

		Log.e("Debug", "nextIMAddress---" + mCurrentIMAddress);

		if (!TextUtils.isEmpty(mCurrentIMAddress)) {
			//存在下一尝试地址，则直接开始连接
			DTAppEnv.postDelayOnUIThread(nSleep, new Runnable() {
				@Override
				public void run () {
					DTSocket.this.startSendThread();
				}
			});
		} else {
			//否则，重新尝试第一连接地址
			DTAppEnv.postDelayOnUIThread(nSleep, mReconnectRunnable);
		}

	}

	/**
	 * 网络断开时清空所有指令（此处为子进程逻辑，与主进程清空逻辑不同）
	 */
	private void removeAllCommand() {
		mRecvBufferContentSize = 0;

		synchronized (mSynAryCmdNeedSend) {
			for (final DTCmd cmd : mSynAryCmdNeedSend) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						if (cmd.mOnSendFailedListener != null) {
							cmd.mOnSendFailedListener.onSendFailed();
						} else if (cmd.mOnCommonFailedListener != null) {
							try {
								cmd.mOnCommonFailedListener.onCommonFailed(FailedType.SendFailed, 0, new JSONObject());
							} catch (JSONException e) {
								e.printStackTrace();
								DTLog.logError();
								return;
							}
						} else {
							cmd.onSendFailed();
						}
					}
				});
			}

			mSynAryCmdNeedSend.clear();
		}

		synchronized (mSynAryCmdSent) {
			for (final DTCmd cmd : mSynAryCmdSent) {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						if (cmd.mOnNoRecvListener != null) {
							cmd.mOnNoRecvListener.onNoRecv();
						} else if (cmd.mOnCommonFailedListener != null) {
							try {
								cmd.mOnCommonFailedListener.onCommonFailed(FailedType.NoRecv, 0, new JSONObject());
							} catch (JSONException e) {
								e.printStackTrace();
								DTLog.logError();
								return;
							}
						} else {
							cmd.onNoRecv();
						}
					}
				});
			}

			mSynAryCmdSent.clear();
		}

		Log.e("Debug", "remove all cmd");
	}

	// 主线程调用
	public void reconnect() {
		DTLog.sign("reconnect");
		DTLog.signGrade(Thread.currentThread().getStackTrace().length - 1 > 5 ? 5 : Thread.currentThread().getStackTrace().length - 1);

		DTAppEnv.getThreadHandler().removeCallbacks(mReconnectRunnable);
		DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
		
		if (getStatus() != DTSocketStatus.None) {
			checkCloseSocket();
		}

		setStatus(DTSocketStatus.Connecting);
		this.connect();
	}

	// 主线程调用
	private void connect() {
		this.firstIMAddress();
		this.startSendThread();
	}

	// 主线程调用
	private void startSendThread() {
		DTLog.sign("startSendThread");

		synchronized (mSyncConnectCount) {
			mReconnectCount++;
			DTLog.e("Debug", "当前尝试次数：" + mReconnectCount);
		}

		synchronized (mSyncThread) {
			if (DTSocket.this.mSendThread != null) {
				return;
			}
		}

		synchronized (mSyncThread) {
			mSendThread = new DTSendThread(new Runnable() {
				public void run() {
					threadFuncSend();
				}
			});

			mSendThread.start();
		}

		DTLog.sign("Try connect " + mCurrentIMAddress);

		DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
		DTAppEnv.getThreadHandler().postDelayed(mCheckReconnectWithNextIMAddressRunnable, 3 * 1000);
	}

	private void threadFuncSend() {
		try {
			if (TextUtils.isEmpty(mCurrentIMAddress)) {
				DTLog.logError();
				return;
			}

			if (!mCurrentIMAddress.contains(":")) {
				DTLog.logError();
				return;
			}

			final String[] arrayStrings = mCurrentIMAddress.split(":");

			Log.e("Debug", "mCurrentIMAddress---" + mCurrentIMAddress);

			if (arrayStrings.length != 2) {
				DTLog.logError();
				return;
			}

			int port = Integer.parseInt(arrayStrings[1]);

			if (port <= 0) {
                DTLog.logError();
                return;
            }

			synchronized (mSyncThread) {
                if (mSendThread == null || mSendThread.isCanceled() || DTSendThread.currentThread() != mSendThread) {
                    return;
                }
            }

			int result = IMProtocol.InitIMSDK(arrayStrings[0], port);

			synchronized (mSyncThread) {
                if (mSendThread == null || mSendThread.isCanceled() || DTSendThread.currentThread() != mSendThread) {
                    return;
                }
            }

			if (result < 0) {
                // 连接失败
				// 立即重连
				DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
				DTAppEnv.getThreadHandler().postDelayed(mCheckReconnectWithNextIMAddressRunnable, 3 * 1000);

                return;
            }

			synchronized (mSyncThread) {
                if (mSendThread == null || mSendThread.isCanceled() || DTSendThread.currentThread() != mSendThread) {
                    // 当前线程已被取消
                    return;
                }
            }

			// 连接成功
			DTLog.sign("connected");
			setStatus(DTSocketStatus.Connected);

			new Thread(new Runnable() {
				@Override
				public void run() {
					DTAppEnv.sIMSDKIpAddress = arrayStrings[0];
					IMActionFile.IM_FILE_URL_DOMAIN_NAME = arrayStrings[0];
					IMActionFile.IM_FILE_URL_REAL_IP = arrayStrings[0];

					if(MainToSocketProcessStub.getInstance().getAddressChangedListener() != null) {
						try {
							MainToSocketProcessStub.getInstance().getAddressChangedListener().addressChanged(0, arrayStrings[0]);
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();

			Thread.sleep(1);

			DTThread recvThread = new DTThread(new Runnable() {
                @Override
                public void run() {
                    threadFuncRecv();
                }
            });

			synchronized (mSyncThread) {
                if (mSendThread == null || mSendThread.isCanceled() || DTSendThread.currentThread() != mSendThread) {
                    // 当前线程已被取消
                    return;
                }

                mSendThread.setRecvThread(recvThread);
            }

			recvThread.start();

			DTCmd cmd = null;

			do {
                synchronized (mSynAryCmdNeedSend) {
                    if (mSynAryCmdNeedSend.size() > 0) {
                        cmd = mSynAryCmdNeedSend.get(0);
                        mSynAryCmdNeedSend.remove(0);
                    } else {
                        cmd = null;
                    }
                }

                if (cmd == null) {
                    synchronized (mSyncThread) {
                        if (mSendThread == null || mSendThread.isCanceled() || mSendThread != DTSendThread.currentThread()) {
                            // 当前线程已被取消
                            return;
                        }
                    }

                    synchronized (mSynAryCmdNeedSend) {
                        try {
                            if(mSynAryCmdNeedSend.size() == 0) {
                                mSynAryCmdNeedSend.wait(1000);
                            }
                        } catch (InterruptedException e) {
                        }
                    }

                    continue;
                }

                byte[] buffer = cmd.getSendData();

                if (buffer == null) {
                    DTLog.logError();
                    return;
                }

                if (buffer.length == 0) {
					DTLog.logError();
					return;
				}

                if (cmd.getCmdTypeValue() == 0) {
                    DTLog.logError();
                    return;
                }

				DTLog.sign("TCP Send CmdType:" +  " " + cmd.getCmdTypeValue()
						+ ", SequenceID: " + cmd.getSequenceID()
						+ "\n TCP Send Json:" + cmd.getSendJson());

				int length = cmd.getSendLength();

                synchronized (mSyncThread) {
                    if (mSendThread == null || mSendThread.isCanceled() || mSendThread != DTSendThread.currentThread()) {
                        // 当前线程已被取消
                        return;
                    }
                }

				DTLog.sign("TCP length:" + length);

                // printBuffer(buffer, length);
                synchronized (mSyncParseObject) {
                    result = IMProtocol.SendIMSDK(buffer, 0, length);

					if (result == 0) {
						// 发送成功
						DTLog.sign("TCP length:" + length);

                        synchronized (mSynAryCmdSent) {
                            mSynAryCmdSent.add(cmd);
                        }

						DTLog.sign("TCP Send Result:" + result);

						continue;
					}
				}

                // printBuffer(buffer, 0, length);

    //			DTLog.sign(sDebuging, "TCP length:" + length);
    //			DTLog.sign(sDebuging, "TCP Send Result:" + result);

                synchronized (mSyncThread) {
                    if (mSendThread == null || mSendThread.isCanceled() || mSendThread != Thread.currentThread()) {
                        return;
                    }
                }

                if (result != 0) {
                    final DTCmd finalCmd = cmd;

                    // 发送失败
                    DTAppEnv.postOnUIThread(new Runnable() {
						@Override
						public void run() {
							// if (finalCmd.getSenderUID() != mUID) {
							// return;
							// }

							if (finalCmd.mOnSendFailedListener != null) {
								finalCmd.mOnSendFailedListener.onSendFailed();
							} else if (finalCmd.mOnCommonFailedListener != null) {
								try {
									finalCmd.mOnCommonFailedListener.onCommonFailed(FailedType.SendFailed, 0,
											new JSONObject());
								} catch (JSONException e) {
									e.printStackTrace();
									DTLog.logError();
									return;
								}
							} else {
								finalCmd.onSendFailed();
							}
						}
					});

					synchronized (mSyncThread) {
						if (mSendThread == null || mSendThread.isCanceled()
								|| mSendThread != Thread.currentThread()) {
							// 客户端主动断开
						} else if (mSendThread == Thread.currentThread() && !mSendThread.isCanceled()) {
							// 网络原因发送失败需要客户端重新连接
							DTAppEnv.postOnUIThread(new Runnable() {
								@Override
								public void run() {
									reconnect();
								}
							});
						} else {
							// 客户端已经做了重连，并且在极短时间内就连上了
						}
					}

                    return;
                }
            } while (!Thread.interrupted());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void threadFuncRecv() {
		DTLog.sign("recvThread");

		int recvSize = 0;

		while (!Thread.interrupted()) {
			DTLog.sign("begin RecvIMSDK");

			synchronized (mSyncThread) {
				if (mSendThread == null || mSendThread.getRecvThread() != Thread.currentThread()
						|| mSendThread.isCanceled()) {
					return;
				}
			}

			recvSize = IMProtocol.RecvIMSDK(mRecvBuffer, mRecvBufferContentSize, 2048 - mRecvBufferContentSize);

			DTLog.sign("end RecvIMSDK");
			DTLog.sign("TCP recvSize:" + recvSize);

			// printBuffer(mRecvBuffer, mRecvBufferContentSize, recvSize);

			synchronized (mSyncThread) {
				if (mSendThread == null || mSendThread.getRecvThread() != Thread.currentThread()
						|| mSendThread.isCanceled()) {
					return;
				}
			}

			if (recvSize <= 0) {
				synchronized (mSyncThread) {
					if (mSendThread != null && mSendThread.getRecvThread() == Thread.currentThread()) {
						// 真正被动断开连接
						DTAppEnv.postOnUIThread(new Runnable() {
							@Override
							public void run() {
								if (getStatus() != DTSocketStatus.None) {
									checkCloseSubThreads();
									setStatus(DTSocketStatus.None);
								}
							}
						});
					} else {
						// 主动断开连接之后recv返回
						// 不处理
					}
				}

				return;
			}

			synchronized (mSyncThread) {
				if (mSendThread == null || mSendThread.getRecvThread() != Thread.currentThread()
						|| mSendThread.isCanceled()) {
					return;
				}
			}

			mRecvBufferContentSize += recvSize;
			DTSocket.this.tryDecode();

			synchronized (mSyncThread) {
				if (mSendThread == null || mSendThread.getRecvThread() != Thread.currentThread()
						|| mSendThread.isCanceled()) {
					return;
				}
			}
		}
	}

	// 子线程调用
	private void tryDecode() {
		while (mRecvBufferContentSize >= 24) {
			int length = 0;
			int cmdTypeValue = 0;
			int version = 0;
			int sequenceID = 0;
			long errorCode = 0;
			long uid = 0;
			long compress = 0;

			int start = 0;

			for (int i = 0; i < 2; i++) {
				length = (length << 8) + (mRecvBuffer[i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				cmdTypeValue = (cmdTypeValue << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				version = (version << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				sequenceID = (sequenceID << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 4; i++) {
				errorCode = (errorCode << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 4;

			for (int i = 0; i < 8; i++) {
				uid = (uid << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 8;

			for (int i = 0; i < 4; i++) {
				compress = (compress << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 4;

			if (length > 2048) {
				DTLog.logError("DTSocket recv error: length>2048");
				DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
				DTAppEnv.getThreadHandler().postDelayed(mCheckReconnectWithNextIMAddressRunnable, 1000);
				return;
			}

			if (version > 10) {
				DTLog.logError("DTSocket recv error: version>10");
				DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
				DTAppEnv.getThreadHandler().postDelayed(mCheckReconnectWithNextIMAddressRunnable, 1000);
				return;
			}

			if (0 == length) {
				DTLog.logError("DTSocket recv error: length=0");
				DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
				DTAppEnv.getThreadHandler().postDelayed(mCheckReconnectWithNextIMAddressRunnable, 1000);
				return;
			}

			if (0 == cmdTypeValue) {
				DTLog.logError("DTSocket recv error: cmdTypeValue=0");
				DTAppEnv.getThreadHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
				DTAppEnv.getThreadHandler().postDelayed(mCheckReconnectWithNextIMAddressRunnable, 1000);
				return;
			}

			if (length > mRecvBufferContentSize) {
				DTLog.logError("DTSocket recv error: length > mRecvBufferContentSize");
				break;
			}

			String json = "";

			try {
				json = new String(mRecvBuffer, 24, length - 24, "UTF8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				return;
			}

			DTLog.sign(cmdTypeValue + " DTSocket recv Code: " + errorCode + ", sequenceID:" + sequenceID
					+ "\n recv Json:" + json);

			if (sequenceID > 0) {
				// 应答包
				DTCmd cmdSent = null;
				boolean needWait = false;

				synchronized (mSynAryCmdSent) {
					for (DTCmd temp : mSynAryCmdSent) {
						if (temp.getCmdTypeValue() == cmdTypeValue && temp.getSequenceID() == sequenceID) {
							cmdSent = temp;
							mSynAryCmdSent.remove(temp);
							break;
						}
					}
				}

				if (cmdSent == null) {
					synchronized (mSyncParseObject) {
						synchronized (mSynAryCmdSent) {
							for (DTCmd temp : mSynAryCmdSent) {
								if (temp.getCmdTypeValue() == cmdTypeValue && temp.getSequenceID() == sequenceID) {
									cmdSent = temp;
									mSynAryCmdSent.remove(temp);
									break;
								}
							}
						}
					}
				}

				if (cmdSent != null) {
					if (cmdSent.isRemoteCmd) {
						// 如果Cmd是属于SocketService， SocketService自行处理
						DTRecvPerform perform = new DTRecvPerform();

						perform.mJson = json;
						perform.mErrorCode = errorCode;
						perform.mCmdObj = cmdSent;
						performRecv(perform);
					} else {
						if (MainToSocketProcessStub.getInstance().getCmdRecvListener() != null) {
							try {
								MainToSocketProcessStub.getInstance().getCmdRecvListener()
										.onRecv(cmdTypeValue, sequenceID, errorCode, json);
							} catch (RemoteException e) {
								e.printStackTrace();
								MainToSocketProcessStub.getInstance().onDestroy();
							}
						} else {
							DTRecvPerform perform = new DTRecvPerform();

							perform.mJson = json;
							perform.mErrorCode = errorCode;
							perform.mCmdObj = cmdSent;
							performRecv(perform);
						}
					}
				} else {
					DTLog.e("Debug", "DTSocket cannot found cmdSent --cmdType:" + cmdTypeValue + ", sequenceID:"
							+ sequenceID);
				}
			} else {
				if (MainToSocketProcessStub.getInstance().getCmdRecvListener() != null) {
					// 发送给client
					try {
						MainToSocketProcessStub.getInstance().getCmdRecvListener()
								.onRecv(cmdTypeValue, sequenceID, errorCode, json);

					} catch (RemoteException e) {
						e.printStackTrace();

						MainToSocketProcessStub.getInstance().onDestroy();
						analysePushData(cmdTypeValue, json);
					}
				} else {
					// Notification提示
					analysePushData(cmdTypeValue, json);
				}
			}

			if (mRecvBufferContentSize < length) {
				DTLog.logError();
				return;
			}

			int leftLength = mRecvBufferContentSize - length;

			if (leftLength > 0) {
				for (int i = 0; i < leftLength; i++) {
					mRecvBuffer[i] = mRecvBuffer[length + i];
				}

				mRecvBufferContentSize = leftLength;
			} else {
				mRecvBufferContentSize = 0;
			}
		}
	}

	// 解析推送包
	private void analysePushData(int cmdTypeValue, String json) {
		// 推送包
		final DTPushCmd cmdPush = this.getPushCmdObject(cmdTypeValue);

		if (cmdPush == null) {
			DTLog.logError();
			return;
		}

		JSONObject jsonObject = null;

		try {
			if (json.length() > 0) {
				jsonObject = new JSONObject(json);
			} else {
				jsonObject = new JSONObject();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return;
		}

		final JSONObject finalJsonObject = jsonObject;
		final String finalJson = json;

		DTAppEnv.postOnUIThread(new Runnable() {
			@Override
			public void run() {
				DTLog.sign("OnRecv Push:" + cmdPush.getClass().getSimpleName());
				DTLog.sign("OnRecv Push:" + finalJson);

				try {
					cmdPush.onRecv(finalJsonObject);
				} catch (JSONException e) {
					e.printStackTrace();
					DTLog.logError();
					return;
				}
			}
		});

	}

	private final class DTRecvPerform {
		public long mErrorCode;
		public DTCmd mCmdObj;
		public String mJson;
	}

	private void performRecv(final DTRecvPerform perform) {
		if (perform == null) {
			DTLog.logError();
			return;
		}

		DTAppEnv.postOnUIThread(new Runnable() {
			@Override
			public void run() {
				if (perform.mCmdObj == null) {
					DTLog.logError();
					return;
				}

				DTLog.sign("OnRecv: " + perform.mCmdObj.getClass().getSimpleName());

				if (perform.mCmdObj.getSenderUID() != IMRemoteMyself.getInstance().getUID()) {
					DTLog.logError();
					return;
				}

				JSONObject jsonObject = null;

				if (perform.mJson != null && perform.mJson.length() > 0) {
					try {
						jsonObject = new JSONObject(perform.mJson);
					} catch (JSONException e) {
						e.printStackTrace();
						DTLog.logError();
						return;
					}
				}

				try {
					if (perform.mErrorCode > 0) {
						DTLog.sign("DTSocket RecvError:" + perform.mErrorCode + " "
								+ perform.mCmdObj.getClass().getSimpleName());

						if (perform.mCmdObj.mOnRecvErrorListener != null) {
							perform.mCmdObj.mOnRecvErrorListener.onRecvError(perform.mErrorCode, jsonObject);
						} else if (perform.mCmdObj.mOnCommonFailedListener != null) {
							perform.mCmdObj.mOnCommonFailedListener.onCommonFailed(FailedType.RecvError,
									perform.mErrorCode, jsonObject);
						} else {
							perform.mCmdObj.onRecvError(perform.mErrorCode, jsonObject);
						}
					} else {
						if (perform.mCmdObj.mOnRecvListener != null) {
							perform.mCmdObj.mOnRecvListener.onRecv(jsonObject);
						} else {
							perform.mCmdObj.onRecv(jsonObject);
						}

						if (perform.mCmdObj.mOnRecvEndListener != null) {
							perform.mCmdObj.mOnRecvEndListener.onRecvEnd(jsonObject);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
					DTLog.logError();
					return;
				}
			}
		});
	}

	private void firstIMAddress() {
		mCurrentIMAddress = null;

		// 1. aryAddressesFromSIS
		if (DTDevice.getInstance().mIMAddressListFromSIS.size() > 0) {
			mCurrentIMAddress = DTDevice.getInstance().mIMAddressListFromSIS.get(0);

			if (TextUtils.isEmpty(mCurrentIMAddress)) {
				DTLog.logError();
				return;
			}

			if (!mCurrentIMAddress.contains(":")) {
				DTLog.logError();
				return;
			}

			return;
		}

		// 2. lastIMAddress
		if (DTDevice.getInstance().mLastIMAddress.length() > 0) {
			mCurrentIMAddress = DTDevice.getInstance().mLastIMAddress;

			if (TextUtils.isEmpty(mCurrentIMAddress)) {
				DTLog.logError();
				return;
			}

			if (!mCurrentIMAddress.contains(":")) {
				DTLog.logError();
				return;
			}

			return;
		}

		// 3. imAddressFromDomain
		if (DTDevice.getInstance().mIMAddressFromDomain.length() > 0) {
			mCurrentIMAddress = DTDevice.getInstance().mIMAddressFromDomain;

			if (TextUtils.isEmpty(mCurrentIMAddress)) {
				DTLog.logError();
				return;
			}

			if (!mCurrentIMAddress.contains(":")) {
				DTLog.logError();
				return;
			}

			return;
		}

		// 4. defaultIMAddress
		mCurrentIMAddress = this.getDefaultIMAddress();

		if (TextUtils.isEmpty(mCurrentIMAddress)) {
			DTLog.logError();
			return;
		}

		if (!mCurrentIMAddress.contains(":")) {
			DTLog.logError();
			return;
		}
	}

	//多线程调用
	public void checkCloseSocket() {
		DTLog.sign("checkCloseSocket");

		if (getStatus() == DTSocketStatus.None) {
			return;
		}

		this.checkCloseSubThreads();
		setStatus(DTSocketStatus.None);
	}

	private void checkCloseSubThreads() {
		DTLog.signGrade(4);
		DTLog.sign("checkCloseSubThreads");

		if (getStatus() == DTSocketStatus.None) {
			DTLog.logError();
			return;
		}

		synchronized (mSyncThread) {
			if (mSendThread == null) {
				return;
			}

			mSendThread.cancel();
			mSendThread = null;
		}

		this.removeAllCommand();
	}

	public abstract DTPushCmd getPushCmdObject(int cmdTypeValue);

	private boolean mIsRunningSIS = false;
	private String mSISDomainName = "";
	private String mSISDefaultAddress = "";
	private String mDomainName = "";
	private String mDefaultIMAddress = "";
	private String mForceIMAddress = "";
	private String mCurrentIMAddress = "";

	private long mSessionID = 0;
	private long mUID = 0;

	private final Object mSyncConnectCount = new Object();
	private int mReconnectCount = 0;

	private final Object mSyncThread = new Object();  // 为mSendThread和
	private final Object mSyncStatus = new Object();  // mStatus加锁
	private DTSendThread mSendThread = null;
	private DTSocketStatus mStatus = DTSocketStatus.None;
	private DTSocketStatus mStatusNotified = DTSocketStatus.None;

	private byte[] mRecvBuffer = new byte[2048];
	private int mRecvBufferContentSize = 0;

	private final ArrayList<DTCmd> mSynAryCmdNeedSend = new ArrayList<DTCmd>();
	private final ArrayList<DTCmd> mSynAryCmdSent = new ArrayList<DTCmd>();
	private Object mSyncParseObject = new Object();

	private Runnable mCheckReconnectWithNextIMAddressRunnable = new Runnable() {
		@Override
		public void run() {
			if (DTAppEnv.isUIThread()) {
				DTLog.logError();
				return;
			}

			DTSocket.this.checkReconnectWithNextIMAddress();
		}
	};

	private Runnable mReconnectRunnable = new Runnable() {
		@Override
		public void run() {
			DTSocket.this.reconnect();
		}
	};

	private void printBuffer(byte[] buff, int offset, int count) {
		System.out.println("==========Begin Socket==========");

		for (int i = offset; i < count; i++) {
			String hex = Integer.toHexString(buff[i] & 0xFF);

			if (hex.length() == 1) {
				hex = '0' + hex;
			}

			System.out.print(hex.toUpperCase() + " ");
		}

		System.out.println("");
		System.out.println("==========End Socket==========");
	}
}
