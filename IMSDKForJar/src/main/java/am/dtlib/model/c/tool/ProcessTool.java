package am.dtlib.model.c.tool;

import java.util.List;

import am.dtlib.model.a.base.DTAppEnv;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;

public class ProcessTool {
	public static String getCurrentProcessName(Context context) {
		int pid = android.os.Process.myPid();
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

		for (RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
			if (appProcess.pid == pid) {
				return appProcess.processName;
			}
		}

		return null;
	}

	public static boolean isRemoteProcessExist(Context context) {
		String processName = context.getPackageName() + ":im";
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

		for (RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
			if (appProcess.processName.equals(processName)) {
				return true;
			}
		}

		return false;
	}

	public static boolean isClientProcessExist(Context context) {
		String processName = context.getPackageName();
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

		for (RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
			if (appProcess.processName.equals(processName)) {
				return true;
			}
		}

		return false;
	}

	public static String getProcessName(Context context) {
		int pid = android.os.Process.myPid();
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

		for (RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
			if (appProcess.pid == pid) {
				return appProcess.processName;
			}
		}

		return "";
	}

	public static boolean isRemoteProcess(Context context) {
		return getProcessName(context).contains(":im");

	}
	
	public static boolean isForeground(Context context) {
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);

		List<ActivityManager.RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(1);
		if (tasksInfo.size() > 0) {
			// 应用程序位于堆栈的顶层
			if (context.getPackageName().equals(tasksInfo.get(0).topActivity
					.getPackageName())) {
				return true;
			}
		}

		return false;
	}

	public static boolean isSocketProcess() {
		return isRemoteProcess(DTAppEnv.getContext());
	}
}
