package am.dtlib.model.c.tool;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import am.dtlib.model.a.base.DTAppEnv;
import am.imsdk.demo.gif.GifEmotionUtils;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.model.IMChatSetting;
import am.imsdk.ui.utils.IMNoticeUtils;

/**
 * Created by fzj on 2015/9/18.
 *
 */
public class NotifyUtils {
    public enum NotifyMsgType {
        UserMsg(1), TeamMeg(2), SystemMsg(3);

        int type;
        NotifyMsgType(int type) {
            this.type = type;
        }

        public int getValue() {
            return type;
        }
    }

    public static class notifyDelayRunnable implements Runnable {

        /** 1:用户聊天消息  2:群消息  3:系统推送消息 */
        private final int notifyType;
        private final String toCustomUserID;
        private final String nickName;
        private final String displayContent;
        private final String fullContent;
        private final String extraData;
        private final long serverSendTime;

        /**
         *
         * @param notifyType 1:用户聊天消息  2:群消息  3:系统推送消息
         * @param toCustomUserID 发送方ID
         * @param nickName 发送方昵称
         * @param displayContent 消息内容
         * @param extraData 消息扩展内容
         * @param serverSendTime 发送时间
         */
        public notifyDelayRunnable(int notifyType, String toCustomUserID, String nickName, String displayContent, String fullContent, String extraData, long serverSendTime) {
            this.notifyType = notifyType;
            this.toCustomUserID =  toCustomUserID;
            this.nickName = nickName;
            this.displayContent = displayContent;
            this.fullContent = fullContent;
            this.serverSendTime = serverSendTime;
            this.extraData = extraData;
        }

        @Override
        public void run() {
            showNotification(notifyType, toCustomUserID, nickName, displayContent, fullContent, extraData, serverSendTime);
        }
    }

    public static NotificationManager manager=null;

    /**
     *
     * @param notifyType 1:用户聊天消息  2:群消息  3:系统推送消息
     * @param toCustomUserID 发送方ID
     * @param nickName 发送方昵称
     * @param displayContent 展示消息内容
     * @param fullContent 具体消息内容
     * @param extraData 消息扩展内容
     * @param serverSendTime 发送时间
     */
    private static void showNotification(int notifyType, String toCustomUserID, String nickName, String displayContent, String fullContent, String extraData, long serverSendTime) {
        if(ProcessTool.isForeground(DTAppEnv.getContext()) && !ProcessTool.isSocketProcess()) {
            //client，并正在前台显示，不用通知nitification
            IMNoticeUtils.playMsgNotice(DTAppEnv.getContext());
            return;
        }

        if (manager == null) {
            manager = (NotificationManager) DTAppEnv.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        }

        manager.cancel(Integer.valueOf(toCustomUserID));

        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//这行代码会解决此问题
        intent.addFlags(Intent.FILL_IN_DATA);

        switch (DTAppEnv.getUserType()) {
            case 2:
                intent.setAction("imsdk.ui.waitingActivity.doctor");
                break;
            case 1:
                intent.setAction("imsdk.ui.waitingActivity.patient");
                break;
            default:
                intent.setAction("imsdk.ui.waitingActivity");
                break;
        }

        intent.putExtra("NotifyType", notifyType);
        intent.putExtra("CustomUserID", toCustomUserID);
        intent.putExtra("ExtraData", extraData);
        intent.putExtra("MsgContent", fullContent);
        intent.putExtra("MsgSendTime", serverSendTime);

        // 创建一个PendingIntent，和Intent类似，不同的是由于不是马上调用，需要在下拉状态条出发的activity，所以采用的是PendingIntent,即点击Notification跳转启动到哪个Activity
        PendingIntent pendingIntent = PendingIntent.getActivity(DTAppEnv.getContext(), Integer.valueOf(toCustomUserID) + 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notify1;
        Notification.Builder builder = new Notification.Builder(DTAppEnv.getContext());
        builder.setContentTitle(nickName);
        builder.setContentText(GifEmotionUtils.filterGifFacePhrase(displayContent));
        builder.setWhen(serverSendTime);
        builder.setSmallIcon(CPResourceUtil.getDrawableId(DTAppEnv.getContext(), "app_icon_small"), 0);
        builder.setContentIntent(pendingIntent);
        builder.setTicker("您有新消息，请注意查收！");
//        builder.setNumber(msgNumber);
        builder.setAutoCancel(true);

        if(IMChatSetting.getInstance().isMsgVibrate()) {
            builder.setVibrate(new long[] { 0, 100, 200, 300 });
        }

        if(IMChatSetting.getInstance().isMsgSound()) {
            builder.setSound(Uri.parse("android.resource://" + DTAppEnv.getContext().getPackageName() + "/" + CPResourceUtil.getRawId(DTAppEnv.getContext(), "notify")));
        }

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            notify1 = builder.getNotification();
        } else {
            notify1 = builder.build();
        }

        notify1.flags |= Notification.FLAG_AUTO_CANCEL;
        // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
        // 通过通知管理器来发起通知。如果id不同，则每click，在statu那里增加一个提示
        manager.notify(Integer.valueOf(toCustomUserID), notify1);
    }

    public static void clearNotification() {
        if (manager == null) {
            manager = (NotificationManager) DTAppEnv.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        }

        manager.cancelAll();
    }

}
