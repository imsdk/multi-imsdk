package am.dtlib.model.c.tool;

import java.util.HashMap;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;

public final class DTNotificationCenter {
	public void addObserver(String notification, Observer observer) {
		if (observer == null) {
			DTLog.logError();
			return;
		}

		Observable observable = mMapObservables.get(notification);

		if (observable == null) {
			observable = new Observable();
			mMapObservables.put(notification, observable);
		}

//		Log.e("observer add", notification + "--" + observer.toString());
		observable.addObserver(observer);
	}

	public static void printCallStatck() {
		Throwable ex = new Throwable();
		StackTraceElement[] stackElements = ex.getStackTrace();
		if (stackElements != null) {
			for (int i = 0; i < stackElements.length; i++) {
				System.out.print(stackElements[i].getClassName()+"/t");
				System.out.print(stackElements[i].getFileName()+"/t");
				System.out.print(stackElements[i].getLineNumber()+"/t");
				System.out.println(stackElements[i].getMethodName());
			}
			System.out.println("-----------------------------------");
		}
	}

	public boolean containObserver(String notification) {
		return mMapObservables.containsKey(notification);
	}

	public void removeObserver(String notification, Observer observer) {
		Observable observable = mMapObservables.get(notification);

		if (observable != null) {
//			Log.e("observer remove", notification + "--" + observer.toString());
			observable.deleteObserver(observer);

			if (observable.countObservers() == 0) {
				mMapObservables.remove(notification);
			}
		}
	}

	public void removeObservers(String notification) {
		Observable observable = mMapObservables.get(notification);

		if (observable != null) {
//			Log.e("observer remove", notification + "--all");
			observable.deleteObservers();

			mMapObservables.remove(notification);
		}
	}

	// public void removeObserver(Observer observer) {
	// Iterator<HashMap.Entry<String, Observable>> iterator = mMapObservables
	// .entrySet().iterator();
	//
	// while (iterator.hasNext()) {
	// HashMap.Entry<String, Observable> entry = iterator.next();
	// Observable observable = entry.getValue();
	//
	// observable.deleteObserver(observer);
	//
	// if (observable.countObservers() == 0) {
	// iterator.remove();
	// }
	// }
	//
	//
	// }

	public void postNotification(String notification) {
		try {
			this.postNotification(notification, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void postSyncNotification(String notification) {
		try {
			this.postSyncNotification(notification, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void postNotification(String notification, final Object object) {
		DTLog.sign("post Notification:" + notification);

		final Observable observable = mMapObservables.get(notification);

		if (observable != null) {
			if (DTAppEnv.isUIThread()) {
				observable.notifyObservers(object);
			} else {
				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						observable.notifyObservers(object);
					}
				});
			}
		}
	}

	public void postSyncNotification(String notification, final Object object) {
		DTLog.sign("post Sync Notification:" + notification);

		final Observable observable = mMapObservables.get(notification);

		if (observable != null) {
			if (DTAppEnv.isUIThread()) {
				observable.notifyObservers(object);
			} else {
				final Object syncObject = new Object();

				DTAppEnv.postOnUIThread(new Runnable() {
					@Override
					public void run() {
						observable.notifyObservers(object);

						synchronized (syncObject) {
							syncObject.notify();
						}
					}
				});

				try {
					synchronized (syncObject) {
						syncObject.wait();
					}
				} catch (InterruptedException e) {
				}
			}
		}
	}

	// singleton
	private volatile static DTNotificationCenter sSingleton;

	private DTNotificationCenter() {
	}

	public static DTNotificationCenter getInstance() {
		if (sSingleton == null) {
			synchronized (DTNotificationCenter.class) {
				if (sSingleton == null) {
					sSingleton = new DTNotificationCenter();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	private HashMap<String, Observable> mMapObservables = new HashMap<String, Observable>();
}
