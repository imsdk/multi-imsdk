package am.imsdk.aacmd.im;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.im.IMPrivateSystemMsg;
import am.imsdk.model.im.IMSystemMsgHistory;
import am.imsdk.model.im.IMSytemMsgHistoriesMgr;
import remote.service.data.IMRemoteMyself;

public final class IMPushCmdIMSystemMsg extends DTPushCmd {
	public IMPushCmdIMSystemMsg(Parcel in) {
		super(in);
	}

	public static final Creator<IMPushCmdIMSystemMsg> CREATOR = new Creator<IMPushCmdIMSystemMsg>() {
		@Override
		public IMPushCmdIMSystemMsg[] newArray(int size) {
			return new IMPushCmdIMSystemMsg[size];
		}

		@Override
		public IMPushCmdIMSystemMsg createFromParcel(Parcel in) {
			return new IMPushCmdIMSystemMsg(in);
		}
	};

	/*
		IMSystemMsgHistory history = IMSytemMsgHistoriesMgr.getInstance().getSystemMsgHistory(); //获取当前用户的历史系统消息管理器
		ArrayList<IMPrivateSystemMsg> systemMsgs = history.getSystemMsgs(); //获取所有历史消息
		ArrayList<IMPrivateSystemMsg> unReadSystemMsgs = history.getUnReadSystemMsgs(); //获取未读历史消息
		history.removeUnReadSystemMsg(unReadSystemMsgs.get(0).mMsgID);  //移除未读历史消息
	 */

	public IMPushCmdIMSystemMsg() {
		mCmdTypeValue = IMCmdType.IM_PUSH_CMD_IM_SYSTEM_MSG.getValue();
	}

	@Override
	public void onRecv(JSONObject recvJsonObject, boolean isOfflineMsg) throws JSONException {
//		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Logined) {
//			return;
//		}
		
		IMPrivateSystemMsg privateSystemMsg = new IMPrivateSystemMsg(recvJsonObject);

		if(ProcessTool.isSocketProcess()) {
			if (privateSystemMsg.mToUID != IMRemoteMyself.getInstance().getUID()) {
				DTLog.logError();
				return;
			}
		} else {
			if (privateSystemMsg.mToUID != IMPrivateMyself.getInstance().getUID()) {
				DTLog.logError();
				return;
			}
		}

		//msg主体 保存本地
		privateSystemMsg.saveFile();

		//msg索引保存到history列表中
		IMSystemMsgHistory history = IMSytemMsgHistoriesMgr.getInstance().getSystemMsgHistory();
		history.insertSystemMsg(privateSystemMsg);
		history.saveFile();

		if(!isOfflineMsg) {
			DTNotificationCenter.getInstance().postNotification("IMReceiveSystemText", privateSystemMsg); //通知前台

			//发送接收返回包
			IMCmdIMSystemMsgReceived cmd = new IMCmdIMSystemMsgReceived();
			cmd.mSystemMsgID = privateSystemMsg.mMsgID;
			cmd.send();
		}
	}
}
