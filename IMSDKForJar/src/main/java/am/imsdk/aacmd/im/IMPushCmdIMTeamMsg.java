package am.imsdk.aacmd.im;

import android.os.Parcel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.team.IMCmdTeamGetInfo;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionDoneListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.ammsgs.IMActionRecvTeamMsg;
import am.imsdk.action.fileserver.IMActionDownloadFile;
import am.imsdk.action.group.IMActionGroupGetInfo;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.imgroup.IMPrivateRecentGroups;
import am.imsdk.model.imteam.IMTeamChatMsgHistory;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamListMgr;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import imsdk.data.localchatmessagehistory.IMGroupChatMessage;
import remote.service.data.IMRemoteMyself;

public final class IMPushCmdIMTeamMsg extends DTPushCmd {
	public IMPushCmdIMTeamMsg(Parcel in) {
		super(in);
	}

	public static final Creator<IMPushCmdIMTeamMsg> CREATOR = new Creator<IMPushCmdIMTeamMsg>() {
		@Override
		public IMPushCmdIMTeamMsg[] newArray(int size) {
			return new IMPushCmdIMTeamMsg[size];
		}

		@Override
		public IMPushCmdIMTeamMsg createFromParcel(Parcel in) {
			return new IMPushCmdIMTeamMsg(in);
		}
	};

	private static int sMsgCount = 0;

	public IMPushCmdIMTeamMsg() {
		mCmdTypeValue = IMCmdType.IM_PUSH_CMD_IM_TEAM_MSG.getValue();
	}

	@Override
	public void onRecv(JSONObject recvJsonObject, final boolean isOfflineMsg) throws JSONException {
		if (++sMsgCount % 5 == 0) {
			DTNotificationCenter.getInstance().postNotification("heartbeat");
		}

//		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Logined) {
//			return;
//		}

		final IMActionRecvTeamMsg action = new IMActionRecvTeamMsg();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				IMTeamMsg teamMsg = action.mTeamMsg;

				if (teamMsg == null) {
					return;
				}

				if (teamMsg.mTeamID == 0) {
					DTLog.logError();
					return;
				}

				if (!IMParamJudge.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
					DTLog.logError();
					return;
				}

				if (teamMsg.mMsgID == 0) {
					DTLog.logError();
					return;
				}

				if (teamMsg.mServerSendTime == 0) {
					DTLog.logError();
					return;
				}

				onRecvGroupMsg(teamMsg, isOfflineMsg);
			}
		};

		action.mRecvJsonObject = recvJsonObject;
		action.begin();
	}

	private void sendTeamMsgReceived(IMTeamMsg teamMsg, boolean isOfflineMsg) {
		if (teamMsg == null) {
			DTLog.logError();
			return;
		}

		if (teamMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		if (teamMsg.getFromUID() == 0) {
			DTLog.logError();
			return;
		}

		if(isOfflineMsg) {
			//离线消息不用发送接收确认
			return;
		}

		IMCmdIMTeamMsgReceived cmd = new IMCmdIMTeamMsgReceived();

		cmd.mMsgID = teamMsg.mMsgID;
		cmd.mTeamID = teamMsg.mTeamID;
		cmd.send();
	}

	private void insertTeamChatMsgHistory(IMTeamMsg teamMsg) {
		if (!IMParamJudge.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
			DTLog.logError();
			return;
		}

		if (!teamMsg.mIsRecv) {
			DTLog.logError();
			return;
		}

		if (teamMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		// 维护聊天记录
		IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(teamMsg.mTeamID);

		history.insertRecvTeamMsg(teamMsg.mMsgID);
		history.mUnreadMessageCount = history.mUnreadMessageCount + 1;
		history.saveFile();

		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey(), teamMsg);

		// 维护最近联系Group
		IMPrivateRecentGroups.getInstance().insert(
				DTTool.getGroupIDFromTeamID(teamMsg.mTeamID));
		IMPrivateRecentGroups.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification("recentGroupsChanged");
	}

	private void onRecvGroupMsg(final IMTeamMsg teamMsg, final boolean isOfflineMsg) {
		long uid = 0;
		if(ProcessTool.isSocketProcess()) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}

		if (uid == teamMsg.getFromUID()) {
			// 服务端bug
			DTLog.logError();
			sendTeamMsgReceived(teamMsg, isOfflineMsg);
			return;
		}

		switch (teamMsg.mTeamMsgType) {
			case Normal: {
				if (teamMsg.isLocalFileExist()) {
					sendTeamMsgReceived(teamMsg, isOfflineMsg);
					return;
				}

				if (!teamMsg.saveFile()) {
					DTLog.logError();
					return;
				}

				IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
						teamMsg.mTeamID);

				if (teamInfo.getUIDList().size() == 0) {
					return;
				}

				sendTeamMsgReceived(teamMsg, isOfflineMsg);

				// 维护聊天记录
				insertTeamChatMsgHistory(teamMsg);

				if(!isOfflineMsg) {
					DTNotificationCenter.getInstance().postNotification("IMReceiveTextFromGroup", teamMsg);  // 发出通知
				}
			}
			break;
			case Audio: {
				if (teamMsg.mContent == null || teamMsg.mContent.length() == 0) {
					DTLog.logError();
					return;
				}

				// 维护本地数据
				if (teamMsg.isLocalFileExist()) {
					sendTeamMsgReceived(teamMsg, isOfflineMsg);
					return;
				}

				final IMAudio audio = IMAudiosMgr.getInstance().getAudio(
						teamMsg.getFileID());

				if (audio.isLocalFileExist()) {
					sendTeamMsgReceived(teamMsg, isOfflineMsg);
					teamMsg.saveFile();

					// 维护聊天记录 & 最近联系人
					insertTeamChatMsgHistory(teamMsg);

					if(!isOfflineMsg) {
						DTNotificationCenter.getInstance().postNotification("IMReceiveTeamAudio", teamMsg);  // 发出通知
					}
					return;
				}

				final IMActionDownloadFile action = new IMActionDownloadFile();

				action.mFileID = teamMsg.getFileID();

				action.mOnActionDoneListener = new OnActionDoneListener() {
					@Override
					public void onActionDone() {
						if (teamMsg.isLocalFileExist()) {
							sendTeamMsgReceived(teamMsg, isOfflineMsg);
							return;
						}

						sendTeamMsgReceived(teamMsg, isOfflineMsg);
						teamMsg.saveFile();

						if (!audio.mFileID.equals(teamMsg.getFileID())) {
							DTLog.logError();
							return;
						}

						IMAudiosMgr.getInstance().replaceClientFileID(audio.mFileID,
								action.mFileID);

						audio.mFileID = action.mFileID;
						audio.mBuffer = action.mBuffer;
						audio.saveFile();

						// 维护聊天记录 & 最近联系人
						insertTeamChatMsgHistory(teamMsg);

						if(!isOfflineMsg) {
							DTNotificationCenter.getInstance().postNotification("IMReceiveTeamAudio", teamMsg);  // 发出通知
						}
					}
				};

				action.begin();
			}
			break;
			case Photo: {
				if (teamMsg.mContent == null || teamMsg.mContent.length() == 0) {
					DTLog.logError();
					return;
				}

				if (teamMsg.isLocalFileExist()) {
					sendTeamMsgReceived(teamMsg, isOfflineMsg);
					return;
				}

				final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
						teamMsg.getFileID());

				if (photo.isLocalFileExist()) {
					sendTeamMsgReceived(teamMsg, isOfflineMsg);
					teamMsg.saveFile();

					// 维护聊天记录 & 最近联系人
					insertTeamChatMsgHistory(teamMsg);

					if(!isOfflineMsg) {
						DTNotificationCenter.getInstance().postNotification("IMReceiveBitmapMessageFromGroup", teamMsg);  // 发出通知
					}
					return;
				}

			/*
				通知前台并开始下载图片
			 */
				sendTeamMsgReceived(teamMsg, isOfflineMsg);
				teamMsg.saveFile();

				// 维护聊天记录 & 最近联系人
				insertTeamChatMsgHistory(teamMsg);

				teamMsg.mStatus = IMGroupChatMessage.SENDING_OR_RECVING;
				DTNotificationCenter.getInstance().postNotification(
						teamMsg.getStatusChangedNotificationKey());

				if(!isOfflineMsg) {
					DTNotificationCenter.getInstance().postNotification("IMReceiveBitmapMessageFromGroup", teamMsg);  // 发出通知
				}

				final IMActionDownloadFile action = new IMActionDownloadFile();

				action.mFileID = teamMsg.getFileID();

				action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
					@Override
					public void onActionPartiallyDone(double percentage) {
						if (teamMsg.getExtraData() == null) {
							DTLog.logError();
							return;
						}

						try {
							teamMsg.getExtraData().put("progress",
									Double.valueOf(percentage / 100));
						} catch (JSONException e) {
							e.printStackTrace();
							DTLog.logError();
							return;
						}

						if(!isOfflineMsg) {
							DTNotificationCenter.getInstance().postNotification("IMReceiveBitmapProgressFromGroup", teamMsg);  // 发出通知
						}
					}
				};

				action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
					@Override
					public void onActionDoneEnd() {
						if (!photo.mFileID.equals(teamMsg.getFileID())) {
							DTLog.logError();
							return;
						}

						photo.setBuffer(action.mBuffer);
						photo.saveFile();


						if(!isOfflineMsg) {
							DTNotificationCenter.getInstance().postNotification("IMReceiveBitmapFromGroup", teamMsg);  // 发出通知

							teamMsg.mStatus = IMGroupChatMessage.SUCCESS;
							DTNotificationCenter.getInstance().postNotification(
									teamMsg.getStatusChangedNotificationKey());
						}
					}
				};

				action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
					@Override
					public void onActionFailedEnd(String error) {
						teamMsg.mStatus = IMGroupChatMessage.FAILURE;
						DTNotificationCenter.getInstance().postNotification(
								teamMsg.getStatusChangedNotificationKey(), teamMsg);
					}
				};

				action.begin();
			}
			break;
			case NormalFileText:
			case CustomFileText: {
				if (!IMParamJudge.isFileIDLegal(teamMsg.getFileID())) {
					DTLog.logError();
					return;
				}

				// 维护本地数据
				if (teamMsg.isLocalFileExist()) {
					sendTeamMsgReceived(teamMsg, isOfflineMsg);
					return;
				}

				final IMActionDownloadFile action = new IMActionDownloadFile();

				action.mFileID = teamMsg.getFileID();

				action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
					@Override
					public void onActionDoneEnd() {
						if (teamMsg.isLocalFileExist()) {
							sendTeamMsgReceived(teamMsg, isOfflineMsg);
							return;
						}

						String string = "";

						try {
							string = new String(action.mBuffer, "UTF8");
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
							DTLog.logError();
							return;
						}

						string = DTTool.getBase64DecodedString(string);
						teamMsg.mContent = string;

						if (teamMsg.mTeamMsgType == TeamMsgType.NormalFileText) {
							teamMsg.mTeamMsgType = TeamMsgType.Normal;
						} else {
							teamMsg.mTeamMsgType = TeamMsgType.Custom;
						}

						teamMsg.saveFile();
						sendTeamMsgReceived(teamMsg, isOfflineMsg);

						// 维护聊天记录 & 最近联系人
						insertTeamChatMsgHistory(teamMsg);

						if(!isOfflineMsg) {
							DTNotificationCenter.getInstance().postNotification("IMReceiveTextFromGroup", teamMsg);  // 发出通知
						}
					}
				};

				action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
					@Override
					public void onActionFailedEnd(String error) {
						sendTeamMsgReceived(teamMsg, isOfflineMsg);
					}
				};

				action.begin();
			}
			break;
			case IMSDKGroupInfoUpdate: {
				sendTeamMsgReceived(teamMsg, isOfflineMsg);
				teamMsg.saveFile();

				// 刷新 group info
				IMCmdTeamGetInfo cmd = new IMCmdTeamGetInfo();

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {

						JSONArray teamInfoList = jsonObject.getJSONArray("teamInfolist");

						if(teamInfoList.length() <= 0) {
							DTLog.logError();
							return;
						}

						DTLog.e("Debug", "GroupInfoUpdate:" + jsonObject.toString());

						for (int i = 0; i < teamInfoList.length(); i++) {
							JSONObject mJsonObjectTeamInfo = (JSONObject) teamInfoList.get(i);

							if (!(mJsonObjectTeamInfo instanceof JSONObject)) {
								return;
							}

							long teamID = mJsonObjectTeamInfo.getLong("teamid");

							if (teamID == 0 || teamID != teamMsg.mTeamID) {
								DTLog.logError();
								return;
							}

							IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

							try {
								teamInfo.parseServerData(mJsonObjectTeamInfo);
							} catch (JSONException e) {
								e.printStackTrace();
								DTLog.logError();
								DTLog.log(mJsonObjectTeamInfo.toString());
								return;
							}

							teamInfo.saveFile();
						}

						// 发出通知
						DTNotificationCenter.getInstance().postNotification(
								"CustomGroupInfoUpdated", teamMsg);
					}
				};

				cmd.addTeamID(teamMsg.mTeamID);
				cmd.send();
			}
			break;
			case IMSDKGroupNewUser: {
				//新用户加入group通知
				if (teamMsg.getOperationUID() == uid) {
					//被添加的用户是自己
					IMActionGroupGetInfo action = new IMActionGroupGetInfo();

					action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
						@Override
						public void onActionDoneEnd() {
							sendTeamMsgReceived(teamMsg, isOfflineMsg);
							teamMsg.saveFile();

							// 刷新数据
							IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance()
									.getTeamInfo(teamMsg.mTeamID);

							if (teamInfo == null) {
								DTLog.logError();
								return;
							}

							IMPrivateTeamListMgr.getInstance().addTeamID(teamMsg.mTeamID);
							IMPrivateTeamListMgr.getInstance().saveFile();

							// 通知
							DTNotificationCenter.getInstance().postNotification(
									"AddedToGroup", teamMsg);
						}
					};

					action.mGroupID = teamMsg.getGroupID();
					action.begin();

					return;
				}

				//被添加的用户是其他人
				sendTeamMsgReceived(teamMsg, isOfflineMsg);
				teamMsg.saveFile();

				// 刷新 group member list
				IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
						teamMsg.mTeamID);

				teamInfo.add(teamMsg.getOperationUID());
				teamInfo.saveFile();
				DTNotificationCenter.getInstance().postNotification(
						"GroupMemberAdded:" + teamInfo.mTeamID);

				// 无需通知
			}
			break;
			case IMSDKGroupUserRemoved: {
				//用户被移出group通知
				sendTeamMsgReceived(teamMsg, isOfflineMsg);
				teamMsg.saveFile();

				// 刷新 group member list
				IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
						teamMsg.mTeamID);

				teamInfo.remove(teamMsg.getOperationUID());
				teamInfo.saveFile();

				DTNotificationCenter.getInstance().postNotification(
						"GroupMemberRemoved:" + teamInfo.mTeamID);
			}
			break;
			case IMSDKGroupDeleted: {
				//解散group通知
				sendTeamMsgReceived(teamMsg, isOfflineMsg);
				teamMsg.saveFile();

				// 刷新 team & groups
				IMPrivateTeamListMgr.getInstance().removeTeam(teamMsg.mTeamID);
				IMPrivateTeamListMgr.getInstance().saveFile();

				// 发出通知
				DTNotificationCenter.getInstance().postNotification("GroupDeletedByUser", teamMsg);
			}
			break;
			case IMSDKGroupQuit: {
				//用户退出group通知
				if (teamMsg.isLocalFileExist()) {
					sendTeamMsgReceived(teamMsg, isOfflineMsg);
					return;
				}

				sendTeamMsgReceived(teamMsg, isOfflineMsg);
				teamMsg.saveFile();

				// 刷新 group member list
				IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
						teamMsg.mTeamID);

				teamInfo.remove(teamMsg.getFromUID());
				teamInfo.saveFile();
				DTNotificationCenter.getInstance().postNotification(
						"GroupMemberRemoved:" + teamInfo.mTeamID,
						teamMsg.getOperationCustomUserID());

				// 无需通知
			}
			break;
			default:
				break;
		}
	}
}
