package am.imsdk.aacmd.im;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB.WBType;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.ammsgs.IMActionRecvUserMsg;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.action.fileserver.IMActionDownloadFile;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserChatMsgHistory;
import am.imsdk.model.im.IMUserChatMsgHistoryWithTag;
import am.imsdk.model.im.IMUserCustomMsgHistory;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.im.IMUserMsgHistory;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;
import am.imsdk.model.teaminfo.IMPrivateTeamListMgr;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import imsdk.data.localchatmessagehistory.IMChatMessage;
import remote.service.data.IMRemoteMyself;

public final class IMPushCmdIMUserMsg extends DTPushCmd {
	public IMPushCmdIMUserMsg(Parcel in) {
		super(in);
	}

	public static final Creator<IMPushCmdIMUserMsg> CREATOR = new Creator<IMPushCmdIMUserMsg>() {
		@Override
		public IMPushCmdIMUserMsg[] newArray(int size) {
			return new IMPushCmdIMUserMsg[size];
		}

		@Override
		public IMPushCmdIMUserMsg createFromParcel(Parcel in) {
			return new IMPushCmdIMUserMsg(in);
		}
	};

	private static int sMsgCount = 0;

	public IMPushCmdIMUserMsg() {
		mCmdTypeValue = IMCmdType.IM_PUSH_CMD_IM_USER_MSG.getValue();
	}

	@Override
	public void onRecv(JSONObject recvJsonObject, final boolean isOfflineMsg) throws JSONException {
		if (++sMsgCount % 5 == 0) {
			DTNotificationCenter.getInstance().postNotification("heartbeat");
		}

//		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Logined) {
//			return;
//		}

		final IMActionRecvUserMsg action = new IMActionRecvUserMsg();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				IMUserMsg userMsg = action.mUserMsg;

				if (userMsg == null) {
					return;
				}

				if (userMsg.mUserMsgType == UserMsgType.Unit) {
					DTLog.logError();
					return;
				}

				if (!IMParamJudge.isCustomUserIDLegal(userMsg.mFromCustomUserID)) {
					DTLog.logError();
					return;
				}

				if (!IMParamJudge.isCustomUserIDLegal(userMsg.mToCustomUserID)) {
					DTLog.logError();
					return;
				}

				if(ProcessTool.isSocketProcess()) {
					if (userMsg.getToUID() != IMRemoteMyself.getInstance().getUID()) {
						DTLog.logError();
						return;
					}
				} else {
					if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
						DTLog.logError();
						return;
					}
				}

				if (userMsg.mMsgID == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.mServerSendTime == 0) {
					DTLog.logError();
					return;
				}

				onRecvUserMsg(userMsg, isOfflineMsg);
			}
		};

		action.mRecvJsonObject = recvJsonObject;
		action.begin();
	}

	private void sendUserMsgReceived(IMUserMsg userMsg, boolean isOfflineMsg) {
		if (userMsg == null) {
			DTLog.logError();
			return;
		}

		if (userMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		if (userMsg.getFromUID() == 0) {
			DTLog.logError();
			return;
		}

		if (userMsg.mContent.length() > IMActionSendUserMsg.IMTextMaxLength) {
			return;
		}

		if(isOfflineMsg) {
			//离线消息不用发送接收确认
			return;
		}

		IMCmdIMUserMsgReceived cmd = new IMCmdIMUserMsgReceived();

		cmd.mMsgID = userMsg.mMsgID;
		cmd.mFromUID = userMsg.getFromUID();
		cmd.send();
	}

	private void insertUserChatMsgHistory(IMUserMsg userMsg) {
		if (!IMParamJudge.isCustomUserIDLegal(userMsg.mFromCustomUserID)) {
			DTLog.logError();
			return;
		}

		if (!userMsg.mIsRecv) {
			DTLog.logError();
			return;
		}

		if (userMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		if(userMsg.getExtraData2() != null && userMsg.getExtraData2().has("orderId")) {
			String tag = "";
			try {
				tag = userMsg.getExtraData2().getString("orderId");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// 维护聊天记录
			IMUserChatMsgHistoryWithTag history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(userMsg.mFromCustomUserID, tag);

			history.insertRecvUserMsg(userMsg.mMsgID);
			history.mUnreadMessageCount = history.mUnreadMessageCount + 1;
			history.saveFile();

			DTNotificationCenter.getInstance().postNotification(
					history.getNewMsgNotificationKey());
		} else {
			// 维护聊天记录
			IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(userMsg.mFromCustomUserID);

			history.insertRecvUserMsg(userMsg.mMsgID);
			history.mUnreadMessageCount = history.mUnreadMessageCount + 1;
			history.saveFile();

			DTNotificationCenter.getInstance().postNotification(
					history.getNewMsgNotificationKey());
		}

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(userMsg.mFromCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());
	}

	private void onRecvUserMsg(final IMUserMsg userMsg, final boolean isOfflineMsg) {
		switch (userMsg.mUserMsgType) {
			case Normal: {
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);
				userMsg.saveFile();

				// 维护聊天记录 & 最近联系人
				insertUserChatMsgHistory(userMsg);

				if(!isOfflineMsg) {
					DTNotificationCenter.getInstance().postNotification("IMReceiveText", userMsg); // 发出通知
				}
			}
			break;
			case Order: {
				sendUserMsgReceived(userMsg, isOfflineMsg);
				userMsg.saveFile();

				// 维护聊天记录 & 最近联系人
				insertUserChatMsgHistory(userMsg);

				if(!isOfflineMsg) {
					DTNotificationCenter.getInstance().postNotification("IMReceiveOrder", userMsg);  // 发出通知
				}
			}
			break;
			case IMSDKNotice: {
				sendUserMsgReceived(userMsg, isOfflineMsg);
				userMsg.saveFile();

				// 维护聊天记录 & 最近联系人
				insertUserChatMsgHistory(userMsg);
			}
			break;
			case Audio: {
				if (!IMParamJudge.isFileIDLegal(userMsg.getFileID())) {
					DTLog.logError();
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				final IMAudio audio = IMAudiosMgr.getInstance().getAudio(
						userMsg.getFileID());

				if(!audio.isLocalFileExist()) {
					userMsg.mStatus = IMChatMessage.SENDING_OR_RECVING;
				}
				userMsg.saveFile();

				sendUserMsgReceived(userMsg, isOfflineMsg);

				// 维护聊天记录 & 最近联系人
				insertUserChatMsgHistory(userMsg);

				if(!isOfflineMsg) {
					DTNotificationCenter.getInstance().postNotification("IMReceiveAudio", userMsg);  // 发出通知
				}

			}
			break;
			case Photo: {
				if (userMsg.mContent.length() == 0) {
					DTLog.logError();
					this.sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				if (!IMParamJudge.isFileIDLegal(userMsg.getFileID())) {
					DTLog.logError();
					this.sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
						userMsg.getFileID());

				if (!photo.isLocalFileExist()) {
					userMsg.mStatus = IMChatMessage.SENDING_OR_RECVING;
				}
				userMsg.saveFile();

				sendUserMsgReceived(userMsg, isOfflineMsg);

				// 维护聊天记录 & 最近联系人
				insertUserChatMsgHistory(userMsg);

				if (!isOfflineMsg) {
					DTNotificationCenter.getInstance().postNotification("IMReceiveBitmapMessage", userMsg);  // 发出通知
				}
			}
			break;
			case Custom: {
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				if (!userMsg.saveFile()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					DTLog.logError();
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);

				// 维护自定义消息历史记录
				IMUserCustomMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
						.getUserCustomMsgHistory(userMsg.mFromCustomUserID);

				history.insertUnsentUserMsg(userMsg.mClientSendTime);
				history.mUnreadMessageCount++;
				history.saveFile();
				DTNotificationCenter.getInstance().postNotification(
						history.getNewMsgNotificationKey());

				if(!isOfflineMsg) {
					DTNotificationCenter.getInstance().postNotification("IMReceiveCustomMessage", userMsg);  // 发出通知
				}
			}
			break;
			case NormalFileText:
			case CustomFileText: {
				if (!IMParamJudge.isFileIDLegal(userMsg.getFileID())) {
					DTLog.logError();
					return;
				}

				// 维护本地数据
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				final IMActionDownloadFile action = new IMActionDownloadFile();

				action.mFileID = userMsg.getFileID();

				action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
					@Override
					public void onActionDoneEnd() {
						if (userMsg.isLocalFileExist()) {
							sendUserMsgReceived(userMsg, isOfflineMsg);
							return;
						}

						String string = "";

						try {
							string = new String(action.mBuffer, "UTF8");
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
							DTLog.logError();
							return;
						}

						string = DTTool.getBase64DecodedString(string);
						userMsg.mContent = string;

						if (userMsg.mUserMsgType == UserMsgType.NormalFileText) {
							userMsg.mUserMsgType = UserMsgType.Normal;
						} else {
							userMsg.mUserMsgType = UserMsgType.Custom;
						}

						userMsg.saveFile();
						sendUserMsgReceived(userMsg, isOfflineMsg);

						// 维护聊天记录 & 最近联系人
						insertUserChatMsgHistory(userMsg);

						if (!isOfflineMsg) {
							DTNotificationCenter.getInstance().postNotification("IMReceiveText", userMsg);  // 发出通知
						}
					}
				};

				action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
					@Override
					public void onActionFailedEnd(String error) {
						sendUserMsgReceived(userMsg, isOfflineMsg);
					}
				};

				action.begin();
				break;
			}
			case IMSDKFriendRequest: {
				if (userMsg.isLocalFileExist())
				{
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}
				if (!userMsg.saveFile()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					DTLog.logError();
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);

				DTNotificationCenter.getInstance().postNotification("ReceiveFriendRequest",
						userMsg);
			}
			break;
			case IMSDKDeleteFriend: {
				DTLog.e("Debug", "收到 IMSDKDeleteFriend 命令");
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				if (!userMsg.saveFile()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					DTLog.logError();
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);

				if (IMPrivateRecentContacts.getInstance().remove(userMsg.mFromCustomUserID)) {
					IMPrivateRecentContacts.getInstance().saveFile();
					DTNotificationCenter.getInstance().postNotification(
							IMPrivateRecentContacts.getInstance().notificationKey());
				}

				if (IMPrivateFriendsMgr.getInstance().remove(userMsg.mFromCustomUserID)) {
					IMPrivateFriendsMgr.getInstance().saveFile();
					DTNotificationCenter.getInstance().postNotification("FriendsListUpdated");
				}

				DTNotificationCenter.getInstance().postNotification("ReceiveFriendDeleted",
						userMsg);
			}
			break;
			case IMSDKAgreeToFriendRequest: {
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				if (!userMsg.saveFile()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					DTLog.logError();
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);

				DTNotificationCenter.getInstance().postNotification(
						"ReceiveAgreeToFriendRequest", userMsg);

				IMCmdTeamAddMember2WB cmd = new IMCmdTeamAddMember2WB();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
											   JSONObject errorJsonObject) throws JSONException {
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						DTNotificationCenter.getInstance().postNotification(
								"BuildFriendshipWithUser", userMsg);
					}
				};

				cmd.mType = WBType.Friends;
				cmd.mUID = userMsg.getFromUID();
				cmd.send();
			}
			break;
			case IMSDKRejectToFriendRequest: {
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				if (!userMsg.saveFile()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					DTLog.logError();
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);

				DTNotificationCenter.getInstance().postNotification(
						"ReceiveRejectToFriendRequest", userMsg);
			}
			break;
			case IMSDKBlacklist: {
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				if (!userMsg.saveFile()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					DTLog.logError();
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);

				if (IMPrivateRecentContacts.getInstance().remove(userMsg.mFromCustomUserID)) {
					IMPrivateRecentContacts.getInstance().saveFile();
					DTNotificationCenter.getInstance().postNotification(
							IMPrivateRecentContacts.getInstance().notificationKey());
				}

				if (IMPrivateFriendsMgr.getInstance().remove(userMsg.mFromCustomUserID)) {
					IMPrivateFriendsMgr.getInstance().saveFile();
					DTNotificationCenter.getInstance().postNotification("FriendsListUpdated");
				}
			}
			break;
			case IMSDKGroupNoticeBeAdded: {
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);

				if (!userMsg.saveFile()) {
					DTLog.logError();
					return;
				}
			}
			break;
			case IMSDKGroupNoticeBeRemoved: {
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);

				if (!userMsg.saveFile()) {
					DTLog.logError();
					return;
				}

				// 刷新数据
				long teamID = DTTool.getTeamIDFromGroupID(userMsg.getGroupID());
				IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

				if (teamInfo == null) {
					DTLog.logError();
					return;
				}

				long uid;
				if(ProcessTool.isSocketProcess()) {
					uid = IMRemoteMyself.getInstance().getUID();
				} else {
					uid = IMPrivateMyself.getInstance().getUID();
				}

				teamInfo.remove(uid);
				teamInfo.saveFile();

				if (teamInfo.mTeamType == TeamType.Group) {
					DTNotificationCenter.getInstance().postNotification(
							"TeamUpdated:" + teamInfo.mTeamID,
							IMPrivateMyself.getInstance().getCustomUserID());
				}

				IMPrivateTeamListMgr.getInstance().removeTeam(teamID);
				IMPrivateTeamListMgr.getInstance().saveFile();

				// 通知
				DTNotificationCenter.getInstance().postNotification("RemovedFromGroup",
						userMsg);
			}
			break;
			case RevokeUserMessage: {  //撤回已发送消息
				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);
				userMsg.saveFile();

				JSONObject extraData = userMsg.getExtraData();

				if(extraData != null && extraData.has("revokeID")) {
					try {
						long msgID = extraData.getLong("revokeID");

						IMUserMsg revokeUserMsg = IMUserMsgsMgr.getInstance().getRecvUserMsg(userMsg.mFromCustomUserID, msgID);

						if(revokeUserMsg == null) {
							DTLog.logError();
							return;
						}

						IMUserMsgHistory msgHistory = null;
						if(!revokeUserMsg.getExtraData2().has("orderId")) {
							msgHistory = IMUserMsgHistoriesMgr.getInstance().getUserChatMsgHistory(userMsg.mFromCustomUserID);
						} else {
							String orderId = revokeUserMsg.getExtraData2().get("orderId").toString();
							msgHistory = IMUserMsgHistoriesMgr.getInstance().getUserChatMsgHistory(userMsg.mFromCustomUserID, orderId);
						}

						if(msgHistory != null) {
							revokeUserMsg.mContent = "\"" + userMsg.getNickName() + "\"撤回了一条消息";
							revokeUserMsg.mUserMsgType = UserMsgType.IMSDKNotice;
							revokeUserMsg.saveFile();

							DTNotificationCenter.getInstance().postNotification(
									msgHistory.getNewMsgNotificationKey());
						} else {
							DTLog.logError();
							return;
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					DTLog.logError();
					return;
				}
			}
			break;
			default: {
				DTLog.logError();

				if (userMsg.isLocalFileExist()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					return;
				}

				if (!userMsg.saveFile()) {
					sendUserMsgReceived(userMsg, isOfflineMsg);
					DTLog.logError();
					return;
				}

				sendUserMsgReceived(userMsg, isOfflineMsg);
			}
			break;
		}
	}
}
