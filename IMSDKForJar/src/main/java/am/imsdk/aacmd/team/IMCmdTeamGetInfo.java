package am.imsdk.aacmd.team;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdTeamGetInfo extends DTCmd {
	public void addTeamID(long teamID) {
		if (!mAryTeamIDs.contains(teamID)) {
			mAryTeamIDs.add(teamID);
		}
	}

	private ArrayList<Long> mAryTeamIDs = new ArrayList<Long>();

	public IMCmdTeamGetInfo() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_GET_INFO.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mAryTeamIDs.size() == 0) {
			DTLog.logError();
			return;
		}

		if (mAryTeamIDs.size() > 4) {
			DTLog.logError();
			return;
		}

		JSONArray jsonArray = new JSONArray(mAryTeamIDs);
		
		mSendJsonObject.put("teamidlist", jsonArray);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}

}
