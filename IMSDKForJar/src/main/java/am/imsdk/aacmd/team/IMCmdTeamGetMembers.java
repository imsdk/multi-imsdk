package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

public class IMCmdTeamGetMembers extends DTCmd {
	public long mTeamID;

	public IMCmdTeamGetMembers() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_GET_MEMBERS.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		long uid;

		if(ProcessTool.isSocketProcess()) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}

		if (uid == 0) {
			DTLog.logError();
			return;
		}
		
		mSendJsonObject.put("teamid", mTeamID);
		mSendJsonObject.put("uid", getSenderUID());
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
