package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB.WBType;

public class IMCmdTeamGetMemberFromWB extends DTCmd {
	public WBType mType = WBType.Friends;
	
	public IMCmdTeamGetMemberFromWB() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_GET_MEMBER_FROM_WB.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mType.getValue() != 1 && mType.getValue() != 2) {
			DTLog.logError();
			return;
		}
		
		mSendJsonObject.put("type", mType.getValue());
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
