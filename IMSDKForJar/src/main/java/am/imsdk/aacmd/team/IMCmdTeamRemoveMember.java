package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

public class IMCmdTeamRemoveMember extends DTCmd {
	public long mTeamID;
	public long mUID;

	public IMCmdTeamRemoveMember() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_REMOVE_MEMBER.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		if (mUID == 0) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("teamid", mTeamID);
		mSendJsonObject.put("uid", mUID);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

		teamInfo.remove(mUID);
		teamInfo.saveFile();

		if (teamInfo.mTeamType == TeamType.Group) {
			String customUserID = IMUsersMgr.getInstance().getCustomUserID(mUID);

			if (customUserID.length() == 0) {
				DTLog.logError();
				return;
			}

			DTNotificationCenter.getInstance().postNotification(
					"GroupMemberRemoved:" + teamInfo.mTeamID, customUserID);
		}
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
