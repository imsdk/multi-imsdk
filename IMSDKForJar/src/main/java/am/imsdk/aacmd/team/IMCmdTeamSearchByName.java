package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdTeamSearchByName extends DTCmd {

	public int mType;
	public String mGroupName;
	public int mStart;
	public int mEnd;

	public IMCmdTeamSearchByName() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_SEARCH.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		mSendJsonObject.put("type", mType);
		mSendJsonObject.put("team_name", mGroupName);
		mSendJsonObject.put("start", mStart);
		mSendJsonObject.put("end", mEnd);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
	
	
}
