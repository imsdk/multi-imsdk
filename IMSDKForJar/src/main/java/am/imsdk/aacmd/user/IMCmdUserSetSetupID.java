package am.imsdk.aacmd.user;

import org.json.JSONException;

import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdUserSetSetupID extends DTCmd {
	public long mSetupID;

	public IMCmdUserSetSetupID() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_SET_SETUP_ID.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		mSendJsonObject.put("setupid", mSetupID);
	}
}
