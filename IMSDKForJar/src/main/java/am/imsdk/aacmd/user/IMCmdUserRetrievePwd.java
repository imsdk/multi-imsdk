package am.imsdk.aacmd.user;

import org.json.JSONException;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMParamJudge;
import android.text.TextUtils;

public class IMCmdUserRetrievePwd extends DTCmd {
	public String mPhoneNum;
	public String mPassword;

	public IMCmdUserRetrievePwd() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_RETRIEVE_PASSWORD.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		
		if (!IMParamJudge.isCustomUserIDLegal(mPhoneNum)) {
			DTLog.logError();
			return;
		}

		if (!IMParamJudge.isPasswordLegal(mPassword)) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("platform", "a");
		
		if (!TextUtils.isEmpty(mPhoneNum)) {
			if(!mPhoneNum.contains("+86")) {
				mPhoneNum = "+86" + mPhoneNum;
			}
			mSendJsonObject.put("phonenum", mPhoneNum);
		}
		
		if (!TextUtils.isEmpty(mPassword)) {
			mSendJsonObject.put("password", DTTool.getMD5String(mPassword));
		}
	}
}