package am.imsdk.aacmd.user;

import org.json.JSONException;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMParamJudge;
import android.text.TextUtils;

/**
 * 修改密码
 * @author xieyuan
 */
public class IMCmdUserModifyPwd extends DTCmd {
	public String mOldPassword;
	public String mNewPassword;

	public IMCmdUserModifyPwd() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_MODIFY_PASSWORD.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {

		if (!IMParamJudge.isPasswordLegal(mNewPassword)) {
			DTLog.logError();
			return;
		}
		
		if (!IMParamJudge.isPasswordLegal(mOldPassword)) {
			DTLog.logError();
			return;
		}
		
		if(mNewPassword.equals(mOldPassword)) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("platform", "a");
		
		if (!TextUtils.isEmpty(mOldPassword)) {
			mSendJsonObject.put("oldpsw", DTTool.getMD5String(mOldPassword));
		}
		if (!TextUtils.isEmpty(mNewPassword)) {
			mSendJsonObject.put("newpsw", DTTool.getMD5String(mNewPassword));
		}
	}
}