package am.imsdk.aacmd.user;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMParamJudge;

public final class IMCmdUserTokenLogin extends DTCmd {
	public String mLoginToken;

	public IMCmdUserTokenLogin() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_TOKEN_LOGIN.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {
		if(!IMParamJudge.isTokenLegal(mLoginToken)) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("token", mLoginToken);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
