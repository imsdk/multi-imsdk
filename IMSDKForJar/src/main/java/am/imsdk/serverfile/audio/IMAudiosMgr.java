package am.imsdk.serverfile.audio;

import java.util.HashMap;

public final class IMAudiosMgr {
	private HashMap<String, IMAudio> mMapAudios = new HashMap<String, IMAudio>();

	public IMAudio getAudio(String fileID) {
		IMAudio audio = mMapAudios.get(fileID);

		if (audio != null) {
			return audio;
		}

		audio = new IMAudio();

		audio.mFileID = fileID;
		audio.readFromFile();
		mMapAudios.put(fileID, audio);
		return audio;
	}

	public IMAudio getAudio(byte[] buffer) {
		IMAudio audio = new IMAudio();

		for (int i = 0; i < 100000000; i++) {
			audio.mFileID = i + "";

			if (!audio.isLocalFileExist() && !mMapAudios.containsKey(audio.mFileID)) {
				break;
			}
		}
		
		if (!audio.isLocalFileExist()) {
			audio.mBuffer = buffer;
			mMapAudios.put(audio.mFileID, audio);
		}
		
		return audio;
	}

	public void replaceClientFileID(String clientFileID, String fileID) {
		IMAudio audio = mMapAudios.get(clientFileID);
		
		if (audio != null) {
			mMapAudios.put(clientFileID, null);
			mMapAudios.put(fileID, audio);
		}
	}

	// singleton
	private volatile static IMAudiosMgr sSingleton;

	private IMAudiosMgr() {
	}

	public static IMAudiosMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMAudiosMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMAudiosMgr();
				}
			}
		}

		return sSingleton;
	}
	// singleton end
}
