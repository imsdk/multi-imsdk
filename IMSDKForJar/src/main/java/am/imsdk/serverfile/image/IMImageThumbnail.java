package am.imsdk.serverfile.image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.HashMap;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;

public final class IMImageThumbnail extends DTLocalModel {
	public String mFileID = "";
	private SoftReference<Bitmap> mBitmapReference = new SoftReference<Bitmap>(null);
	private SoftReference<byte[]> mBufferReference = new SoftReference<byte[]>(null);
	public int mWidth;
	public int mHeight;

	public IMImageThumbnail() {
		addDirectory("II");
		addDecryptedDirectory("IMImage");
	}

	@Override
	public boolean generateLocalFullPath() {
		if (mFileID.length() == 0) {
			return false;
		}

		if (mWidth == 0) {
			DTLog.logError();
			return false;
		}

		if (mHeight == 0) {
			DTLog.logError();
			return false;
		}

		mLocalFileName = DTTool.getMD5String(mFileID) + "_" + mWidth + "_" + mHeight;
		mDecryptedLocalFileName = mFileID + "_" + mWidth + "_" + mHeight + ".jpg";
		return true;
	}

	@Override
	public boolean saveFile() {
		if (!generateLocalFullPath()) {
			DTLog.logError();
			return false;
		}

		if (mBufferReference.get() != null) {
			protectedWriteToFile(mBufferReference.get(), getLocalFullPath());
			return false;
		}

		if (mBitmapReference.get() == null) {
			DTLog.logError();
			return false;
		}

		FileOutputStream outputSteam;

		try {
			outputSteam = new FileOutputStream(getLocalFullPath());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			DTLog.logError();
			return false;
		}

		mBitmapReference.get().compress(CompressFormat.JPEG, 50, outputSteam);

		try {
			outputSteam.flush();
			outputSteam.close();
		} catch (IOException e) {
			e.printStackTrace();
			DTLog.logError();
			return false;
		}

		IMThumbnailInfo info = new IMThumbnailInfo();

		info.mFileID = mFileID;
		info.readFromFile();

		String key = mWidth + "_" + mHeight;

		info.mMapThumbExist.put(key, true);

		return info.saveFile();
	}

	@Override
	public boolean readFromFile() {
		if (!generateLocalFullPath()) {
			return false;
		}

		if (isLocalFileExist()) {
			File file = new File(getLocalFullPath());
			FileInputStream fileInputStream;

			try {
				fileInputStream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				DTLog.logError();
				return false;
			}

			byte[] buffer = null;

			try {
				buffer = new byte[fileInputStream.available()];
				fileInputStream.read(buffer);
				fileInputStream.close();
			} catch (IOException e) {
				DTLog.logError();
				return false;
			}

			mBufferReference = new SoftReference<byte[]>(buffer);

			Bitmap bitmap = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);

			if (bitmap == null) {
				DTLog.logError();
				return false;
			}

			mBitmapReference = new SoftReference<Bitmap>(bitmap);

			return true;
		}

		IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(mFileID);

		if (photo.getBitmap() != null) {
			if (photo.getBuffer() == null) {
				DTLog.logError();
				return false;
			}

			Bitmap bitmap = Bitmap.createScaledBitmap(photo.getBitmap(), mWidth,
					mHeight, false);

			if (bitmap == null) {
				DTLog.logError();
				return false;
			}

			mBitmapReference = new SoftReference<Bitmap>(bitmap);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			if (!bitmap.compress(CompressFormat.JPEG, 50, outputStream)) {
				DTLog.logError();

				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					DTLog.logError();
					return false;
				}

				return false;
			}

			byte[] buffer = outputStream.toByteArray();

			mBufferReference = new SoftReference<byte[]>(buffer);

			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				return false;
			}

			saveFile();
			return true;
		}

		IMThumbnailInfo info = new IMThumbnailInfo();

		info.mFileID = mFileID;

		if (!info.readFromFile()) {
			return false;
		}

		if (info.mMapThumbExist.keySet().size() == 0) {
			return false;
		}

		int thumbWidth = 0;
		int thumbHeight = 0;

		for (String key : info.mMapThumbExist.keySet()) {
			String[] strings = key.split("_");

			if (strings.length != 2) {
				DTLog.logError();
				return false;
			}

			int width = Integer.parseInt(strings[0]);
			int height = Integer.parseInt(strings[1]);

			if (width >= mWidth && height >= mHeight) {
				thumbWidth = width;
				thumbHeight = height;
				break;
			}
		}

		if (thumbWidth == 0) {
			return false;
		}

		if (thumbWidth == mWidth && thumbHeight == mHeight) {
			DTLog.logError();
			return false;
		}

		IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(mFileID,
				thumbWidth, thumbHeight);

		if (thumbnail.getBitmap() == null) {
			DTLog.logError();
			return false;
		}

		Bitmap bitmap = Bitmap.createScaledBitmap(thumbnail.getBitmap(), mWidth,
				mHeight, false);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		if (!bitmap.compress(CompressFormat.JPEG, 50, outputStream)) {
			DTLog.logError();

			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				return false;
			}

			return false;
		}

		byte[] buffer = outputStream.toByteArray();

		try {
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			DTLog.logError();
			return false;
		}

		mBufferReference = new SoftReference<byte[]>(buffer);

		saveFile();
		return true;
	}

	public Bitmap getBitmap() {
		if (mBitmapReference.get() == null && mBufferReference.get() == null) {
			readFromFile();
		}
		
		if (mBitmapReference.get() == null && mBufferReference.get() != null
				&& mBufferReference.get().length != 0) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(mBufferReference.get(), 0,
					mBufferReference.get().length);
			
			mBitmapReference = new SoftReference<Bitmap>(bitmap);
		}

		return mBitmapReference.get();
	}

	public byte[] getBuffer() {
		byte[] buffer = mBufferReference.get();

		if (buffer == null) {
			buffer = new byte[0];
		}

		return buffer;
	}

	public long getBufferLength() {
		if (mBufferReference.get() != null) {
			return mBufferReference.get().length;
		} else {
			return 0;
		}
	}

	public void setBuffer(byte[] buffer) {
		mBufferReference = new SoftReference<byte[]>(buffer);
	}

	public void setBitmap(Bitmap bitmap) {
		mBitmapReference = new SoftReference<Bitmap>(bitmap);
	}

	private static class IMThumbnailInfo extends DTLocalModel {
		private String mFileID = "";
		private HashMap<String, Boolean> mMapThumbExist = new HashMap<String, Boolean>();

		public IMThumbnailInfo() {
			addDirectory("II");
			addDecryptedDirectory("IMImage");
		}

		@Override
		public boolean generateLocalFullPath() {
			if (mFileID.length() == 0) {
				return false;
			}

			mLocalFileName = DTTool.getMD5String(mFileID) + "_ti";
			mDecryptedLocalFileName = mFileID + "thumbinfo.txt";
			return true;
		}
	}
	
	public Uri getUri() {
		return Uri.fromFile(new File(getLocalFullPath()));
	}
}
