package am.imsdk.serverfile.image;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.imsdk.model.IMParamJudge;

// 只管理内存
public final class IMImagesMgr {
	private HashMap<String, DTLocalModel> mMapImages = new HashMap<String, DTLocalModel>();

	public IMImagePhoto getPhoto(String fileID) {
		if (!IMParamJudge.isFileIDLegal(fileID)) {
			DTLog.logError();
			return null;
		}

		IMImagePhoto photo = (IMImagePhoto) mMapImages.get(fileID);

		if (photo != null) {
			return photo;
		}

		photo = new IMImagePhoto();

		photo.mFileID = fileID;
		photo.readFromFile();
		mMapImages.put(fileID, photo);
		return photo;
	}

	public IMImagePhoto getPhoto(Bitmap bitmap) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);

		byte[] buffer = byteArrayOutputStream.toByteArray();

		IMImagePhoto photo = new IMImagePhoto();

		for (int i = 0; i < 100000; i++) {
			photo.mFileID = i + "";

			if (!photo.isLocalFileExist() && !mMapImages.containsKey(photo.mFileID)) {
				break;
			}
		}

		photo.setBitmap(bitmap);
		photo.setBuffer(buffer);
		mMapImages.put(photo.mFileID, photo);
		return photo;
	}

	public IMImagePhoto getPhoto(String imgPath, boolean isLocal) {
		FileInputStream fStream = null;

		try {
			fStream = new FileInputStream(imgPath);
		} catch (FileNotFoundException e) {
			DTLog.logError();
			return null;
		}

		byte[] buffer = null;

		try {
			buffer = new byte[fStream.available()];
			fStream.read(buffer);
			fStream.close();
		} catch (IOException e) {
			DTLog.logError();
			return null;
		}

		IMImagePhoto photo = new IMImagePhoto();

		for (int i = 0; i < 100000; i++) {
			photo.mFileID = i + "";

			if (!photo.isLocalFileExist() && !mMapImages.containsKey(photo.mFileID)) {
				break;
			}
		}

		photo.setBuffer(buffer);
		mMapImages.put(photo.mFileID, photo);
		return photo;

	}

	public IMImageThumbnail getThumbnail(String fileID, int width, int height) {
		if (fileID.length() == 0) {
			DTLog.logError();
			return null;
		}

		if (width == 0) {
			DTLog.logError();
			return null;
		}

		if (height == 0) {
			DTLog.logError();
			return null;
		}

		String key = fileID + "_" + width + "_" + height;
		IMImageThumbnail thumbnail = (IMImageThumbnail) mMapImages.get(key);

		if (thumbnail != null) {
			return thumbnail;
		}

		thumbnail = new IMImageThumbnail();

		thumbnail.mFileID = fileID;
		thumbnail.mWidth = width;
		thumbnail.mHeight = height;
		thumbnail.readFromFile();

		mMapImages.put(key, thumbnail);
		return thumbnail;
	}

	public void replacePhotoFileID(String oldFileID, String newFileID) {
		if (oldFileID == null || oldFileID.length() == 0) {
			DTLog.logError();
			return;
		}

		if (!IMParamJudge.isFileIDLegal(newFileID)) {
			DTLog.logError();
			return;
		}

		IMImagePhoto photo = getPhoto(oldFileID);

		if (photo == null) {
			DTLog.logError();
			return;
		}

		photo.removeFile();  //删除图片
		deleteSimilarThumbnail(oldFileID);  //删除其对应的缩略图

		mMapImages.put(oldFileID, null);
		photo.mFileID = newFileID;
		mMapImages.put(newFileID, photo);
	}

	/**
	 * 删除相似的缩略图
	 * @param fileID 文件ID
	 */
	private void deleteSimilarThumbnail(String fileID) {
		String strPattern = fileID + "_[0-9]{3}_[0-9]{3}";
		Pattern p = Pattern.compile(strPattern);

		Iterator iterator = mMapImages.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			String fid = entry.getKey().toString();

			Matcher m = p.matcher(fid);

			if(m.matches()) {
				IMImageThumbnail thumbnail = (IMImageThumbnail) mMapImages.get(fid);
				if (thumbnail != null) {
					thumbnail.removeFile();
				}

				mMapImages.put(fid, null);
			}
		}

	}

	// singleton
	private volatile static IMImagesMgr sSingleton;

	private IMImagesMgr() {
	}

	public static IMImagesMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMImagesMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMImagesMgr();
				}
			}
		}

		return sSingleton;
	}
	// singleton end
}
