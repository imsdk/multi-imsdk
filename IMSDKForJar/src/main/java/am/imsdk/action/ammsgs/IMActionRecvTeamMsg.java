package am.imsdk.action.ammsgs;

import org.json.JSONException;
import org.json.JSONObject;

import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsgsMgr;

// 1. 获取CustomUserID
// 2. 生成teamMsg
public final class IMActionRecvTeamMsg extends IMAction {
	public IMTeamMsg mTeamMsg;
	public JSONObject mRecvJsonObject;
	private long mFromUID;
	
	public IMActionRecvTeamMsg() {
		mStepCount = 2;
	}
	
	@Override
	public void onActionBegan() {
		if (mRecvJsonObject == null) {
			doneWithIMSDKError();
			return;
		}
		
		if (mTeamMsg != null) {
			doneWithIMSDKError();
			return;
		}
		
		try {
			mFromUID = mRecvJsonObject.getLong("fromuid");
		} catch (JSONException e) {
			e.printStackTrace();
			doneWithIMSDKError();
			return;
		}
		
		if (mFromUID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1:
		{
			checkBeginGetCustomUserID(mFromUID);
		}
			break;
		case 2:
		{
			mTeamMsg = IMTeamMsgsMgr.getInstance().getRecvTeamMsg(mRecvJsonObject);
			
			if (mTeamMsg == null) {
				done();
				return;
			}
			
			if (!IMParamJudge.isCustomUserIDLegal(mTeamMsg.mFromCustomUserID)) {
				doneWithIMSDKError();
				return;
			}
			
			done();
		}
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onActionDone() {
	}
}
