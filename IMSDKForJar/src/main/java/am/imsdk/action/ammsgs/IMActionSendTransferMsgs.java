package am.imsdk.action.ammsgs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTFileTool;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.DataTool;
import am.imsdk.action.IMAction;
import am.imsdk.action.fileserver.IMActionUploadFile;
import am.imsdk.demo.util.FileUtils;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;

/**
 * 批量上传转发消息（即A中转， 代替B、C向对方发送消息）
 * 1. 上传图片并拼接json
 * 2. 上传音频并拼接json
 * 3. 整理文本消息并拼接json
 * 4. 发送打包后的消息
 */
public class IMActionSendTransferMsgs extends IMAction {
    private static final int IMTextMaxLength = 400;

    public List<Uri> mPicList; // 图片集
    public String mAudioPath; // 音频存储路径
    public long mAudioDuration;
    public String mMsgText;      // 消息文本
    public String mHintText1;     // 只发给对象B的文本
    public String mHintText2;     // 只发给对象C的文本
    public String mCustomUserID1; // 对象B
    public String mCustomUserID2; // 对象C
    public HashMap<String, Object> mTags1; // 对象B的附加消息
    public HashMap<String, Object> mTags2; // 对象C的附加消息
    public String mNickName1;  // 对象B的昵称
    public String mNickName2;  // 对象C的昵称

    private List<JSONObject> mJsonList = new ArrayList<JSONObject>();

    private int picIndex = 0; // 当前图片上传索引

    private int taskCount = 0; // 总任务数
    private int taskIndex = 0; // 当前已进行的任务数

    public IMActionSendTransferMsgs() {
        mStepCount = 4;
    }

    @Override
    public void onActionBegan() {
        if (!TextUtils.isEmpty(mAudioPath)) {
            File audio = new File(mAudioPath);

            if (!audio.exists()) {
                DTLog.logError();
                return;
            }
        }

        if (TextUtils.isEmpty(mCustomUserID1)) {
            DTLog.logError();
            return;
        }

        if (TextUtils.isEmpty(mCustomUserID2)) {
            DTLog.logError();
            return;
        }

        try {
            Long.parseLong(mCustomUserID1);
        } catch (NumberFormatException e) {
            DTLog.logError();
            return;
        }

        try {
            Long.parseLong(mCustomUserID2);
        } catch (NumberFormatException e) {
            DTLog.logError();
            return;
        }

        taskCount = 0;
        taskIndex = 0;

        if(mPicList != null && mPicList.size() > 0) {
            taskCount += mPicList.size();
        }

        if (!TextUtils.isEmpty(mAudioPath)) {
            taskCount++;
        }

        if (!TextUtils.isEmpty(mMsgText) || !TextUtils.isEmpty(mHintText1) || !TextUtils.isEmpty(mHintText2)) {
            taskCount++;
        }
    }

    @Override
    public void onActionStepBegan(int stepNumber) {
        /**
         * 1. 上传图片并拼接json
         * 2. 上传音频并拼接json
         * 3. 整理文本消息并拼接json
         * 4. 发送打包后的消息
         */
        switch (stepNumber) {
            case 1: {
                if (mPicList == null || mPicList.size() <= 0) {
                    nextStep();
                    return;
                }

                if (picIndex >= mPicList.size()) {
                    nextStep();
                    return;
                }

                DTAppEnv.getThreadHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = getPhoto(mPicList.get(picIndex));

                        if (bitmap == null) {
                            DTAppEnv.postOnUIThread(new Runnable() {
                                @Override
                                public void run() {
                                    done("bitmap not found");
                                }
                            });
                            return;
                        }

                        final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(bitmap);
                        if (photo != null && photo.saveFile()) {
                            uploadAndPack(IMUserMsg.UserMsgType.Photo, photo.mFileID, (picIndex == mPicList.size() - 1));
                        } else {
                            DTAppEnv.postOnUIThread(new Runnable() {
                                @Override
                                public void run() {
                                    done("bitmap create failed");
                                }
                            });
                        }
                    }
                });
                break;
            }
            case 2: {
                if (TextUtils.isEmpty(mAudioPath)) {
                    nextStep();
                    return;
                }

                DTAppEnv.getThreadHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        byte[] buff = DTFileTool.getContentOfFile(mAudioPath);

                        if (buff == null || buff.length <= 0) {
                            done("audio file is not found");
                            return;
                        }

                        IMAudio audio = IMAudiosMgr.getInstance().getAudio(buff);

                        if (audio != null && audio.saveFile()) {
                            uploadAndPack(IMUserMsg.UserMsgType.Audio, audio.mFileID, true);
                        } else {
                            DTAppEnv.postOnUIThread(new Runnable() {
                                @Override
                                public void run() {
                                    done("audio create failed");
                                }
                            });
                        }
                    }
                });
                break;
            }
            case 3: {
                if (!TextUtils.isEmpty(mMsgText)) {
                    if (mMsgText.length() > IMTextMaxLength) {
                        uploadAndPack(IMUserMsg.UserMsgType.Normal, mMsgText, true);
                    } else {
                        packData(IMUserMsg.UserMsgType.Normal, mMsgText, 1);
                        packData(IMUserMsg.UserMsgType.Normal, mMsgText, 2);
                    }
                }


                if(!TextUtils.isEmpty(mHintText1)) {
                    packData(IMUserMsg.UserMsgType.Normal, mHintText1, 1);
                }

                if(!TextUtils.isEmpty(mHintText2)) {
                    packData(IMUserMsg.UserMsgType.Normal, mHintText2, 2);
                }

                nextStep();
                break;
            }
            case 4: {
                JSONObject fullObject = new JSONObject();

                try {
                    JSONArray msgArr = new JSONArray();

                    for (JSONObject object : mJsonList) {
                        msgArr.put(object);
                    }

                    fullObject.put("msglist", msgArr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                DTLog.e("Debug", fullObject.toString());
                DoTransferTask task = new DoTransferTask();
                task.execute(fullObject.toString());
                break;
            }
        }
    }

    private Bitmap getPhoto(Uri originalUri) {
        if (originalUri == null) {
            return null;
        }

        final String path = FileUtils.getPath(DTAppEnv.getContext(), originalUri);
        final BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        int inSampleSize;

        inSampleSize = options.outWidth * options.outHeight * 4 / 262144;
        inSampleSize = (int) Math.sqrt(inSampleSize);

        options.inSampleSize = inSampleSize <= 0 ? 1 : inSampleSize;
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    /**
     * 上传并打包
     *
     * @param msgType         消息类型
     * @param fileIDorContent 文件ID或内容
     * @param isOver          列表循环是否结束
     */
    private void uploadAndPack(final IMUserMsg.UserMsgType msgType, final String fileIDorContent, final boolean isOver) {
        if (TextUtils.isEmpty(fileIDorContent)) {
            return;
        }

        byte[] tempByte = null;

        switch (msgType) {
            case Photo: {
                IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(fileIDorContent);

                if (photo == null) {
                    DTAppEnv.postOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            doneWithIMSDKError();
                        }
                    });
                    return;
                } else {
                    if (photo.getBuffer() == null || photo.getBufferLength() == 0) {
                        DTAppEnv.postOnUIThread(new Runnable() {
                            @Override
                            public void run() {
                                doneWithIMSDKError();
                            }
                        });
                        return;
                    }
                }

                tempByte = photo.getBuffer();
                break;
            }
            case Audio: {
                IMAudio audio = IMAudiosMgr.getInstance().getAudio(fileIDorContent);

                if (audio == null || audio.mBuffer == null || audio.mBuffer.length == 0) {
                    DTAppEnv.postOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            doneWithIMSDKError();
                        }
                    });
                    return;
                }

                if (!audio.isLocalFileExist()) {
                    audio.saveFile();
                }

                tempByte = audio.mBuffer;
                break;
            }
            case Normal:
            case Custom: {
                // 构造FileText
                IMFileText fileText = IMFileTextsMgr.getInstance().getFileTextWithText(fileIDorContent);

                if (fileText == null) {
                    DTLog.logError();
                    return;
                }

                fileText.saveFile();

                try {
                    tempByte = DTTool.getBase64EncodedString(fileText.mText).getBytes(
                            "UTF8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    DTAppEnv.postOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            doneWithIMSDKError();
                        }
                    });
                    return;
                }

                break;
            }
            default: {
                DTAppEnv.postOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        doneWithIMSDKError();
                    }
                });
                return;
            }
        }

        final IMActionUploadFile action = new IMActionUploadFile();

        action.mOnActionFailedListener = new OnActionFailedListener() {
            @Override
            public void onActionFailed(final String error) {
                done(error);
            }
        };

        action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
            @Override
            public void onActionPartiallyDone(double percentage) {
                System.out.println("-----------taskIndex:" + taskIndex + ", percentage:" + percentage + ", taskCount:" + taskCount);
                done((taskIndex * 100 + percentage) * (1d / taskCount));
            }
        };

        action.mOnActionDoneListener = new OnActionDoneListener() {
            @Override
            public void onActionDone() {
                switch (msgType) {
                    case Normal:
                    case Custom: {
                        IMFileText fileText = IMFileTextsMgr.getInstance()
                                .getFileText(fileIDorContent);

                        if (fileText == null) {
                            doneWithIMSDKError();
                            return;
                        }

                        if (!fileText.isLocalFileExist()) {
                            doneWithIMSDKError();
                            return;
                        }

                        fileText.removeFile();
                        IMFileTextsMgr.getInstance().replace(
                                fileText.mFileID, action.mFileID);
                        fileText.mFileID = action.mFileID;
                        fileText.saveFile();

                        // 创建content
                        JSONObject jsonContentObject = new JSONObject();
                        try {
                            jsonContentObject.put("fileID", fileText.mFileID);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DTLog.logError();
                        }

                        if (msgType == IMUserMsg.UserMsgType.Normal) {
                            packData(IMUserMsg.UserMsgType.NormalFileText, jsonContentObject.toString(), 1);
                            packData(IMUserMsg.UserMsgType.NormalFileText, jsonContentObject.toString(), 2);
                        } else {
                            packData(IMUserMsg.UserMsgType.CustomFileText, jsonContentObject.toString(), 1);
                            packData(IMUserMsg.UserMsgType.CustomFileText, jsonContentObject.toString(), 2);
                        }
                    }
                    break;
                    case Audio: {
                        IMAudio audio = IMAudiosMgr.getInstance().getAudio(fileIDorContent);

                        if (audio == null || audio.mBuffer == null || audio.mBuffer.length <= 0) {
                            doneWithIMSDKError();
                            return;
                        }

                        audio.removeFile();
                        IMAudiosMgr.getInstance().replaceClientFileID(audio.mFileID,
                                action.mFileID);
                        audio.mFileID = action.mFileID;
                        audio.saveFile();

                        // 创建content
                        JSONObject jsonContentObject = new JSONObject();
                        try {
                            jsonContentObject.put("fileID", audio.mFileID);
                            jsonContentObject.put("durationInMilliSeconds", mAudioDuration);
                            jsonContentObject.put("format", "AMR_NB");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DTLog.logError();
                        }

                        packData(msgType, jsonContentObject.toString(), 1);
                        packData(msgType, jsonContentObject.toString(), 2);
                    }
                    break;
                    case Photo: {
                        IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(fileIDorContent);

                        if (photo == null) {
                            doneWithIMSDKError();
                            return;
                        }

                        IMImagesMgr.getInstance().replacePhotoFileID(photo.mFileID,
                                action.mFileID);
                        photo.mFileID = action.mFileID;
                        photo.saveFile();

                        // 创建content
                        JSONObject jsonContentObject = new JSONObject();
                        try {
                            jsonContentObject.put("fileID", photo.mFileID);
                            jsonContentObject.put("width", photo.getBitmap().getWidth());
                            jsonContentObject.put("height", photo.getBitmap().getHeight());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DTLog.logError();
                        }

                        //打包并拼接Json数据
                        packData(msgType, jsonContentObject.toString(), 1);
                        packData(msgType, jsonContentObject.toString(), 2);
                    }
                    break;
                    default: {
                        doneWithIMSDKError();
                        return;
                    }
                }

                taskIndex++;

                //列表是否读取完毕， 是否需要进行下一步操作
                if (isOver) {
                    nextStep();
                } else {
                    picIndex++;
                    beginStep(getCurrentStep());
                }
            }
        };

        action.mBuffer = tempByte;

        DTAppEnv.postOnUIThread(new Runnable() {
            @Override
            public void run() {
                action.begin();
            }
        });
    }

    /**
     * 根据内容打包数据
     *
     * @param msgType    消息类型
     * @param msgContent 消息内容
     * @param userNum    1,2---用户1还是用户2
     */
    private void packData(final IMUserMsg.UserMsgType msgType, final String msgContent, int userNum) {
        long actionTime = System.currentTimeMillis() / 1000;

        switch (userNum) {
            case 1:
                try {
                    JSONObject jsonObject1 = new JSONObject();

                    //发送给对象B的消息
                    jsonObject1.put("sendtime", actionTime);
                    jsonObject1.put("msgid", 0l);
                    jsonObject1.put("touid", Long.parseLong(mCustomUserID1));
                    jsonObject1.put("fromuid", Long.parseLong(mCustomUserID2));
                    jsonObject1.put("msgtype", msgType.getValue());
                    jsonObject1.put("msgcontent", msgContent);
                    jsonObject1.put("cmdtype", 35101);

                    JSONObject extraData1 = new JSONObject();
                    extraData1.put("nickName", mNickName2);

                    if (mTags1 != null && mTags1.size() > 0) {
                        for (String key : mTags1.keySet()) {
                            extraData1.put(key, mTags1.get(key));
                        }
                    }
                    jsonObject1.put("extraData", extraData1.toString());

                    mJsonList.add(jsonObject1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    JSONObject jsonObject2 = new JSONObject();
                    //发送给对象C的消息
                    jsonObject2.put("sendtime", actionTime);
                    jsonObject2.put("msgid", 0l);
                    jsonObject2.put("touid", Long.parseLong(mCustomUserID2));
                    jsonObject2.put("fromuid", Long.parseLong(mCustomUserID1));
                    jsonObject2.put("msgtype", msgType.getValue());
                    jsonObject2.put("msgcontent", msgContent);
                    jsonObject2.put("cmdtype", 35101);

                    JSONObject extraData2 = new JSONObject();
                    extraData2.put("nickName", mNickName1);

                    if (mTags2 != null && mTags2.size() > 0) {
                        for (String key : mTags2.keySet()) {
                            extraData2.put(key, mTags2.get(key));
                        }
                    }
                    jsonObject2.put("extraData", extraData2.toString());

                    mJsonList.add(jsonObject2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private class DoTransferTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            if(params.length <= 0) {
                DTLog.logError();
                return "";
            }

            String strJson = params[0];

            if(TextUtils.isEmpty(strJson)) {
                DTLog.logError();
                return "";
            }

            String uriAPI = "http://" + DTAppEnv.sIMSDKIpAddress + ":3000/rest/message/transfer";// Post方式没有参数在这里
            URL url = null;
            String result = "";

            try {
                url = new URL(uriAPI);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            if (url == null) {
                DTLog.logError();
                return "";
            }

            byte[] contentBuff = strJson.getBytes(Charset.forName("UTF-8"));

            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);

                conn.setRequestProperty("Charset", "UTF-8");
                conn.setRequestProperty("Content-Length", String.valueOf(contentBuff.length));
                conn.setRequestProperty("Content-Type", "application/json");

                conn.setConnectTimeout(5 * 1000);

                OutputStream outStream = conn.getOutputStream();
                outStream.write(contentBuff);
                outStream.flush();
                outStream.close();

                System.out.println(conn.getResponseCode()); //响应代码 200表示成功
                if (conn.getResponseCode() == 200) {
                    InputStream inStream = conn.getInputStream();

                    result = new String(DataTool.readInputStream(inStream), "UTF-8");
                } else {
                    conn.disconnect();
                    DTLog.logError("http responseCode:" + conn.getResponseCode());
                }
            } catch (IOException e) {
                e.printStackTrace();
                DTLog.logError();
            } catch (Exception e) {
                e.printStackTrace();
                DTLog.logError();
            }

            DTLog.e("Debug", "transfer receive:" + result);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result.equals("{\"state\":0,\"msg\":\"ok\"}")) {
                done();
            } else {
                done("do transfer error:" + result);
            }
        }

    }
}
