package am.imsdk.action.ammsgs;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Xml;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicInteger;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.tool.DataTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.im.IMPushCmdIMSystemMsg;
import am.imsdk.aacmd.im.IMPushCmdIMTeamMsg;
import am.imsdk.aacmd.im.IMPushCmdIMUserMsg;
import am.imsdk.aacmd.user.IMPushCmdUserLoginConflict;
import am.imsdk.action.fileserver.IMActionFile;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

public class IMActionRecvOffLineMsg extends IMActionFile {
	public String jsonString;
	private long mUid;

	public IMActionRecvOffLineMsg() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if(ProcessTool.isSocketProcess()) {
			mUid = IMRemoteMyself.getInstance().getUID();
		} else {
			mUid = IMPrivateMyself.getInstance().getUID();
		}

		if(mUid <= 0) {
			doneWithIMSDKError();
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			new downloadOfflineMsg().execute();
			break;
		}
		case 2: {
			if(TextUtils.isEmpty(jsonString)) {
				doneWithIMSDKError();
				return;
			}

			analysePushData(jsonString);
			break;
		}
		default:
			break;
		}
	}

	@Override
	public long getTotalLengthExpected() {
		return 0;
	}

	private class downloadOfflineMsg extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("Uid", mUid);
				jsonObject.put("Count", 1000);
			} catch (JSONException e1) {
				e1.printStackTrace();
				DTLog.logError();
				return "";
			}

			String uriAPI = "http://" + DTAppEnv.sIMSDKIpAddress + ":3000/rest/offline_msg/list";// Post方式没有参数在这里
			URL url = null;
			try {
				url = new URL(uriAPI);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

			if (url == null) {
				DTLog.logError();
				return "";
			}

			byte[] contentBuff = jsonObject.toString().getBytes(Charset.forName("UTF-8"));

			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setDoOutput(true);

				conn.setRequestProperty("Charset", "UTF-8");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.setRequestProperty("Content-Length", String.valueOf(contentBuff.length));
				conn.setConnectTimeout(5 * 1000);
				OutputStream outStream = conn.getOutputStream();
				outStream.write(contentBuff);
				outStream.flush();
				outStream.close();
//				System.out.println(conn.getResponseCode()); //响应代码 200表示成功
				if (conn.getResponseCode() == 200) {
					InputStream inStream = conn.getInputStream();

					return new String(DataTool.readInputStream(inStream), "UTF-8");
				} else {
					conn.disconnect();
					DTLog.logError();
					return "";
				}
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				return "";
			} catch (Exception e1) {
				DTLog.logError();
				return "";
			}
		}
		
		@Override
		 protected void onPostExecute(String result) {
			super.onPostExecute(result);

			jsonString = result;
			IMActionRecvOffLineMsg.this.nextStep();
		}
		
	}

	public DTPushCmd getPushCmdObject(int cmdTypeValue) {
		if (cmdTypeValue == 0) {
			DTLog.logError();
			return null;
		}

		IMCmdType cmdType = IMCmdType.fromInt(cmdTypeValue);

		if (cmdType == null) {
			DTLog.logError();
			return null;
		}

		switch (cmdType) {
			case IM_PUSH_CMD_IM_USER_MSG:
				return new IMPushCmdIMUserMsg();
			case IM_PUSH_CMD_IM_SYSTEM_MSG:
				return new IMPushCmdIMSystemMsg();
			case IM_PUSH_CMD_IM_TEAM_MSG:
				return new IMPushCmdIMTeamMsg();
			case IM_PUSH_CMD_USER_LOGIN_CONFLICT:
				return new IMPushCmdUserLoginConflict();
			default:
				break;
		}

		DTLog.logError();
		DTLog.log(cmdType.toString());

		return null;
	}

	// 解析推送包
	private void analysePushData(String jsonString) {
		try {
			final JSONObject jsonObject = new JSONObject(jsonString);

			if (!jsonObject.has("msgs")) {
				DTLog.e("Debug", jsonString);
				done();
				return;
			}

			JSONArray msgsArr = jsonObject.getJSONArray("msgs");

			if(msgsArr != null && msgsArr.length() > 0) {
				final AtomicInteger checkCount = new AtomicInteger(msgsArr.length());

				for (int i = 0; i < msgsArr.length(); i++) {
					JSONObject msgObject = (JSONObject) msgsArr.get(i);

					DTLog.sign(true, "OnRecv Push:" + msgObject.toString());

					int cmdType = (Integer) msgObject.get("cmdtype");

					// 推送包
					final DTPushCmd cmdPush = this.getPushCmdObject(cmdType);

					if (cmdPush == null) {
						DTLog.logError(msgObject.toString());
						if(checkCount.decrementAndGet() == 0) {
							done();
							return;
						} else {
							continue;
						}
					}

					final JSONObject finalJsonObject = msgObject;

					DTAppEnv.postOnUIThread(new Runnable() {
						@Override
						public void run() {
							DTLog.sign(true, "OnRecv Push:" + cmdPush.getClass().getSimpleName());

							try {
								cmdPush.onRecv(finalJsonObject, true);
							} catch (JSONException e) {
								e.printStackTrace();
								DTLog.logError(finalJsonObject.toString());
							} finally {
								if(checkCount.decrementAndGet() == 0) {
									done();
								}
							}

						}
					});
				}
			} else {
				DTLog.e("Debug", jsonString);
				done();
			}

		} catch (JSONException e) {
			e.printStackTrace();

			done(jsonString);
			return;
		}

	}

}
