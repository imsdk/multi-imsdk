package am.imsdk.action.ammsgs;

import java.util.HashMap;

import am.dtlib.model.b.log.DTLog;


public final class IMFileTextsMgr {
	private HashMap<String, IMFileText> mMapFileTexts = new HashMap<String, IMFileText>();
	
	public IMFileText getFileText(String fileID) {
		IMFileText fileText = mMapFileTexts.get(fileID);
		
		if (fileText != null) {
			if (!(fileText instanceof IMFileText)) {
				DTLog.logError();
				return null;
			}
			
			return fileText;
		}
		
		fileText = new IMFileText();
		
		fileText.mFileID = fileID;
		fileText.readFromFile();
		mMapFileTexts.put(fileID, fileText);
		return fileText;
	}
	
	public IMFileText getFileTextWithText(String text) {
		IMFileText fileText = new IMFileText();
		
		for (int i = 0; i < 100000000; i++) {
			fileText.mFileID = i + "";
			
			if (!fileText.isLocalFileExist()) {
				break;
			}
		}
		
		if (fileText.isLocalFileExist()) {
			DTLog.logError();
			return null;
		}
		
		fileText.mText = text;
		mMapFileTexts.put(fileText.mFileID, fileText);
		
		return fileText;
	}
	
	public void replace(String clientFileID, String realFileID) {
		IMFileText fileText = mMapFileTexts.get(clientFileID);
		
		if (fileText != null) {
			mMapFileTexts.put(clientFileID, null);
			mMapFileTexts.put(realFileID, fileText);
		}
	}
	
	// singleton
	private volatile static IMFileTextsMgr sSingleton;

	private IMFileTextsMgr() {
	}

	public static IMFileTextsMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMFileTextsMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMFileTextsMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end
}
