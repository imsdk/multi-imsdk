package am.imsdk.action.ammsgs;

import am.dtlib.model.b.log.DTLog;
import imsdk.data.localchatmessagehistory.IMChatMessage;

import java.io.UnsupportedEncodingException;

import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.action.IMAction;
import am.imsdk.action.fileserver.IMActionUploadFile;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;

// 发送各种类型的UserMsg
// 只负责大容量消息转文件
// 维护内存IMUserMsgsMgr
// 1. 上传文件
// 2. 发送文本消息
public final class IMActionSendUserMsg extends IMAction {
	public static final int IMTextMaxLength = 400;
	public IMUserMsg mUserMsg;
	private IMUserMsg mRealUserMsg;
	private byte[] mBuffer;

	public IMActionSendUserMsg() {
		mStepCount = 2;
	}

	@Override
	public void begin() {
		if (mUserMsg == null) {
			doneWithIMSDKError();
			return;
		}

		super.begin();

		mUserMsg.mStatus = IMChatMessage.SENDING_OR_RECVING;

		if (!mUserMsg.saveFile()) {
			doneWithIMSDKError();
		}
	}

	@Override
	public void onActionDone() {
		if (mUserMsg.mContent.length() > IMTextMaxLength) {
			mRealUserMsg.mStatus = IMChatMessage.SUCCESS;
			mRealUserMsg.saveFile();
			mUserMsg.mMsgID = mRealUserMsg.mMsgID;
			mUserMsg.mServerSendTime = mRealUserMsg.mServerSendTime;
		}

		IMUserMsgsMgr.getInstance().moveUnsentUserMsgToBeSent(mUserMsg);
		mUserMsg.mStatus = IMChatMessage.SUCCESS;
		mUserMsg.saveFile();

		DTNotificationCenter.getInstance().postNotification(
				mUserMsg.getStatusChangedNotificationKey());
	}

	@Override
	public void onActionFailed(String error) {
		mUserMsg.mStatus = IMChatMessage.FAILURE;
		mUserMsg.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				mUserMsg.getStatusChangedNotificationKey());
	}

	@Override
	public void onActionBegan() {
		if (mUserMsg == null) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mUserMsgType == null) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mUserMsgType == UserMsgType.Unit) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mIsRecv) {
			doneWithIMSDKError();
			return;
		}

		if (!IMParamJudge.isCustomUserIDLegal(mUserMsg.mToCustomUserID)) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mContent == null) {
			mUserMsg.mContent = "";
		}

		if (!IMParamJudge.isCustomUserIDLegal(mUserMsg.mFromCustomUserID)) {
			mUserMsg.mFromCustomUserID = IMPrivateMyself.getInstance()
					.getCustomUserID();
		}

		if (!mUserMsg.mFromCustomUserID.equals(IMPrivateMyself.getInstance()
				.getCustomUserID())) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mClientSendTime == 0) {
			doneWithIMSDKError();
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			switch (mUserMsg.mUserMsgType) {
			case Normal:
			case Custom:
			case Order:
			case IMSDKNotice: {
				if (mUserMsg.mContent.length() <= IMTextMaxLength) {
					nextStep();
					return;
				}
				
				// 构造FileText
				IMFileText fileText = IMFileTextsMgr.getInstance().getFileTextWithText(
						mUserMsg.mContent);

				if(fileText == null) {
					DTLog.logError();
					return;
				}

				fileText.saveFile();

				try {
					mBuffer = DTTool.getBase64EncodedString(fileText.mText).getBytes(
							"UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					doneWithIMSDKError();
					return;
				}

				mRealUserMsg = new IMUserMsg();

				mRealUserMsg.mFromCustomUserID = IMPrivateMyself.getInstance()
						.getCustomUserID();
				mRealUserMsg.mToCustomUserID = mUserMsg.mToCustomUserID;
				mRealUserMsg.mClientSendTime = mUserMsg.mClientSendTime;
				mRealUserMsg.mTags = mUserMsg.mTags;

				if (mUserMsg.mUserMsgType == UserMsgType.Normal) {
					mRealUserMsg.mUserMsgType = UserMsgType.NormalFileText;
				} else {
					mRealUserMsg.mUserMsgType = UserMsgType.CustomFileText;
				}

//				mRealUserMsg.mContent = fileText.mFileID;
				mRealUserMsg.setFileID(fileText.mFileID);
			}
				break;
			case Audio: {
				IMAudio audio = IMAudiosMgr.getInstance()
						.getAudio(mUserMsg.getFileID());

				if (audio == null || audio.mBuffer == null || audio.mBuffer.length == 0) {
					doneWithIMSDKError();
					return;
				}

				if (!audio.isLocalFileExist()) {
					audio.saveFile();
				}

				mBuffer = audio.mBuffer;
			}
				break;
			case Photo: {
				IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
						mUserMsg.getFileID());

				if (photo == null || photo.getBuffer() == null || photo.getBufferLength() == 0) {
					if (!photo.readFromFile()) {
						doneWithIMSDKError();
						return;
					}
				}

				if (!photo.isLocalFileExist()) {
					photo.saveFile();
				}

				mBuffer = photo.getBuffer();
			}
				break;
			default:
				nextStep();
				return;
			}

			final IMActionUploadFile action = new IMActionUploadFile();

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					done(error);
				}
			};

			action.mOnActionDoneListener = new OnActionDoneListener() {
				@Override
				public void onActionDone() {
					switch (mUserMsg.mUserMsgType) {
					case Normal:
					case Custom: {
						if (mRealUserMsg == null) {
							doneWithIMSDKError();
							return;
						}

						IMFileText fileText = IMFileTextsMgr.getInstance()
								.getFileText(mRealUserMsg.getFileID());

						if (fileText == null) {
							doneWithIMSDKError();
							return;
						}

						if (!fileText.isLocalFileExist()) {
							doneWithIMSDKError();
							return;
						}

						fileText.removeFile();
						IMFileTextsMgr.getInstance().replace(
								fileText.mFileID, action.mFileID);
						fileText.mFileID = action.mFileID;
						fileText.saveFile();
					}
						break;
					case Audio: {
						mRealUserMsg = mUserMsg;

						IMAudio audio = IMAudiosMgr.getInstance().getAudio(
								mUserMsg.getFileID());

						audio.removeFile();
						IMAudiosMgr.getInstance().replaceClientFileID(audio.mFileID,
								action.mFileID);
						audio.mFileID = action.mFileID;
						audio.saveFile();
					}
						break;
					case Photo: {
						mRealUserMsg = mUserMsg;

						IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
								mUserMsg.getFileID());

						if(photo == null) {
							DTLog.logError();
							return;
						}

						IMImagesMgr.getInstance().replacePhotoFileID(photo.mFileID,
								action.mFileID);
						photo.mFileID = action.mFileID;
						photo.saveFile();
					}
						break;
					default:
						doneWithIMSDKError();
						return;
					}

					if (mRealUserMsg == null) {
						doneWithIMSDKError();
						return;
					}

					mRealUserMsg.setFileID(action.mFileID);
					nextStep();
				}
			};

			action.mBuffer = mBuffer;
			action.begin();
		}
			break;
		case 2: {
			IMActionSendBaseUserMsg action = new IMActionSendBaseUserMsg();

			if (mRealUserMsg != null) {
				action.mUserMsg = mRealUserMsg;
			} else {
				action.mUserMsg = mUserMsg;
			}

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					done();
				}
			};

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					done(error);
				}
			};
			action.mTimeoutInterval = mTimeoutInterval;
			action.begin();
		}
		default:
			break;
		}
	}
}
