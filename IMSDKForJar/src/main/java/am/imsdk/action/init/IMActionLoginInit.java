package am.imsdk.action.init;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.action.IMAction;
import am.imsdk.action.ammsgs.IMActionRecvOffLineMsg;
import am.imsdk.action.relation.IMActionRelationInit;
import am.imsdk.action.team.IMActionTeamInit;
import am.imsdk.model.IMPrivateMyself;
import imsdk.data.IMMyself;
import remote.service.data.IMRemoteMyself;

/**
 * 登录后初始化操作
 * i : SocketService 自启动并登录， 初始化在SocketService中
 * ii: Client触发登录，初始化在client中
 * Created by fzj on 2015/10/13.
 */
public class IMActionLoginInit extends IMAction {

    // 1. InitGroupInfo 初始化群信息
    // 2. InitRelations 初始化好友信息
    // 3. InitOffLineMsg 拉取离线消息

    private int mRetryTime;

    public IMActionLoginInit() {
        mStepCount = 3;
    }

    @Override
    public void onActionBegan() {
        if(ProcessTool.isSocketProcess()) {
            if(IMRemoteMyself.getInstance().getRemoteLoginStatus() != IMMyself.LoginStatus.Logined) {
                DTLog.logError();
                return;
            }
        } else {
            if(IMPrivateMyself.getInstance().getLoginStatus() != IMMyself.LoginStatus.Logined) {
                DTLog.logError();
                return;
            }
        }
    }

    @Override
    public void onActionStepBegan(int stepNumber) {
        switch (stepNumber) {
            case 1:
                final IMActionTeamInit teamInitAction = new IMActionTeamInit();

                teamInitAction.mOnActionFailedListener = new OnActionFailedListener() {
                    @Override
                    public void onActionFailed(String error) {
                        if(mRetryTime++ >= 3) {
                            nextStep();

                            mRetryTime = 0;
                            return;
                        }

                        beginStep(1);
                    }
                };

                teamInitAction.mOnActionDoneEndListener = new OnActionDoneEndListener() {
                    @Override
                    public void onActionDoneEnd() {
                        IMPrivateMyself.getInstance().setGroupInfoInited(true);

                        DTNotificationCenter.getInstance().postSyncNotification("GroupInitialized");

                        mRetryTime = 0;

                        nextStep();
                    }
                };

                teamInitAction.begin();
                break;
            case 2:
                IMActionRelationInit relationInitAction = new IMActionRelationInit();

                relationInitAction.mOnActionFailedListener = new OnActionFailedListener() {
                    @Override
                    public void onActionFailed(String error) {
                        if(mRetryTime++ >= 3) {
                            nextStep();

                            mRetryTime = 0;
                            return;
                        }

                        beginStep(2);
                    }
                };

                relationInitAction.mOnActionDoneEndListener = new OnActionDoneEndListener() {
                    @Override
                    public void onActionDoneEnd() {
                        IMPrivateMyself.getInstance().setRelationsInited(true);

                        DTNotificationCenter.getInstance().postSyncNotification("RelationsInitialized");

                        mRetryTime = 0;

                        nextStep();
                    }
                };

                relationInitAction.begin();
                break;
            case 3:
                final IMActionRecvOffLineMsg offlineMsgInitAction = new IMActionRecvOffLineMsg();

                offlineMsgInitAction.mOnActionFailedListener = new OnActionFailedListener() {
                    @Override
                    public void onActionFailed(String error) {
                        if(mRetryTime++ >= 3) {
                            nextStep();
                            return;
                        }

                        done();
                    }
                };

                offlineMsgInitAction.mOnActionDoneEndListener = new OnActionDoneEndListener() {
                    @Override
                    public void onActionDoneEnd() {
                        DTNotificationCenter.getInstance().postSyncNotification("OffLineMsgInitialized");

                        done();
                    }
                };

                offlineMsgInitAction.begin();
                break;
        }
    }

    @Override
    public void onActionDone() {

    }


}
