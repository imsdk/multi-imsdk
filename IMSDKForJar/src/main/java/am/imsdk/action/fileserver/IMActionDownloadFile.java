package am.imsdk.action.fileserver;

import android.os.AsyncTask;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.DataTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

// 1. 获取url
// 2. 获取文件
public class IMActionDownloadFile extends IMActionFile {
	public String mFileID = "";
	public int mRealFileLength;
	public byte[] mBuffer;

	protected String mRealURL = "";
	protected long mUID;
	protected static final int MAX_FILE_LENGTH = 1024 * 1024;

	public IMActionDownloadFile() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isFileIDLegal(mFileID)) {
			doneWithIMSDKError();
			return;
		}

		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			mUID = IMRemoteMyself.getInstance().getUID();
		} else {
			mUID = IMPrivateMyself.getInstance().getUID();
		}

		if (mUID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
			case 1: {
				FetchURLTask task = new FetchURLTask();

				task.mUID = mUID;
				task.execute(mFileID);
			}
			break;
			case 2: {
				DTLog.sign("mRealURL:" + mRealURL);

				if (mRealURL == null) {
					doneWithIMSDKError();
					return;
				}

				if (mRealURL.length() == 0) {
					doneWithIMSDKError();
					return;
				}

				DownFileTask task = new DownFileTask();

				task.execute(mRealURL);
			}
			break;
			default:
				break;
		}
	}

	private class FetchURLTask extends AsyncTask<String, Integer, String> {
		private long mUID = 0;

		@Override
		protected String doInBackground(String... params) {
			try {
				return fetchURL(params[0]);
			} catch (Exception e) {
				e.printStackTrace();
				DTLog.logError();
				doneWithServerError();
				return "";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (result == null) {
				result = "";
			}

			if (result.length() == 0) {
				doneWithServerError();
				return;
			}

			JSONObject jsonObject;

			try {
				jsonObject = new JSONObject(result);
			} catch (JSONException e) {
				e.printStackTrace();
				doneWithIMSDKError();
				return;
			}

			long errorCode = 0;

			try {
				errorCode = jsonObject.getLong("errcode");
			} catch (JSONException e) {
			}

			if (errorCode > 0) {
				done("Illegal FileID");
				return;
			}

			try {
				mRealURL = jsonObject.getString("url");
			} catch (JSONException e) {
				e.printStackTrace();
				doneWithIMSDKError();
				return;
			}

			if (mRealURL.length() == 0) {
				doneWithIMSDKError();
				return;
			}

			nextStep();
		}

		private String fetchURL(String fileID) throws IOException {
			String httpAddress = "http://" + IM_FILE_URL_DOMAIN_NAME + ":8889/file/download";

			URL url = null;
			HttpURLConnection conn = null;
			try {
				url = new URL(httpAddress);
				conn = (HttpURLConnection) url.openConnection();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

			if (url == null || conn == null) {
				httpAddress = "http://" + IM_FILE_URL_REAL_IP + ":8889/file/download";

				try {
					url = new URL(httpAddress);
					conn = (HttpURLConnection) url.openConnection();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}

				if (url == null || conn == null) {
					DTLog.logError();
					return "";
				}
			}

			String md5 = DTTool.getMD5String(fileID + mUID);

			String content = "fid=" + URLEncoder.encode(fileID, "UTF-8") + "&fromuid=" + mUID + "&vercode=" + URLEncoder.encode(md5, "UTF-8");
			byte[] contentBuff = content.getBytes(Charset.forName("UTF-8"));

			try {
				conn.setRequestMethod("POST");
				conn.setDoOutput(true);

				conn.setRequestProperty("Charset", "UTF-8");
				conn.setConnectTimeout(5 * 1000);
				OutputStream outStream = conn.getOutputStream();
				outStream.write(contentBuff);
				outStream.flush();
				outStream.close();
				if (conn.getResponseCode() == 200) {
					InputStream inStream = conn.getInputStream();

					return new String(DataTool.readInputStream(inStream), "UTF-8");
				} else {
					conn.disconnect();
					DTLog.logError();
					return "";
				}
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				return "";
			} catch (Exception e1) {
				DTLog.logError();
				return "";
			}
		}
	}

	private class DownFileTask extends AsyncTask<String, Integer, byte[]> {
		@Override
		protected byte[] doInBackground(String... params) {
			try {
				return downFile(params[0]);
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				doneWithServerError();
				return null;
			}
		}

		@Override
		protected void onPostExecute(byte[] result) {
			super.onPostExecute(result);
			mBuffer = result;
			nextStep();
		}

		private byte[] downFile(String realURL) throws IOException {
			if (TextUtils.isEmpty(realURL)) {
				DTLog.logError();
				return null;
			}

			DTLog.d("Debug", "下载图片：" + realURL);

			URL url = null;
			try {
				url = new URL(realURL);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

			if (url == null) {
				DTLog.logError();
				return null;
			}

			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");

				conn.setConnectTimeout(5 * 1000);
				if (conn.getResponseCode() == 200) {
					InputStream inStream = conn.getInputStream();
					int recvLength = 0;
					int readResult = 0;

					mRealFileLength = conn.getContentLength();

					DTLog.sign("mRealFileLength:" + mRealFileLength);

					byte[] bytes = new byte[mRealFileLength];

					try {
						while (readResult != -1 && recvLength <= mRealFileLength) {
							readResult = inStream.read(bytes, recvLength, mRealFileLength - recvLength > 2 * 1024 ? 2 * 1024 : mRealFileLength - recvLength);

							if (readResult != -1) {
								recvLength += readResult;
								mLengthFinished += readResult;
								done(100.0 * mLengthFinished / getTotalLengthExpected());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					if (mRealFileLength != recvLength) {
						DTLog.logError();
						return null;
					}

					return bytes;
				} else {
					DTLog.logError();
					return null;
				}
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				return null;
			} catch (Exception e1) {
				DTLog.logError();
				return null;
			}
		}
	}

	@Override
	public long getTotalLengthExpected() {
		if (mRealFileLength == 0) {
			return MAX_FILE_LENGTH;
		} else {
			return 121 + 188 + 166 + mRealFileLength + mLengthOffset;
		}
	}

	protected String getHttpAddress1(boolean ipAddress) {
		ipAddress = true;

		String httpAddress = "http://" + (ipAddress ? IM_FILE_URL_REAL_IP
				: IM_FILE_URL_DOMAIN_NAME) + ":8889/file/download";

		return httpAddress;
	}
}
