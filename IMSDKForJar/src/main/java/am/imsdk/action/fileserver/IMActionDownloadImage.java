package am.imsdk.action.fileserver;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImageThumbnail;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

// 1. 获取url
// 2. 获取文件 并保存本地
public class IMActionDownloadImage extends IMActionDownloadFile {
	public int mWidth;
	public int mHeight;
	public Bitmap mBitmap;

	@Override
	protected String getHttpAddress1(boolean ipAddress) {
		ipAddress = true;
		
		if (mWidth == 0) {
			return super.getHttpAddress1(ipAddress);
		} else {
			return "http://" + (ipAddress ? IM_FILE_URL_REAL_IP : IM_FILE_URL_DOMAIN_NAME)
					+ ":8889/file/" + (mWidth > 0 ? "image" : "download");
		}
	}
	
	@Override
	public void onActionDone() {
		if (mBuffer == null) {
			DTLog.logError();
			return;
		}

		if (mBuffer.length == 0) {
			DTLog.logError();
			return;
		}
		
		DTLog.sign("mBuffer.length:" + mBuffer.length);

		mBitmap = BitmapFactory.decodeByteArray(mBuffer, 0, mBuffer.length);

		if (mBitmap == null) {
			DTLog.logError();
			return;
		}

		if (mWidth != 0) {
			IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
					mFileID, mWidth, mHeight);

			thumbnail.setBitmap(mBitmap);
			thumbnail.setBuffer(mBuffer);
			thumbnail.saveFile();
		} else {
			IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(mFileID);

			photo.setBitmap(mBitmap);
			photo.setBuffer(mBuffer);
			photo.saveFile();
		}
	}
}
