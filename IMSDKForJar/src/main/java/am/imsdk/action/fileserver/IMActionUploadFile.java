package am.imsdk.action.fileserver;

import android.os.AsyncTask;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.DataTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

// 1. 获取token
// 2. 上传文件获取fileID
public final class IMActionUploadFile extends IMActionFile {
	public static final String BOUNDARY = "----WebKitFormBoundaryLL7ybo6JOpDyMqfb";
	public byte[] mBuffer = null;
	public String mFileID = "";

	private String mUploadToken = "";
	private String mUploadKey = "";
	private String mUploadURL = "";

	protected long mUID;

	public IMActionUploadFile() {
		super();
		mStepCount = 2;
	}

	public interface ProgressListener {
		void transferred(long num);
	}

	public static class CountingOutputStream extends FilterOutputStream {
		private final ProgressListener listener;
		private long transferred;

		public CountingOutputStream(final OutputStream out,
									final ProgressListener listener) {
			super(out);
			this.listener = listener;
			this.transferred = 0;
		}

		public void write(byte[] b, int off, int len) throws IOException {
			out.write(b, off, len);
			this.transferred += len;
			this.listener.transferred(this.transferred);
		}

		public void write(int b) throws IOException {
			out.write(b);
			this.transferred++;
			this.listener.transferred(this.transferred);
		}
	}

//	public interface OnHttpListener {
//
//	}
//
//	public class ProgressHttpEntity extends HttpEntityWrapper {
//		public ProgressHttpEntity(HttpEntity wrapped) {
//			super(wrapped);
//		}
//
//		@Override
//		public void writeTo(OutputStream outstream) throws IOException {
//			super.writeTo(outstream);
//		}
//	}

	private class UploadTask extends AsyncTask<byte[], Integer, String> {
		private String mUploadToken = "";
		private String mUploadKey = "";
		private String mUploadURL = "";

		@Override
		protected void onPreExecute() {
			if (this.mUploadToken.length() == 0) {
				doneWithIMSDKError();
				this.cancel(true);
				return;
			}

			if (this.mUploadKey.length() == 0) {
				doneWithIMSDKError();
				this.cancel(true);
				return;
			}

			if (this.mUploadURL.length() == 0) {
				doneWithIMSDKError();
				this.cancel(true);
				return;
			}

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(byte[]... params) {
			try {
				return uploadBuffer(params[0]);
			} catch (Exception e) {
				e.printStackTrace();
				doneWithIMSDKError();
				return "";
			}
		}

		private String uploadBuffer(byte[] buffer) throws IOException {
			String PREFIX = "--";
			String LINEND = "\r\n";
			String MULTIPART_FROM_DATA = "multipart/form-data";
			URL uri = new URL(this.mUploadURL);

			DTLog.d("Debug", "上传图片：" + mUploadURL);

			HttpURLConnection connection = (HttpURLConnection) uri.openConnection();

			connection.setReadTimeout(5 * 1000);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
					+ ";boundary=" + BOUNDARY);

			StringBuilder stringBuilder = new StringBuilder();

			stringBuilder.append(LINEND);
			stringBuilder.append(PREFIX);
			stringBuilder.append(BOUNDARY);
			stringBuilder.append(LINEND);

			stringBuilder.append("Content-Disposition: form-data; name=\"" + "key"
					+ "\"" + LINEND);
			stringBuilder.append(LINEND);
			stringBuilder.append(mUploadKey);
			stringBuilder.append(LINEND);
			stringBuilder.append(PREFIX);
			stringBuilder.append(BOUNDARY);
			stringBuilder.append(LINEND);

			stringBuilder.append("Content-Disposition: form-data; name=\"" + "token"
					+ "\"" + LINEND);
			stringBuilder.append(LINEND);
			stringBuilder.append(mUploadToken);
			stringBuilder.append(LINEND);
			stringBuilder.append(PREFIX);
			stringBuilder.append(BOUNDARY);
			stringBuilder.append(LINEND);

			DataOutputStream outputStream = new DataOutputStream(
					connection.getOutputStream());

			byte[] tempBuffer1 = stringBuilder.toString().getBytes();

			// 709
			// 701
			outputStream.write(tempBuffer1);

			mLengthFinished += tempBuffer1.length;
			mLengthOffset += tempBuffer1.length - 701;

			done(100.0 * mLengthFinished / getTotalLengthExpected());

			stringBuilder.append("Content-Disposition: form-data; name=\"" + "file"
					+ "\"; filename=\"filename\"" + LINEND);
			stringBuilder.append(LINEND);

			byte[] tempBuffer2 = stringBuilder.toString().getBytes();

			// 777
			// 769
			outputStream.write(tempBuffer2);

			mLengthFinished += tempBuffer2.length;
			mLengthOffset += tempBuffer2.length - 769;

			done(100.0 * mLengthFinished / getTotalLengthExpected());

			outputStream.write(buffer);

			// 802
			// 1834

			mLengthFinished += buffer.length;
			done(100.0 * mLengthFinished / getTotalLengthExpected());

			// 44
			byte[] endData = (LINEND + PREFIX + BOUNDARY + LINEND).getBytes();

			outputStream.write(endData);
			outputStream.flush();
			outputStream.close();

			mLengthFinished += endData.length;
			mLengthOffset += endData.length - 44;

			done(100.0 * mLengthFinished / getTotalLengthExpected());

			int result = connection.getResponseCode();

			if (result != 200) {
				connection.disconnect();
				return "";
			}

			InputStream inputStream = connection.getInputStream();

			int readResult = 0;
			int recvLength = 0;
			byte[] bytes = new byte[1024];

			while (readResult != -1 && recvLength < 1024) {
				readResult = inputStream.read(bytes, recvLength, 1024 - recvLength);

				if (readResult != -1) {
					recvLength += readResult;
				}
			}

			if (recvLength == 1024) {
				DTLog.logError();
			}

			mLengthFinished += recvLength;
			mLengthOffset += recvLength - 63;

			done(100.0 * mLengthFinished / getTotalLengthExpected());

			inputStream.close();
			connection.disconnect();

			return new String(bytes, 0, recvLength, "UTF8");
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (result.length() == 0) {
				doneWithIMSDKError();
				return;
			}

			try {
				JSONObject jsonObject = new JSONObject(result);

				mFileID = jsonObject.getString("fid");
			} catch (JSONException e) {
				e.printStackTrace();
				doneWithIMSDKError();
				return;
			}

			nextStep();
		}
	}

	private class FetchTokenTask extends AsyncTask<byte[], Integer, String> {
		@Override
		protected String doInBackground(byte[]... params) {
			try {
				return fetchToken(params[0]);
			} catch (Exception e) {
				e.printStackTrace();
				DTLog.logError();
				doneWithServerError();
				return "";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (TextUtils.isEmpty(result)) {
				doneWithServerError();
				return;
			}

			try {
				JSONObject jsonObject = new JSONObject(result);

				mUploadToken = jsonObject.getString("token");
				mUploadKey = jsonObject.getString("key");
				mUploadURL = jsonObject.getString("url");

				DTLog.d("Debug", "上传图片：" + result);
			} catch (JSONException e) {
				e.printStackTrace();
				DTLog.log("upload result: " + result);
				doneWithServerError();
				return;
			}

			nextStep();
		}

		private String fetchToken(byte[] buffer) throws IOException {
			String httpAddress = "http://" + IM_FILE_URL_DOMAIN_NAME + ":8889/file/upload";

			URL url = null;
			HttpURLConnection conn = null;
			try {
				url = new URL(httpAddress);
				conn = (HttpURLConnection) url.openConnection();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

			if (url == null || conn == null) {
				httpAddress = "http://" + IM_FILE_URL_REAL_IP + ":8889/file/upload";

				try {
					url = new URL(httpAddress);
					conn = (HttpURLConnection) url.openConnection();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}

				if (url == null || conn == null) {
					DTLog.logError();
					return "";
				}
			}

			String md51 = DTTool.getMD5String(buffer);
			String md52 = DTTool.getMD5String(md51 + mUID);

			String content = "hash=" + URLEncoder.encode(md51, "UTF-8") + "&fromuid=" + mUID + "&vercode=" + URLEncoder.encode(md52, "UTF-8");
			byte[] contentBuff = content.getBytes(Charset.forName("UTF-8"));

			try {
				conn.setRequestMethod("POST");
				conn.setDoOutput(true);

				conn.setRequestProperty("Charset", "UTF-8");
				conn.setConnectTimeout(5 * 1000);
				OutputStream outStream = conn.getOutputStream();
				outStream.write(contentBuff);
				outStream.flush();
				outStream.close();
				if (conn.getResponseCode() == 200) {
					InputStream inStream = conn.getInputStream();

					return new String(DataTool.readInputStream(inStream), "UTF-8");
				} else {
					conn.disconnect();
					DTLog.logError();
					return "";
				}
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				return "";
			} catch (Exception e1) {
				DTLog.logError();
				return "";
			}
		}
	}

	@Override
	public void begin() {
		if (mBuffer == null) {
			DTLog.logError();
			return;
		}

		if (mBuffer.length == 0) {
			DTLog.logError();
			return;
		}

		super.begin();
	}

	@Override
	public void onActionBegan() {
		if (mBuffer == null) {
			doneWithIMSDKError();
			return;
		}

		if (mBuffer.length == 0) {
			doneWithIMSDKError();
			return;
		}

//		if (mBuffer.length > 262144) {
//			DTLog.sign("mBuffer.length:" + mBuffer.length);
//			done("File to Upload is Over Size.");
//			return;
//		}

		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			mUID = IMRemoteMyself.getInstance().getUID();
		} else {
			mUID = IMPrivateMyself.getInstance().getUID();
		}

		if (mUID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
			case 1: {
				FetchTokenTask fetchTokenTask = new FetchTokenTask();

				fetchTokenTask.execute(mBuffer);
			}
			break;
			case 2: {
				if (mUploadToken.length() == 0) {
					doneWithIMSDKError();
					return;
				}

				if (mUploadKey.length() == 0) {
					doneWithIMSDKError();
					return;
				}

				if (mUploadURL.length() == 0) {
					doneWithIMSDKError();
					return;
				}

				UploadTask uploadTask = new UploadTask();

				uploadTask.mUploadToken = mUploadToken;
				uploadTask.mUploadKey = mUploadKey;
				uploadTask.mUploadURL = mUploadURL;
				uploadTask.execute(mBuffer);
			}
			break;
			default:
				break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
	}

	@Override
	public void onActionFailed(String error) {
	}

	@Override
	public long getTotalLengthExpected() {
		if (mBuffer == null) {
			DTLog.logError();
			return 1;
		}

		return 101 + 538 + 701 + 769 + mBuffer.length + 44 + 63 + mLengthOffset;
	}
}
