package am.imsdk.action;

import android.os.RemoteException;
import android.text.TextUtils;
import android.util.SparseArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.socket.DTSocket;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.aacmd.IMSocket;
import am.imsdk.aacmd.user.IMCmdUserLogin;
import am.imsdk.aacmd.user.IMCmdUserRegister;
import am.imsdk.aacmd.user.IMCmdUserTokenLogin;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMParamJudge;
import imsdk.data.IMMyself.LoginStatus;
import remote.service.MainToSocketProcessStub;
import remote.service.data.IMRemoteMyself;

// 注册或登录，用户行为或非用户行为
// 1. 判断网络，重新连接
// 2. 注册、登录
// 完成
public final class IMActionRemoteLogin extends IMAction {
	public String mCustomUserID;
	private String mPassword;
	public String mLoginToken;
	public String mAppKey;
	private IMActionLoginType mType = IMActionLoginType.None;

	private JSONObject mCmdJsonObjectResult = null;

	private Observer mSocketUpdatedObserver;
	
	private IMActionRemoteLogin() {
		mStepCount = 3;
	}

	@Override
	public void onActionBegan() {
		if (mType == null) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.None) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.OneKeyLogin
				|| mType == IMActionLoginType.OneKeyRegister) {
			if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (mType == IMActionLoginType.OneKeyLogin
				|| mType == IMActionLoginType.OneKeyRegister) {
			if (!IMParamJudge.isPasswordLegal(mPassword)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (mType == IMActionLoginType.AutoLogin) {
			if (TextUtils.isEmpty(mCustomUserID)) {
				doneWithIMSDKError();
				return;
			}

			if (!IMParamJudge.isTokenLegal(mLoginToken)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (mSocketUpdatedObserver != null) {
			DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED", mSocketUpdatedObserver);
		}
		
		DTLog.e("Debug", "IMActionRemoteLogin--创建--"+ IMActionRemoteLogin.this);

		mSocketUpdatedObserver = new Observer() {
			@Override
			public void update(Object data) {
				if(data == null || !(data instanceof DTSocketStatus)) {
					DTLog.logError();
					return;
				}

				DTSocketStatus status = (DTSocketStatus) data;
				
				DTLog.e("Debug", "IMActionRemoteLogin--mSocketUpdatedObserver:"
						+ IMActionRemoteLogin.this + "---"
						+ status);

				if (isOver() || IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this) {
					DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED", this);
					return;
				}

				if (status == DTSocketStatus.Connected) {

					DTLog.e("Debug", "IMActionRemoteLogin--"
							+ status);

					if (getCurrentStep() != 1) {
						DTLog.e("Debug", "IMActionRemoteLogin--"
								+ status
								+ "--but currStep = 2");
//						doneWithNetworkError();
						return;
					}

					IMActionRemoteLogin.this.nextStep();
				} else if (status == DTSocketStatus.None) {
					resetStep();
					
//					if(mReTryTime++ >= 3) {
//						done("Login failed");
//						DTSocket.getInstance().checkCloseSocket();
//					}
				}
			}
		};

		DTNotificationCenter.getInstance().addObserver("SOCKET_UPDATED",
				mSocketUpdatedObserver);
	}

	private void registerIMSDK() {
		IMCmdUserRegister cmd = new IMCmdUserRegister();

		if (IMActionRemoteLogin.getInstance() != this || isOver()) {
			return;
		}

		if (TextUtils.isEmpty(mCustomUserID)) {
			return;
		}

		if (TextUtils.isEmpty(mPassword)) {
			return;
		}

		cmd.mCustomUserID = mCustomUserID;
		cmd.mPassword = mPassword;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				if (type == FailedType.RecvError) {
					if (100 == errorCode) {
						done("Wrong Password");
					} else {
						done(errorJsonObject != null ? errorJsonObject.toString()
								: "Error code:" + errorCode);
					}
				} else {
					DTSocket.getInstance().reconnect();
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				mCmdJsonObjectResult = jsonObject;
				done();
			}
		};

		cmd.send();
	}

	private void loginIMSDK() {
		IMCmdUserLogin cmd = new IMCmdUserLogin();

		if (IMActionRemoteLogin.getInstance() != this || isOver()) {
			return;
		}

		if (TextUtils.isEmpty(mCustomUserID)) {
			done("customUserID is null");
			return;
		}

		if (TextUtils.isEmpty(mPassword)) {
			done("Password is null");
			return;
		}

		cmd.mPhoneNum = mCustomUserID;
		cmd.mPassword = mPassword;
		cmd.mSetupID = IMAppSettings.getInstance().mSetupID;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				if (type == FailedType.RecvError) {
					if (DTCmd.CMD_ERROR_PASSWORD == errorCode) {
						done("Wrong Password");
					} else {
						done(errorJsonObject != null ? errorJsonObject.toString() + ", Error code:" + errorCode
								: "Error code:" + errorCode);
					}
				} else {
					DTSocket.getInstance().reconnect();
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				mCmdJsonObjectResult = jsonObject;
				done();
			}
		};

		cmd.send();
	}

	private void autoLoginOrReconnect() {
		IMCmdUserTokenLogin cmd = new IMCmdUserTokenLogin();

		if (IMActionRemoteLogin.getInstance() != this || isOver()) {
			return;
		}
		
		if (TextUtils.isEmpty(mCustomUserID)) {
			done("customUserID is null");
			return;
		}

		if (TextUtils.isEmpty(mLoginToken)) {
			done("Password is null");
			return;
		}

		cmd.mLoginToken = mLoginToken;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				if (type == FailedType.RecvError) {
					if (DTCmd.CMD_ERROR_PASSWORD == errorCode) {
						done("Wrong Password");
					} else if (DTCmd.CMD_ERROR_LOGIN_CONFLICT == errorCode) {
						done("Login Conflict");
					} else {
						done(errorJsonObject != null ? errorJsonObject.toString()
								: "Error code:" + errorCode);
					}
				} else {
					done("No Response");
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				mCmdJsonObjectResult = jsonObject;
				done();
			}
		};

		cmd.send();
	}

	@Override
	public void onActionStepBegan(final int stepNumber) {
		DTLog.e("Debug", "onActionStepBegan:" + stepNumber + "---"
				+ IMActionRemoteLogin.this);

		DTSocketStatus status = DTSocket.getInstance().getStatus();

		switch (stepNumber) {
		case 1: {
			if (status == DTSocketStatus.Connected) {
				DTLog.e("Debug", "start step 2");
				IMActionRemoteLogin.this.nextStep();
				return;
			}

			DTLog.e("Debug", "onActionStepBegan step = " + status + "--reconnect");
			DTSocket.getInstance().reconnect();
		}
			break;
		case 2:
			if(TextUtils.isEmpty(IMAppSettings.getInstance().mSetupID)) {
				IMAppSettings.getInstance().mSetupID = UUID.randomUUID().toString();
				IMAppSettings.getInstance().saveFile();
			}

			IMActionRemoteLogin.this.nextStep();
			break;
		case 3: {
			if (status != DTSocketStatus.Connected) {
				DTLog.e("Debug", "onActionStepBegan step = " + stepNumber + "---"
						+ status + "--error");
				doneWithNetworkError();
				return;
			}

			 DTLog.e("Debug", mType + "");

			switch (mType) {
			case OneKeyLogin: {
				if (mCustomUserID == null) {
					mCustomUserID = "";
				}

				if (mPassword == null) {
					mPassword = "";
				}

				// 登录
				loginIMSDK();
			}
				break;
			case OneKeyRegister: {
				if (mCustomUserID == null) {
					mCustomUserID = "";
				}

				if (mPassword == null) {
					mPassword = "";
				}

				// 注册
				registerIMSDK();
			}
				break;
			case Reconnect: {
				autoLoginOrReconnect();
			}
				break;
			case AutoLogin:
				autoLoginOrReconnect();
				break;
			default:
				DTLog.logError();
				break;
			}
		}
			break;
		default:
			DTLog.logError();
			break;
		}

	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
		if (IMActionRemoteLogin.getInstance() != this) {
			return;
		}

		sSingleton = null;

		if (mCmdJsonObjectResult == null) {
			doneWithIMSDKError();
			return;
		}

		long uid = 0;
		long sid = 0;
		String token = "";

		try {
			uid = mCmdJsonObjectResult.getLong("uid");
			sid = mCmdJsonObjectResult.getLong("sid");
			token = mCmdJsonObjectResult.getString("token");

			if (uid <= 0) {
				doneWithServerError();
				return;
			}

			if (sid <= 0) {
				doneWithServerError();
				return;
			}

			if(!IMParamJudge.isTokenLegal(token)) {
				doneWithServerError();
				return;
			}

			if(!mCustomUserID.equals(uid + "")) {
				DTLog.logError();
				this.done("发送和接收的uid不匹配");
				return;
			}
			
			IMSocket.getInstance().setUID(uid);
			IMSocket.getInstance().setSessionID(sid);

			IMRemoteMyself.getInstance().setUID(uid);
//			IMRemoteMyself.getInstance().setSessionID(sid);
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			this.done("IMSDK Server error");
			return;
		}
		
		IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.Logined);

		IMAppSettings.getInstance().mLastLoginUID = uid;
		IMAppSettings.getInstance().mLastLoginToken = token;
		IMAppSettings.getInstance().saveFile();

		if (!IMParamJudge.isTokenLegal(IMAppSettings.getInstance().mLastLoginToken)) {
			DTLog.logError();
		}

		IMActionRemoteLogin.removeInstance();
		
		if (mType == IMActionLoginType.OneKeyLogin
				|| mType == IMActionLoginType.OneKeyRegister
				|| mType == IMActionLoginType.AutoLogin
				|| mType == IMActionLoginType.Reconnect) {
			if(MainToSocketProcessStub.getInstance().getLoginRecvListener() != null) {
				try {
					MainToSocketProcessStub.getInstance().getLoginRecvListener().onRecvSuccess(mCmdJsonObjectResult.toString());
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		} else {
			DTLog.logError();
		}
	}

	@Override
	public void onActionFailed(String error) {
		if (IMActionRemoteLogin.getInstance() != this) {
			return;
		}

		sSingleton = null;

		DTLog.e("Debug", "IMActionRemoteLogin failed----type:" + mType);

		if (MainToSocketProcessStub.getInstance().getLoginRecvListener() != null) {
			try {
				MainToSocketProcessStub.getInstance().getLoginRecvListener().onRecvFailure(error);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		switch (mType) {
		case OneKeyLogin:
		case OneKeyRegister:
		// 用户行为
		{
			IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
			
			IMAppSettings.getInstance().mLastLoginToken = "";
			IMAppSettings.getInstance().saveFile();
		}
			break;
		case Reconnect: {
			if (error.equals("Wrong Password") || error.equals("Login Conflict")) {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
				IMAppSettings.getInstance().mLastLoginToken = "";
				IMAppSettings.getInstance().saveFile();
			} else {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
			}
		}
			break;
		case AutoLogin: {
			DTLog.sign("AutoLogin done");

			if (error.equals("Wrong Password") || error.equals("Login Conflict")) {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
				IMAppSettings.getInstance().mLastLoginToken = "";
				IMAppSettings.getInstance().saveFile();
			} else {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
			}
		}
			break;
		default:
			break;
		}
		
		IMActionRemoteLogin.removeInstance();
	}

	public void setType(IMActionLoginType type) {
		mType = type;
		DTLog.sign("IMActionLoginType:" + type.toString());
	}
	
	public void setType(int type) {
		mType = IMActionLoginType.fromInt(type);
		DTLog.sign("IMActionLoginType:" + mType.toString());
	}

	public IMActionLoginType getType() {
		return mType;
	}

	public void setPassword(String password) {
		if (password == null) {
			DTLog.logError();
			return;
		}

		mPassword = password;
	}

	public enum IMActionLoginType {
		None(0),

		// 用户行为
		OneKeyLogin(3), OneKeyRegister(4),

		// 非用户行为
		Reconnect(10), AutoLogin(11);

		private final int value;

		IMActionLoginType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<IMActionLoginType> sMapValues = new SparseArray<IMActionLoginType>();

		static {
			for (IMActionLoginType type : IMActionLoginType.values()) {
				sMapValues.put(type.value, type);
			}
		}

		public static IMActionLoginType fromInt(int i) {
			IMActionLoginType type = sMapValues.get(Integer.valueOf(i));

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}

	// singleton
	private volatile static IMActionRemoteLogin sSingleton;

	public static IMActionRemoteLogin getInstance() {
		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
//		printCallStatck();
		synchronized (IMActionRemoteLogin.class) {
			if (sSingleton != null && sSingleton.mSocketUpdatedObserver != null) {
				DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED",
						sSingleton.mSocketUpdatedObserver);
			}

			sSingleton = new IMActionRemoteLogin();
		}
	}

	public static void removeInstance() {
		synchronized (IMActionRemoteLogin.class) {
			if (sSingleton != null) {
				if(sSingleton.mSocketUpdatedObserver != null) {
					DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED",
							sSingleton.mSocketUpdatedObserver);
				}

				sSingleton = null;
			}
		}
	}
	
	public static void printCallStatck() {
        Throwable ex = new Throwable();
        StackTraceElement[] stackElements = ex.getStackTrace();
        if (stackElements != null) {
            for (int i = 0; i < stackElements.length; i++) {
                System.out.print(stackElements[i].getClassName()+"--");
                System.out.print(stackElements[i].getFileName()+"--");
                System.out.print(stackElements[i].getLineNumber()+"--");
                System.out.println(stackElements[i].getMethodName());
            }
        }
        System.out.println("**********************************");
    }
}
