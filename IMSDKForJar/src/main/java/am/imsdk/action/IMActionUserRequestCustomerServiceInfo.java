package am.imsdk.action;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.user.IMCmdUserGetCustomerServiceInfo;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.kefu.IMCustomerServiceMgr;

public final class IMActionUserRequestCustomerServiceInfo extends IMAction {
	public String mAppKey = "";
	public String mCustomerServiceID = "";
	private long mUID = 0;

	public IMActionUserRequestCustomerServiceInfo() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isCustomerServiceIDLegal(mCustomerServiceID)) {
			doneWithIMSDKError();
			return;
		}

		if (!IMParamJudge.isAppKeyLegal(mAppKey)) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			mUID = IMCustomerServiceMgr.getInstance().getUID(mCustomerServiceID);

			if (mUID == 0) {
				checkBeginGetUID(mCustomerServiceID);
			} else {
				nextStep();
			}
		}
			break;
		case 2: {
			if (mUID == 0) {
				doneWithIMSDKError();
				return;
			}

			final IMCmdUserGetCustomerServiceInfo cmd = new IMCmdUserGetCustomerServiceInfo();

			cmd.mAppKey = mAppKey;
			cmd.addUID(mUID);

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					done(errorJsonObject != null ? errorJsonObject.toString() : "");
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					done();
				}
			};

			cmd.send();
		}
			break;
		default:
			break;
		}
	}
}
