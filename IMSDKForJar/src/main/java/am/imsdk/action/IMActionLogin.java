package am.imsdk.action;

import android.text.TextUtils;
import android.util.SparseArray;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTRemoteMgr;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.action.init.IMActionLoginInit;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.userinfo.IMUsersMgr;
import imsdk.data.IMMyself;
import imsdk.data.IMMyself.LoginStatus;

// 注册或登录，用户行为
// 1. 拉取SocketService数据，判断时候登录成功
// 2. 发送登录指令 , 等待接收返回值，并设置属性
// 完成
public final class IMActionLogin extends IMAction {
	private long mUID;
	public String mCustomUserID;
	private String mPassword;
	public String mAppKey;
	private IMActionLoginType mType = IMActionLoginType.None;

	private IMPrivateMyself mPrivateMyself;

	private IMActionLogin() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (mType == null) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.None) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.OneKeyLogin
				|| mType == IMActionLoginType.OneKeyRegister) {
			if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (mType == IMActionLoginType.OneKeyLogin
				|| mType == IMActionLoginType.OneKeyRegister) {
			if (!IMParamJudge.isPasswordLegal(mPassword)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (!IMParamJudge.isAppKeyLegal(mAppKey)) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.OneKeyLogin
				|| mType == IMActionLoginType.OneKeyRegister
				|| mType == IMActionLoginType.AutoLogin) {
			IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.Logining);
			IMPrivateMyself.getInstance().setCustomUserInfoInited(false);
			IMPrivateMyself.getInstance().setGroupInfoInited(false);
			IMPrivateMyself.getInstance().setRelationsInited(false);
//			IMPrivateMyself.getInstance().setAppInfoInitedInFact(false);
			IMPrivateMyself.getInstance().saveFile();
		} else {
			DTLog.logError();
		}

		DTNotificationCenter.getInstance().removeObservers("LoginReceiveSuccess");
		DTNotificationCenter.getInstance().removeObservers("LoginReceiveFailure");

		DTLog.e("Debug", "IMActionLogin--创建--"+ IMActionLogin.this);

		DTNotificationCenter.getInstance().addObserver("LoginReceiveSuccess",
				new Observer() {

					@Override
					public void update(Object data) {
						String strJson = data.toString();

						try {
							JSONObject jsonObject = new JSONObject(strJson);

							try {
								mUID = jsonObject.getLong("uid");

								if (mUID <= 0) {
									doneWithServerError();
									return;
								}

								if (!mCustomUserID.equals(mUID + "")) {
									IMMyself.logout(null);
									doneWithServerError();
									return;
								}

								done();
							} catch (JSONException e) {
								e.printStackTrace();
								DTLog.logError();
								DTLog.log(e.toString());
								done("IMSDK Server error");
								return;
							}
						} catch (JSONException e) {
//							e.printStackTrace();
							done("Login Err: 数据错误");
						}

						if (mCustomUserID != null && !mCustomUserID.equals(IMMyself.getCustomUserID())) {
							return;
						}

						if (mPrivateMyself != IMPrivateMyself.getInstance()) {
							return;
						}

						IMUsersMgr.getInstance().setCustomUserIDForUID(String.valueOf(mUID), mUID);
						IMUsersMgr.getInstance().saveFile();

						boolean groupInited = IMPrivateMyself.getInstance().getGroupInfoInited();
						boolean relationInited = IMPrivateMyself.getInstance().getRelationsInited();

						IMMyself.setCustomUserID(String.valueOf(mUID));

						IMPrivateMyself.getInstance().setGroupInfoInited(groupInited);
						IMPrivateMyself.getInstance().setRelationsInited(relationInited);
						IMPrivateMyself.getInstance().setPassword(mPassword);
						IMPrivateMyself.getInstance().saveFile();

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						IMPrivateMyself.getInstance().setLoginStatus(IMMyself.LoginStatus.Logined);

						// init modules
						if (mType == IMActionLoginType.OneKeyLogin) {
							IMPrivateMyself.getInstance().onUserLoginInit();
						} else if (mType == IMActionLoginType.OneKeyRegister) {
							IMPrivateMyself.getInstance().onUserRegisterInit();
						} else {
							IMPrivateMyself.getInstance().onMachineLoginInit();
						}

						IMActionLoginInit action = new IMActionLoginInit();

						action.mOnActionDoneEndListener = new IMAction.OnActionDoneEndListener() {
							@Override
							public void onActionDoneEnd() {

							}
						};

						action.mOnActionFailedEndListener = new IMAction.OnActionFailedEndListener() {
							@Override
							public void onActionFailedEnd(String error) {

							}
						};

						action.begin();

						DTNotificationCenter.getInstance().postNotification("IMActionLogin_done");
					}
				});

		DTNotificationCenter.getInstance().addObserver("LoginReceiveFailure",
				new Observer() {

					@Override
					public void update(Object data) {
						switch (mType) {
							case OneKeyLogin:
							case OneKeyRegister:
							case AutoLogin:
								// 用户行为
							{
								if (data.toString().equals("Login Conflict")) {
									DTNotificationCenter.getInstance().postNotification("LoginConflict");
								}

								DTNotificationCenter.getInstance().postNotification(
										"IMActionUserLogin_failed", data.toString());


							}
							break;
							default:
								break;
						}

						done(data.toString());
					}
				});

		mPrivateMyself = IMPrivateMyself.getInstance();
	}

	@Override
	public void onActionStepBegan(final int stepNumber) {
		DTLog.e("Debug", "onActionStepBegan:" + stepNumber + "---"
				+ IMActionLogin.this);

		switch (stepNumber) {
			case 1:
				if (mType == IMActionLoginType.OneKeyRegister) {
					nextStep();
					break;
				}

				DTRemoteMgr.getInstance().getLoginInfo(new DTRemoteMgr.OnActionListener() {

					@Override
					public void onSuccess(Object result) {
						String jsonResult = result.toString();

						DTLog.sign("jsonResult:" + jsonResult);

						if (!TextUtils.isEmpty(jsonResult)) {
							try {
								JSONObject jsonObject = new JSONObject(jsonResult);

								mUID = jsonObject.getInt("uid");

								if(mUID <= 0) {
									nextStep();
									return;
								}

								if (!mCustomUserID.equals(mUID + "")) {
									DTLog.logError("后台登录用户为" + mUID + ", 当前准备登录用户为" + mCustomUserID);
									IMMyself.logout(null);
									nextStep();
									return;
								}

								DTNotificationCenter.getInstance().postNotification("LoginReceiveSuccess", jsonResult);

								done();
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}

						nextStep();
					}

					@Override
					public void onFailure() {
						nextStep();
					}
				});
				break;
			case 2:
				DTLog.e("Debug", mType + "");

				switch (mType) {
					case OneKeyLogin:
					case OneKeyRegister:
					case AutoLogin: {
						if (mCustomUserID == null) {
							mCustomUserID = "";
						}

						if (mPassword == null) {
							mPassword = "";
						}

						// 登录
						DTRemoteMgr.getInstance().loginOrRegister(mType.getValue(), mCustomUserID,
								mPassword);
					}
					break;
					default:
						DTLog.logError();
						doneWithIMSDKError();
						break;
				}
				break;
		}

	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {


	}

	@Override
	public void onActionFailed(String error) {

	}

	public void setType(IMActionLoginType type) {
		mType = type;
		DTLog.sign("IMActionLoginType:" + type.toString());
	}

	public IMActionLoginType getType() {
		return mType;
	}

	public void setPassword(String password) {
		mPassword = password;
	}

	public enum IMActionLoginType {
		None(0),

		// 用户行为
		// PureLogin(1), PureRegister(2),

		// 用户行为
		OneKeyLogin(3), OneKeyRegister(4),

		// 非用户行为
		Reconnect(10), AutoLogin(11);

		private final int value;

		IMActionLoginType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<IMActionLoginType> sMapValues = new SparseArray<IMActionLoginType>();

		static {
			for (IMActionLoginType type : IMActionLoginType.values()) {
				sMapValues.put(type.value, type);
			}
		}

		public static IMActionLoginType fromInt(int i) {
			IMActionLoginType type = sMapValues.get(i);

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}

	// singleton
	private volatile static IMActionLogin sSingleton;

	public static IMActionLogin getInstance() {
		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
//		printCallStatck();
		synchronized (IMActionLogin.class) {
			if (sSingleton != null) {
				DTNotificationCenter.getInstance().removeObservers("LoginReceiveSuccess");
				DTNotificationCenter.getInstance().removeObservers("LoginReceiveFailure");
			}

			sSingleton = new IMActionLogin();
		}
	}
	
	public static void printCallStatck() {
        Throwable ex = new Throwable();
        StackTraceElement[] stackElements = ex.getStackTrace();
        if (stackElements != null) {
            for (int i = 0; i < stackElements.length; i++) {
                System.out.print(stackElements[i].getClassName()+"--");
                System.out.print(stackElements[i].getFileName()+"--");
                System.out.print(stackElements[i].getLineNumber()+"--");
                System.out.println(stackElements[i].getMethodName());
            }
        }
        System.out.println("**********************************");
    }
}
