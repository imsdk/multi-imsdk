package am.imsdk.action.group;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.team.IMCmdTeamGetInfo;
import am.imsdk.aacmd.team.IMCmdTeamGetMembers;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;

// a. 获取Group信息
// b 获取Group成员列表
public final class IMActionGroupGetInfo extends IMAction {
	public String mGroupID = "";
	private long mTeamID;
	private boolean mStepADone = false;
	private boolean mStepBDone = false;

	public IMActionGroupGetInfo() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isGroupIDLegal(mGroupID)) {
			doneWithIMSDKError();
			return;
		}

		mTeamID = DTTool.getUnsecretLongValue(mGroupID);

		if (mTeamID <= 0) {
			doneWithIMSDKError();
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			IMCmdTeamGetInfo cmd = new IMCmdTeamGetInfo();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					JSONArray teamInfoList = jsonObject.getJSONArray("teamInfolist");

					if(teamInfoList.length() <= 0) {
						DTLog.logError();
						return;
					}

					for (int i = 0; i < teamInfoList.length(); i++) {
						JSONObject mJsonObjectTeamInfo = (JSONObject) teamInfoList.get(i);

						if (!(mJsonObjectTeamInfo instanceof JSONObject)) {
							return;
						}

						long teamID = mJsonObjectTeamInfo.getLong("teamid");

						if (teamID == 0 || teamID != mTeamID) {
							DTLog.logError();
							return;
						}

						IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

						try {
							teamInfo.parseServerData(mJsonObjectTeamInfo);
						} catch (JSONException e) {
							e.printStackTrace();
							DTLog.logError();
							DTLog.log(mJsonObjectTeamInfo.toString());
							return;
						}

						teamInfo.saveFile();
					}

					nextStep();
				}
			};

			cmd.addTeamID(mTeamID);
			cmd.send();
		}
			break;
		case 2: {
			IMCmdTeamGetMembers cmd = new IMCmdTeamGetMembers();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
										   JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					JSONArray jsonArray = jsonObject.getJSONArray("members");
					ArrayList<Long> mAryUIDs = new ArrayList<Long>();

					for (int i = 0; i < jsonArray.length(); i++) {
						long uid = jsonArray.getLong(i);

						if (uid == 0) {
							DTLog.logError();
							return;
						}

						mAryUIDs.add(uid);
					}

					IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

					teamInfo.clear();
					teamInfo.setUIDList(mAryUIDs);
					teamInfo.saveFile();

					done();
				}
			};

			cmd.mTeamID = mTeamID;
			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
	}

	@Override
	public void onActionFailed(String error) {
	}
}
