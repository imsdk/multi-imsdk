package am.imsdk.action.group;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.team.IMCmdTeamSearchByName;
import am.imsdk.action.IMAction;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;

/**
 * 通过群名称搜索群列表
 */
public class IMActionGroupSearchByName extends IMAction {

    public int mType;
    public String mGroupName;
    public int mStart;
    public int mEnd;

    public ArrayList<Long> mTeamIDList = new ArrayList<Long>();

    public IMActionGroupSearchByName() {
        mStepCount = 1;
    }

    @Override
    public void onActionBegan() {
        if(mStart < 0) {
            DTLog.logError();
            return;
        }

        if(mEnd <= 0) {
            DTLog.logError();
            return;
        }

        if(mStart >= mEnd) {
            DTLog.logError();
            return;
        }
    }

    @Override
    public void onActionStepBegan(int stepNumber) {
        IMCmdTeamSearchByName cmd = new IMCmdTeamSearchByName();

        cmd.mOnCommonFailedListener = new DTCmd.OnCommonFailedListener() {
            @Override
            public void onCommonFailed(DTCmd.FailedType type, long errorCode, JSONObject errorJsonObject) throws JSONException {
                done("IMSDK error");
            }
        };

        cmd.mOnRecvEndListener = new DTCmd.OnRecvEndListener() {
            @Override
            public void onRecvEnd(JSONObject jsonObject) throws JSONException {
                if(!(jsonObject instanceof JSONObject)) {
                    done("IMSDK error");
                    return;
                }

                if(jsonObject.length() <= 0) {
                    done("IMSDK error");
                    return;
                }

                if(!jsonObject.has("team_list")) {
                    done("IMSDK error");
                    return;
                }

                JSONArray infoList = jsonObject.getJSONArray("team_list");

                if(infoList.length() > 0) {
                    for (int i = 0; i < infoList.length(); i++) {
                        try {
                            JSONObject jsonTeam = infoList.getJSONObject(i);

                            if (!(jsonTeam instanceof JSONObject)) {
                                return;
                            }

                            long teamID = jsonTeam.getLong("teamid");

                            if (teamID == 0) {
                                DTLog.logError();
                                return;
                            }

                            IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

                            teamInfo.parseServerData(jsonTeam);

                            teamInfo.saveFile();

                            mTeamIDList.add(teamID);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DTLog.logError();
                            DTLog.log(infoList.get(i).toString());
                            return;
                        }
                    }

                    done();
                }
            }
        };

        cmd.mType = mType;
        cmd.mGroupName = mGroupName;
        cmd.mStart = mStart;
        cmd.mEnd = mEnd;
        cmd.send();
    }
}
