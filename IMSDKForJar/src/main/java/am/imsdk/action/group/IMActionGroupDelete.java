package am.imsdk.action.group;

import imsdk.data.IMMyself;
import imsdk.data.group.IMGroupInfo;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.im.IMCmdIMSendTeamMsg;
import am.imsdk.aacmd.team.IMCmdTeamDelete;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.teaminfo.IMTeamsMgr;

// 1. 通知Group成员
// 2. 删除Group
public class IMActionGroupDelete extends IMAction {
	public long mTeamID;

	public IMActionGroupDelete() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (mTeamID <= 0) {
			doneWithIMSDKError();
			return;
		}

		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			IMGroupInfo groupInfo = IMTeamsMgr.getInstance().getGroupInfo(mTeamID);

			if (groupInfo.getMemberList().size() <= 0) {
				doneWithIMSDKError();
				return;
			}

			if (groupInfo.getMemberList().size() == 1) {
				String customUserID = groupInfo.getMemberList().get(0);

				if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
					doneWithIMSDKError();
					return;
				}

				if (!customUserID.equals(IMMyself.getCustomUserID())) {
					doneWithIMSDKError();
					return;
				}

				nextStep();
				return;
			}

			IMCmdIMSendTeamMsg cmd = new IMCmdIMSendTeamMsg();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};

			cmd.mTeamMsgType = TeamMsgType.IMSDKGroupDeleted;
			cmd.mToTeamID = mTeamID;
			cmd.send();
		}
			break;
		case 2: {
			IMCmdTeamDelete cmd = new IMCmdTeamDelete();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};

			cmd.mTeamID = mTeamID;
			cmd.send();
		}
			break;
		default:
			break;
		}
	}
}
