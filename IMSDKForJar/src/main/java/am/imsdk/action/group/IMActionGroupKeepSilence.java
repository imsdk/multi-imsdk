package am.imsdk.action.group;

import android.text.TextUtils;

import am.imsdk.action.IMAction;
import imsdk.data.IMMyself;
import imsdk.data.group.IMMyselfGroup;

/**
 * 群禁言管理
 */
public class IMActionGroupKeepSilence extends IMAction {

    public String mCustomUserID;
    public String mGroupID;
    public String mGroupName;
    public String mExtraInfo;
    public String mCoreInfo;

    public IMActionGroupKeepSilence() {
        mStepCount = 2;
    }

    @Override
    public void onActionBegan() {
        if(TextUtils.isEmpty(mCustomUserID)) {
            doneWithIMSDKError();
            return;
        }

        if(TextUtils.isEmpty(mGroupID)) {
            doneWithIMSDKError();
        }
    }

    @Override
    public void onActionStepBegan(int stepNumber) {
        switch (stepNumber) {
            case 1:
                IMActionGroupSetInfo action = new IMActionGroupSetInfo();
                action.mGroupID = mGroupID;
                action.mGroupName = mGroupName;
                action.mExInfo = mExtraInfo;
                action.mCoreInfo = mCoreInfo;

                action.mOnActionFailedListener = new OnActionFailedListener() {
                    @Override
                    public void onActionFailed(String error) {
                        done(error);
                    }
                };

                action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
                    @Override
                    public void onActionDoneEnd() {
                       nextStep();
                    }
                };

                action.begin();
                break;
            case 2:
                IMMyselfGroup.sendNoticeMsg("成员" + mCustomUserID + ", 被管理员禁言", mGroupID, new IMMyself.OnActionListener() {
                    @Override
                    public void onSuccess() {
                        done();
                    }

                    @Override
                    public void onFailure(String error) {
                        done(error);
                    }
                });
                break;
        }
    }
}
