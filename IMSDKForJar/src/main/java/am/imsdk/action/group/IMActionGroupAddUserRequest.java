package am.imsdk.action.group;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.im.IMCmdIMSendUserMsg;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import imsdk.data.custommessage.IMMyselfCustomMessage;

/**
 * 添加用户前的申请
 */
public class IMActionGroupAddUserRequest extends IMAction {

    public String mCustomUserId;
    public long mTeamID;
    public String mContent;

    private IMPrivateTeamInfo mTeamInfo;

    // 1.1 用户申请--向所有管理员发出申请
    // 1.2 管理员申请--向用户发出申请
    public IMActionGroupAddUserRequest() {
        mStepCount = 1;
    }

    @Override
    public void onActionBegan() {
        if(!IMParamJudge.isCustomUserIDLegal(mCustomUserId)) {
            DTLog.logError();
            return;
        }

        if(mTeamID <= 0) {
            DTLog.logError();
            return;
        }

        mTeamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

        if(mTeamInfo == null) {
            DTLog.logError();
            return;
        }
    }

    @Override
    public void onActionStepBegan(int stepNumber) {
        if (IMPrivateMyself.getInstance().getUID() == Long.valueOf(mCustomUserId)) {
            //用户申请--向所有管理员发出申请
            ArrayList<IMPrivateTeamInfo.OwnerInfo> ownerInfoList = mTeamInfo.getManagerInfoList();

            // 创建content
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("groupID", DTTool.getSecretString(mTeamID));
//                jsonObject.put("customUserID", mCustomUserId);
                jsonObject.put("customMsgType", IMMyselfCustomMessage.CustomMsgType.AddUserRequestFromUser.getValue());
                jsonObject.put("content", mContent);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            for(IMPrivateTeamInfo.OwnerInfo ownerInfo : ownerInfoList) {
                IMCmdIMSendUserMsg cmd = new IMCmdIMSendUserMsg();

                cmd.mOnCommonFailedListener = new DTCmd.OnCommonFailedListener() {
                    @Override
                    public void onCommonFailed(DTCmd.FailedType type, long errorCode,
                                               JSONObject errorJsonObject) throws JSONException {
                        commonFailedDealWithJudge(type);
                    }
                };

                cmd.mOnRecvEndListener = new DTCmd.OnRecvEndListener() {
                    @Override
                    public void onRecvEnd(JSONObject jsonObject) throws JSONException {

                    }
                };

                cmd.mToUID = ownerInfo.getUID();
                cmd.mUserMsgType = IMUserMsg.UserMsgType.Custom;
                cmd.mContent = jsonObject.toString();
                cmd.send();
            }

            done();

        } else {
            //管理员申请--向用户发出申请
            // 创建content
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("groupID", DTTool.getSecretString(mTeamID));
//                jsonObject.put("customUserID", mCustomUserId);
                jsonObject.put("customMsgType", IMMyselfCustomMessage.CustomMsgType.AddUserRequestFromManager.getValue());
                jsonObject.put("content", mContent);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            IMCmdIMSendUserMsg cmd = new IMCmdIMSendUserMsg();

            cmd.mOnCommonFailedListener = new DTCmd.OnCommonFailedListener() {
                @Override
                public void onCommonFailed(DTCmd.FailedType type, long errorCode,
                                           JSONObject errorJsonObject) throws JSONException {
                    commonFailedDealWithJudge(type);
                }
            };

            cmd.mOnRecvEndListener = new DTCmd.OnRecvEndListener() {
                @Override
                public void onRecvEnd(JSONObject jsonObject) throws JSONException {
                    done();
                }
            };

            cmd.mToUID = Long.valueOf(mCustomUserId);
            cmd.mUserMsgType = IMUserMsg.UserMsgType.Custom;
            cmd.mContent = jsonObject.toString();
            cmd.send();
        }
    }
}
