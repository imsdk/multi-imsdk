package am.imsdk.action;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMPrivateMyself;

/**
 * IMAction管理
 * 批量删除IMAction
 */
public final class IMActionsMgr {

	private IMActionsMgr() {}

	public void beginAction(IMAction action) {
		if (mActionsList.contains(action)) {
			DTLog.logError();
			return;
		}

		mActionsList.add(action);
	}

	public void endAction(IMAction action) {
		if (!mActionsList.contains(action)) {
			DTLog.logError();
			return;
		}

		mActionsList.remove(action);
	}

	private ArrayList<IMAction> mActionsList = new ArrayList<IMAction>();

	// singleton
	private volatile static IMActionsMgr sSingleton;

	public static IMActionsMgr getInstance() {
		synchronized (IMActionsMgr.class) {
			if (sSingleton == null) {
				sSingleton = new IMActionsMgr();
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMActionsMgr.class) {
			sSingleton = new IMActionsMgr();
		}
	}
	// newInstance end
}
