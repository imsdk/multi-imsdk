package am.imsdk.action.team;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.aacmd.team.IMCmdTeamDelete;
import am.imsdk.aacmd.team.IMCmdTeamGetAll;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.teaminfo.IMPrivateTeamListMgr;
import imsdk.data.IMMyself;
import remote.service.data.IMRemoteMyself;

// 1. 获取所有Team
// 2. 更新所有Team
// 3. 删除非法Team

// 初始化我的分组信息
public final class IMActionTeamInit extends IMAction {
	private ArrayList<Long> mAryTeamIDs = new ArrayList<Long>();
	private ArrayList<Long> mAryUpdatingTeamIDs = new ArrayList<Long>();
	private ArrayList<IMCmdTeamDelete> mCmdTeamDeleteList = new ArrayList<IMCmdTeamDelete>();

	private long mUID;

	public IMActionTeamInit() {
		mStepCount = 3;
	}

	@Override
	 public void onActionBegan() {
		mAryTeamIDs.clear();
		mAryUpdatingTeamIDs.clear();

		if(ProcessTool.isSocketProcess()) {
			if (IMRemoteMyself.getInstance().getRemoteLoginStatus() != IMMyself.LoginStatus.Logined) {
				doneWithIMSDKError();
				return;
			}

			mUID = IMRemoteMyself.getInstance().getUID();
		} else {
			if (IMPrivateMyself.getInstance().getLoginStatus() != IMMyself.LoginStatus.Logined) {
				doneWithIMSDKError();
				return;
			}

			mUID = IMPrivateMyself.getInstance().getUID();
		}

		if (mUID == 0) {
			doneWithIMSDKError();
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			IMCmdTeamGetAll cmd = new IMCmdTeamGetAll();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					if (getOwnerUID() != mUID) {
						return;
					}

					JSONArray jsonArray = jsonObject.getJSONArray("teamidlist");

					for (int i = 0; i < jsonArray.length(); i++) {
						long teamID = jsonArray.getLong(i);

						if (teamID == 0) {
							doneWithServerError();
							return;
						}

						if (mAryTeamIDs.contains(Long.valueOf(teamID))) {
							doneWithServerError();
							return;
						}

						mAryTeamIDs.add(teamID);
					}
					
					nextStep();
				}
			};

			cmd.mUID = mUID;
			cmd.send();
		}
			break;
		case 2: {
			if (mAryTeamIDs.size() == 0) {
				nextStep();
				return;
			}

			if (mAryUpdatingTeamIDs.size() != 0) {
				doneWithIMSDKError();
				return;
			}

			mAryUpdatingTeamIDs.addAll(mAryTeamIDs);

			for (long teamID : mAryTeamIDs) {
				final IMActionTeamUpdate action = new IMActionTeamUpdate();

				action.mOnActionFailedListener = new OnActionFailedListener() {
					@Override
					public void onActionFailed(String error) {
						
						done(error);
					}
				};

				action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
					@Override
					public void onActionDoneEnd() {
						
						if (!mAryUpdatingTeamIDs.remove(Long.valueOf(action.mTeamID))) {
							
							doneWithIMSDKError();
							return;
						}

						if (mAryUpdatingTeamIDs.size() == 0) {
							
							nextStep();
						}
					}
				};

				action.mTeamID = teamID;
				action.begin();
			}
		}
			break;
		case 3: {
			nextStep();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionDone() {
		IMPrivateTeamListMgr.getInstance().setTeamIDs(mAryTeamIDs);
		IMPrivateTeamListMgr.getInstance().saveFile();
	}

	private void deleteTeam(long teamID) {
		if (teamID <= 0) {
			DTLog.logError();
			return;
		}

		final IMCmdTeamDelete cmd = new IMCmdTeamDelete();

		mCmdTeamDeleteList.add(cmd);

		cmd.mTeamID = teamID;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				commonFailedDealWithJudge(type, errorCode, errorJsonObject);
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				mCmdTeamDeleteList.remove(cmd);
			}
		};

		cmd.send();
	}
}
