package am.imsdk.model.amimteam;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.ambase.IMBaseMsgsMgr;

// 负责管理内存
// 负责从本地读取，不负责本地保存
public final class IMTeamMsgsMgr extends IMBaseMsgsMgr {
	public IMTeamMsg getUnsentTeamMsg(long teamID, long clientSendTime) {
		if (mUID == 0) {
			DTLog.logError();
			return null;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return null;
		}
		
		if (teamID == 0) {
			DTLog.logError();
			return null;
		}

		String key = "" + clientSendTime;
		IMTeamMsg teamMsg = (IMTeamMsg) mMapBaseMsgsUnsent.get(key);

		if (teamMsg != null) {
			return teamMsg;
		}

		teamMsg = new IMTeamMsg();

		teamMsg.mFromCustomUserID = IMPrivateMyself.getInstance().getCustomUserID();
		teamMsg.mTeamID = teamID;
		teamMsg.mClientSendTime = clientSendTime;
		teamMsg.readFromFile();
		mMapBaseMsgsUnsent.put(key, teamMsg);

		return teamMsg;
	}

	public IMTeamMsg getSentTeamMsg(long teamID, long msgID) {
		if (mUID == 0) {
			DTLog.logError();
			return null;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return null;
		}

		if (teamID == 0) {
			DTLog.logError();
			return null;
		}
		
		if (msgID == 0) {
			DTLog.logError();
			return null;
		}

		String key = teamID + "_" + msgID;
		IMTeamMsg teamMsg = (IMTeamMsg) mMapBaseMsgsSent.get(key);

		if (teamMsg != null) {
			return teamMsg;
		}

		teamMsg = new IMTeamMsg();

		teamMsg.mFromCustomUserID = IMPrivateMyself.getInstance().getCustomUserID();
		teamMsg.mTeamID = teamID;
		teamMsg.mMsgID = msgID;
		teamMsg.readFromFile();
		mMapBaseMsgsSent.put(key, teamMsg);

		return teamMsg;
	}

	public IMTeamMsg getRecvTeamMsg(long teamID, long msgID) {
		if (mUID == 0) {
			DTLog.logError();
			return null;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return null;
		}
		
		if (teamID == 0) {
			DTLog.logError();
			return null;
		}

		if (msgID == 0) {
			DTLog.logError();
			return null;
		}

		String key = teamID + "_" + msgID;
		IMTeamMsg teamMsg = (IMTeamMsg) mMapBaseMsgsRecv.get(key);

		if (teamMsg != null) {
			return teamMsg;
		}

		teamMsg = new IMTeamMsg();

		teamMsg.mTeamID = teamID;
		teamMsg.mMsgID = msgID;
		teamMsg.mIsRecv = true;
		teamMsg.readFromFile();

		mMapBaseMsgsRecv.put(key, teamMsg);

		return teamMsg;
	}

	public IMTeamMsg getRecvTeamMsg(JSONObject jsonObject) {
		if (mUID == 0) {
			DTLog.logError();
			return null;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return null;
		}

		if (jsonObject == null) {
			DTLog.logError();
			return null;
		}

		if (!(jsonObject instanceof JSONObject)) {
			DTLog.logError();
			return null;
		}

		long teamID = 0;
		long msgID = 0;
		
		try {
			teamID = jsonObject.getLong("toteamid");
			msgID = jsonObject.getLong("msgid");
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return null;
		}
		
		if (teamID == 0) {
			DTLog.logError();
			return null;
		}
		
		if (msgID == 0) {
			DTLog.logError();
			return null;
		}
		
		IMTeamMsg teamMsg = null;
		String key = teamID + "_" + msgID;
		
		teamMsg = (IMTeamMsg) mMapBaseMsgsRecv.get(key);
		
		if (teamMsg != null) {
			return teamMsg;
		}
		
		teamMsg = new IMTeamMsg(jsonObject);
		mMapBaseMsgsRecv.put(key, teamMsg);
		
		return teamMsg;
	}
	
	public void moveUnsentTeamMsgToBeSent(IMTeamMsg teamMsg) {
		if (mUID == 0) {
			DTLog.logError();
			return;
		}
		
		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return;
		}
		
		if (teamMsg == null) {
			DTLog.logError();
			return;
		}
		
		if (teamMsg.mIsRecv) {
			DTLog.logError();
			return;
		}
		
		if (teamMsg.mClientSendTime == 0) {
			DTLog.logError();
			return;
		}
		
		if (teamMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}
		
		if (teamMsg.mTeamID == 0) {
			DTLog.logError();
			return;
		}
		
		String oldKey = "" + teamMsg.mClientSendTime;
		String newKey = teamMsg.mTeamID + "_" + teamMsg.mMsgID;
		
		mMapBaseMsgsSent.put(newKey, teamMsg);
		mMapBaseMsgsUnsent.put(oldKey, null);
	}

	// singleton
	private volatile static IMTeamMsgsMgr sSingleton;

	public static IMTeamMsgsMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMTeamMsgsMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMTeamMsgsMgr();
				}
			}
		}

		return sSingleton;
	}
	// singleton end
	
	// newInstance
	public static void newInstance() {
		synchronized (IMTeamMsgsMgr.class) {
			sSingleton = new IMTeamMsgsMgr();
		}
	}
	// newInstance end
}
