package am.imsdk.model.userslist;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMBaseUsersMgr;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

public final class IMPrivateFriendsMgr extends IMBaseUsersMgr {
	public IMPrivateFriendsMgr() {
		super();

		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			mUID = IMRemoteMyself.getInstance().getUID();
		} else {
			mUID = IMPrivateMyself.getInstance().getUID();
		}

		addDirectory("IFL");
		addDecryptedDirectory("IMFriendsList");
		
		if (mUID != 0) {
			readFromFile();
		}
	}
	
	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}
		
		mLocalFileName = DTTool.getSecretString(mUID);
		mDecryptedLocalFileName = mUID + "";
		return true;
	}

	// singleton
	private volatile static IMPrivateFriendsMgr sSingleton;

	public static IMPrivateFriendsMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMPrivateFriendsMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMPrivateFriendsMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMPrivateFriendsMgr.class) {
			sSingleton = new IMPrivateFriendsMgr();
		}
	}
	// newInstance end
}
