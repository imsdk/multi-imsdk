package am.imsdk.model.teaminfo;

import java.util.ArrayList;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

/**
 * Created by fzj on 2015/9/17.
 */
public final class IMPrivateTeamListMgr extends DTLocalModel {

    private final long mUID;
    private ArrayList<Long> mAryTeamIDs = new ArrayList<Long>();
    private ArrayList<Long> mGroupIDsList = new ArrayList<Long>();

    public IMPrivateTeamListMgr() {
        if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
            mUID = IMRemoteMyself.getInstance().getUID();
        } else {
            mUID = IMPrivateMyself.getInstance().getUID();
        }

        addDirectory("IPTL");
        addDecryptedDirectory("IMPrivateTeamListMgr");

        if(mUID != 0) {
            this.readFromFile();
        }
    }

    @Override
    public boolean generateLocalFullPath() {
        if (mUID == 0) {
            return false;
        }

        mLocalFileName = DTTool.getSecretString(mUID);
        mDecryptedLocalFileName = mUID + "";
        return true;
    }

    public boolean removeTeam(long teamID) {
        if (!mAryTeamIDs.contains(teamID)) {
            if (mGroupIDsList.contains(teamID)) {
                mGroupIDsList.remove(Long.valueOf(teamID));
                DTLog.logError();
                return false;
            }

            return false;
        }

        mAryTeamIDs.remove(Long.valueOf(teamID));
        mGroupIDsList.remove(Long.valueOf(teamID));
        return true;
    }

    public ArrayList<Long> getTeamIDList() {
        return mAryTeamIDs;
    }

    public ArrayList<Long> getGroupIDList() {
        return mGroupIDsList;
    }

    public void addTeamID(long teamID) {
        if (mAryTeamIDs.contains(teamID)) {
            return;
        }

        mAryTeamIDs.add(teamID);

        IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

        if (teamInfo.mTeamType == IMPrivateTeamInfo.TeamType.Group) {
            if (mGroupIDsList.contains(teamID)) {
                return;
            }

            mGroupIDsList.add(teamID);
        }
    }

    public void setTeamIDs(ArrayList<Long> aryTeamIDs) {
        mAryTeamIDs.clear();
        mAryTeamIDs.addAll(aryTeamIDs);
        mGroupIDsList.clear();

        for (long teamID : mAryTeamIDs) {
            IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

            if (teamInfo.mTeamType == IMPrivateTeamInfo.TeamType.Group) {
                if (mGroupIDsList.contains(teamID)) {
                    return;
                }

                mGroupIDsList.add(teamID);
            }
        }
    }

    // singleton
    private volatile static IMPrivateTeamListMgr sSingleton;

    public static IMPrivateTeamListMgr getInstance() {
        if (sSingleton == null) {
            synchronized (IMPrivateTeamListMgr.class) {
                if (sSingleton == null) {
                    sSingleton = new IMPrivateTeamListMgr();
                }
            }
        }

        return sSingleton;
    }

    // singleton end

    // newInstance
    public static void newInstance() {
        synchronized (IMPrivateTeamListMgr.class) {
            sSingleton = new IMPrivateTeamListMgr();
        }
    }
    // newInstance end

}
