package am.imsdk.model.teaminfo;

import android.text.TextUtils;
import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMBaseUsersMgr;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

public final class IMPrivateTeamInfo extends IMBaseUsersMgr {
	public IMPrivateTeamInfo() {
		super();
		addDirectory("IPTI");
		addDecryptedDirectory("IMPrivateTeamInfo");

		addIgnoreField("mAuthKickOutList");
		addIgnoreField("mAuthKeepSilenceList");
		addIgnoreField("mManagerList");
		addIgnoreField("mUserSilenceList");
	}

	@Override
	public boolean readFromFile() {
		boolean result = super.readFromFile();

		getManagerInfoList();

		return result;
	}

	public enum TeamType {
		Default(0),
		Group(1), //普通群
		PublicGroup(2); //公开群

		private final int value;

		TeamType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<TeamType> sValuesArray = new SparseArray<TeamType>();

		static {
			for (TeamType type : TeamType.values()) {
				sValuesArray.put(type.value, type);
			}
		}

		public static TeamType fromInt(int i) {
			TeamType type = sValuesArray.get(i);

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}

	public long mTeamID;
	public TeamType mTeamType = TeamType.Default;
	public String mTeamName = "";
	public long mFounderUID;
//	public long mOwnerUID;
	public String mCoreInfo = "";
	public String mExInfo = "";
	public long mMaxCount;

	/** 管理员列表(群核心资料CoreInfo)(临时) */
	private ArrayList<OwnerInfo> mManagerList;
	/** 被禁言用户列表(群核心资料CoreInfo)(临时) */
	private ArrayList<Long> mUserSilenceList;

	/** 踢人权限列表(临时) */
	private ArrayList<Long> mAuthKickOutList;
	/** 禁言权限列表(临时) */
	private ArrayList<Long> mAuthKeepSilenceList;
	/** 拉人权限列表（临时） */
	private ArrayList<Long> mAuthAddUserList;

	public class OwnerInfo extends DTLocalModel {
		/** 用户ID */
		long uid;

		/** 权限名称 */
		String authName;
		/**
		 * 权限等级
		 * 1：踢人
		 * 2：禁言
		 * 3：踢人+禁言
		 * 4：拉人
		 */
		int authLevel;

		public String getAuthName() {
			if(TextUtils.isEmpty(authName)) {
				return "管理员";
			}
			return authName;
		}

		public int getAuthLevel() {
			if(authLevel <= 0) {
				return 0;
			}
			return authLevel;
		}

		public long getUID() {
			return uid;
		}

		public OwnerInfo(long uid,String authName, int authLevel) {
			this.uid = uid;
			this.authName = authName;
			this.authLevel = authLevel;
		}
	}

	public ArrayList<OwnerInfo> getManagerInfoList() {
		if(mManagerList != null) {
			return mManagerList;
		}

		if(TextUtils.isEmpty(mCoreInfo)) {
			mManagerList = new ArrayList<OwnerInfo>();
			return mManagerList;
		}

		try {
			JSONObject jsonObject = new JSONObject(mCoreInfo);

			parseManagerInfo(jsonObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}


		return mManagerList;
	}

	public ArrayList<Long> getSilenceUserList() {
		if(mUserSilenceList != null) {
			return mUserSilenceList;
		}

		if(TextUtils.isEmpty(mCoreInfo)) {
			mUserSilenceList = new ArrayList<Long>();
			return mUserSilenceList;
		}

		try {
			JSONObject jsonObject = new JSONObject(mCoreInfo);

			parseUserSilenceInfo(jsonObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return mUserSilenceList;
	}

	public ArrayList<Long> getAuthKickOutList() {
		if(mAuthKickOutList != null) {
			return mAuthKickOutList;
		}

		mAuthKickOutList = new ArrayList<Long>();

		for (OwnerInfo ownerInfo : mManagerList) {
			ArrayList<Integer> authList = DTTool.seperateAuth(ownerInfo.authLevel);

			if(authList.contains(1)) {
				mAuthKickOutList.add(ownerInfo.getUID());
			}
		}

		return mAuthKickOutList;
	}

	public ArrayList<Long> getAuthKeepSilenceList() {
		if(mAuthKeepSilenceList != null) {
			return mAuthKeepSilenceList;
		}

		mAuthKeepSilenceList = new ArrayList<Long>();

		for (OwnerInfo ownerInfo : mManagerList) {
			ArrayList<Integer> authList = DTTool.seperateAuth(ownerInfo.authLevel);

			if(authList.contains(2)) {
				mAuthKeepSilenceList.add(ownerInfo.getUID());
			}
		}

		if(!mAuthKeepSilenceList.contains(mFounderUID)) {
			mAuthKeepSilenceList.add(mFounderUID);
		}

		return mAuthKeepSilenceList;
	}

	public ArrayList<Long> getAuthAddUserList() {
		if(mAuthAddUserList != null) {
			return mAuthAddUserList;
		}

		mAuthAddUserList = new ArrayList<Long>();

		for (OwnerInfo ownerInfo : mManagerList) {
			ArrayList<Integer> authList = DTTool.seperateAuth(ownerInfo.authLevel);

			if(authList.contains(4)) {
				mAuthAddUserList.add(ownerInfo.getUID());
			}
		}

		if(!mAuthAddUserList.contains(mFounderUID)) {
			mAuthAddUserList.add(mFounderUID);
		}

		return mAuthAddUserList;
	}


	/** 改变用户的权限信息 */
	public boolean changeAuthInfo(long userID, String authName, int authLevel) {
		if(!contains(userID)) {
			DTLog.logError();
			return false;
		}

		long uid;

		if(ProcessTool.isSocketProcess()) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}

		//用户不能修改自己的权限
		if(userID == uid) {
			DTLog.logError();
			return false;
		}

		//只有群主才能修改他人权限
		if(uid != mFounderUID) {
			DTLog.logError();
			return false;
		}

		//删除无用的权限信息
		delUnValidUserFromManagerList();

		//将mAuthKickOutList与mAuthKeepSilenceList加入内存
		getAuthKeepSilenceList();
		getAuthKickOutList();
		getAuthAddUserList();

		ArrayList<Integer> authList = DTTool.seperateAuth(authLevel);

		if(authList == null) {
			DTLog.logError();
			return false;
		}

		//踢人权限
		if(authList.contains(1)) {
			if(!mAuthKickOutList.contains(userID)) {
				mAuthKickOutList.add(userID);
			}
		} else {
			if(mAuthKickOutList.contains(userID)) {
				mAuthKickOutList.remove(userID);
			}
		}

		//禁言权限
		if(authList.contains(2)) {
			if(!mAuthKeepSilenceList.contains(userID)) {
				mAuthKeepSilenceList.add(userID);
			}
		} else {
			if(mAuthKeepSilenceList.contains(userID)) {
				mAuthKeepSilenceList.remove(userID);
			}
		}

		//拉人权限
		if(authList.contains(4)) {
			if(!mAuthAddUserList.contains(userID)) {
				mAuthAddUserList.add(userID);
			}
		} else {
			if(mAuthAddUserList.contains(userID)) {
				mAuthAddUserList.remove(userID);
			}
		}

		OwnerInfo unChangedInfo = null; //判断需要添加还是修改
		for (OwnerInfo ownerInfo : mManagerList) {
			if(ownerInfo.getUID() == userID) {
				unChangedInfo = ownerInfo;
			}
		}

		if(unChangedInfo == null) {
			if(authList.size() == 0) {
				DTLog.logError();
				return false;
			}
			unChangedInfo = new OwnerInfo(userID, authName, authLevel);

			mManagerList.add(unChangedInfo);
		} else {
			if(authList.size() == 0) {
				mManagerList.remove(unChangedInfo);
			} else {
				unChangedInfo.authLevel = authLevel;
				unChangedInfo.authName = authName;
			}
		}

		return true;
	}

	/**
	 * 从管理员列表中过滤掉不存在用户（当用户被移除后管理员列表可能有冗余数据）
	 */
	private void delUnValidUserFromManagerList() {
		ArrayList<OwnerInfo> tempList = new ArrayList<OwnerInfo>();

		for (OwnerInfo ownerInfo : mManagerList) {
			if(contains(ownerInfo.getUID())) {
				tempList.add(ownerInfo);
			}
		}

		mManagerList.clear();
		mManagerList.addAll(tempList);
	}

	public void insertSilenceUser(long userID) {
		if(!mUserSilenceList.contains(userID)) {
			mUserSilenceList.add(userID);
		}
	}

	public void removeSilenceUser(long userID) {
		if(mUserSilenceList.contains(userID)) {
			mUserSilenceList.remove(userID);
		}
	}

	@Override
	public boolean generateLocalFullPath() {
		if (mTeamID == 0) {
			return false;
		}

		mDecryptedLocalFileName = mTeamID + "";
		mLocalFileName = DTTool.getSecretString(mTeamID);
		return true;
	}

	public void parseServerData(JSONObject jsonObjectTeamInfo) throws JSONException {
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		if(jsonObjectTeamInfo == null) {
			DTLog.logError();
			return;
		}

		long teamID = jsonObjectTeamInfo.getLong("teamid");

		if (teamID == 0) {
			DTLog.logError();
			return;
		}

		if (teamID != mTeamID) {
			DTLog.logError();
			return;
		}

		mTeamType = TeamType.fromInt(jsonObjectTeamInfo.getInt("teamtype"));
		mTeamName = jsonObjectTeamInfo.getString("teamname");
		mFounderUID = jsonObjectTeamInfo.getLong("uid");

		if (mFounderUID == 0) {
			DTLog.logError();
			return;
		}

		if (jsonObjectTeamInfo.has("coreinfo")) {
			mCoreInfo = jsonObjectTeamInfo.getString("coreinfo");

			if(!TextUtils.isEmpty(mCoreInfo)) {
				try {
					JSONObject jsonObject = new JSONObject(mCoreInfo);

					parseManagerInfo(jsonObject);

					parseUserSilenceInfo(jsonObject);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		if (jsonObjectTeamInfo.has("exinfo")) {
			mExInfo = jsonObjectTeamInfo.getString("exinfo");

		}

		mMaxCount = jsonObjectTeamInfo.getLong("maxcount");

		if (mMaxCount == 0) {
			DTLog.logError();
		}
	}

	private void parseManagerInfo(JSONObject jsonObject) {
		try {
			mManagerList = new ArrayList<OwnerInfo>();

			JSONArray tempManagerList = jsonObject.getJSONArray("managerlist");
			if(tempManagerList != null && tempManagerList.length() > 0) {
				for (int i = 0; i < tempManagerList.length(); i++) {
					JSONObject jObject = (JSONObject) tempManagerList.get(i);
					long uid = jObject.getLong("uid");
					String authname = jObject.getString("authname");
					int authtype = jObject.getInt("authtype");

					mManagerList.add(new OwnerInfo(uid, authname, authtype));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void parseUserSilenceInfo(JSONObject jsonObject) {
		try {
			mUserSilenceList = new ArrayList<Long>();

			JSONArray tempUserSilenceList = jsonObject.getJSONArray("usersilencelist");
			if(tempUserSilenceList != null && tempUserSilenceList.length() > 0) {
				for (int i = 0; i < tempUserSilenceList.length(); i++) {
					mUserSilenceList.add(Long.valueOf(tempUserSilenceList.get(i).toString()));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 打包群核心信息(CoreInfo)
	 * @return json
	 */
	public String packageCoreInfo() {
		JSONObject coreInfoJsonObject = new JSONObject();

		// 1. managerlist
		JSONArray managerJsonArr = new JSONArray();

		for (OwnerInfo ownerInfo : mManagerList) {
			JSONObject tempManager = new JSONObject();
			try {
				tempManager.put("uid", ownerInfo.uid);
				tempManager.put("authname", ownerInfo.authName);
				tempManager.put("authtype", ownerInfo.authLevel);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			managerJsonArr.put(tempManager);
		}

		try {
			coreInfoJsonObject.put("managerlist", managerJsonArr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// 2. usersilencelist
		JSONArray userSilenceJsonArr = new JSONArray();
		for(long tempUid : mUserSilenceList) {
			userSilenceJsonArr.put(tempUid);
		}

		try {
			coreInfoJsonObject.put("usersilencelist", userSilenceJsonArr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return coreInfoJsonObject.toString();
	}
}
