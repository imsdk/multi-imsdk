package am.imsdk.model.im;

import java.util.ArrayList;

import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;

/**
 *
 */
public class IMUserChatMsgTags extends DTLocalModel {

    private ArrayList<String> tags = new ArrayList<String>();
    public long mUID;

    @Override
    public boolean generateLocalFullPath() {
        addDecryptedDirectory("IMUserMsg");

        setDirectory(DTTool.getSecretString(mUID), 1);
        setDecryptedDirectory(mUID + "", 1);

        mLocalFileName = "IUCMT";
        mDecryptedLocalFileName = "IMUserChatMsgTags";

        return true;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public boolean insert(String tag) {
        if(!tags.contains(tag)) {
            tags.add(tag);
            return true;
        }

        return false;
    }

    public IMUserChatMsgTags() {
    }


}
