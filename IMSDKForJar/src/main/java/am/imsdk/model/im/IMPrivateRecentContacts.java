package am.imsdk.model.im;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMBaseUsersMgr;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

// 只记录本地数据，只记录CustomUserID
public final class IMPrivateRecentContacts extends IMBaseUsersMgr {
	public IMPrivateRecentContacts() {
		long uid = 0;
		
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}
		
		if (uid > 0) {
			mUID = uid;
			addDirectory("IUM");
			addDecryptedDirectory("IMUserMsg");
			addDirectory(DTTool.getSecretString(mUID));
			addDecryptedDirectory("" + mUID);
			mLocalFileName = "irc";
			mDecryptedLocalFileName = "IMRecentContacts";
			this.readFromFile();
		}
	}

	@Override
	public boolean generateLocalFullPath() {
		return mUID > 0;

	}

	public String notificationKey() {
		return "recentContactsChanged";
	}

	@Override
	public boolean insert(String customUserID) {
		if (mUID == 0) {
			DTLog.logError();
			return false;
		}
		
		long uid = 0;
		
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}

		if (mUID != uid) {
			DTLog.logError();
			return false;
		}

		if (mUIDsList == null) {
			DTLog.logError();
			return false;
		}

		if (mCustomUserIDsList == null) {
			DTLog.logError();
			return false;
		}

		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			DTLog.logError();
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return false;
		}

//		if (uid == 0) {
//			DTLog.logError();
//			return false;
//		}

		return super.insert(customUserID);
	}

	// singleton
	private volatile static IMPrivateRecentContacts sSingleton;

	public static IMPrivateRecentContacts getInstance() {
		if (sSingleton == null) {
			synchronized (IMPrivateRecentContacts.class) {
				if (sSingleton == null) {
					sSingleton = new IMPrivateRecentContacts();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMPrivateRecentContacts.class) {
			sSingleton = new IMPrivateRecentContacts();
		}
	}
	// newInstance end
}
