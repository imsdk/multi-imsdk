package am.imsdk.model.im;

import imsdk.data.IMSystemMessage;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;

public final class IMPrivateSystemMsg extends DTLocalModel {
	public long mToUID;
	public long mMsgID;
	public IMSystemMessage mSystemMessage = new IMSystemMessage();
	
	public IMPrivateSystemMsg() {
		super();

		addForceField("mSystemMessage");
		addRenameField("mCustonUserId", "mCustomUserId");
	}
	
	public IMPrivateSystemMsg(JSONObject jsonObject) {
		this();
		
		try {
			mToUID = jsonObject.getLong("touid");
			mSystemMessage.mServerSendTime = jsonObject.getLong("sendtime") * 1000;
			mSystemMessage.mContent = jsonObject.getString("msgcontent");
			mSystemMessage.mCustomUserId = jsonObject.getString("fromuid");
			mMsgID = jsonObject.getLong("msgid");
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
		}
	}
	
	@Override
	public boolean generateLocalFullPath() {
		setDirectory("ISMH", 0);
		setDecryptedDirectory("IMSystemMsgHistory", 0);

//		setDirectory(DTTool.getSecretString(mToUID), 1);
//		setDecryptedDirectory("" + mToUID, 1);

//		setDirectory(DTTool.getSecretString(Long.parseLong(mSystemMessage.mCustomUserId)), 2);
//		setDecryptedDirectory("" + mSystemMessage.mCustomUserId, 2);

		mLocalFileName = DTTool.getSecretString(mMsgID);
		mDecryptedLocalFileName = "" + mMsgID;
		return true;
	}

	@Override
	public boolean readFromFile() {
		return super.readFromFile();
	}
}
