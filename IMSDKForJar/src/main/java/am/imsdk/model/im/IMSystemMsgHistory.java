package am.imsdk.model.im;

import java.util.ArrayList;

import am.dtlib.model.c.tool.DTLocalModel;

public class IMSystemMsgHistory extends DTLocalModel {
	
	public long mUID;
	
	public ArrayList<Long> mArrAllSytemMsgKeys = new ArrayList<Long>();
	public ArrayList<Long> mArrUnReadSytemMsgKeys = new ArrayList<Long>();

	public IMSystemMsgHistory() {
		super();
	}

	@Override
	public boolean generateLocalFullPath() {
		setDirectory("ISMH", 0);
		setDecryptedDirectory("IMSystemMsgHistory", 0);

//		setDirectory(DTTool.getSecretString(mUID), 1);
//		setDecryptedDirectory("" + mUID, 1);

		mLocalFileName = "SystemMsgList";
		mDecryptedLocalFileName = "SML";
		return true;
	}
	
	public void insertSystemMsg(IMPrivateSystemMsg msg) {
		mArrAllSytemMsgKeys.add(0, msg.mMsgID);
		mArrUnReadSytemMsgKeys.add(0, msg.mMsgID);
	}
	
	public void removeUnReadSystemMsg(long systemMsgID) {
		mArrUnReadSytemMsgKeys.remove(systemMsgID);
		
		for (int i = mArrUnReadSytemMsgKeys.size() - 1; i >= 0; i--) {
			long msgID = mArrUnReadSytemMsgKeys.get(i);
			
			if(msgID == systemMsgID) {
				mArrUnReadSytemMsgKeys.remove(i);
				return;
			}
		}
	}
	
	public ArrayList<IMPrivateSystemMsg> getUnReadSystemMsgs() {
		ArrayList<IMPrivateSystemMsg> msgArr = new ArrayList<IMPrivateSystemMsg>();
		
		for (int i = 0; i < mArrUnReadSytemMsgKeys.size(); i++) {
			IMPrivateSystemMsg msg = new IMPrivateSystemMsg();
			msg.mMsgID = mArrUnReadSytemMsgKeys.get(i);
			msg.readFromFile();
			
			msgArr.add(msg);
		}
		
		return msgArr;
	}
	
	public ArrayList<IMPrivateSystemMsg> getSystemMsgs() {
		ArrayList<IMPrivateSystemMsg> msgArr = new ArrayList<IMPrivateSystemMsg>();
		
		for (int i = 0; i < mArrAllSytemMsgKeys.size(); i++) {
			IMPrivateSystemMsg msg = new IMPrivateSystemMsg();
			msg.mMsgID = mArrAllSytemMsgKeys.get(i);
			msg.readFromFile();
			
			msgArr.add(msg);
		}
		
		return msgArr;
	}

}
