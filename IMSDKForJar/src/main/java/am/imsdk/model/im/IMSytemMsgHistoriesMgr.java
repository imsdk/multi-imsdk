package am.imsdk.model.im;

import java.util.ArrayList;

import remote.service.data.IMRemoteMyself;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMPrivateMyself;


public final class IMSytemMsgHistoriesMgr {
	
	public IMSystemMsgHistory getSystemMsgHistory() {
		long uid = 0;
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}
		
		for(IMSystemMsgHistory history : mArrSystemMsgHistories) {
			if(history.mUID == uid) {
				return history;
			}
		}
		
		IMSystemMsgHistory history = new IMSystemMsgHistory();
		history.mUID = uid;
		history.readFromFile();
		
		mArrSystemMsgHistories.add(history);
		
		return history;
	}
	
	private ArrayList<IMSystemMsgHistory> mArrSystemMsgHistories = new ArrayList<IMSystemMsgHistory>();
	

	// singleton
	private volatile static IMSytemMsgHistoriesMgr sSingleton;

	private IMSytemMsgHistoriesMgr() {
	}

	public static IMSytemMsgHistoriesMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMSytemMsgHistoriesMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMSytemMsgHistoriesMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end
	
	// newInstance
	public static void newInstance() {
		synchronized (IMSytemMsgHistoriesMgr.class) {
			sSingleton = new IMSytemMsgHistoriesMgr();
		}
	}
	// newInstance end
}
