package am.imsdk.model.im;

import java.util.ArrayList;
import java.util.List;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMPrivateMyself;
import remote.service.data.IMRemoteMyself;

public final class IMUserMsgHistoriesMgr {
	public IMUserChatMsgHistory getUserChatMsgHistory(String customUserID) {
		long uid = 0;
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}
		
		for (IMUserChatMsgHistory history : mAryUserChatMsgHistories) {
			if (history.mUID != uid) {
				DTLog.logError();
				return null;
			}
			
			if (history.mOppositeCustomUserID.equals(customUserID)) {
				return history;
			}
		}
		
		IMUserChatMsgHistory result = new IMUserChatMsgHistory();
		
		result.mOppositeCustomUserID = customUserID;
		result.readFromFile();
		
		mAryUserChatMsgHistories.add(result);
		return result;
	}
	
	public IMUserChatMsgHistoryWithTag getUserChatMsgHistory(String customUserID, String tag) {
		long uid = 0;
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}

		IMUserChatMsgTags mUserMsgTag = null;
		for(IMUserChatMsgTags msgTag : mArrUserMsgTags) {
			if(msgTag.mUID == uid) {
				mUserMsgTag = msgTag;
				break;
			}
		}

		if(mUserMsgTag == null) {
			mUserMsgTag = new IMUserChatMsgTags();
			mUserMsgTag.mUID = uid;
			mUserMsgTag.readFromFile();

			mArrUserMsgTags.add(mUserMsgTag);
		}
		
		for (IMUserChatMsgHistoryWithTag history : mAryUserChatMsgHistoryWithTags) {
			if (history.mUID != uid) {
				DTLog.logError();
				return null;
			}

			if (history.mOppositeCustomUserID.equals(customUserID) && history.tag.equals(tag)) {
				if(mUserMsgTag.insert(tag)) {
					mUserMsgTag.saveFile();
				}

				return history;
			}
		}
		
		IMUserChatMsgHistoryWithTag result = new IMUserChatMsgHistoryWithTag();
		
		result.tag = tag;
		result.mOppositeCustomUserID = customUserID;
		result.readFromFile();
		
		mAryUserChatMsgHistoryWithTags.add(result);

		if(mUserMsgTag.insert(tag)) {
			mUserMsgTag.saveFile();
		}

		return result;
	}

	public List<IMUserChatMsgHistoryWithTag> getUserChatMsgHistoryWithTag(String customUserID) {
		List<IMUserChatMsgHistoryWithTag> histories = new ArrayList<IMUserChatMsgHistoryWithTag>();

		long uid = 0;
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}

		IMUserChatMsgTags mUserMsgTag = null;
		for(IMUserChatMsgTags msgTag : mArrUserMsgTags) {
			if(msgTag.mUID == uid) {
				mUserMsgTag = msgTag;
				break;
			}
		}

		if(mUserMsgTag == null) {
			mUserMsgTag = new IMUserChatMsgTags();
			mUserMsgTag.mUID = uid;
			mUserMsgTag.readFromFile();

			mArrUserMsgTags.add(mUserMsgTag);
		}

		ArrayList<String> tags = mUserMsgTag.getTags();
		for(int i = 0; i < tags.size(); i++) {
			IMUserChatMsgHistoryWithTag hi = getUserChatMsgHistory(customUserID, tags.get(i));
			histories.add(hi);
		}

		return histories;
	}

	public IMUserCustomMsgHistory getUserCustomMsgHistory(String customUserID) {
		long uid = 0;
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}
		
		for (IMUserCustomMsgHistory history : mAryUserCustomMsgHistories) {
			if (history.mUID != uid) {
				DTLog.logError();
				return null;
			}
			
			if (history.mOppositeCustomUserID.equals(customUserID)) {
				return history;
			}
		}
		
		IMUserCustomMsgHistory result = new IMUserCustomMsgHistory();
		
		result.mOppositeCustomUserID = customUserID;
		result.readFromFile();
		
		mAryUserCustomMsgHistories.add(result);
		return result;
	}

	private ArrayList<IMUserChatMsgHistoryWithTag> mAryUserChatMsgHistoryWithTags = new ArrayList<IMUserChatMsgHistoryWithTag>();
	private ArrayList<IMUserChatMsgHistory> mAryUserChatMsgHistories = new ArrayList<IMUserChatMsgHistory>();
	private ArrayList<IMUserCustomMsgHistory> mAryUserCustomMsgHistories = new ArrayList<IMUserCustomMsgHistory>();

	private ArrayList<IMUserChatMsgTags> mArrUserMsgTags = new ArrayList<IMUserChatMsgTags>();

	// singleton
	private volatile static IMUserMsgHistoriesMgr sSingleton;

	private IMUserMsgHistoriesMgr() {
	}

	public static IMUserMsgHistoriesMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMUserMsgHistoriesMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMUserMsgHistoriesMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end
	
	// newInstance
	public static void newInstance() {
		synchronized (IMUserMsgHistoriesMgr.class) {
			sSingleton = new IMUserMsgHistoriesMgr();
		}
	}
	// newInstance end
}
