package am.imsdk.model.a2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTRemoteMgr;
import am.dtlib.model.c.tool.DTFileTool;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.NotifyUtils;
import am.dtlib.model.c.tool.Observer;
import am.dtlib.model.c.tool.PollingUtils;
import am.dtlib.model.d.DTDevice;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.IMActionLogin;
import am.imsdk.action.IMActionLogin.IMActionLoginType;
import am.imsdk.action.ammsgs.IMActionSendTransferMsgs;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.action.fileserver.IMActionFile;
import am.imsdk.demo.util.FacesXml;
import am.imsdk.model.IMAudioSender;
import am.imsdk.model.IMAudioSender.OnAudioSenderListener;
import am.imsdk.model.IMChatSetting;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMPrivateSystemMsg;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.im.IMUserMsgHistory;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import imsdk.data.IMMyself;
import imsdk.data.IMMyself.IMMyselfListener;
import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.IMMyself.OnConnectionChangedListener;
import imsdk.data.IMMyself.OnLoginStatusChangedListener;
import imsdk.data.IMMyself.OnReceiveBitmapListener;
import imsdk.data.IMMyself.OnReceiveOrderListener;
import imsdk.data.IMMyself.OnReceiveTextListener;
import remote.service.SocketService;

public class IMMyself2 {
	public static boolean init(Context applicationContext) {
		if (applicationContext == null) {
			IMParamJudge.setLastError("context couldn't be null");
			return false;
		}

//		if (!IMParamJudge.isAppKeyLegal(appKey)) {
//			return false;
//		}

		if (DTAppEnv.getContext() != applicationContext) {
			// 设置context -- 最高级别环境配置
			DTAppEnv.setContext(applicationContext);

			// 环境配置
			DTAppEnv.setRootDirectory("IMSDK.im");
			DTLog.checkCreateDirectory(DTAppEnv.getIMSDKDirectoryFullPath());

			DTDevice.getInstance();
			DTRemoteMgr.newInstance();
		}

		IMPrivateMyself.getInstance().reinit();
		IMPrivateMyself.getInstance().readFromFile();

//		String address = DTFileTool.getFromAssets(applicationContext, "IMPath.txt");
//		if(TextUtils.isEmpty(address)) {
//			address = "112.74.66.141";
//		}

		String address1 = DTAppEnv.isDebuggable ? "192.168.20.51" : "120.25.86.163";
		String address2 = DTAppEnv.isDebuggable ? "192.168.20.51" : "112.74.66.141";

		Log.e("Debug", "---------IMPath:" + address1);
		DTAppEnv.sIMSDKIpAddress = address1;
		IMActionFile.IM_FILE_URL_DOMAIN_NAME = address1;
		IMActionFile.IM_FILE_URL_REAL_IP = address1;

		//AlarmManager 定时120秒重启一次
		PollingUtils.startPollingService(applicationContext, 60 * 2, SocketService.class, SocketService.ACTION);

		DTRemoteMgr.getInstance().checkConnectState(null);

		return true;
	}

	public static boolean setCustomUserID(String customUserID) {
		if (!IMParamJudge.isAppKeyLegal(IMMyself.getAppKey())) {
			IMParamJudge.setLastError("You should init first!");
			return false;
		}

		if (!(customUserID instanceof String)) {
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID) && customUserID != null
				&& customUserID.length() != 0) {
			return false;
		}

		if (sCustomUserID.equals(customUserID)) {
			return true;
		}

		sCustomUserID = customUserID;

//		if (IMMyself.getLoginStatus() == IMMyself.LoginStatus.Logined) {
//			return true;
//		}

		IMPrivateMyself.getInstance().reinit();
		IMPrivateMyself.getInstance().readFromFile();
		return true;
	}

	public static boolean setPassword(String password) {
		if (IMPrivateMyself.getInstance().getPassword() == password) {
			return true;
		}

		IMPrivateMyself.getInstance().setPassword(password);
		return true;
	}

	public static String getPassword() {
		return IMPrivateMyself.getInstance().getPassword();
	}

	public static boolean isLogined() {
		return IMPrivateMyself.getInstance().getLoginStatus()
				.equals(IMMyself.LoginStatus.Logined);
	}

	public static long register(long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis();

		if(IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.None) {
			l.onFailure("登录状态有误，如有需要，请查看当前登录状态");
			return actionTime;
		}

		if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())) {
			commonActionFailure(l, "register", actionTime);
			return actionTime;
		}

		if (!IMParamJudge.isPasswordLegal(getPassword())) {
			commonActionFailure(l, "register", actionTime);
			return actionTime;
		}

//		if (getAppKey().length() == 0) {
//			commonActionFailure(l, "register", actionTime, "appKey不能为空");
//			return actionTime;
//		}

		IMActionLogin.newInstance();
		IMActionLogin.getInstance().setType(IMActionLoginType.OneKeyRegister);
		IMActionLogin.getInstance().mTimeoutInterval = timeoutInterval;
		IMActionLogin.getInstance().mCustomUserID = IMMyself.getCustomUserID();
		IMActionLogin.getInstance().setPassword(
				IMPrivateMyself.getInstance().getPassword());
		IMActionLogin.getInstance().begin();
		IMActionLogin.getInstance().mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "register", actionTime, error);
			}
		};
		IMActionLogin.getInstance().mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				commonActionSuccess(l, "register", actionTime);
			}
		};

		return actionTime;
	}

	public static void login() {
		login(false, 0, null);
	}

	/**
	 *
	 * @param autoLogin 当前操作是否属于自动登录（只有手动登录能挤掉另一登录账户）
	 * @param timeoutInterval
	 * @param l
	 */
	public static void login(final boolean autoLogin, final long timeoutInterval,
							 final OnActionListener l) {
		final long actionTime = System.currentTimeMillis();
		final String actionType = "login";

		if (!autoLogin) {
			if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())
					&& getCustomUserID() != null && getCustomUserID().length() != 0) {
				commonActionFailure(l, actionType, actionTime);
				return;
			}
		}

		if (!autoLogin) {
			if (!IMParamJudge.isPasswordLegal(getPassword()) && getPassword() != null
					&& getPassword().length() != 0) {
				commonActionFailure(l, actionType, actionTime);
				return;
			}
		}

		IMActionLogin.newInstance();
		IMActionLogin.getInstance().setType(autoLogin ? IMActionLoginType.AutoLogin : IMActionLoginType.OneKeyLogin);
		IMActionLogin.getInstance().mTimeoutInterval = timeoutInterval;
		IMActionLogin.getInstance().mCustomUserID = IMMyself.getCustomUserID();
		IMActionLogin.getInstance().setPassword(IMPrivateMyself.getInstance().getPassword());
		IMActionLogin.getInstance().mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, actionType, actionTime, error);
			}
		};
		IMActionLogin.getInstance().mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				commonActionSuccess(l, "register", actionTime);
			}
		};
		IMActionLogin.getInstance().begin();

	}

	public static void logout(OnActionListener listener) {
		IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);

		DTRemoteMgr.getInstance().logout(listener);
	}
	
	public static void stopSocketService() {
		PollingUtils.stopPollingService(DTAppEnv.getContext(), SocketService.class, SocketService.ACTION);
		
		DTAppEnv.getContext().stopService(new Intent(DTAppEnv.getContext(), SocketService.class));
	}

	public static long sendRevoke(long msgID, String toCustomUserID, HashMap<String, Object> tags,
								  long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendRevoke", actionTime);
			return actionTime;
		}

		if (msgID <= 0) {
			commonActionFailure(l, "sendRevoke", actionTime);
			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		JSONObject mContent = new JSONObject();

		try {
			mContent.put("revokeID", msgID);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = mContent.toString();
		userMsg.mUserMsgType = UserMsgType.RevokeUserMessage;
		userMsg.mTags = tags;
		userMsg.saveFile();

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				commonActionSuccess(l, "sendText", actionTime);
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "sendText", actionTime, error);
			}
		};

		action.mUserMsg = userMsg;

		if(timeoutInterval < 10) {
			timeoutInterval = 10;
		}
		action.mTimeoutInterval = timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	public static long sendText(String text, String toCustomUserID) {
		return sendText(text, toCustomUserID, 10, null);
	}

	public static long sendText(String text, String toCustomUserID,
								long timeoutInterval, final OnActionListener l) {
		return sendText(text, toCustomUserID, null, timeoutInterval, l);
	}

	public static long sendText(String text, String toCustomUserID, HashMap<String, Object> tags,
								long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendText", actionTime);
			return actionTime;
		}

		if (!IMParamJudge.isIMTextLegal(text)) {
			commonActionFailure(l, "sendText", actionTime);
			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = text;
		userMsg.mUserMsgType = UserMsgType.Normal;
		userMsg.mTags = tags;
		userMsg.saveFile();

		final IMUserMsgHistory history;
		if(tags != null && tags.size() > 0 && tags.containsKey("orderId")) {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID, tags.get("orderId").toString());
		} else {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID);
		}

		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentUserMsgToSent(userMsg);
				history.saveFile();

				commonActionSuccess(l, "sendText", actionTime);
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "sendText", actionTime, error);
			}
		};

		action.mUserMsg = userMsg;
		
		if(timeoutInterval < 10) {
			timeoutInterval = 10;
		}
		action.mTimeoutInterval = timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	public static long showNoticeMsg(String text, String toCustomUserID,
									 HashMap<String, Object> tags) {
		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = text;
		userMsg.mUserMsgType = UserMsgType.IMSDKNotice;
		userMsg.mTags = tags;
		userMsg.saveFile();

		final IMUserMsgHistory history;
		if(tags != null && tags.size() > 0 && tags.containsKey("orderId")) {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID, tags.get("orderId").toString());
		} else {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID);
		}

		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		return actionTime;
	}
	
	public static long sendNoticeMsg(String text, String toCustomUserID,
									 HashMap<String, Object> tags, long timeoutInterval,
									 final OnActionListener l) {

		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendNoticeMsg", actionTime);
			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = text;
		userMsg.mUserMsgType = UserMsgType.IMSDKNotice;
		userMsg.mTags = tags;
		userMsg.saveFile();

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				commonActionSuccess(l, "sendNoticeMsg", actionTime);
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "sendNoticeMsg", actionTime, error);
			}
		};

		action.mUserMsg = userMsg;
		if(timeoutInterval < 10) {
			timeoutInterval = 10;
		}
		action.mTimeoutInterval = timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}
	
	/**
	 * 
	 * @Title: sendOrderState 
	 * @Description: 发送订单
	 * @param orderType 0-结束确认 1-结束确认被拒绝 2-订单成功结束
	 * @return long    返回类型 
	 * @throws 
	 * @author 方子君
	 */
	public static long sendOrderState(int orderType, String toCustomUserID, HashMap<String, Object> tags,
									  long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendOrder", actionTime);
			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		userMsg.mToCustomUserID = toCustomUserID;
		
		JSONObject content = new JSONObject();
		try {
			content.put("subType", orderType);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		userMsg.mContent = content.toString();
		userMsg.mUserMsgType = UserMsgType.Order;
		userMsg.mTags = tags;
		userMsg.saveFile();

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
//				history.replaceUnsentUserMsgToSent(userMsg);
//				history.saveFile();

				commonActionSuccess(l, "sendOrder", actionTime);
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "sendOrder", actionTime, error);
			}
		};

		action.mUserMsg = userMsg;
		action.mTimeoutInterval = timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	public static boolean startRecording(String toCustomUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			return false;
		}

		return IMAudioSender.getInstance().startRecordingToUser(toCustomUserID);
	}

	public static long stopRecording(boolean needSend, HashMap<String, Object> tags, final long timeoutInterval,
									 final OnActionListener l) {
		final long actionTime = System.currentTimeMillis();

		boolean result = IMAudioSender.getInstance().stopRecording(needSend, tags,
				timeoutInterval, new OnAudioSenderListener() {
					@Override
					public void onSendSuccess() {
						commonActionSuccess(l, "SendAudio", actionTime);
					}

					@Override
					public void onSendFailure(String error) {
						commonActionFailure(l, "SendAudio", actionTime);
					}

					@Override
					public void onRecordSuccess() {
					}

					@Override
					public void onRecordFailure(String error) {
						commonActionFailure(l, "SendAudio", actionTime);
					}
				}, actionTime);

		if (!result) {
			if (l != null) {
				l.onFailure("IMSDK Error");
			}
		}

		return actionTime;
	}

	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID,
								  final long timeoutInterval, final OnActionProgressListener l) {
		return sendBitmap(bitmap, toCustomUserID, null, timeoutInterval, l);
	}

	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID, HashMap<String, Object> tags,
								  final long timeoutInterval, final OnActionProgressListener l) {
		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", IMParamJudge.getLastError(),
						actionTime);
			}

			return actionTime;
		}

		// 生成IMImagePhoto
		final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(bitmap);

		photo.saveFile();

		// 创建content
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("fileID", photo.mFileID);
			jsonObject.put("width", photo.getBitmap().getWidth());
			jsonObject.put("height", photo.getBitmap().getHeight());
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();

			if (l != null) {
				l.onFailure("IMSDK Error");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", "IMSDK Error", actionTime);
			}

			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = jsonObject.toString();
		userMsg.mUserMsgType = UserMsgType.Photo;
		userMsg.mTags = tags;
		userMsg.saveFile();

		final IMUserMsgHistory history;
		if(tags != null && tags.size() > 0 && tags.containsKey("orderId")) {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID, tags.get("orderId").toString());
		} else {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID);
		}
		
		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentUserMsgToSent(userMsg);
				history.saveFile();

				if (l != null) {
					l.onSuccess();
				}

				if (sListener != null) {
					sListener.onActionSuccess("SendBitmap", actionTime);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage / 100);
				}
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}

				if (sListener != null) {
					sListener.onActionFailure("SendBitmap", error, actionTime);
				}
			}
		};

		action.mUserMsg = userMsg;

		if(timeoutInterval < 10) {
			action.mTimeoutInterval = 10;
		} else {
			action.mTimeoutInterval = timeoutInterval;
		}

		action.mBeginTime = actionTime;
		action.begin();
		return actionTime;
	}

	public static long sendBitmap(final String imgPath, final String toCustomUserID, HashMap<String, Object> tags,
								  final long timeoutInterval, final OnActionProgressListener l) {
		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", IMParamJudge.getLastError(),
						actionTime);
			}

			return actionTime;
		}

		// 生成IMImagePhoto
		final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(imgPath, true);

		if(photo == null) {
			if (l != null) {
				l.onFailure("cannot create IMImagePhoto");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", IMParamJudge.getLastError(),
						actionTime);
			}

			return actionTime;
		}

		photo.saveFile();

		// 创建content
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("fileID", photo.mFileID);
			jsonObject.put("width", photo.getBitmap().getWidth());
			jsonObject.put("height", photo.getBitmap().getHeight());
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();

			if (l != null) {
				l.onFailure("IMSDK Error");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", "IMSDK Error", actionTime);
			}

			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = jsonObject.toString();
		userMsg.mUserMsgType = UserMsgType.Photo;
		userMsg.mTags = tags;
		userMsg.saveFile();

		final IMUserMsgHistory history;
		if(tags != null && tags.size() > 0 && tags.containsKey("orderId")) {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID, tags.get("orderId").toString());
		} else {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID);
		}

		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentUserMsgToSent(userMsg);
				history.saveFile();

				if (l != null) {
					l.onSuccess();
				}

				if (sListener != null) {
					sListener.onActionSuccess("SendBitmap", actionTime);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage / 100);
				}
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}

				if (sListener != null) {
					sListener.onActionFailure("SendBitmap", error, actionTime);
				}
			}
		};

		action.mUserMsg = userMsg;

		if(timeoutInterval < 10) {
			action.mTimeoutInterval = 10;
		} else {
			action.mTimeoutInterval = timeoutInterval;
		}

		action.mBeginTime = actionTime;
		action.begin();
		return actionTime;
	}

	public static long sendAudio(final String audioPath, long durationInMilliSeconds, final String toCustomUserID, HashMap<String, Object> tags,
								 final long timeoutInterval, final OnActionProgressListener l) {
		final long actionTime = System.currentTimeMillis();

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", IMParamJudge.getLastError(),
						actionTime);
			}

			return actionTime;
		}

		byte[] buff = DTFileTool.getContentOfFile(audioPath);

		if (buff == null || buff.length <= 0) {
			if (sListener != null) {
				sListener.onActionFailure("sendAudio", "audio file is not found",
						actionTime);
			}

			return actionTime;
		}

		IMAudio audio = IMAudiosMgr.getInstance().getAudio(buff);

		if (audio == null || !audio.saveFile()) {
			if (sListener != null) {
				sListener.onActionFailure("sendAudio", "audio create failed",
						actionTime);
			}

			return actionTime;
		}

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("fileID", audio.mFileID);
			jsonObject.put("durationInMilliSeconds", durationInMilliSeconds);
			jsonObject.put("format", "AMR_NB");
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = jsonObject.toString();
		userMsg.mUserMsgType = UserMsgType.Audio;
		userMsg.mTags = tags;
		userMsg.saveFile();

		// 维护聊天记录
		final IMUserMsgHistory history;
		if(tags != null && tags.size() > 0 && tags.containsKey("orderId")) {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID, tags.get("orderId").toString());
		} else {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID);
		}

		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());


		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentUserMsgToSent(userMsg);
				history.saveFile();

				if (l != null) {
					l.onSuccess();
				}

				if (sListener != null) {
					sListener.onActionSuccess("sendAudio", actionTime);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage / 100);
				}
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}

				if (sListener != null) {
					sListener.onActionFailure("sendAudio", error, actionTime);
				}
			}
		};

		action.mUserMsg = userMsg;
		action.mTimeoutInterval = timeoutInterval < 10 ? 10 : timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	/**
	 * 批量上传转发消息（即A中转， 代替B、C向对方发送消息）
	 * @param picList 图片集
	 * @param msgText 消息文本
	 * @param hintText1 只发给对象B的文本
	 * @param hintText2 只发给对象C的文本
	 * @param audioPath 音频存储路径
	 * @param audioDuration 音频时长
	 * @param toCustomUserID1 对象B
	 * @param toCustomUserID2 对象C
	 * @param nickName1 对象B的昵称
	 * @param nickName2 对象C的昵称
	 * @param tags1 对象B的附加消息
	 * @param tags2 对象C的附加消息
	 * @param l 进度监听器
	 * @return actionTime
	 */
	public static long sendTransferMsgs(final List<Uri> picList, final String msgText,
											final String hintText1, final String hintText2,
											final String audioPath, final long audioDuration,
											final String toCustomUserID1, final String toCustomUserID2,
											final String nickName1, final String nickName2,
											HashMap<String, Object> tags1, HashMap<String, Object> tags2,
											final OnActionProgressListener l) {
		final long actionTime = System.currentTimeMillis();

		IMActionSendTransferMsgs action = new IMActionSendTransferMsgs();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if(l != null) {
					l.onSuccess();
				}
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if(l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if(l != null) {
					l.onProgress(percentage);
				}
			}
		};

		action.mPicList = picList;
		action.mMsgText = msgText;
		action.mHintText1 = hintText1;
		action.mHintText2 = hintText2;
		action.mAudioPath = audioPath;
		action.mAudioDuration = audioDuration;
		action.mCustomUserID1 = toCustomUserID1;
		action.mCustomUserID2 = toCustomUserID2;
		action.mNickName1 = nickName1;
		action.mNickName2 = nickName2;
		action.mTags1 = tags1;
		action.mTags2 = tags2;

		action.begin();

		return actionTime;
	}

	public static void closeSocket() {
		DTRemoteMgr.getInstance().setOffline();
	}

	public static void setListener(IMMyself.IMMyselfListener listener) {
		sListener = listener;
	}

	public static String getCustomUserID() {
		return IMMyself2.sCustomUserID != null ? IMMyself2.sCustomUserID : "";
	}
	
	public static void setNickName(String nickName) {
		IMPrivateMyself.getInstance().setNickName(nickName);
	}
	
	public static String getAppKey() {
//		if (IMAppSettings.getInstance().mAppKey == null) {
//			IMAppSettings.getInstance().mAppKey = "";
//		}
//
//		return IMAppSettings.getInstance().mAppKey;
		return "";
	}

	public static LoginStatus getLoginStatus() {
		return IMPrivateMyself.getInstance().getLoginStatus();
	}

	protected static void commonActionSuccess(final OnActionListener l,
											  final String actionType, final long actionTime) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (l != null) {
			DTAppEnv.postDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					l.onSuccess();
				}
			});
		}

		if (sListener != null) {
			DTAppEnv.postDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					sListener.onActionSuccess(actionType, actionTime);
				}
			});
		}
	}
	
	public static void setMsgSound(boolean msgSound) {
		//current process
		IMChatSetting.getInstance().setMsgSound(msgSound);
		IMChatSetting.getInstance().saveFile();
	}
	
	public static void setMsgVibrate(boolean msgVibrate) {
		//current process
		IMChatSetting.getInstance().setMsgVibrate(msgVibrate);
		IMChatSetting.getInstance().saveFile();
	}

	protected static void commonActionFailure(OnActionListener l, String actionType,
											  long actionTime) {
		commonActionFailure(l, actionType, actionTime, IMParamJudge.getLastError());
	}

	protected static void commonActionFailure(final OnActionListener l,
											  final String actionType, final long actionTime, final String error) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (l != null) {
			DTAppEnv.postDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					l.onFailure(error);
				}
			});
		}

		if (sListener != null) {
			DTAppEnv.postDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					sListener.onActionFailure(actionType, error, actionTime);
				}
			});
		}
	}

	public static void setOnReceiveTextListener(OnReceiveTextListener l) {
		sOnReceiveTextListener = l;
	}

	public static void setOnReceiveBitmapListener(OnReceiveBitmapListener l) {
		sOnReceiveBitmapListener = l;
	}
	
	public static void setOnReceiveOrderListener(OnReceiveOrderListener l) {
		sOnReceiveOrderListener = l;
	}

	public static void setOnConnectionChangedListener(OnConnectionChangedListener l) {
		sOnConnectionChangedListener = l;
	}

	private static String sCustomUserID = "";
//	private static long sLastSendTextTimeMillis = 0;

	// 通用Listener
	private static IMMyselfListener sListener;

	// IM相关
	private static OnReceiveTextListener sOnReceiveTextListener;
	private static OnReceiveBitmapListener sOnReceiveBitmapListener;
	private static OnReceiveOrderListener sOnReceiveOrderListener;

	// 登录与连接相关
	private static OnConnectionChangedListener sOnConnectionChangedListener;
	private static OnLoginStatusChangedListener sOnLoginStatusChangedListener;

	private static IMMyself.OnInitializedListener sOnOffLineMsgInitializedListener;

	private static NotifyUtils.notifyDelayRunnable sUserNotifyRunnable;
	private static NotifyUtils.notifyDelayRunnable sSystemNotifyRunnable;

	static {
		/*
		socket连接状态更新
		 */
		DTNotificationCenter.getInstance().addObserver("SOCKET_UPDATED",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof Integer)) {
							DTLog.logError();
							return;
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMActionLogin_done",
				new Observer() {
					@Override
					public void update(Object data) {
						FacesXml.downLoadGif(DTAppEnv.getContext());

						IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.Logined);
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMActionUserLogin_failed",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof String)) {
							DTLog.logError();
							return;
						}

						DTLog.logError(data.toString());

						IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);
					}
				});

		/*
		登录状态更新
		 */
		DTNotificationCenter.getInstance().addObserver("IMLoginStatusUpdated",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof LoginStatus)) {
							DTLog.logError();
							return;
						}

						if (sOnLoginStatusChangedListener == null) {
							return;
						}

						LoginStatus oldLoginStatus = (LoginStatus) data;

						sOnLoginStatusChangedListener.onLoginStatusChanged(
								oldLoginStatus, getLoginStatus());
					}
				});

		/*
		userMsg
		接收到文字消息
		 */
		DTNotificationCenter.getInstance().addObserver("IMReceiveText", new Observer() {
			@Override
			public void update(Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Normal) {
					DTLog.logError();
					return;
				}

				if (getLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMPrivateMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMPrivateMyself.getInstance().getUID():"
							+ IMPrivateMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}


				if (sOnReceiveTextListener != null) {
					sOnReceiveTextListener.onReceiveText(userMsg.mContent,
							userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.getExtraData2().toString());
				}

				if (sListener != null) {
					sListener.onReceiveText(userMsg.mContent,
							userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.getExtraData2().toString());
				}

				if(sUserNotifyRunnable != null) {
					DTAppEnv.cancelPreviousPerformRequest(sUserNotifyRunnable);
				}

				sUserNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.UserMsg.getValue(), userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mContent, userMsg.mContent, userMsg.getExtraData2().toString(), userMsg.mServerSendTime);
				DTAppEnv.postDelayOnUIThread(0.2, sUserNotifyRunnable);
			}
		});

		/*
		userMsg
		接收到订单消息
		 */
		DTNotificationCenter.getInstance().addObserver("IMReceiveOrder", new Observer() {
			@Override
			public void update(Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Order) {
					DTLog.logError();
					return;
				}

				if (getLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMPrivateMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMPrivateMyself.getInstance().getUID():"
							+ IMPrivateMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				if(sUserNotifyRunnable != null) {
					DTAppEnv.cancelPreviousPerformRequest(sUserNotifyRunnable);
				}

				sUserNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.UserMsg.getValue(), userMsg.mFromCustomUserID, userMsg.getNickName(),  "[订单消息]", "[订单消息]", userMsg.getExtraData2().toString(), userMsg.mServerSendTime);
				DTAppEnv.postDelayOnUIThread(0.2, sUserNotifyRunnable);

				if (sOnReceiveOrderListener != null) {
					sOnReceiveOrderListener.onReceiveOrder(userMsg.mContent, userMsg.mFromCustomUserID,
							userMsg.getNickName(), userMsg.mServerSendTime, userMsg.getExtraData2().toString());
				}

			}
		});

		/*
		userMsg
		接收到语音消息
		 */
		DTNotificationCenter.getInstance().addObserver("IMReceiveAudio", new Observer() {
			@Override
			public void update(Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Audio) {
					DTLog.logError();
					return;
				}

				if (getLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMPrivateMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMPrivateMyself.getInstance().getUID():"
							+ IMPrivateMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				if(sUserNotifyRunnable != null) {
					DTAppEnv.cancelPreviousPerformRequest(sUserNotifyRunnable);
				}

				sUserNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.UserMsg.getValue(), userMsg.mFromCustomUserID, userMsg.getNickName(),  "[语音]", "[语音]", userMsg.getExtraData2().toString(), userMsg.mServerSendTime);
				DTAppEnv.postDelayOnUIThread(0.2, sUserNotifyRunnable);

				if (sOnReceiveTextListener != null) {
					sOnReceiveTextListener.onReceiveAudio(DTTool.getSecretString(userMsg.mMsgID),
							userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.getExtraData2().toString());
				}

				if (sListener != null) {
					sListener.onReceiveAudio(DTTool.getSecretString(userMsg.mMsgID),
							userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.getExtraData2().toString());
				}
			}
		});

		/*
		userMsg
		获取用户级别系统消息
		 */
		DTNotificationCenter.getInstance().addObserver("IMReceiveSystemText",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMPrivateSystemMsg)) {
							DTLog.logError();
							return;
						}

						IMPrivateSystemMsg systemMessage = (IMPrivateSystemMsg) data;

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if(sSystemNotifyRunnable != null) {
							DTAppEnv.cancelPreviousPerformRequest(sSystemNotifyRunnable);
						}

						sSystemNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.SystemMsg.getValue(), systemMessage.mSystemMessage.mCustomUserId, "通知", "[系统消息]", systemMessage.mSystemMessage.mContent,  "", systemMessage.mSystemMessage.mServerSendTime);
						DTAppEnv.postDelayOnUIThread(0.2, sSystemNotifyRunnable);

						if (sOnReceiveTextListener != null) {
							DTLog.sign("onReceiveText");
							sOnReceiveTextListener.onReceiveSystemText(
									systemMessage.mMsgID,
									systemMessage.mSystemMessage.mCustomUserId,
									systemMessage.mSystemMessage.mContent,
									systemMessage.mSystemMessage.mServerSendTime);
						}

						if (sListener != null) {
							DTLog.sign("sListener onReceiveText");
							sListener.onReceiveSystemText(
									systemMessage.mMsgID,
									systemMessage.mSystemMessage.mCustomUserId,
									systemMessage.mSystemMessage.mContent,
									systemMessage.mSystemMessage.mServerSendTime);
						}
					}
				});

		/*
		登录冲突
		 */
		DTNotificationCenter.getInstance().addObserver("LoginConflict", new Observer() {
			@Override
			public void update(Object data) {
				IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);

				DTRemoteMgr.getInstance().logout(null);

				if (sOnConnectionChangedListener != null) {
					sOnConnectionChangedListener.onDisconnected(true);
				}

				if (sListener != null) {
					sListener.onDisconnected(true);
				}
			}
		});

		/*
		本地发送语音成功
		*/
		DTNotificationCenter.getInstance().addObserver("StopRecordingThenSendAudio",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (userMsg.mUserMsgType != UserMsgType.Audio) {
							DTLog.logError();
							return;
						}

						if (!DTAppEnv.isUIThread()) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							sListener.onActionSuccess("StopRecordingThenSendAudio",
									userMsg.mClientSendTime);
						}
					}
				});

		/*
		本地发送语音失败
		 */
		DTNotificationCenter.getInstance().addObserver(
				"StopRecordingThenSendAudioFailed", new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (userMsg.mUserMsgType != UserMsgType.Audio) {
							DTLog.logError();
							return;
						}

						if (!DTAppEnv.isUIThread()) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							sListener.onActionFailure("StopRecordingThenSendAudio", "",
									userMsg.mClientSendTime);
						}
					}
				});

		/*
		userMsg
		已收到图片通知，还未开始下载图片
		 */
		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapMessage",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						if(sUserNotifyRunnable != null) {
							DTAppEnv.cancelPreviousPerformRequest(sUserNotifyRunnable);
						}

						sUserNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.UserMsg.getValue(), userMsg.mFromCustomUserID, userMsg.getNickName(), "[图片]", "[图片]", userMsg.getExtraData2().toString(), userMsg.mServerSendTime);
						DTAppEnv.postDelayOnUIThread(0.2, sUserNotifyRunnable);

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmapMessage(
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.getExtraData2().toString());
						}

						if (sListener != null) {
							sListener.onReceiveBitmapMessage(
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.getExtraData2().toString());
						}
					}
				});

		/*
		userMsg
		图片下载进度
		 */
		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapProgress",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						if (userMsg.mExtraData == null) {
							DTLog.logError();
							return;
						}

						double progress = 0;

						try {
							progress = userMsg.mExtraData.getDouble("progress");
						} catch (JSONException e) {
							e.printStackTrace();
						}

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmapProgress(progress,
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}

						if (sListener != null) {
							sListener.onReceiveBitmapProgress(progress,
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		/*
		 userMsg
		 图片已下载成功
		 */
		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmap",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						String fileID = userMsg.getFileID();

						if (!IMParamJudge.isFileIDLegal(fileID)) {
							DTLog.logError();
							return;
						}

						IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(fileID);

						if (photo.getBitmap() == null) {
							DTLog.logError();
							return;
						}

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmap(photo.getBitmap(),
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}

						if (sListener != null) {
							sListener.onReceiveBitmap(photo.getBitmap(),
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		//离线消息已经初始化成功
		DTNotificationCenter.getInstance().addObserver("OffLineMsgInitialized", new Observer() {
			@Override
			public void update(Object o) {
				if(sOnOffLineMsgInitializedListener != null) {
					sOnOffLineMsgInitializedListener.onInitialized();
				}
			}
		});
	}

	public static void setOnLoginStatusChangedListener(OnLoginStatusChangedListener l) {
		sOnLoginStatusChangedListener = l;
	}

	public static void setOnOffLineMsgInitializedListener(IMMyself.OnInitializedListener l) {
		sOnOffLineMsgInitializedListener = l;
	}
}