package am.imsdk.model.ambase;

import java.util.HashMap;

import remote.service.data.IMRemoteMyself;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMPrivateMyself;

public class IMBaseMsgsMgr {
	// 未发送成功
	protected HashMap<String, IMBaseMsg> mMapBaseMsgsUnsent = new HashMap<String, IMBaseMsg>();
	// 已发送成功
	protected HashMap<String, IMBaseMsg> mMapBaseMsgsSent = new HashMap<String, IMBaseMsg>();
	// 已接收成功
	protected HashMap<String, IMBaseMsg> mMapBaseMsgsRecv = new HashMap<String, IMBaseMsg>();

	public long mUID;

	public IMBaseMsgsMgr() {
		long uid = 0;
		
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			uid = IMRemoteMyself.getInstance().getUID();
		} else {
			uid = IMPrivateMyself.getInstance().getUID();
		}
		
		mUID = uid;
	}
}
