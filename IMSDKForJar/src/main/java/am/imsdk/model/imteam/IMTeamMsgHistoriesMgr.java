package am.imsdk.model.imteam;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMPrivateMyself;

public final class IMTeamMsgHistoriesMgr {
	public IMTeamChatMsgHistory getTeamChatMsgHistory(long teamID) {
		for (IMTeamChatMsgHistory history : mAryTeamChatMsgHistories) {
			if (history.mUID != IMPrivateMyself.getInstance().getUID()) {
				DTLog.logError();
				return null;
			}

			if (history.mTeamID == teamID) {
				return history;
			}
		}

		IMTeamChatMsgHistory result = new IMTeamChatMsgHistory();

		result.mTeamID = teamID;
		result.readFromFile();

		mAryTeamChatMsgHistories.add(result);
		return result;
	}

	public IMTeamCustomMsgHistory getTeamCustomMsgHistory(long teamID) {
		for (IMTeamCustomMsgHistory history : mAryTeamCustomMsgHistories) {
			if (history.mUID != IMPrivateMyself.getInstance().getUID()) {
				DTLog.logError();
				return null;
			}

			if (history.mTeamID == teamID) {
				return history;
			}
		}

		IMTeamCustomMsgHistory result = new IMTeamCustomMsgHistory();

		result.mTeamID = teamID;
		result.readFromFile();

		mAryTeamCustomMsgHistories.add(result);
		return result;
	}

	private ArrayList<IMTeamChatMsgHistory> mAryTeamChatMsgHistories = new ArrayList<IMTeamChatMsgHistory>();
	private ArrayList<IMTeamCustomMsgHistory> mAryTeamCustomMsgHistories = new ArrayList<IMTeamCustomMsgHistory>();

	// singleton
	private volatile static IMTeamMsgHistoriesMgr sSingleton;

	private IMTeamMsgHistoriesMgr() {
	}

	public static IMTeamMsgHistoriesMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMTeamMsgHistoriesMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMTeamMsgHistoriesMgr();
				}
			}
		}

		return sSingleton;
	}
	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMTeamMsgHistoriesMgr.class) {
			sSingleton = new IMTeamMsgHistoriesMgr();
		}
	}
	// newInstance end
}
