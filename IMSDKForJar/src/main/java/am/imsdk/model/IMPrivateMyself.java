package am.imsdk.model;

import org.json.JSONException;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.action.IMActionLogin;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.amimteam.IMTeamMsgsMgr;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.imgroup.IMPrivateRecentGroups;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.model.userslist.IMPrivateBlacklistMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;
import imsdk.data.IMMyself;
import imsdk.data.IMMyself.LoginStatus;

public final class IMPrivateMyself extends IMPrivateUserInfo {
	private IMPrivateMyself() {
		super();

		mLevel = 2;
		this.addIgnoreField("mLoginStatus");
		this.addIgnoreField("mCustomUserInfoInited");
		this.addIgnoreField("mGroupInfoInited");
		this.addIgnoreField("mRelationsInited");
		this.addIgnoreField("mAllInited");
		this.addIgnoreField("mCheckInitRunnable");
		this.addIgnoreField("mPassword");

		addRenameField("mGroupIDsList", "mGroupIDsList");

		if (IMMyself.getCustomUserID() != null
				&& IMMyself.getCustomUserID().length() > 0) {
			mUID = IMUsersMgr.getInstance().getUID(IMMyself.getCustomUserID());

			if (mUID > 0) {
				long uid = mUID;

				this.readFromFile();

				if (mUID != uid) {
					DTLog.logError();
				}
			}
		}

		DTLog.e("Debug", "IMPrivateMyself construction");
	}

	public static void printCallStatck() {
		Throwable ex = new Throwable();
		StackTraceElement[] stackElements = ex.getStackTrace();
		if (stackElements != null) {
			for (int i = 0; i < stackElements.length; i++) {
				System.out.print(stackElements[i].getClassName()+"/t");
				System.out.print(stackElements[i].getFileName()+"/t");
				System.out.print(stackElements[i].getLineNumber()+"/t");
				System.out.println(stackElements[i].getMethodName());
			}
			System.out.println("-----------------------------------");
		}
	}

	public String getNickName() {
		if (mNickName == null) {
			mNickName = "";
		}

		return mNickName;
	}

	public void setNickName(String mNickName) {
		this.mNickName = mNickName;
	}

	public void setPassword(String password) {
		mPassword = password;
	}

	public String getPassword() {
		if (mPassword == null) {
			mPassword = "";
		}

		return mPassword;
	}

	private String mNickName;
	private String mPassword = "";

	private LoginStatus mLoginStatus = LoginStatus.None;
	private boolean mCustomUserInfoInited;
	private boolean mGroupInfoInited;
	private boolean mRelationsInited;

	private boolean mCustomUserInfoInitedInFact;
//	private boolean mMainPhotoInitedInFact;

	public boolean getCustomUserInfoInited() {
		return mCustomUserInfoInited
				&& IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined;
	}

	public boolean getRelationsInited() {
		return mRelationsInited
				&& IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined;
	}

	public void setCustomUserInfoInited(boolean customUserInfoInited) {
		mCustomUserInfoInited = customUserInfoInited;
	}

	public void setRelationsInited(boolean relationsInited) {
		mRelationsInited = relationsInited;
	}

	public void reinit() {
		DTLog.sign("reinit");

		if (IMMyself.getCustomUserID().length() == 0) {
			return;
		}

		IMPrivateMyself.newInstance();

		// login action
		IMActionLogin.newInstance();

		// Around
//		IMPrivateAroundUsers.newInstance();

		// Relations
		IMPrivateFriendsMgr.newInstance();
		IMPrivateBlacklistMgr.newInstance();

		// IM
		IMUserMsgsMgr.newInstance();
		IMPrivateRecentContacts.newInstance();
		IMPrivateRecentGroups.newInstance();
		IMTeamMsgsMgr.newInstance();

		// IM History
		// [IMSystemMsgHistory newInstance];
		IMUserMsgHistoriesMgr.newInstance();
		IMTeamMsgHistoriesMgr.newInstance();
	}

	private void initCustomUserInfo() {
//		if (mCustomUserInfoInitedInFact) {
//			DTLog.logError();
//			return;
//		}
//
//		IMActionUserRequestCustomUserInfo action = new IMActionUserRequestCustomUserInfo();
//
//		action.mOnActionFailedListener = new OnActionFailedListener() {
//			@Override
//			public void onActionFailed(String error) {
//				if(mCustomUserInfoRetryTime++ >= 3) {
//					mCustomUserInfoInitedInFact = true;
//					return;
//				}
//
//				mCustomUserInfoInited = true;
//
//				DTLog.e("Debug", "IMActionUserRequestCustomUserInfo failed:" + mCustomUserInfoRetryTime);
////				checkAllInited();
//			}
//		};
//
//		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
//			@Override
//			public void onActionDoneEnd() {
//				mCustomUserInfoInited = true;
//
//				if (!mCustomUserInfoInitedInFact) {
//					mCustomUserInfoInitedInFact = true;
//					IMPrivateMyself.this.saveFile();
//
//					DTNotificationCenter.getInstance().postNotification(
//							"CustomUserInfoInitialized");
//				}
//
//				DTLog.e("Debug", "IMActionUserRequestCustomUserInfo done");
////				checkAllInited();
//			}
//		};
//
//		action.mCustomUserID = getCustomUserID();
//		action.mTimeoutInterval = 20;
//		action.begin();
	}


	public void onUserLoginInit() {
		mCustomUserInfoInited = false;
		mGroupInfoInited = false;
		mRelationsInited = false;

//		mMainPhotoInitedInFact = false;

		IMPrivateMyself.this.saveFile();

//		checkAllInited();
	}

	public void onUserRegisterInit() {
		mCustomUserInfoInited = true;
		mGroupInfoInited = true;
		mRelationsInited = true;

//		mMainPhotoInitedInFact = true;

		IMPrivateMyself.this.saveFile();
//		checkAllInited();
	}

	public void onMachineLoginInit() {
		mCustomUserInfoInited = true;
		mGroupInfoInited = true;
		mRelationsInited = true;

//		checkInitAllInFact();
//		checkAllInited();
	}

//	private void checkInitAllInFact() {
////		DTLog.e("Debug", "checkInitAllInFact -- mCustomUserInfoInitedInFact:" + mCustomUserInfoInitedInFact);
//		if (!mCustomUserInfoInitedInFact) {
//			initCustomUserInfo();
//		}
//
//		DTLog.e("Debug", "checkInitAllInFact -- mMainPhotoInitedInFact:" + mMainPhotoInitedInFact);
//		if (!mMainPhotoInitedInFact) {
//			initMainPhoto();
//		}
//	}
//
//	private Runnable mCheckInitRunnable = new Runnable() {
//
//		@Override
//		public void run() {
//			checkAllInited();
//		}
//	};
//
//	private void initMainPhoto() {
//		if (mMainPhotoInitedInFact) {
//			DTLog.logError();
//			return;
//		}
//
//		if (!mCustomUserInfoInited) {
//			return;
//		}
//
//		if (!isLogined()) {
//			DTLog.logError();
//			return;
//		}
//
//		if (IMPrivateMyself.getInstance().getMainPhotoFileID().length() == 0) {
//			if (mMainPhotoInitedInFact) {
//				return;
//			}
//
//			mMainPhotoInitedInFact = true;
//			IMPrivateMyself.getInstance().saveFile();
//			DTNotificationCenter.getInstance().postNotification(
//					"MainPhotoModuleInitialized");
//		}
//
//		IMSDKMainPhoto2.request(IMPrivateMyself.getInstance().getCustomUserID(), 20,
//				new OnBitmapRequestProgressListener() {
//					@Override
//					public void onSuccess(Bitmap mainPhoto, byte[] buffer) {
//						if (mMainPhotoInitedInFact) {
//							return;
//						}
//
//						mMainPhotoInitedInFact = true;
//						IMPrivateMyself.getInstance().saveFile();
//						DTNotificationCenter.getInstance().postNotification(
//								"MainPhotoModuleInitialized");
//					}
//
//					@Override
//					public void onProgress(double progress) {
//					}
//
//					@Override
//					public void onFailure(String error) {
//					}
//				});
//	}

//	private void initAppInfo() {
//		if (mAppInfoInitedInFact) {
//			DTLog.logError();
//			return;
//		}
//
//		if (IMMyself.getAppKey().length() == 0) {
//			DTLog.logError();
//			return;
//		}
//
//		IMActionAppInfoInit action = new IMActionAppInfoInit();
//
//		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
//			@Override
//			public void onActionFailedEnd(String error) {
//				if(mAppInfoRetryTime++ >= 3) {
//					return;
//				}
//				
//				mAppInfoInited = true;
//				
//				DTLog.e("Debug", "IMActionAppInfoInit failed:" + mAppInfoRetryTime);
//				checkAllInited();
//			}
//		};
//
//		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
//			@Override
//			public void onActionDoneEnd() {
//				mAppInfoInited = true;
//				mAppInfoInitedInFact = true;
//				IMPrivateMyself.getInstance().saveFile();
//				
//				DTLog.e("Debug", "IMActionAppInfoInit done");
//				checkAllInited();
//
//				DTNotificationCenter.getInstance().postNotification(
//						"CustomerServiceInited");
//			}
//		};
//
//		action.mTimeoutInterval = 20;
//		action.begin();
//
//		// IMCmdUserGetAppInfo cmd = new IMCmdUserGetAppInfo();
//		//
//		// cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
//		// @Override
//		// public void onCommonFailed(FailedType type, long errorCode,
//		// JSONObject errorJsonObject) throws JSONException {
//		// mAppInfoInited = true;
//		// checkAllInited();
//		// }
//		// };
//		//
//		// cmd.mOnRecvEndListener = new OnRecvEndListener() {
//		// @Override
//		// public void onRecvEnd(JSONObject jsonObject) throws JSONException {
//		// mAppInfoInited = true;
//		// mAppInfoInitedInFact = true;
//		// IMPrivateMyself.getInstance().saveFile();
//		// checkAllInited();
//		// }
//		// };
//		//
//		// cmd.send();
//	}

//	public void checkAllInited() {
//		DTAppEnv.cancelPreviousPerformRequest(mCheckInitRunnable);
//
//		if (mCustomUserInfoInitedInFact) {
//			saveFile();
//			return;
//		} else {
//			DTAppEnv.postDelayOnUIThread(60, mCheckInitRunnable);
//		}
//
//		checkInitAllInFact();
//	}

	public boolean getGroupInfoInited() {
		return mGroupInfoInited && mLoginStatus == LoginStatus.Logined;
	}

	public void setGroupInfoInited(boolean groupInfoInited) {
		mGroupInfoInited = groupInfoInited;
	}

	public void setMainPhotoFileID(String fileID) {
		if (fileID == null) {
			fileID = "";
		}

		if (fileID.equals(getMainPhotoFileID())) {
			return;
		}

		mMainPhotoFileID = fileID;

		try {
			mBaseInfoJsonObject.put("p", mMainPhotoFileID);
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return;
		}

		mBaseInfo = mBaseInfoJsonObject.toString();
	}

	public void setLoginStatus(LoginStatus loginStatus) {
		if (mLoginStatus == loginStatus) {
			return;
		}

		LoginStatus oldLoginStatus = mLoginStatus;

		mLoginStatus = loginStatus;
		DTNotificationCenter.getInstance().postNotification("IMLoginStatusUpdated",
				oldLoginStatus);
		DTLog.sign("newLoginStatus:" + mLoginStatus);
	}

	public LoginStatus getLoginStatus() {
		return mLoginStatus;
	}

	public boolean isLogined() {
		DTLog.e("Debug", "curr LoginStatus:" + mLoginStatus);
		return mLoginStatus == LoginStatus.Logined;
	}

	public void checkIfIPAddressChanged() {
		// 未完成 by lyc
		// DTDevice.getInstance().queryIPAddress();
	}

	// singleton
	private volatile static IMPrivateMyself sSingleton;

	public static IMPrivateMyself getInstance() {
		if (sSingleton == null) {
			synchronized (IMPrivateMyself.class) {
				if (sSingleton == null) {
					sSingleton = new IMPrivateMyself();
				}
			}
		}

		if (sSingleton == null) {
			DTLog.logError();
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMPrivateMyself.class) {
			sSingleton = new IMPrivateMyself();
		}
	}
	// newInstance end

}
