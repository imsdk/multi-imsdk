package am.imsdk.demo.gif;

import android.graphics.drawable.AnimationDrawable;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class GifTextDrawable extends AnimationDrawable {
	private WeakReference<TextView> mTextViewReference = null;
	private int mCurrentFrameIndex = -1;
	private boolean mStopped = false;

	public GifTextDrawable(TextView textView) {
		setTextView(textView);
	}

	public void setTextView(TextView textView) {
		this.mTextViewReference = new WeakReference<TextView>(textView);
	}

	@Override
	public boolean selectDrawable(int index) {
		mCurrentFrameIndex = index;
		return super.selectDrawable(index);
	}
	
	@Override
	public void scheduleSelf(Runnable what, long when) {
		if (!mStopped) {
			if (mTextViewReference != null && mTextViewReference.get() != null) {
				TextView textView = mTextViewReference.get();

				textView.postInvalidate();
				textView.postDelayed(this, this.getDuration(mCurrentFrameIndex));
			}
		}
	}
	
	public int getCurrentFrame(){
		return mCurrentFrameIndex;
	}

	@Override
	public void stop() {
		super.stop();
		mStopped = true;
	}
}
