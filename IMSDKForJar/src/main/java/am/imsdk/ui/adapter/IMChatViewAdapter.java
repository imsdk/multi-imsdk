package am.imsdk.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.action.IMAction;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.action.fileserver.IMActionDownloadFile;
import am.imsdk.demo.gif.GifEmotionUtils;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.DateUtils;
import am.imsdk.model.IMAudioPlayer;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.a2.IMMyself2;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.im.IMUserMsgHistory;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImageThumbnail;
import am.imsdk.serverfile.image.IMImagesMgr;
import am.imsdk.ui.views.IMBaseChatView;
import am.imsdk.ui.views.dialog.AlertDialog;
import am.imsdk.ui.views.dialog.WaitingDialog;
import imsdk.data.IMMessage;
import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.localchatmessagehistory.IMChatMessage;
import imsdk.data.mainphoto.IMChatImageView;
import imsdk.views.RoundedImageView;

public class IMChatViewAdapter extends IMBaseChatViewAdapter {
	// init
	private IMUserMsgHistory mUserChatMsgHistory;

	private LayoutInflater inflater;
	protected WeakReference<Activity> mActivityReference;

	private static final int MESSAGE_TYPE_RECV_TXT = 0;
	private static final int MESSAGE_TYPE_SENT_TXT = 1;
	private static final int MESSAGE_TYPE_SENT_IMAGE = 2;
	private static final int MESSAGE_TYPE_SENT_LOCATION = 3;
	private static final int MESSAGE_TYPE_RECV_LOCATION = 4;
	private static final int MESSAGE_TYPE_RECV_IMAGE = 5;
	private static final int MESSAGE_TYPE_SENT_VOICE = 6;
	private static final int MESSAGE_TYPE_RECV_VOICE = 7;
	private static final int MESSAGE_TYPE_SENT_VIDEO = 8;
	private static final int MESSAGE_TYPE_RECV_VIDEO = 9;
	private static final int MESSAGE_TYPE_SENT_FILE = 10;
	private static final int MESSAGE_TYPE_RECV_FILE = 11;
	private static final int MESSAGE_TYPE_SENT_VOICE_CALL = 12;
	private static final int MESSAGE_TYPE_RECV_VOICE_CALL = 13;
	private static final int MESSAGE_TYPE_RECV_ORDER = 14;
	private static final int MESSAGE_TYPE_RECV_Notice = 15;

	private ArrayList<String> mKeyList = new ArrayList<>();

	public IMChatViewAdapter(IMBaseChatView imchatView, Context context,
							 GifEmotionUtils gifEmotionUtils, ArrayList<Integer> ids,
							 boolean userNameVisible, boolean userMainPhotoVisible, int userMainPhotoCornerRadius,
							 IMBaseChatView.OnChatViewTouchListener onChatViewTouchListener,
							 String customUserID, HashMap<String, Object> tag) {
		super(context, gifEmotionUtils, ids, userNameVisible, userMainPhotoVisible,
				userMainPhotoCornerRadius, onChatViewTouchListener);

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return;
		}

		inflater = LayoutInflater.from(context);
		mActivityReference = new WeakReference<Activity>((Activity) context);

		mImchatView = imchatView;

		setCustomUserID(customUserID, tag);
	}

	public void setCustomUserID(String customUserID, HashMap<String, Object> tag) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return;
		}

		if(tag != null && tag.size() > 0 && tag.containsKey("orderId")) {
			mUserChatMsgHistory = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID, tag.get("orderId").toString());
		} else {
			mUserChatMsgHistory = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID);
		}

		if (mUserChatMsgHistory == null) {
			DTLog.logError();
		}
	}

	@Override
	public IMUserMsg getItem(int position) {
		return mUserChatMsgHistory.getUserMsg(mUserChatMsgHistory.getCount() - 1
				- position);
	}

	@Override
	public int getCount() {
		return mUserChatMsgHistory.getCount();
	}

	/**
	 * 获取item的View类型
	 */
	public int getItemViewType(int position) {
		IMUserMsg userMsg = getItem(position);

		switch (userMsg.mUserMsgType) {
			case Normal:
			case Custom: //文字
				return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_TXT : MESSAGE_TYPE_SENT_TXT;
			case Audio:  //语音
				return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_VOICE : MESSAGE_TYPE_SENT_VOICE;
			case Photo:  //图片
				return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_IMAGE : MESSAGE_TYPE_SENT_IMAGE;
			case Video:  //视频
				return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_VIDEO : MESSAGE_TYPE_SENT_VIDEO;
			case File:   //文件
				return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_FILE : MESSAGE_TYPE_SENT_FILE;
			case Order:
				return MESSAGE_TYPE_RECV_ORDER;
			case IMSDKNotice:
				return MESSAGE_TYPE_RECV_Notice;
			default:
				return -1;
		}
	}

	/**
	 * 得到总的视图的个数
	 */
	public int getViewTypeCount() {
		return 16;
	}

	/**
	 * 通过messageType获取View
	 */
	private View createViewByMessage(IMUserMsg message) {
		switch (message.mUserMsgType) {
			case Audio:  //语音
				return message.mIsRecv ?
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_received_voice"), null) :
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_sent_voice"), null);
			case Photo:  //图片
				return message.mIsRecv ?
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_received_picture"), null) :
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_sent_picture"), null);
			case Video:  //视频
				return message.mIsRecv ?
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_received_video"), null) :
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_sent_video"), null);
			case File:   //文件
				return message.mIsRecv ?
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_received_file"), null) :
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_sent_file"), null);
			case Order:
				return inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
						"im_chatview_item_received_order"), null);
			case IMSDKNotice:
				return inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
						"im_chatview_item_received_notice"), null);
			case Normal:
			case Custom: //文字
			default:
				return message.mIsRecv ?
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_received_message"), null) :
						inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
								"im_chatview_item_sent_message"), null);
		}
	}

	private void createItemByMessage(IMUserMsg message, View convertView, ViewHolder holder) {
		switch (message.mUserMsgType) {
			case Audio:  //语音
				try {
					holder.lb_content = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "voice_view"));
					holder.iv = ((ImageView) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "iv_voice")));
					holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "iv_userhead"));
					holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "tv_length"));
					holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "pb_sending"));
					holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "msg_status"));
					holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "tv_userid"));
					holder.iv_read_status = (ImageView) convertView
							.findViewById(CPResourceUtil.getId(mActivityReference.get(),
									"iv_unread_voice"));
				} catch (Exception e) {
				}
				break;
			case Photo:  //图片
				try {
					holder.iv = ((ImageView) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "iv_sendPicture")));
					holder.iv_sendPicture_2 = ((ImageView) convertView
							.findViewById(CPResourceUtil.getId(mActivityReference.get(),
									"iv_sendPicture_2")));
					holder.iv_sendPicture_3 = ((ImageView) convertView
							.findViewById(CPResourceUtil.getId(mActivityReference.get(),
									"iv_sendPicture_3")));
					holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "iv_userhead"));
					holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "percentage"));
					holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "pb_sending"));
					holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "msg_status"));
					holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "tv_userid"));
				} catch (Exception e) {
				}
				break;
			case Video:  //视频
				try {
					holder.iv = ((ImageView) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "chatting_content_iv")));
					holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "iv_userhead"));
					holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "percentage"));
					holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "progressBar"));
					holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "msg_status"));
					holder.size = (TextView) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "chatting_size_iv"));
					holder.timeLength = (TextView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "chatting_length_iv"));
					holder.playBtn = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "chatting_status_btn"));
					holder.container_status_btn = (LinearLayout) convertView
							.findViewById(CPResourceUtil.getId(mActivityReference.get(),
									"container_status_btn"));
					holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "tv_userid"));

				} catch (Exception e) {
				}
				break;
			case File:   //文件
				try {
					holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "iv_userhead"));
					holder.tv_file_name = (TextView) convertView
							.findViewById(CPResourceUtil.getId(mActivityReference.get(),
									"tv_file_name"));
					holder.tv_file_size = (TextView) convertView
							.findViewById(CPResourceUtil.getId(mActivityReference.get(),
									"tv_file_size"));
					holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "pb_sending"));
					holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "msg_status"));
					holder.tv_file_download_state = (TextView) convertView
							.findViewById(CPResourceUtil.getId(mActivityReference.get(),
									"tv_file_state"));
					holder.ll_container = (LinearLayout) convertView
							.findViewById(CPResourceUtil.getId(mActivityReference.get(),
									"ll_file_container"));
					// 这里是进度值
					holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "percentage"));

					holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil
							.getId(mActivityReference.get(), "tv_userid"));
				} catch (Exception e) {
				}
				break;
			case Order:
				try {
					holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "iv_userhead"));
					holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_userid"));

					holder.order_view_1 = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "order_view_1"));
					holder.order_view_2 = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "order_view_2"));
					holder.order_view_3 = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "order_view_3"));
					holder.order_clickview_2 = convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "order_clickview_2"));
					holder.order_clickview_3 = convertView.findViewById(CPResourceUtil.getId(
							mActivityReference.get(), "order_clickview_3"));
				} catch (Exception e) {
				}
				break;
			case IMSDKNotice:
				try {
					holder.tv_chatnotice = (TextView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_chatnotice"));
				} catch (Exception e) {
				}
				break;
			case Normal:
			case Custom: //文字
			default:
				try {
					holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "pb_sending"));
					holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "msg_status"));
					holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "iv_userhead"));
					// 这里是文字内容
					holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_chatcontent"));
					holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_userid"));
					holder.rl_chat_content = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "rl_chat_content"));
				} catch (Exception e) {
				}
				break;
		}
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		final IMUserMsg message = getItem(position);

		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = createViewByMessage(message);

			createItemByMessage(message, convertView, holder);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (!message.mIsRecv) {
			if(message.mStatus == IMChatMessage.SENDING_OR_RECVING) {

				//注册发送状态监听事件
				DTNotificationCenter.getInstance().removeObservers(
						message.getStatusChangedNotificationKey());

				DTNotificationCenter.getInstance().addObserver(
						message.getStatusChangedNotificationKey(), new Observer() {
							@Override
							public void update(Object data) {
								switch (message.mUserMsgType) {
									case Normal:
									case Custom:
									case Audio:
										updateTextUI(message.mStatus, holder);
										break;
									case Photo:
										updatePhotoUI(message.mStatus, 100, holder);
										break;
									default:
										break;
								}
							}
						});

				mKeyList.add(message.getStatusChangedNotificationKey());
			} else {
				DTNotificationCenter.getInstance().removeObservers(
						message.getStatusChangedNotificationKey());

				mKeyList.remove(message.getStatusChangedNotificationKey());
			}
		}

		//单聊不需要显示用户名称
		if (message.mIsRecv) {
			if(holder.tv_userId != null) {
				IMUserMsg latestMsg = mUserChatMsgHistory.getLatestRecvUserMsg();

				if(latestMsg != null && latestMsg.getNickName() != null && latestMsg.getNickName().length() > 0) {
					holder.tv_userId.setVisibility(View.VISIBLE);
					holder.tv_userId.setText(latestMsg.getNickName());
				} else {
					holder.tv_userId.setVisibility(View.GONE);
				}
			}
		}

		showChatHeardIcon(message, holder.head_iv);

		switch (message.mUserMsgType) {
			// 根据消息type显示item
			case Photo: // 图片
				handleImageMessage(message, holder, position, convertView);
				break;
			case Custom:
			case Normal: // 文本
				handleTextMessage(message, holder, position);
				break;
			case Audio: // 语音
				handleVoiceMessage(message, holder, position, convertView);
				break;
			case Video: // 视频
//			handleVideoMessage(message, holder, position, convertView);
				break;
			case File: // 一般文件
//			handleFileMessage(message, holder, position, convertView);
				break;
			case Order:
				handleOrderMessage(message, holder, position, convertView);
				break;
			case IMSDKNotice:
				handleNoticeMessage(message, holder, position, convertView);
				break;
			default:
				// not supported
		}

		long currTime = 0;
		long lastTime = 0;

		if(message.mIsRecv) {
			currTime = message.mServerSendTime;
		} else {
			currTime = message.mClientSendTime;
		}

		View timeLayout = convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "timeLayout"));

		TextView timestamp = (TextView) convertView.findViewById(CPResourceUtil
				.getId(mActivityReference.get(), "timestamp"));

		if(timestamp != null) {
			if (position == 0) {
				timeLayout.setVisibility(View.VISIBLE);
				timestamp.setText(DateUtils.getTimestampString(new Date(currTime)));
			} else {
				if(getItem(position - 1).mIsRecv) {
					lastTime = getItem(position - 1).mServerSendTime;
				} else {
					lastTime = getItem(position - 1).mClientSendTime;
				}

				// 两条消息时间离得如果稍长，显示时间
				if (Math.abs(currTime - lastTime) < 1000 * 60) {
					timeLayout.setVisibility(View.GONE);
				} else {
					timestamp.setText(DateUtils.getTimestampString(new Date(currTime)));
					timeLayout.setVisibility(View.VISIBLE);
				}
			}
		}


		return convertView;
	}

	private void showChatHeardIcon(IMUserMsg message, ImageView iv_head) {
		if(iv_head == null) {
			return;
		}

		if (mUserMainPhotoVisible) {
			iv_head.setVisibility(View.VISIBLE);

			if (iv_head instanceof RoundedImageView) {
				RoundedImageView roundedImageView = (RoundedImageView) iv_head;

//				roundedImageView.setCornerRadius(mUserMainPhotoCornerRadius);

				if (message.mIsRecv) {
					if(mChatingHead != null) {
						roundedImageView.setImageBitmap(mChatingHead);
					} else {
						roundedImageView.setImageResource(CPResourceUtil.getDrawableId(mContext, "im_mini_avatar_shadow"));
					}
				} else {
					if(mMainHead != null) {
						roundedImageView.setImageBitmap(mMainHead);
					} else {
						roundedImageView.setImageResource(CPResourceUtil.getDrawableId(mContext, "im_mini_avatar_shadow"));
					}
				}
				//头像点击事件
				dealHeadPhotoClick(roundedImageView, message);
			}
		} else {
			iv_head.setVisibility(View.GONE);
		}
	}

	/**
	 * 图片消息
	 */
	private void handleImageMessage(final IMUserMsg message, final ViewHolder holder, final int position, final View convertView) {
		if (message.mUserMsgType != UserMsgType.Photo) {
			return;
		}

		holder.iv.setImageBitmap(null);

		final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
				message.getFileID());

		if (photo == null || photo.getBitmap() == null) {
			switch (message.mStatus) {
				case IMMessage.SUCCESS:
				case IMMessage.FAILURE:
					holder.pb.setVisibility(View.GONE);
					holder.tv.setVisibility(View.GONE);
					holder.staus_iv.setVisibility(View.GONE);

					holder.iv.setVisibility(View.GONE);
					holder.iv_sendPicture_2.setVisibility(View.GONE);
					holder.iv_sendPicture_3.setVisibility(View.VISIBLE);

					holder.iv_sendPicture_3.setImageResource(CPResourceUtil.getDrawableId(mContext,
							"im_download_image_damage_icon"));

					holder.iv_sendPicture_3.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							Toast.makeText(mContext, "图片损坏，重新下载...", Toast.LENGTH_SHORT).show();

							message.mStatus = IMChatMessage.SENDING_OR_RECVING;
							message.saveFile();

							downloadFile(message.mUserMsgType, message.getFileID(), new IMMyself.OnActionProgressListener() {
								@Override
								public void onSuccess() {
									message.mStatus = IMChatMessage.SUCCESS;
									message.saveFile();

									handleImageMessage(message, holder, position, convertView);
								}

								@Override
								public void onProgress(double progress) {
									updatePhotoUI(message.mStatus, (int) progress, holder);
								}

								@Override
								public void onFailure(String error) {
									message.mStatus = IMChatMessage.FAILURE;
									message.saveFile();

									handleImageMessage(message, holder, position, convertView);
								}
							});
						}
					});

					holder.iv_sendPicture_3.setTag(position);
					holder.iv_sendPicture_3.setOnLongClickListener(mContentLongClickListener);
					break;
				case IMMessage.SENDING_OR_RECVING:
					holder.staus_iv.setVisibility(View.GONE);
					holder.pb.setVisibility(View.VISIBLE);
					holder.tv.setVisibility(View.VISIBLE);

					holder.iv.setVisibility(View.GONE);
					holder.iv_sendPicture_2.setVisibility(View.GONE);
					holder.iv_sendPicture_3.setVisibility(View.VISIBLE);

					holder.iv_sendPicture_3.setImageResource(CPResourceUtil.getDrawableId(mContext,
							"im_download_image_damage_icon"));

					downloadFile(message.mUserMsgType, message.getFileID(), new IMMyself.OnActionProgressListener() {
						@Override
						public void onSuccess() {
							message.mStatus = IMChatMessage.SUCCESS;
							message.saveFile();

							handleImageMessage(message, holder, position, convertView);
						}

						@Override
						public void onProgress(double progress) {
							updatePhotoUI(message.mStatus, (int) progress, holder);
						}

						@Override
						public void onFailure(String error) {
							message.mStatus = IMChatMessage.FAILURE;
							message.saveFile();

							handleImageMessage(message, holder, position, convertView);
						}
					});
					break;
			}

		} else {
			DTLog.e("Debug", "position:" + position + ", photo is available, status=" + message.mStatus);

			int width = photo.getWidth();
			int height = photo.getHeight();

			float scale = calculateImageScaleRatio(width, height);

			width = (int) (width * scale);
			height = (int) (height * scale);

			IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
					message.getFileID(), width, height);

			if (thumbnail == null) {
				DTLog.logError();
				return;
			}

			holder.iv.setImageBitmap(thumbnail.getBitmap());

			final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);

			holder.iv.setLayoutParams(params);
			holder.iv_sendPicture_2.setLayoutParams(params);

			holder.iv.setVisibility(View.VISIBLE);
			holder.iv_sendPicture_2.setVisibility(View.VISIBLE);
			holder.iv_sendPicture_3.setVisibility(View.GONE);

			switch (message.mStatus) {
				case IMMessage.SUCCESS:
					holder.pb.setVisibility(View.GONE);
					holder.tv.setVisibility(View.GONE);
					holder.staus_iv.setVisibility(View.GONE);
					break;
				case IMMessage.FAILURE:
					holder.pb.setVisibility(View.GONE);
					holder.tv.setVisibility(View.GONE);
					holder.staus_iv.setVisibility(View.VISIBLE);
					break;
				case IMMessage.SENDING_OR_RECVING:
					holder.pb.setVisibility(View.VISIBLE);
					holder.tv.setVisibility(View.VISIBLE);
					holder.staus_iv.setVisibility(View.GONE);
					break;
			}
		}

		//聊天图片点击事件
		holder.iv_sendPicture_2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (photo == null || photo.getBitmap() == null) {
					return;
				}

				// 大图显示初始化
				DisplayMetrics dm = new DisplayMetrics();
				WindowManager windowManager = (WindowManager) mContext
						.getSystemService(Context.WINDOW_SERVICE);
				windowManager.getDefaultDisplay().getMetrics(dm);

				if (popupWindow == null || chatImageView == null) {
					chatImageView = mInflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
							"im_chatview_image"), null);
					popupWindow = new PopupWindow(chatImageView, dm.widthPixels, dm.heightPixels);
				}

				popupWindow.setFocusable(true);
				popupWindow.setOutsideTouchable(true);// 设置允许在外点击消失
				popupWindow.setBackgroundDrawable(new BitmapDrawable(mContext.getResources(),
						Bitmap.createBitmap(dm.widthPixels, dm.heightPixels, Bitmap.Config.ALPHA_8)));

				IMChatImageView image = (IMChatImageView) chatImageView.findViewById(CPResourceUtil.getId(mContext, "imimg_image"));

				image.setImageBitmap(photo.getBitmap());

				popupWindow.showAtLocation(mImchatView, Gravity.CENTER, 0, 0);
			}
		});

		holder.iv_sendPicture_2.setTag(position);
		holder.iv_sendPicture_2.setOnLongClickListener(mContentLongClickListener);

		if(!message.mIsRecv) {
			holder.staus_iv.setTag(message);
			holder.staus_iv.setOnClickListener(mResendClickListener);
		}
	}

	private void downloadFile(final UserMsgType msgType, final String fileID, final IMMyself.OnActionProgressListener l) {
		final IMActionDownloadFile action = new IMActionDownloadFile();

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				DTLog.e("Debug", "download file percentage:" + percentage);

				l.onProgress(percentage);
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				DTLog.e("Debug", "download file success!");

				switch (msgType) {
					case Normal:
					case Custom:
						break;
					case Audio:
						final IMAudio audio = IMAudiosMgr.getInstance().getAudio(
								fileID);

						if(audio != null) {
							audio.mBuffer = action.mBuffer;
							audio.saveFile();
						}

						if (audio == null || !audio.isLocalFileExist()) {
							DTLog.logError();
							l.onFailure("download failed!");
							return;
						}
						break;
					case Photo:
						final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
								fileID);

						if(photo != null) {
							photo.setBuffer(action.mBuffer);
							photo.saveFile();
						}

						if (photo == null || photo.getBitmap() == null) {
							DTLog.logError();
							l.onFailure("download failed!");
							return;
						}
						break;
					default:
						break;
				}

				l.onSuccess();
			}
		};

		action.mOnActionFailedListener = new IMAction.OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				DTLog.e("Debug", "download file failed!");

				l.onFailure(error);
			}
		};

		action.mFileID = fileID;
		action.begin();
	}

	/**
	 * 文本消息
	 */
	private void handleTextMessage( final IMUserMsg message, final ViewHolder holder, final int position) {
		if (message.mUserMsgType != UserMsgType.Normal && message.mUserMsgType != UserMsgType.Custom) {
			return;
		}

		if(holder.rl_chat_content != null){
			holder.rl_chat_content.setVisibility(View.VISIBLE);
		}

		if (message.mContent != null) {
			mGifEmotionUtils.setSpannableText(holder.tv, message.mContent, mHandler);
			holder.tv.setVisibility(View.VISIBLE);
			holder.tv.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
					null);
			holder.tv.setOnTouchListener(mOnTouchListener);
		} else {
			holder.tv.setText("");
			holder.tv.setVisibility(View.VISIBLE);
		}

		holder.tv.setTag(position);
		holder.tv.setOnLongClickListener(mContentLongClickListener);

		if (!message.mIsRecv) {
			updateTextUI(message.mStatus, holder);

			holder.staus_iv.setTag(message);
			holder.staus_iv.setOnClickListener(mResendClickListener);
		}
	}

	private void handleVoiceMessage(final IMUserMsg message, final ViewHolder holder, final int position, final View convertView) {
		if(message.mUserMsgType != UserMsgType.Audio) {
			return;
		}

		final IMAudio audio = IMAudiosMgr.getInstance().getAudio(message.getFileID());

		if(audio == null || !audio.isLocalFileExist() || audio.mBuffer == null || audio.mBuffer.length <= 0) {
			switch (message.mStatus) {
				case IMChatMessage.SENDING_OR_RECVING:
					holder.iv.setImageDrawable(null);
					downloadFile(message.mUserMsgType, message.getFileID(), new IMMyself.OnActionProgressListener() {
						@Override
						public void onSuccess() {
							message.mStatus = IMChatMessage.SUCCESS;
							message.saveFile();

							handleVoiceMessage(message, holder, position, convertView);
						}

						@Override
						public void onProgress(double progress) {

						}

						@Override
						public void onFailure(String error) {
							message.mStatus = IMChatMessage.FAILURE;
							message.saveFile();

							handleVoiceMessage(message, holder, position, convertView);
						}
					});
					break;
				default:
					holder.iv.setImageResource(CPResourceUtil.getDrawableId(mContext, "im_voice_node_err"));
					break;
			}
		} else {
			if (message.mIsRecv) {
				holder.iv.setImageResource(CPResourceUtil.getAnimId(mContext, "im_chatting_left_voice_play"));

				switch (message.mStatus) {
					case IMMessage.SUCCESS:
						holder.pb.setVisibility(View.GONE);
						break;
					case IMMessage.FAILURE:
						holder.pb.setVisibility(View.GONE);
						break;
					case IMMessage.SENDING_OR_RECVING:
						holder.pb.setVisibility(View.VISIBLE);
						break;
				}
			} else {
				holder.iv.setImageResource(CPResourceUtil.getAnimId(mContext, "im_chatting_right_voice_play"));

				switch (message.mStatus) {
					case IMMessage.SUCCESS:
						holder.pb.setVisibility(View.GONE);
						holder.staus_iv.setVisibility(View.GONE);
						break;
					case IMMessage.FAILURE:
						holder.pb.setVisibility(View.GONE);
						holder.staus_iv.setVisibility(View.VISIBLE);
						break;
					case IMMessage.SENDING_OR_RECVING:
						holder.pb.setVisibility(View.VISIBLE);
						holder.staus_iv.setVisibility(View.GONE);
						break;
				}
			}
		}

		final AnimationDrawable animationDrawable;
		if(holder.iv.getDrawable() instanceof AnimationDrawable) {
			animationDrawable = (AnimationDrawable) holder.iv.getDrawable();
		} else {
			animationDrawable = null;
		}

		if (-1 != mAudioPlayingPosition && position == mAudioPlayingPosition) {
			if(animationDrawable != null) {
				animationDrawable.start();
			}
		} else {
			if (animationDrawable != null && animationDrawable.isRunning()) {
				animationDrawable.stop();
				animationDrawable.selectDrawable(0);
			}
		}

		OnClickListener audioClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (position == mAudioPlayingPosition) {
					IMAudioPlayer.getInstance().stop();

					if (animationDrawable != null && animationDrawable.isRunning()) {
						animationDrawable.stop();
						animationDrawable.selectDrawable(0);
						mLastAnimationDrawable = null;
					}

					mAudioPlayingPosition = -1;
				} else {
					if(message.mIsRecv &&  message.mStatus == IMChatMessage.SENDING_OR_RECVING) {
						Toast.makeText(mContext, "正在获取语音，请稍后", Toast.LENGTH_SHORT).show();
						return;
					}

					final IMAudio audio = IMAudiosMgr.getInstance().getAudio(
							message.getFileID());

					if(audio == null || !audio.isLocalFileExist() || audio.mBuffer == null || audio.mBuffer.length <= 0) {
						Toast.makeText(mContext, "语音有误，重新下载...", Toast.LENGTH_SHORT).show();

						message.mStatus = IMChatMessage.SENDING_OR_RECVING;
						message.saveFile();

						downloadFile(message.mUserMsgType, message.getFileID(), new IMMyself.OnActionProgressListener() {
							@Override
							public void onSuccess() {
								message.mStatus = IMChatMessage.SUCCESS;
								message.saveFile();

								handleVoiceMessage(message, holder, position, convertView);
							}

							@Override
							public void onProgress(double progress) {
//								updatePhotoUI(message.mStatus, (int) progress, holder);
							}

							@Override
							public void onFailure(String error) {
								message.mStatus = IMChatMessage.FAILURE;
								message.saveFile();

								handleVoiceMessage(message, holder, position, convertView);
							}
						});
						return;
					}

					mAudioPlayingPosition = position;
					animationDrawable.start();

					if (mLastAnimationDrawable != null
							&& mLastAnimationDrawable.isRunning()) {
						mLastAnimationDrawable.stop();
						mLastAnimationDrawable.selectDrawable(0);
					}

					mLastAnimationDrawable = animationDrawable;

					IMAudioPlayer.getInstance().play(message,
							new IMMyself.OnAudioPlayListener() {
								@Override
								public void onError() {
									Toast.makeText(mContext, "语音有误，无法播放", Toast.LENGTH_SHORT).show();
								}

								@Override
								public void onCompletion(MediaPlayer mp) {
									animationDrawable.stop();
									animationDrawable.selectDrawable(0);
									mLastAnimationDrawable = null;
									mAudioPlayingPosition = -1;
								}
							});

					if(message.mIsRecv && !message.isListened()) {
						message.setVoiceListen();
						message.saveFile();

						holder.iv_read_status.setVisibility(View.INVISIBLE);
					}

				}
			}
		};

		//根据录音长短显示聊天气泡长度
		float  dpValue = message.getAudioDuration() * 2 + 95;
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(dip2px(mContext, dpValue), RelativeLayout.LayoutParams.WRAP_CONTENT);

		if (message.mIsRecv) {
			params.addRule(RelativeLayout.RIGHT_OF, CPResourceUtil.getId(
					mActivityReference.get(), "iv_userhead"));
			params.addRule(RelativeLayout.BELOW,CPResourceUtil.getId(
					mActivityReference.get(), "tv_userid"));

			if(message.isListened()) {
				holder.iv_read_status.setVisibility(View.INVISIBLE);
			} else {
				holder.iv_read_status.setVisibility(View.VISIBLE);
			}
		} else {
			params.addRule(RelativeLayout.LEFT_OF, CPResourceUtil.getId(
					mActivityReference.get(), "iv_userhead"));
		}

		holder.lb_content.setLayoutParams(params);
//		holder.iv.setLayoutParams(params);
		holder.tv.setText(message.getAudioDuration() + "\"");
		holder.lb_content.setTag(position);
		holder.lb_content.setOnClickListener(audioClickListener);
		holder.lb_content.setOnLongClickListener(mContentLongClickListener);

		// until here, deal with send voice msg
		if(!message.mIsRecv) {
			switch (message.mStatus) {
				case IMMessage.SUCCESS: // 发送成功
					holder.pb.setVisibility(View.GONE);
					holder.staus_iv.setVisibility(View.GONE);
					break;
				case IMMessage.FAILURE: // 发送失败
					holder.pb.setVisibility(View.GONE);
					holder.staus_iv.setVisibility(View.VISIBLE);
					break;
				case IMMessage.SENDING_OR_RECVING: // 发送中
					holder.pb.setVisibility(View.VISIBLE);
					holder.staus_iv.setVisibility(View.GONE);
					break;
			}

			holder.staus_iv.setTag(message);
			holder.staus_iv.setOnClickListener(mResendClickListener);
		} else {
			switch (message.mStatus) {
				case IMMessage.SUCCESS: // 接收成功
					holder.pb.setVisibility(View.GONE);
					break;
				case IMMessage.FAILURE: // 接收失败
					holder.pb.setVisibility(View.GONE);
					break;
				case IMMessage.SENDING_OR_RECVING: // 接收中
					holder.pb.setVisibility(View.VISIBLE);
					break;
			}
		}
	}

	private void handleOrderMessage(final IMUserMsg message,
									final ViewHolder holder, final int position, View convertView) {
		if(message.mUserMsgType != UserMsgType.Order) {
			return;
		}

		try {
			JSONObject content = new JSONObject(message.mContent);

			int subType = content.getInt("subType");

			if (subType == 0) {
				holder.order_view_1.setVisibility(View.VISIBLE);
				holder.order_view_2.setVisibility(View.GONE);
				holder.order_view_3.setVisibility(View.GONE);

				Button btn1 = (Button) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "order_btn_1"));
				btn1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
//						JSONObject content = new JSONObject();
//						try {
//							content.put("subType", 1);
//						} catch (JSONException e) {
//							e.printStackTrace();
//						}
//						message.mContent = content.toString();
//						message.saveFile();
//
//						holder.order_view_1.setVisibility(View.GONE);
//						holder.order_view_2.setVisibility(View.VISIBLE);
//						holder.order_view_3.setVisibility(View.GONE);

						if(mOrderBtn1ClickRunnable != null) {
							v.setTag(position);
							mOrderBtn1ClickRunnable.onClick(v);
						}
					}
				});

				Button btn2 = (Button) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "order_btn_2"));
				btn2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
//						JSONObject content = new JSONObject();
//						try {
//							content.put("subType", 2);
//						} catch (JSONException e) {
//							e.printStackTrace();
//						}
//						message.mContent = content.toString();
//						message.saveFile();
//
//						holder.order_view_1.setVisibility(View.GONE);
//						holder.order_view_2.setVisibility(View.GONE);
//						holder.order_view_3.setVisibility(View.VISIBLE);

						if(mOrderBtn2ClickRunnable != null) {
							v.setTag(position);
							mOrderBtn2ClickRunnable.onClick(v);
						}
					}
				});
			} else if (subType == 1) {
				holder.order_view_1.setVisibility(View.GONE);
				holder.order_view_2.setVisibility(View.VISIBLE);
				holder.order_view_3.setVisibility(View.GONE);
			} else if (subType == 2) {
				holder.order_view_1.setVisibility(View.GONE);
				holder.order_view_2.setVisibility(View.GONE);
				holder.order_view_3.setVisibility(View.VISIBLE);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void handleNoticeMessage(IMUserMsg message, ViewHolder holder,
									 int position, View convertView) {
		if(message.mUserMsgType != UserMsgType.IMSDKNotice) {
			return;
		}

		if(holder.tv_chatnotice != null) {
			holder.tv_chatnotice.setText(message.mContent);
		}
	}

	public void dealOrder1(int position) {
		IMUserMsg userMsg = getItem(position);

		JSONObject content = new JSONObject();
		try {
			content.put("subType", 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		userMsg.mContent = content.toString();
		userMsg.saveFile();

		this.notifyDataSetChanged();
	}

	public void dealOrder2(int position) {
		IMUserMsg userMsg = getItem(position);

		JSONObject content = new JSONObject();
		try {
			content.put("subType", 2);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		userMsg.mContent = content.toString();
		userMsg.saveFile();

		this.notifyDataSetChanged();
	}

	/**
	 * 头像点击事件
	 */
	private void dealHeadPhotoClick(RoundedImageView roundedImageView,
									final IMUserMsg userMsg) {

		roundedImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(mHeadPhotoClickListener != null){
					mHeadPhotoClickListener.onClick(v,userMsg.mFromCustomUserID);
				}
			}
		});

	}

	private void updatePhotoUI(int msgStatus, int progress, ViewHolder holder) {
		switch (msgStatus) {
			case IMMessage.SUCCESS:
				holder.pb.setVisibility(View.GONE);
				holder.tv.setVisibility(View.GONE);
				holder.staus_iv.setVisibility(View.GONE);
				break;
			case IMMessage.FAILURE:
				holder.pb.setVisibility(View.GONE);
				holder.tv.setVisibility(View.GONE);
				holder.staus_iv.setVisibility(View.VISIBLE);
				break;
			case IMMessage.SENDING_OR_RECVING:
				holder.staus_iv.setVisibility(View.GONE);
				holder.pb.setVisibility(View.VISIBLE);
				holder.tv.setVisibility(View.VISIBLE);

				holder.tv.setText(progress + "%");
				break;
		}
	}

	private void updateTextUI(int msgStatus, ViewHolder holder) {
		switch (msgStatus) {
			case IMMessage.SUCCESS: // 发送成功
				holder.pb.setVisibility(View.GONE);
				holder.staus_iv.setVisibility(View.GONE);
				break;
			case IMMessage.FAILURE: // 发送失败
				holder.pb.setVisibility(View.GONE);
				holder.staus_iv.setVisibility(View.VISIBLE);
				break;
			case IMMessage.SENDING_OR_RECVING: // 发送中
				holder.pb.setVisibility(View.VISIBLE);
				holder.staus_iv.setVisibility(View.GONE);
				break;
		}
	}

	private OnClickListener mResendClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(v.getTag() instanceof IMUserMsg) {
				resend((IMUserMsg) v.getTag());
			}
		}
	};

	@Override
	public void resend(int position) {
		resend(getItem(position));
	}

	public void resend(final IMUserMsg userMsg) {
		if(userMsg == null) return;

		mUserChatMsgHistory.removeUnsentUserMsgWithClientSendTime(userMsg.mClientSendTime);
		mUserChatMsgHistory.saveFile();

		userMsg.removeFile();

		final long actionTime = System.currentTimeMillis();

		IMChatViewAdapter.this.notifyDataSetChanged();

		// 创建IMUserMsg
		final IMUserMsg photoMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				userMsg.mToCustomUserID, actionTime);

		if (photoMsg == null) {
			DTLog.logError();
			return;
		}

		if (userMsg.mUserMsgType == UserMsgType.Normal) {
			IMMyself.sendText(userMsg.mContent, userMsg.mToCustomUserID, userMsg.mTags,
					10, new OnActionListener() {
						@Override
						public void onSuccess() {
						}

						@Override
						public void onFailure(String error) {
						}
					});

			return;
		} else if (userMsg.mUserMsgType == UserMsgType.Photo) {
			photoMsg.mUserMsgType = UserMsgType.Photo;
		} else if (userMsg.mUserMsgType == UserMsgType.Audio) {
			photoMsg.mUserMsgType = UserMsgType.Audio;
		} else {
			return;
		}

		photoMsg.mToCustomUserID = userMsg.mToCustomUserID;
		photoMsg.mContent = userMsg.mContent;
		photoMsg.mTags = userMsg.mTags;
		photoMsg.saveFile();

		// 维护聊天记录
		mUserChatMsgHistory.insertUnsentUserMsg(photoMsg.mClientSendTime);
		mUserChatMsgHistory.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				mUserChatMsgHistory.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(
				photoMsg.mToCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance()
				.postNotification(
						IMPrivateRecentContacts.getInstance()
								.notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				mUserChatMsgHistory.replaceUnsentUserMsgToSent(photoMsg);
				mUserChatMsgHistory.saveFile();
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
			}
		};

		action.mUserMsg = photoMsg;

		action.mBeginTime = actionTime;
		action.begin();
	}

	@Override
	public void revoke(int position) {
		final IMUserMsg userMsg = getItem(position);

		if((System.currentTimeMillis() - userMsg.mClientSendTime) >= 2 * 60 * 1000) {
			final AlertDialog alertDialog1 = new AlertDialog(mActivityReference.get());
			alertDialog1.show();
			alertDialog1.setOneBtnListener("确定", new OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog1.cancel();
				}
			});
			alertDialog1.setDialogMsg("发送时间超过2分钟的消息，不能被撤回");
			return;
		}

		final WaitingDialog waitingDialog = new WaitingDialog(mActivityReference.get());
		waitingDialog.show();
		waitingDialog.setDialogMsg("正在撤回消息...");

		IMMyself2.sendRevoke(userMsg.mMsgID, userMsg.mToCustomUserID, userMsg.mTags, 10, new OnActionListener() {
			@Override
			public void onSuccess() {
				waitingDialog.cancel();

				userMsg.mContent = "你撤回了一条消息";
				userMsg.mUserMsgType = UserMsgType.IMSDKNotice;
				userMsg.saveFile();

				IMChatViewAdapter.this.notifyDataSetChanged();
			}

			@Override
			public void onFailure(String error) {
				waitingDialog.cancel();

				final AlertDialog alertDialog2 = new AlertDialog(mActivityReference.get());
				alertDialog2.show();
				alertDialog2.setOneBtnListener("确定", new OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.cancel();
					}
				});
				alertDialog2.setDialogMsg("撤回失败，请重新尝试");
			}
		});
	}

	private int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

	public void stopAudio() {
		IMAudioPlayer.getInstance().stop();
	}

	public void destroy() {
		if (mKeyList != null && mKeyList.size() > 0) {
			Iterator<String> iterator = mKeyList.iterator();

			while (iterator.hasNext()) {
				String key = iterator.next();

				DTNotificationCenter.getInstance().removeObservers(key);
			}
		}
	}

	public static class ViewHolder {
		ImageView iv_sendPicture_2;
		ImageView iv_sendPicture_3;
		View lb_content;
		ImageView iv;
		TextView tv;
		ProgressBar pb;
		ImageView staus_iv;
		ImageView head_iv;
		TextView tv_userId;
		ImageView playBtn;
		TextView timeLength;
		TextView size;
		LinearLayout container_status_btn;
		LinearLayout ll_container;
		ImageView iv_read_status;
		// 显示已读回执状态
		TextView tv_ack;
		// 显示送达回执状态
		TextView tv_delivered;

		TextView tv_file_name;
		TextView tv_file_size;
		TextView tv_file_download_state;

		//消息内容
		RelativeLayout rl_chat_content;

		ImageView iv_location;

		RelativeLayout order_view_1;
		RelativeLayout order_view_2;
		RelativeLayout order_view_3;
		View order_clickview_2;
		View order_clickview_3;

		TextView tv_chatnotice;
	}
}
