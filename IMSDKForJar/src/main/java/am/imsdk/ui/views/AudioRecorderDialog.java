package am.imsdk.ui.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.demo.util.CPResourceUtil;

public class AudioRecorderDialog extends Dialog {
	private static final int MSG_RELOAD_DB = 0x01;
	private static final int MSG_START = 0x02;
	private static final int MSG_STOP = 0x03;
	private static final int MSG_DISMISS = 0x04;
	private static final int MSG_COUNT_DOWN = 0x05;

	private final WeakReference<Activity> mActivityReferences;
	private OnErrorListener mErrorListener;

	private ImageView mVoiceLevelImageView;
	private View mRecordingAnimRootView;
	private View mCancelRecordRootView;

	private View mRecordingRootView;
	private View mLoadingRootView;
	private TextView mCountDownText;

	private View mTooShortRootView;
	private long mStartTime;
	private long mEndTime;
	private int[] mIDs;

	private AudioTimer mCountDownTimer = new AudioTimer(this);
	private TimerTask mTask;

	public interface OnTimeLimitListener {
		void onTimeLimit();
	}

	private OnTimeLimitListener mOnTimeLimitListener;

	private static class AudioTimer extends Timer {
		private final WeakReference<AudioRecorderDialog> mDialogReference;

		public AudioTimer(AudioRecorderDialog dialog) {
			mDialogReference = new WeakReference<AudioRecorderDialog>(dialog);
		}
	}

	private static class AudioRecorderDialogHandler extends Handler {
		private final WeakReference<AudioRecorderDialog> mDialogReference;

		public AudioRecorderDialogHandler(AudioRecorderDialog dialog) {
			mDialogReference = new WeakReference<AudioRecorderDialog>(dialog);
		}

		@Override
		public void handleMessage(Message msg) {
			if (mDialogReference.get() == null) {
				return;
			}

			if (mDialogReference.get().mActivityReferences.get() == null) {
				return;
			}

			switch (msg.what) {
			case MSG_RELOAD_DB:
				int level = mDialogReference.get().getLevel(
						mDialogReference.get().volume);
				int resourceID = mDialogReference.get().mIDs[level - 1];

//				Log.i("am.test", "level : " + level + " id: " + resourceID);

				if (resourceID > 0) {
					mDialogReference.get().mVoiceLevelImageView
							.setBackgroundDrawable(mDialogReference.get().mActivityReferences.get()
									.getResources().getDrawable(resourceID));
				}

				mDialogReference.get().mHandler.sendEmptyMessageDelayed(
						MSG_RELOAD_DB, 150L);
				break;
			case MSG_START:
				try {
					mDialogReference.get().showRecordingView();
					mDialogReference.get().mHandler.sendEmptyMessage(MSG_RELOAD_DB);
				} catch (final Exception e) {
					if (mDialogReference.get().mErrorListener != null) {
						DTAppEnv.postOnUIThread(new Runnable() {
							@Override
							public void run() {
								mDialogReference.get().mErrorListener.onError(e.getMessage());
							}
						});

					}
				}
				break;
			case MSG_STOP:
				mDialogReference.get().mHandler.removeMessages(MSG_RELOAD_DB);
				break;
			case MSG_DISMISS:
				mDialogReference.get().dismiss();
				break;
			case MSG_COUNT_DOWN:
				//还可以说10秒
				long number = 60 - (System.currentTimeMillis() - mDialogReference.get().mStartTime) / 1000;
				mDialogReference.get().mCountDownText.setText("还可以说" + number + "秒");
				break;
			default:
				break;
			}
		}
	}

	private Handler mHandler = new AudioRecorderDialogHandler(this);

	public AudioRecorderDialog(Activity context, int theme) {
		super(context, theme);

		mActivityReferences = new WeakReference<Activity>(context);
//		mContext = context;
		mIDs = new int[7];

		for (int i = 0; i < mIDs.length; i++) {
			mIDs[i] = CPResourceUtil.getDrawableId(context, "im_amp" + (i + 1));
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(CPResourceUtil.getLayoutId(getContext(), "im_voice_rcd_dialog"));

		mVoiceLevelImageView = (ImageView) findViewById(CPResourceUtil.getId(getContext(),
				"voice_rcd_hint_anim"));
		mRecordingAnimRootView = findViewById(CPResourceUtil.getId(getContext(),
				"voice_rcd_hint_anim_area"));
		mCancelRecordRootView = findViewById(CPResourceUtil.getId(getContext(),
				"voice_rcd_hint_cancel_area"));

		mRecordingRootView = findViewById(CPResourceUtil.getId(getContext(),
				"voice_rcd_hint_rcding"));
		mLoadingRootView = findViewById(CPResourceUtil.getId(getContext(),
				"voice_rcd_hint_loading"));
		mTooShortRootView = findViewById(CPResourceUtil.getId(getContext(),
				"voice_rcd_hint_tooshort"));

		mCountDownText = (TextView) findViewById(CPResourceUtil.getId(getContext(), "voice_rcd_hint_count_down_text"));
	}

	private int volume = 10;

	private Observer observer = new Observer() {

		@Override
		public void update(Object data) {
			// TODO Auto-generated method stub
			if (!(data instanceof Integer)) {
				DTLog.logError();
				return;
			}
			volume = (Integer) data;
		}
	};

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mCancelRecordRootView.setVisibility(View.GONE);
		mRecordingAnimRootView.setVisibility(View.VISIBLE);
		mRecordingRootView.setVisibility(View.GONE);
		mTooShortRootView.setVisibility(View.GONE);
		mLoadingRootView.setVisibility(View.VISIBLE);

		mStartTime = System.currentTimeMillis();
		mHandler.sendEmptyMessage(MSG_START);
		mHandler.removeMessages(MSG_RELOAD_DB);

		DTNotificationCenter.getInstance().addObserver("volumeUpdated", observer);
		mCountDownText.setText("手指上滑，取消传送");

		mTask = new TimerTask() {
			@Override
			public void run() {
				long limitTime = System.currentTimeMillis() - mStartTime;
				if(limitTime >= 50 * 1000) {
					mHandler.sendEmptyMessage(MSG_COUNT_DOWN);

					if(limitTime >= 60 * 1000) {
						if(mOnTimeLimitListener != null) {
							DTAppEnv.postOnUIThread(new Runnable() {
								@Override
								public void run() {
									mOnTimeLimitListener.onTimeLimit();
								}
							});

						}

						mTask.cancel();
						mTask = null;
					}
				}
				System.out.println("--------------limitTime:" + limitTime);
			}
		};
		mCountDownTimer.schedule(mTask, 1 * 1000, 1000);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mHandler.sendEmptyMessage(MSG_STOP);
		mHandler.removeMessages(MSG_RELOAD_DB);
		DTNotificationCenter.getInstance().removeObserver("volumeUpdated", observer);

		if(mTask != null) {
			mTask.cancel();
			mTask = null;
		}
	}

//	@Override
//	public boolean onTouchEvent(MotionEvent event) {
//		mHandler.sendEmptyMessage(MSG_STOP);
//		mHandler.removeMessages(MSG_RELOAD_DB);
//		DTNotificationCenter.getInstance().removeObserver("volumeUpdated", observer);
//		if(mErrorListener != null) {
//			DTAppEnv.postOnUIThread(new Runnable() {
//				@Override
//				public void run() {
//					mErrorListener.onError("");
//				}
//			});
//		}
//
//		return super.onTouchEvent(event);
//	}

	public void setOnErrorListener(OnErrorListener listener) {
		mErrorListener = listener;
	}

	public void setOnTimeLimitListener(OnTimeLimitListener l) {
		this.mOnTimeLimitListener = l;
	}

	public void requestDismiss() {
		mEndTime = System.currentTimeMillis();
		if (mEndTime - mStartTime < 1000) {
			mRecordingRootView.setVisibility(View.GONE);
			mLoadingRootView.setVisibility(View.GONE);
			mTooShortRootView.setVisibility(View.VISIBLE);
			mHandler.sendEmptyMessageDelayed(MSG_DISMISS, 200);
		} else {
			dismiss();
		}
	}

	public boolean isTooShort() {
		return mEndTime - mStartTime < 1000;
	}

	public void showRecordingView() {
		mTooShortRootView.setVisibility(View.GONE);
		mLoadingRootView.setVisibility(View.GONE);
		mRecordingRootView.setVisibility(View.VISIBLE);

		mCancelRecordRootView.setVisibility(View.GONE);
		mRecordingAnimRootView.setVisibility(View.VISIBLE);
	}

	public void showCancelRecordView() {
		mTooShortRootView.setVisibility(View.GONE);
		mLoadingRootView.setVisibility(View.GONE);
		mRecordingRootView.setVisibility(View.VISIBLE);

		mRecordingAnimRootView.setVisibility(View.GONE);
		mCancelRecordRootView.setVisibility(View.VISIBLE);
	}

	public int getLevel(int splValue) {
		if (splValue >= 50.0 && splValue < 60.0) {
			return 2;
		} else if (splValue >= 60.0 && splValue < 70) {
			return 3;
		} else if (splValue >= 70.0 && splValue < 80) {
			return 4;
		} else if (splValue >= 80.0 && splValue < 85) {
			return 5;
		} else if (splValue >= 85.0 && splValue < 95) {
			return 6;
		} else if (splValue >= 95.0) {
			return 7;
		} else {
			return 1;
		}
	}

	public interface OnErrorListener {
		void onError(String msg);
	}
}
