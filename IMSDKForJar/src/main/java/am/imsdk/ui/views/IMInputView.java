package am.imsdk.ui.views;

import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.image.IMImagePhoto;
import android.content.Context;
import android.view.View;

public final class IMInputView extends View {
	public IMInputView(Context context) {
		super(context);
	}

	public interface OnInputDone {
		void onInputText(String text);

		void onInputAudio(IMAudio audio);

		void onInputImage(IMImagePhoto image);
	}
}
