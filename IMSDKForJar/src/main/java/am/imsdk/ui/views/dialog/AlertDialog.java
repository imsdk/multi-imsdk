package am.imsdk.ui.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import am.imsdk.demo.R;
import am.imsdk.demo.util.CPResourceUtil;

/**
 * Created by fzj on 2016/1/18.
 * 提示对话框
 */
public class AlertDialog extends Dialog {

    private DialogType mDialogType = DialogType.OneBtnDialog;

    private View mOneBtnLayout;
    private View mTwoBtnLayout;
    private Button mOneBtn;
    private Button mTwoBtn1;
    private Button mTwoBtn2;
    private TextView mTvMsg;

    public enum DialogType {
        OneBtnDialog, // 一个按钮的对话框
        TwoBtnDialog  // 两个按钮的对话框
    }

    public AlertDialog(Context context) {
        this(context, CPResourceUtil.getStyleId(context, "myDialogTheme"), DialogType.OneBtnDialog);
    }

    public AlertDialog(Context context, int styleId, DialogType type) {
        super(context, CPResourceUtil.getStyleId(context, "myDialogTheme"));

        mDialogType = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(CPResourceUtil.getLayoutId(getContext(), "im_alert_dialog"));

        mOneBtnLayout = findViewById(CPResourceUtil.getId(getContext(), "one_btn_layout"));
        mTwoBtnLayout = findViewById(CPResourceUtil.getId(getContext(), "two_btn_layout"));
        mOneBtn = (Button) findViewById(CPResourceUtil.getId(getContext(), "one_btn"));
        mTwoBtn1 = (Button) findViewById(CPResourceUtil.getId(getContext(), "two_btn_1"));
        mTwoBtn2 = (Button) findViewById(CPResourceUtil.getId(getContext(), "two_btn_2"));
        mTvMsg = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_msg"));

        if (mDialogType == DialogType.OneBtnDialog) {
            mOneBtnLayout.setVisibility(View.VISIBLE);
            mTwoBtnLayout.setVisibility(View.GONE);
        } else if (mDialogType == DialogType.TwoBtnDialog) {
            mOneBtnLayout.setVisibility(View.GONE);
            mTwoBtnLayout.setVisibility(View.VISIBLE);
        }

        this.setCancelable(false);
    }

    public void setDialogMsg(CharSequence text) {
        if(mTvMsg != null) {
            mTvMsg.setText(text);
        }
    }

    public void setOneBtnListener(CharSequence text, View.OnClickListener l) {
        if(mOneBtn != null) {
            mOneBtn.setText(text);
            mOneBtn.setOnClickListener(l);
        }
    }

    public void setTwoBtn1Listener(CharSequence text, View.OnClickListener l) {
        if(mTwoBtn1 != null) {
            mTwoBtn1.setText(text);
            mTwoBtn1.setOnClickListener(l);
        }
    }

    public void setTwoBtn2Listener(CharSequence text, View.OnClickListener l) {
        if(mTwoBtn2 != null) {
            mTwoBtn2.setText(text);
            mTwoBtn2.setOnClickListener(l);
        }
    }

}
