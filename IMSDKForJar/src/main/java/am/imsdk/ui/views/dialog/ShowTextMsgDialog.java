package am.imsdk.ui.views.dialog;

import am.imsdk.demo.util.CPResourceUtil;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * 文本消息框
 */
public class ShowTextMsgDialog extends Dialog {
	private boolean isPhoto;
	
	private TextView tvNickName;
	private TextView tvCopy;
	private TextView tvForward;
	private TextView tvDelete;
	private TextView tvAtHe;
	private TextView tvResend;
	private TextView tvRevoke;
	
	public ShowTextMsgDialog(Context context, boolean isPhoto) {
		super(context, CPResourceUtil.getStyleId(context, "myDialogTheme"));
		this.isPhoto = isPhoto;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(CPResourceUtil.getLayoutId(getContext(), "im_text_msg_dialog"));

		tvNickName = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_nickname"));
		tvCopy = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_copy"));
		tvForward = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_forward"));
		tvDelete = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_delete"));
		tvAtHe = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_at_he"));
		tvResend = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_resend"));
		tvRevoke = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_revoke"));
		
		if (isPhoto) {
			tvCopy.setText("保存到手机");
		} else {
			
		}
	}
	
	public void setCopyClickListener(View.OnClickListener l) {
		if(tvCopy != null) {
			tvCopy.setOnClickListener(l);
		}
	}
	
	public void setForwardClickListener(View.OnClickListener l) {
		if(tvForward != null) {
			tvForward.setOnClickListener(l);
		}
	}
	
	public void setDeleteClickListener(View.OnClickListener l) {
		if(tvDelete != null) {
			tvDelete.setOnClickListener(l);
		}
	}
	
	public void setAtHeClickListener(View.OnClickListener l) {
		if(tvAtHe != null) {
			tvAtHe.setOnClickListener(l);
		}
	}

	public void setResendClickListener(View.OnClickListener l) {
		if(tvResend != null) {
			tvResend.setVisibility(View.VISIBLE);
			tvResend.setOnClickListener(l);

			tvRevoke.setVisibility(View.GONE);
		}
	}

	public void setRevokeClickListener(View.OnClickListener l) {
		if(tvRevoke != null) {
			tvRevoke.setOnClickListener(l);
		}
	}

	public void setRevokeVisibility(int visibility) {
		if(tvRevoke != null) {
			tvRevoke.setVisibility(visibility);
		}
	}
}
