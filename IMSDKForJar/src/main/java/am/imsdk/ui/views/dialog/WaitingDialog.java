package am.imsdk.ui.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import am.imsdk.demo.R;
import am.imsdk.demo.util.CPResourceUtil;

/**
 * Created by fzj on 2016/1/18.
 * 等待界面
 */
public class WaitingDialog extends Dialog {

    private ImageView loadImageView;
    private TextView tipTextView;

    public WaitingDialog(Context context) {
        super(context, CPResourceUtil.getStyleId(context, "myDialogTheme"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(CPResourceUtil.getLayoutId(getContext(), "im_waiting_dialog"));

        // main.xml中的ImageView  
        tipTextView = (TextView) this.findViewById(CPResourceUtil.getId(getContext(), "tipTextView"));// 提示文字
        loadImageView = (ImageView) this.findViewById(CPResourceUtil.getId(getContext(), "loadingImageView"));

        this.setCancelable(false);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        final Animation anim = AnimationUtils.loadAnimation(getContext(), CPResourceUtil.getAnimId(getContext(), "im_progress_round"));
        LinearInterpolator lir = new LinearInterpolator();
        anim.setInterpolator(lir);

        loadImageView.startAnimation(anim);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        loadImageView.clearAnimation();
    }

    public void setDialogMsg(CharSequence text) {
        if(TextUtils.isEmpty(text)) {
            tipTextView.setVisibility(View.GONE);
        } else {
            tipTextView.setVisibility(View.VISIBLE);
            tipTextView.setText(text);// 设置加载信息
        }
    }
}
