package am.imsdk.ui.views.dialog;

import am.imsdk.demo.util.CPResourceUtil;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class OperationChatDialog extends Dialog {
	private TextView tv_delete;
	private TextView tv_playing_mode;
	private TextView tvResend;
	private TextView tvRevoke;
	
	public OperationChatDialog(Context context) {
		super(context, CPResourceUtil.getStyleId(context, "myDialogTheme"));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(CPResourceUtil.getLayoutId(getContext(), "im_chat_operation_dialog"));
		
		tv_playing_mode = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_playing_mode"));
		tv_delete = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_delete"));
		tvResend = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_resend"));
		tvRevoke = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_revoke"));
	}
	
	public void setDeleteClickListener(View.OnClickListener l) {
		if(tv_delete != null) {
			tv_delete.setOnClickListener(l);
		}
	}
	
	public void setPlayModeClickListener(View.OnClickListener l) {
		if(tv_playing_mode != null) {
			tv_playing_mode.setOnClickListener(l);
		}
	}

	public void setResendClickListener(View.OnClickListener l) {
		if(tvResend != null) {
			tvResend.setVisibility(View.VISIBLE);
			tvResend.setOnClickListener(l);

			tvRevoke.setVisibility(View.GONE);
		}
	}

	public void setRevokeClickListener(View.OnClickListener l) {
		if(tvRevoke != null) {
			tvRevoke.setOnClickListener(l);
		}
	}

	public void setRevokeVisibility(int visibility) {
		if(tvRevoke != null) {
			tvRevoke.setVisibility(visibility);
		}
	}
}
