package remote.service;

import android.os.RemoteException;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.socket.DTSocket;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.imsdk.action.IMActionRemoteLogin;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMChatSetting;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.teaminfo.IMPrivateTeamListMgr;
import am.imsdk.model.userslist.IMPrivateBlacklistMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;
import imsdk.data.IMMyself.LoginStatus;
import remote.service.aidl.IMainToSocketService;
import remote.service.aidl.OnLoginRecvListener;
import remote.service.aidl.OnRemoteAddressChangedListener;
import remote.service.aidl.OnRemoteCmdRecvListener;
import remote.service.aidl.OnRemoteLoginStatusChangedListener;
import remote.service.aidl.OnRemoteSocketStatusChangedListener;
import remote.service.data.IMRemoteMyself;

public class MainToSocketProcessStub extends IMainToSocketService.Stub {
	OnRemoteCmdRecvListener mOnCmdRecvListener;
	OnRemoteSocketStatusChangedListener mOnSocketStatusChangedListener;
	OnRemoteLoginStatusChangedListener mOnLoginStatusChangedListener;
	OnLoginRecvListener mOnLoginRecvListener;
	OnRemoteAddressChangedListener mOnRemoteAddressChangedListener;

	/**
	 * 
	 * @Description 清除监听
	 * @throws
	 * @author 方子君
	 */
	public void onDestroy() {
		mOnCmdRecvListener = null;
		mOnSocketStatusChangedListener = null;
		mOnLoginStatusChangedListener = null;
		mOnLoginRecvListener = null;
		mOnRemoteAddressChangedListener = null;

		//client下线，service重新获取缓存数据
		IMChatSetting.newInstance();
		IMUserMsgHistoriesMgr.newInstance();
		IMPrivateRecentContacts.newInstance();
		IMChatSetting.newInstance();
		IMPrivateFriendsMgr.newInstance();
		IMPrivateBlacklistMgr.newInstance();
		IMPrivateTeamListMgr.newInstance();
	}
	
	public OnRemoteLoginStatusChangedListener getLoginStatusChangedListener() {
		return mOnLoginStatusChangedListener;
	}

	public OnRemoteCmdRecvListener getCmdRecvListener() {
		return mOnCmdRecvListener;
	}

	public OnRemoteSocketStatusChangedListener getSocketStatusChangedListener() {
		return mOnSocketStatusChangedListener;
	}
	
	public OnLoginRecvListener getLoginRecvListener() {
		return mOnLoginRecvListener;
	}

	public OnRemoteAddressChangedListener getAddressChangedListener() {
		return mOnRemoteAddressChangedListener;
	}
	
	public void setLoginRecvListener() {
		mOnLoginRecvListener = null;
	}

	@Override
	public void setOnRemoteSocketStatusChangedListener(
			OnRemoteSocketStatusChangedListener listener) throws RemoteException {
		mOnSocketStatusChangedListener = listener;

		if (mOnSocketStatusChangedListener != null) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						mOnSocketStatusChangedListener.onSocketStatusChanged(DTSocket.getInstance().getStatus()
								.getValue());
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}

	@Override
	public void setOnRemoteCmdRecvListener(OnRemoteCmdRecvListener listener)
			throws RemoteException {
		mOnCmdRecvListener = listener;
	}

	@Override
	public void setOnRemoteLoginStatusChangedListener(
			OnRemoteLoginStatusChangedListener listener) throws RemoteException {
		mOnLoginStatusChangedListener = listener;

		if (mOnLoginStatusChangedListener != null) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						mOnLoginStatusChangedListener.onLoginStatusChanged(IMRemoteMyself.getInstance()
								.getRemoteLoginStatus().getValue());
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}

	@Override
	public void setOnRemoteAddressChangedListener(
			OnRemoteAddressChangedListener listener) throws RemoteException {
		mOnRemoteAddressChangedListener = listener;
	}

//	@Override
//	public int getLoginState() throws RemoteException {
//		return 0;
//	}
	
	@Override
	public void loginOrRegister(final int loginType, final String customUserID, final String pwd, OnLoginRecvListener l)
			throws RemoteException {
		mOnLoginRecvListener = l;
		
		DTAppEnv.postOnUIThread(new Runnable() {

			@Override
			public void run() {
				String tempCustomUID = "";
				String tempPWD = "";
				String tempToken = "";

				boolean checkParam = true;

				if (loginType == IMActionRemoteLogin.IMActionLoginType.AutoLogin.getValue()) {
					tempCustomUID = IMAppSettings.getInstance().mLastLoginUID + "";
					tempToken = IMAppSettings.getInstance().mLastLoginToken;

					if(TextUtils.isEmpty(tempCustomUID) || tempCustomUID.equals("0")) {
						DTLog.logError();
						checkParam = false;
					}

					if(TextUtils.isEmpty(tempToken)) {
						DTLog.logError();
						checkParam = false;
					}

					if(!tempCustomUID.equals(customUserID)) {
						DTLog.logError();
						checkParam = false;
					}
				} else if (loginType == IMActionRemoteLogin.IMActionLoginType.OneKeyLogin.getValue() ||
						loginType == IMActionRemoteLogin.IMActionLoginType.OneKeyRegister.getValue()) {
					tempCustomUID = customUserID;
					tempPWD = pwd;

					if(TextUtils.isEmpty(tempCustomUID) || tempCustomUID.equals("0")) {
						DTLog.logError();
						checkParam = false;
					}

					if(TextUtils.isEmpty(tempPWD)) {
						DTLog.logError();
						checkParam = false;
					}
				}

				if(!checkParam) {
					DTAppEnv.postDelayOnUIThread(1, new Runnable() {
						@Override
						public void run() {
							try {
								mOnLoginRecvListener.onRecvFailure("");
							} catch (RemoteException e) {
								e.printStackTrace();
							}
						}
					});
					return;
				}

				IMActionRemoteLogin.newInstance();
				IMActionRemoteLogin.getInstance().setType(loginType);
				IMActionRemoteLogin.getInstance().mTimeoutInterval = 5;
				IMActionRemoteLogin.getInstance().mCustomUserID = tempCustomUID;
				IMActionRemoteLogin.getInstance().setPassword(tempPWD);
				IMActionRemoteLogin.getInstance().mLoginToken = tempToken;
				IMActionRemoteLogin.getInstance().mAppKey = "";
				IMActionRemoteLogin.getInstance().begin();
			}
		});
	}
	
	
	
	@Override
	public void reconnect() throws RemoteException {
		DTAppEnv.postOnUIThread(new Runnable() {
			@Override
			public void run() {
				DTSocket.getInstance().reconnect();
			}
		});
	}
	
//	@Override
//	public int getSocketState() throws RemoteException {
//		return DTSocket.getInstance().getStatus().getValue();
//	}

	@Override
	public boolean sendCmd(final DTCmd cmd) throws RemoteException {
		if (DTSocket.getInstance().getStatus() == DTSocketStatus.Connected) {
			DTAppEnv.postOnUIThread(new Runnable() {
				@Override
				public void run() {
					DTSocket.getInstance().sendCmd(cmd);
				}
			});

			return true;
		} else {
			return false;
		}
	}

	@Override
	public void setOffline() throws RemoteException {
		DTAppEnv.postOnUIThread(new Runnable() {

			@Override
			public void run() {
				DTSocket.getInstance().checkCloseSocket();
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
			}
		});
	}
	
	@Override
	public String getLoginInfo() throws RemoteException {
		if(IMRemoteMyself.getInstance().getRemoteLoginStatus() == LoginStatus.Logined) {
			if(IMRemoteMyself.getInstance().getUID() <= 0) {
				return "";
			}

			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("uid", IMRemoteMyself.getInstance().getUID());
				return jsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return "";
	}
	
	@Override
	public void logout() throws RemoteException {
		DTAppEnv.postOnUIThread(new Runnable() {

			@Override
			public void run() {
//				IMAppSettings.getInstance().mLastLoginPassword = "";
				IMAppSettings.getInstance().mLastLoginUID = 0;
				IMAppSettings.getInstance().mLastLoginToken = "";
				IMAppSettings.getInstance().saveFile();

				DTSocket.getInstance().checkCloseSocket();
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
			}
		});
	}

	private MainToSocketProcessStub() {}

	public static MainToSocketProcessStub instance;

	public static synchronized MainToSocketProcessStub getInstance() {
		if (instance == null) {
			instance = new MainToSocketProcessStub();
		}

		return instance;
	}

}
