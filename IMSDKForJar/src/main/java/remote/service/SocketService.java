package remote.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.ProcessTool;
import am.dtlib.model.d.DTDevice;
import am.imsdk.aacmd.IMSocket;
import am.imsdk.action.IMActionRemoteLogin;
import am.imsdk.action.IMActionRemoteLogin.IMActionLoginType;
import am.imsdk.action.fileserver.IMActionFile;
import am.imsdk.model.IMAppSettings;
import imsdk.data.IMMyself;
import remote.service.data.IMRemoteHeartBeat;
import remote.service.data.IMRemoteMyself;

public class SocketService extends Service {
	public static final String ACTION = "imsdk.service.remote.SocketService";

	@Override
	public IBinder onBind(Intent arg0) {
		return MainToSocketProcessStub.getInstance();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}

	public void onCreate() {
		super.onCreate();

		// 环境配置
		DTAppEnv.setContext(getApplicationContext());
		DTAppEnv.setRootDirectory("IMSDK.im");
		DTLog.checkCreateDirectory(DTAppEnv.getIMSDKDirectoryFullPath());
		
		NetConnectedObserver.getInstance().run();

		IMRemoteHeartBeat.getInstance().heartbeat();

//		String address = DTFileTool.getFromAssets(getApplicationContext(), "IMPath.txt");
//		if(TextUtils.isEmpty(address)) {
//			address = "112.74.66.141";
//		}
		String address1 = DTAppEnv.isDebuggable ? "192.168.20.51" : "120.25.86.163";
		String address2 = DTAppEnv.isDebuggable ? "192.168.20.51" : "112.74.66.141";

		DTDevice.getInstance();
		DTDevice.getInstance().mLastIMAddress = address1 + ":9100";
		IMSocket.getInstance().setDefaultIMAddress(address2 + ":9100");

		Log.e("Debug", "---------IMPath:" + address1);

		DTAppEnv.sIMSDKIpAddress = address1;
		IMActionFile.IM_FILE_URL_DOMAIN_NAME = address1;
		IMActionFile.IM_FILE_URL_REAL_IP = address1;

//		DTDevice.getInstance().mLastSISAddress = address2 + ":9100";
//		IMSocket.getInstance().setSISDefaultAddress(address1 + ":9100");

		IMRemoteMyself.getInstance();
		
		IMAppSettings.newInstance();
		IMAppSettings.getInstance().readFromFile();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		DTAppEnv.cancelPreviousPerformRequest(mCheckRunnable);
		DTAppEnv.postDelayOnUIThread(10, mCheckRunnable);
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	private Runnable mCheckRunnable = new Runnable() {
		
		@Override
		public void run() {
			checkAutoConnect();
		}
	};

	private void checkAutoConnect() {
		DTLog.signGrade(4);
		DTAppEnv.cancelPreviousPerformRequest(mCheckRunnable);
		
		if (!ProcessTool.isClientProcessExist(getApplicationContext()) && IMRemoteMyself.getInstance().getRemoteLoginStatus() == IMMyself.LoginStatus.None) {
			String token = IMAppSettings.getInstance().mLastLoginToken;
			String customUserID = IMAppSettings.getInstance().mLastLoginUID + "";

			if(TextUtils.isEmpty(token)) {
				DTLog.logError();
				return;
			}

			if(TextUtils.isEmpty(customUserID) || customUserID.equals("0")) {
				DTLog.logError();
				return;
			}

			IMActionRemoteLogin.newInstance();
			IMActionRemoteLogin.getInstance().setType(IMActionLoginType.AutoLogin);
			IMActionRemoteLogin.getInstance().mTimeoutInterval = 5;
			IMActionRemoteLogin.getInstance().mCustomUserID = customUserID;
			IMActionRemoteLogin.getInstance().mLoginToken = token;
			IMActionRemoteLogin.getInstance().mAppKey = "";
			IMActionRemoteLogin.getInstance().begin();
		}
	}
}
