package remote.service.data;

import android.os.RemoteException;
import android.text.TextUtils;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTSocket;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.NotifyUtils;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.aacmd.IMSocket;
import am.imsdk.action.IMActionRemoteLogin;
import am.imsdk.action.IMActionRemoteLogin.IMActionLoginType;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.im.IMPrivateSystemMsg;
import imsdk.data.IMMyself.LoginStatus;
import remote.service.MainToSocketProcessStub;

public class IMRemoteMyself {
	private final Object mSyncStatus = new Object();
	private LoginStatus mRemoteLoginStatus = LoginStatus.None;
	
//	private long mSessionID;
	private long mUID;
//	private boolean mRelativeInfoInited;
//	private boolean mGroupInfoInited;
//
//	public boolean isGroupInfoInited() {
//		return mGroupInfoInited;
//	}
//
//	public boolean isRelativeInfoInited() {
//		return mRelativeInfoInited;
//	}
//
//	public void setRelativeInfoInited(boolean mRelativeInfoInited) {
//		this.mRelativeInfoInited = mRelativeInfoInited;
//	}
//
//	public void setGroupInfoInited(boolean mGroupInfoInited) {
//		this.mGroupInfoInited = mGroupInfoInited;
//	}

	//	public long getSessionID() {
//		return mSessionID;
//	}

//	public void setSessionID(long mSession) {
//		this.mSessionID = mSession;
//	}
	
	public long getUID() {
		return mUID;
	}

	public void setUID(long mUID) {
		this.mUID = mUID;
	}

	public LoginStatus getRemoteLoginStatus() {
		synchronized (mSyncStatus) {
			return mRemoteLoginStatus;
		}
	}
	
	public void setRemoteLoginStatus(LoginStatus newStatus) {
		synchronized (mSyncStatus) {
			mRemoteLoginStatus = newStatus;

			if(MainToSocketProcessStub.getInstance().getLoginStatusChangedListener() != null) {
				try {
					MainToSocketProcessStub.getInstance().getLoginStatusChangedListener().onLoginStatusChanged(mRemoteLoginStatus.getValue());
				} catch (RemoteException e) {
					e.printStackTrace();
					MainToSocketProcessStub.getInstance().onDestroy();
				}
			}
		}
	}
	
	private static void tryAutoLogin() {
		String token = IMAppSettings.getInstance().mLastLoginToken;
		String customUID = IMAppSettings.getInstance().mLastLoginUID + "";

		if(TextUtils.isEmpty(token)) {
			DTLog.logError();
			return;
		}

		if(TextUtils.isEmpty(customUID) || customUID.equals("0")) {
			DTLog.logError();
			return;
		}
		
		IMActionRemoteLogin.newInstance();
		IMActionRemoteLogin.getInstance().setType(IMActionLoginType.AutoLogin);
		IMActionRemoteLogin.getInstance().mTimeoutInterval = 5;
		IMActionRemoteLogin.getInstance().mCustomUserID = customUID;
		IMActionRemoteLogin.getInstance().mLoginToken = token;
		IMActionRemoteLogin.getInstance().mAppKey = "";
		IMActionRemoteLogin.getInstance().begin();
	}

	private static void tryReconnect() {
		DTLog.sign("tryReconnect");
		
		if (IMRemoteMyself.getInstance().getRemoteLoginStatus() == LoginStatus.Logined) {
			if (IMSocket.getInstance().getStatus() == DTSocketStatus.Connected) {
				DTLog.logError();
				return;
			}

			IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.Reconnecting);
		}
		
		if (IMRemoteMyself.getInstance().getRemoteLoginStatus() != LoginStatus.Reconnecting) {
			DTLog.logError();
			return;
		}

		String token = IMAppSettings.getInstance().mLastLoginToken;
		String customUID = IMAppSettings.getInstance().mLastLoginUID + "";

		if(TextUtils.isEmpty(token)) {
			DTLog.logError();
			return;
		}

		if(TextUtils.isEmpty(customUID) || customUID.equals("0")) {
			DTLog.logError();
			return;
		}
		
		IMActionRemoteLogin.newInstance();
		IMActionRemoteLogin.getInstance().setType(IMActionLoginType.Reconnect);
		IMActionRemoteLogin.getInstance().mTimeoutInterval = 5;
		IMActionRemoteLogin.getInstance().mCustomUserID = customUID;
		IMActionRemoteLogin.getInstance().mLoginToken = token;
		IMActionRemoteLogin.getInstance().mAppKey = "";
		IMActionRemoteLogin.getInstance().begin();
	}

	private static Observer sSocketUpdatedObserver = new Observer() {
		@Override
		public void update(Object data) {
			if(data == null || !(data instanceof DTSocketStatus)) {
				DTLog.logError();
				return;
			}

			DTSocketStatus status = (DTSocketStatus) data;
			
			DTLog.e("Debug", "IMRemoteMyself---sSocketUpdatedObserver");
			DTLog.sign("IMRemoteMyself.getInstance().getRemoteLoginStatus():"
					+ IMRemoteMyself.getInstance().getRemoteLoginStatus());

			if (IMActionRemoteLogin.getInstance() != null) {
				DTLog.sign("IMActionRemoteLogin.getInstance().getType():"
						+ IMActionRemoteLogin.getInstance().getType());
			}

			DTLog.sign("IMSocket.getInstance().getStatusWithoutSync():"
					+ status);

			switch (IMRemoteMyself.getInstance().getRemoteLoginStatus()) {
			case None:
				break;
			case Logining: {
				if (IMActionRemoteLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.OneKeyLogin
						&& IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.OneKeyRegister) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
			}
				break;
			case Reconnecting:
				if (IMActionRemoteLogin.getInstance() == null) {
					tryReconnect();
				}

				if (IMActionRemoteLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.Reconnect) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
				break;
			case AutoLogining: {
				if (IMActionRemoteLogin.getInstance() != null) {
					DTLog.sign("LoginType:" + IMActionRemoteLogin.getInstance().getType());
				}

				// reinit的时候被清理掉
				if (IMActionRemoteLogin.getInstance() == null
						|| IMActionRemoteLogin.getInstance().getType() == IMActionLoginType.None) {
					if (status != DTSocketStatus.Connected) {
						tryAutoLogin();
						return;
					}

					tryAutoLogin();
				}

				if (IMActionRemoteLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.AutoLogin) {
					DTLog.logError();
					DTLog.log(IMActionRemoteLogin.getInstance().getType().toString());
					return;
				}

				if (IMActionRemoteLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
			}
				break;
			case Logined:
				switch (status) {
				case None:
				case Connecting: {
					// 断线重连
					if (IMActionRemoteLogin.getInstance() != null) {
//						DTLog.logError();
//						return;
						
						IMActionRemoteLogin.newInstance();
					}
					
					//网络不通，不用重试
					if(!DTAppEnv.isNetworkConnected()) {
						IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
						return;
					}

					tryReconnect();
				}
					break;
				case Connected: {
					DTLog.logError();
				}
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}

	};

	private static NotifyUtils.notifyDelayRunnable sUserNotifyRunnable;
	private static NotifyUtils.notifyDelayRunnable sSystemNotifyRunnable;

	static {
		DTNotificationCenter.getInstance().addObserver("SOCKET_UPDATED",
				sSocketUpdatedObserver);
		
		DTNotificationCenter.getInstance().addObserver("DTNetConnected", new Observer() {
			
			@Override
			public void update(Object data) {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.Reconnecting);

				if (IMActionRemoteLogin.getInstance() == null) {
					tryReconnect();
				} else {
					DTLog.logError(IMActionRemoteLogin.getInstance().getType().toString());
					return;
				}

				if (IMActionRemoteLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.Reconnect) {
					DTLog.logError(IMActionRemoteLogin.getInstance().getType().toString());
					return;
				}

				if (IMActionRemoteLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveText", new Observer() {
			@Override
			public void update(Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Normal) {
					DTLog.logError();
					return;
				}

				if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMRemoteMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}
				
				if (userMsg.getToUID() != IMRemoteMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMRemoteMyself.getInstance().getUID():"
							+ IMRemoteMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				if(sUserNotifyRunnable != null) {
					DTAppEnv.cancelPreviousPerformRequest(sUserNotifyRunnable);
				}

				sUserNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.UserMsg.getValue(), userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mContent, userMsg.mContent, userMsg.getExtraData2().toString(), userMsg.mServerSendTime);
				DTAppEnv.postDelayOnUIThread(0.2, sUserNotifyRunnable);
			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveOrder", new Observer() {
			@Override
			public void update(Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Order) {
					DTLog.logError();
					return;
				}

				if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMRemoteMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMRemoteMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMRemoteMyself.getInstance().getUID():"
							+ IMRemoteMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				if(sUserNotifyRunnable != null) {
					DTAppEnv.cancelPreviousPerformRequest(sUserNotifyRunnable);
				}

				sUserNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.UserMsg.getValue(), userMsg.mFromCustomUserID, userMsg.getNickName(),  "[订单消息]", "[订单消息]", userMsg.getExtraData2().toString(), userMsg.mServerSendTime);
				DTAppEnv.postDelayOnUIThread(0.2, sUserNotifyRunnable);

			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveAudio", new Observer() {
			@Override
			public void update(Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Audio) {
					DTLog.logError();
					return;
				}

				if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMRemoteMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMRemoteMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMRemoteMyself.getInstance().getUID():"
							+ IMRemoteMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				if(sUserNotifyRunnable != null) {
					DTAppEnv.cancelPreviousPerformRequest(sUserNotifyRunnable);
				}

				sUserNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.UserMsg.getValue(), userMsg.mFromCustomUserID, userMsg.getNickName(),  "[语音]", "[语音]", userMsg.getExtraData2().toString(), userMsg.mServerSendTime);
				DTAppEnv.postDelayOnUIThread(0.2, sUserNotifyRunnable);
			}
		});

		DTNotificationCenter.getInstance().addObserver("IMReceiveSystemText",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMPrivateSystemMsg)) {
							DTLog.logError();
							return;
						}

						IMPrivateSystemMsg systemMessage = (IMPrivateSystemMsg) data;

						if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMRemoteMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if(sSystemNotifyRunnable != null) {
							DTAppEnv.cancelPreviousPerformRequest(sSystemNotifyRunnable);
						}

						sSystemNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.SystemMsg.getValue(), systemMessage.mSystemMessage.mCustomUserId, "通知", "[系统消息]", systemMessage.mSystemMessage.mContent,  "", systemMessage.mSystemMessage.mServerSendTime);
						DTAppEnv.postDelayOnUIThread(0.2, sSystemNotifyRunnable);
					}
				});

		DTNotificationCenter.getInstance().addObserver("LoginConflict", new Observer() {
			@Override
			public void update(Object data) {
				IMAppSettings.getInstance().mLastLoginToken = "";
				IMAppSettings.getInstance().saveFile();
				
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
				DTSocket.getInstance().checkCloseSocket();
			}
		});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapMessage",
				new Observer() {
					@Override
					public void update(Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMRemoteMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMRemoteMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						if(sUserNotifyRunnable != null) {
							DTAppEnv.cancelPreviousPerformRequest(sUserNotifyRunnable);
						}

						sUserNotifyRunnable = new NotifyUtils.notifyDelayRunnable(NotifyUtils.NotifyMsgType.UserMsg.getValue(), userMsg.mFromCustomUserID, userMsg.getNickName(), "[图片]", "[图片]", userMsg.getExtraData2().toString(), userMsg.mServerSendTime);
						DTAppEnv.postDelayOnUIThread(0.2, sUserNotifyRunnable);
					}
				});

	}
	
	private IMRemoteMyself() {}

	private volatile static IMRemoteMyself instance;

	public static synchronized IMRemoteMyself getInstance() {
		if (instance == null) {
			instance = new IMRemoteMyself();
		}

		return instance;
	}

}
