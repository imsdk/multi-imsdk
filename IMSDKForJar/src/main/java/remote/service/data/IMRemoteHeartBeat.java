package remote.service.data;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.Observer;
import am.imsdk.aacmd.user.IMCmdUserHeartbeat;
import imsdk.data.IMMyself.LoginStatus;

public class IMRemoteHeartBeat {
	public void heartbeat() {
		DTLog.sign("heartbeat:" + IMRemoteMyself.getInstance().getRemoteLoginStatus());

		if (IMRemoteMyself.getInstance().getRemoteLoginStatus() == LoginStatus.Logined) {
			IMCmdUserHeartbeat cmd = new IMCmdUserHeartbeat();

			cmd.send();
		}

		DTAppEnv.cancelPreviousPerformRequest(mHeartbeatRunnable);
		DTAppEnv.postDelayOnUIThread(80, mHeartbeatRunnable);
	}

	private Runnable mHeartbeatRunnable = new Runnable() {
		@Override
		public void run() {
			DTLog.e("Debug", "IMPrivateMyself Runnable:" + IMRemoteHeartBeat.this);
			heartbeat();
		}
	};

	private IMRemoteHeartBeat() {
		DTNotificationCenter.getInstance().addObserver("heartbeat", new Observer() {
			@Override
			public void update(Object data) {
				DTLog.e("Debug", "IMPrivateMyself Observer");
				heartbeat();
			}
		});
	}

	private volatile static IMRemoteHeartBeat instance;

	public static synchronized IMRemoteHeartBeat getInstance() {
		if (instance == null) {
			instance = new IMRemoteHeartBeat();
		}

		return instance;
	}
}
