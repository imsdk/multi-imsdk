#include <stdafx.h>

#ifndef   WIN32

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>	
#include <errno.h>
#include <unistd.h>
#define closesocket	close
#endif

#include "mytcp.h"
#include "Lock.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef BUILD_FROM_SOURCE
#include <utils/Log.h>
#else
#include <android/log.h>
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , LOG_TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO   , LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN   , LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , LOG_TAG, __VA_ARGS__)
#endif

#define JNI_DEBUG_JAVA_LOGCAT

void JNI_DEBUG_LOGCAT(const char* pMessage) {
#ifdef JNI_DEBUG_JAVA_LOGCAT
	LOGI("%s", pMessage);
#endif
}

enum {
	ERRCODE_OK = 0, ERRCODE_SENDBUFF = -1001, ERRCODE_CONN, // -1000
	ERRCODE_INPUT, // -999
	ERRCODE_SEND, // -998
	ERRCODE_RECV, // -997
	ERRCODE_CLOSE, // -996
	ERRCODE_RESPHTTPHEAD, // -995
	ERRCODE_TIMEOUT, // -994
	ERRCODE_INVALIDSOCK, // -993
	ERRCODE_INVALIDPKG, //-992
};

enum {
	KKCMD_GET_SESSION = 1,
	KKCMD_HARTBEAT,
	KKCMD_LOGIN,
	KKCMD_BIND,
	KKCMD_MSG_SVR_RECVED,
	KKCMD_PRESENCE,
	KKCMD_MESSAGE,
	KKCMD_VCARD_SET,
	KKCMD_CHATSTATUS,
	KKCMD_COMLOGIN = 26
};

enum {
	KKPUSHCMD_REG,
	KKPUSHCMD_LOGIN,
	KKPUSHCMD_HARTBEAT,
	KKPUSHCMD_MESSAGE,
	KKPUSHCMD_MESSAGEED,
	KKPUSHCMD_LOGOUT,
	KKPUSHCMD_GETCHANNELID,
	KKPUSHCMD_DECCHANNELID,
	KKPUSHCMD_MAX,
	KKPUSHCMD_PUSH_NEW,
	KKPUSHCMD_TAG_ALAIS,
	KKPUSHCMD_ENABLECHNNELID,
	KKPUSHCMD_PUSH_TIME,
	KKPUSH_DEVICETOKEN_REPORT,
	KKPUSHCMD_UNREGCHANNEID,
	KKPUSHCMD_CLIENTSENDMSG,
	KKPUSHCMD_GETCHANNELID2,
	KKPUSHCMD_DECCHANNELID2
};

//#include <linux/tcp.h>

CMutex mSocketLock;

CMyTcp::CMyTcp() {
	JNI_DEBUG_LOGCAT("socket construction!");
	m_nSocket = -1;
}

CMyTcp::~CMyTcp() {
	if (m_nSocket >= 0) {
		JNI_DEBUG_LOGCAT("close socket success!");
		sprintf(m_errMsg, "close socket success!");

		close(m_nSocket);
		m_nSocket = -1;
	} else {
		JNI_DEBUG_LOGCAT("nothing to close!");
		sprintf(m_errMsg, "nothing to close!");
	}
}

int CMyTcp::Init(char* szIP, int port) {
	if (NULL == szIP) {
		return -1;
	}

	if (strlen(szIP) == 0) {
		return -1;
	}

	mSocketLock.Lock();
	if (m_nSocket > 0) {
		Close();
	}

	JNI_DEBUG_LOGCAT("---------------11");

	m_nSocket = CreateClientTCPSocket(szIP, port);
	mSocketLock.Unlock();

	if (m_nSocket < 0) {
		sprintf(m_errMsg, "connect to server %s:%d fail", szIP, port);
		return ERRCODE_INVALIDSOCK;
	}

	JNI_DEBUG_LOGCAT("---------------55");

	return 0;
}

int CMyTcp::Send(char* pkg, int len) {
	if (m_nSocket < 0) {
		JNI_DEBUG_LOGCAT("please init first!");
		sprintf(m_errMsg, "please init first!");
		return ERRCODE_INVALIDSOCK;
	}

	int nSendLength = 0;
	int nSendResult = 0;

	while (nSendLength < len) {
		nSendResult = send(m_nSocket, pkg + nSendLength, len - nSendLength, 0);

		if (nSendResult > 0) {
			nSendLength += nSendResult;
		} else {
			return ERRCODE_SEND;
		}
	}

	return 0;
}

int CMyTcp::Recv(char* pkg, int len) {
	if (m_nSocket <= 0) {
//		LOGI("CMyTcp::Recv 1");
		return -1;
	}

//	LOGI("CMyTcp::Recv 2");
	return recv(m_nSocket, pkg, len, 0);
}

int CMyTcp::isConnected() {
	if(m_nSocket > 0) {
		return 1;
	}

	return 0;
}

void CMyTcp::Close() {
	if (m_nSocket >= 0) {
		JNI_DEBUG_LOGCAT("close socket success!");
		sprintf(m_errMsg, "close socket success!");

		close(m_nSocket);
		m_nSocket = -1;
	} else {
		JNI_DEBUG_LOGCAT("nothing to close!");
		sprintf(m_errMsg, "nothing to close!");
	}
}

int CMyTcp::CreateClientTCPSocket(char* szIP, unsigned short ushPort) {
	if (szIP == NULL) {
		return -1;
	}

	if (strlen(szIP) == 0) {
		return -1;
	}

	int nSocketID = 0;
	struct sockaddr_in server_addr;

	nSocketID = socket(AF_INET, SOCK_STREAM, 0);

	if (nSocketID == -1) {
		return -1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(szIP);
	server_addr.sin_port = htons(ushPort);
	bzero(&(server_addr.sin_zero), 8);

	int on = 1;

	setsockopt(nSocketID, SOL_SOCKET, 0x1022, (void *)&on, sizeof(on));

	int nConnectResult = connect(nSocketID, (struct sockaddr *)&server_addr, sizeof(server_addr));

	JNI_DEBUG_LOGCAT("---------------33");


	if (nConnectResult < 0) {
		close(nSocketID);
		return -1;
	}

	JNI_DEBUG_LOGCAT("---------------44");


	return nSocketID;
}
