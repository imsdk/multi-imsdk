package remote.service.aidl;

// 通知前端连接地址已经改变
interface OnRemoteAddressChangedListener {
    //type 扩展出不同类型的地址
    void addressChanged(int type, String address);
}
